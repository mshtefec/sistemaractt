// Get the ul that holds the collection of escuelas
var collectionFormula = jQuery('.formulas');
var index;

jQuery(document).ready(function () {

    collectionFormula.data('index', collectionFormula.find(':input').length);

    jQuery('.formulas').delegate('.btnRemoveformula', 'click', function (e) {
        // prevent the link from creating a "#" on  the URL
        e.preventDefault();
        $contador--;
        // remove the li for the tag form
        var li = jQuery(this).closest('.rowremove').attr("li");
        jQuery('#' + li).remove();
        removeForm(jQuery(this).closest('.rowremove'));
    });

    jQuery('.ribon_dom').delegate('.add_formula_link', 'click', function (e) {
        // prevent the link from creating a "#" on the URL                
        e.preventDefault();
        // remove the li for the tag form
        index = addForm(collectionFormula, jQuery('.formulas'));
        $contador++;

        addUl(index, 'Crear Formula ' + $contador);
        $("#tecspro_modulobundle_grupo_formulas_" + index + "_orden").val($contador);
        
        


    });

});
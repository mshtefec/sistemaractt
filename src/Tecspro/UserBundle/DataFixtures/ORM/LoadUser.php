<?php

namespace Tecspro\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Tecspro\UserBundle\Entity\User;

class LoadUser extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        //Creando SuperAdmin !!
        $user = new User();
        $user->setUsername('superadmin');
        $user->setPassword('superadmin');
        $user->setEmail('superadmin@tecspro.com.ar');
        $user->addRole($this->getReference('ROLE_SUPERADMIN'));
        $this->createPasswordAndFlush($user, $manager);
        //Creando Admin !!
        $user = new User();
        $user->setUsername('admin');
        $user->setPassword('admin');
        $user->setEmail('admin@tecspro.com.ar');
        $user->addRole($this->getReference('ROLE_ADMIN'));
        $this->createPasswordAndFlush($user, $manager);
        //Creando Validacion !!
        $user = new User();
        $user->setUsername('cliente');
        $user->setPassword('cliente');
        $user->setEmail('cliente@tecspro.com.ar');
        $user->addRole($this->getReference('ROLE_CLIENTE'));
        $this->createPasswordAndFlush($user, $manager);
    }

    private function createPasswordAndFlush($user, ObjectManager $manager)
    {
        // Completar las propiedades que el usuario no rellena en el formulario
        $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
        $passwordCodificado = $encoder->encodePassword(
            $user->getPassword(),
            $user->getSalt()
        );
        $user->setPassword($passwordCodificado);
        $user->setEnabled(true);
        // Guardar el nuevo usuario en la base de datos
        $manager->persist($user);
        $manager->flush();
        $this->addReference($user->getUsername(), $user);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 2; // the order in which fixtures will be loaded
    }
}
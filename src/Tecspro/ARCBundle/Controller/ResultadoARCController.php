<?php

namespace Tecspro\ARCBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Tecspro\ARCBundle\Entity\ResultadoARC;
use Tecspro\ARCBundle\Form\ResultadoARCType;
use Tecspro\ARCBundle\Form\ResultadoARCFilterType;

/**
 * ResultadoARC controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/resultadoarc")
 */
class ResultadoARCController extends Controller
{
    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Tecspro/ARCBundle/Resources/config/ResultadoARC.yml',
    );

    /**
     * Lists all ResultadoARC entities.
     *
     * @Route("/", name="resultadoarc")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->config['filterType'] = new ResultadoARCFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new ResultadoARC entity.
     *
     * @Route("/", name="resultadoarc_create")
     * @Method("POST")
     * @Template("TecsproARCBundle:ResultadoARC:new.html.twig")
     */
    public function createAction()
    {
        $this->config['newType'] = new ResultadoARCType();
        $response = parent::createAction();

        return $response;
    }

    /**
     * Displays a form to create a new ResultadoARC entity.
     *
     * @Route("/new", name="resultadoarc_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->config['newType'] = new ResultadoARCType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a ResultadoARC entity.
     *
     * @Route("/{id}", name="resultadoarc_show", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing ResultadoARC entity.
     *
     * @Route("/{id}/edit", name="resultadoarc_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->config['editType'] = new ResultadoARCType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing ResultadoARC entity.
     *
     * @Route("/{id}", name="resultadoarc_update")
     * @Method("PUT")
     * @Template("TecsproARCBundle:ResultadoARC:edit.html.twig")
     */
    public function updateAction($id)
    {
        $this->config['editType'] = new ResultadoARCType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a ResultadoARC entity.
     *
     * @Route("/{id}", name="resultadoarc_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter ResultadoARC.
     *
     * @Route("/exporter/{format}", name="resultadoarc_export")
     */
    public function getExporter($format)
    {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Autocomplete a ResultadoARC entity.
     *
     * @Route("/autocomplete-forms/get-arcCampos", name="ResultadoARC_autocomplete_arcCampos")
     */
    public function getAutocompleteARCCampos()
    {
        $options = array(
            'repository' => "TecsproARCBundle:ARCCampos",
            'field'      => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Datatable ResultadoARC.
     *
     * @Route("/get-table/", name="resultadoarc_table")
     */
    public function getDatatable()
    {
        $response = parent::getTable();

        return $response;
    }
 
}
<?php

namespace Tecspro\TablaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;

/**
 * CentroGravedadFilterType filtro.
 * @author Nombre Apellido <name@gmail.com>
 */
class CentroGravedadFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('vehiculo', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('indice', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('batallaCategoria1', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('batallaCategoria2', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('batallaCategoria3', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('batallaCategoria4', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('batallaCateogira5', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('categoria', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('altura', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('desviacionEstandar', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('fuente', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
        ;

        $listener = function(FormEvent $event)
        {
            // Is data empty?
            foreach ((array)$event->getForm()->getData() as $data) {
                if ( is_array($data)) {
                    foreach ($data as $subData) {
                        if (!empty($subData)) {
                            return;
                        }
                    }
                } else {
                    if (!empty($data)) {
                        return;
                    }    
                }
            }
            $event->getForm()->addError(new FormError('Filter empty'));
        };
        $builder->addEventListener(FormEvents::POST_SUBMIT, $listener);
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Tecspro\TablaBundle\Entity\CentroGravedad'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'tecspro_tablabundle_centrogravedadfiltertype';
    }
}

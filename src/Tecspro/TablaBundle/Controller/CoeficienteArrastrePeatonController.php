<?php

namespace Tecspro\TablaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Tecspro\TablaBundle\Entity\CoeficienteArrastrePeaton;
use Tecspro\TablaBundle\Form\CoeficienteArrastrePeatonType;
use Tecspro\TablaBundle\Form\CoeficienteArrastrePeatonFilterType;

/**
 * CoeficienteArrastrePeaton controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/coeficientearrastrepeaton")
 */
class CoeficienteArrastrePeatonController extends Controller
{
    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Tecspro/TablaBundle/Resources/config/CoeficienteArrastrePeaton.yml',
    );

    /**
     * Lists all CoeficienteArrastrePeaton entities.
     *
     * @Route("/", name="admin_coeficientearrastrepeaton")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->config['filterType'] = new CoeficienteArrastrePeatonFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new CoeficienteArrastrePeaton entity.
     *
     * @Route("/", name="admin_coeficientearrastrepeaton_create")
     * @Method("POST")
     * @Template("TecsproTablaBundle:CoeficienteArrastrePeaton:new.html.twig")
     */
    public function createAction()
    {
        $this->config['newType'] = new CoeficienteArrastrePeatonType();
        $response = parent::createAction();

        return $response;
    }

    /**
     * Displays a form to create a new CoeficienteArrastrePeaton entity.
     *
     * @Route("/new", name="admin_coeficientearrastrepeaton_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->config['newType'] = new CoeficienteArrastrePeatonType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a CoeficienteArrastrePeaton entity.
     *
     * @Route("/{id}", name="admin_coeficientearrastrepeaton_show", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing CoeficienteArrastrePeaton entity.
     *
     * @Route("/{id}/edit", name="admin_coeficientearrastrepeaton_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->config['editType'] = new CoeficienteArrastrePeatonType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing CoeficienteArrastrePeaton entity.
     *
     * @Route("/{id}", name="admin_coeficientearrastrepeaton_update")
     * @Method("PUT")
     * @Template("TecsproTablaBundle:CoeficienteArrastrePeaton:edit.html.twig")
     */
    public function updateAction($id)
    {
        $this->config['editType'] = new CoeficienteArrastrePeatonType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a CoeficienteArrastrePeaton entity.
     *
     * @Route("/{id}", name="admin_coeficientearrastrepeaton_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter CoeficienteArrastrePeaton.
     *
     * @Route("/exporter/{format}", name="admin_coeficientearrastrepeaton_export")
     */
    public function getExporter($format)
    {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Datatable CoeficienteArrastrePeaton.
     *
     * @Route("/get-table/", name="admin_coeficientearrastrepeaton_table")
     */
    public function getDatatable()
    {
        $response = parent::getTable();

        return $response;
    }
 
}
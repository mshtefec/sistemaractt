<?php

namespace Tecspro\ModuloBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Exclude;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Categoria
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Tecspro\ModuloBundle\Entity\CategoriaRepository")
 */
class FormulaTranslation extends \MWSimple\Bundle\AdminCrudBundle\Entity\BaseFile implements \A2lix\I18nDoctrineBundle\Doctrine\Interfaces\OneLocaleInterface {

    use Translation;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     * @Type("string")
     */
    private $nombre;

    /**
     * @var string
     * @Type("string")
     */
    protected $uploadDir = "uploads/referencias";

    /**
     *
     * @ORM\Column(name="file_path", type="string", length=255, nullable=true)
     * @Type("string")
     */
    protected $filePath;

    /**
     * @Assert\File()
     * @Exclude
     */
    protected $file;

    /**
     * @var string
     * @Exclude
     */
    protected $temp;

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return FormulaTranslation
     */
    public function setNombre($nombre) {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre() {
        return $this->nombre;
    }

}

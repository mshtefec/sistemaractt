Doctos Consultora Symfony v2.7
================================

Bundles:

- UserBundle: Administración de Usuarios.
- ModuloBundle: Contiene Formulas, Unidades, Constantes y Variables.
- VehiculoBundle: Contiene una clase abstracta que es vehículo, donde auto y motocicleta extienden de él, estas son las entidades con las que trabaja módulos.
- ConfiguracionBundle: Datos preestablecidos o configuraciones guardadas, por ejemplo formulas con valores, resultados guardados.
- ExportBundle: Distintos métodos de exportación. Resultados, Formulas, información.
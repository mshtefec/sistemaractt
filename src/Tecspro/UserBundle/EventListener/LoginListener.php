<?php

namespace Tecspro\UserBundle\EventListener;

use Symfony\Component\Security\Core\Event\AuthenticationEvent;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Session\Session;

class LoginListener {

    private $ES = "es";
    private $PT = "pt";

    /**
     * @var string
     */
    protected $lenguaje;

    /**
     * @var string
     */
    protected $modulo;

    /**
     * @var string
     */
    protected $moduloNombreSeleccionado;

    /**
     * @var string
     */
    protected $user;

    /**
     * Router
     *
     * @var Router
     */
    protected $router;

    /**
     * EntityManager
     *
     * @var EntityManager
     */
    protected $em;

    /**
     * @var SecurityContext
     */
    protected $securityContext;

    /**
     * @var Session
     */
    protected $session;
    private $tokenFos;
    private $modulosSeleccionados;

    /**
     * @param userManager $UserManager
     * @param SecurityContext $securityContext
     * @param Router $router The router
     * @param EntityManager $em
     */
    public function __construct(SecurityContext $securityContext, Router $router, EntityManager $em, Session $session, $token) {
        $this->tokenFos = $token;
        $this->securityContext = $securityContext;
        $this->router = $router;
        $this->em = $em;
        $this->session = $session;
    }

    public function handle(AuthenticationEvent $event) {
        $token = $event->getAuthenticationToken();
        $this->user = $token->getUser();

        try {
            if ($this->user != "anon.") {

                /* $session = $this->em->getRepository('UserBundle:sessions')->findBySess_id($this->session->getId());

                  if (is_null($session) == false) {

                  //$session->setToken($tokenGenerado);
                  $this->user->setConfirmationTokenSession($session->getToken());
                  } */

                //ladybug_dump_die($this->session->getId());
                /* $session = $this->em->getRepository('UserBundle:sessions')->findBySess_id($this->session->getId());
                  if (is_null($session) == false) {
                  $this->user->addSessione($session);
                  /*$this->em->persist($this->user);
                  $this->em->flush();
                  } */
                // $this->session->clear();
                if (!is_null($this->user->getConfiguraciones())) {
                    //lenguaje
                    $this->lenguaje = "";
                    if (isset($this->user->getConfiguraciones()["lenguaje"])) {
                        $this->lenguaje = $this->user->getConfiguraciones()["lenguaje"];
                    } else {
                        $this->lenguaje = $this->ES;
                    }
                    //modulo
                    if (isset($this->user->getConfiguraciones()["ulitmoModulo"])) {
                        //ladybug_dump_die($this);
                        $this->modulo = $this->user->getConfiguraciones()["ulitmoModulo"];
                        $modulo = $this->em->getRepository("ModuloBundle:Modulo")->find($this->modulo);
                        if ($this->lenguaje == "" || $this->lenguaje == $this->ES) {
                            $this->em->getFilters()->enable('oneLocale')->setParameter('locale', $this->ES);
                        } else if ($this->lenguaje == $this->PT) {
                            $this->em->getFilters()->enable('oneLocale')->setParameter('locale', $this->PT);
                        }
                        $this->moduloNombreSeleccionado = $modulo->getNombre();
                    }
                }
            }
        } catch (Exception $e) {
            
        }

        /* $isString = is_string($token->getUser()->getRoles()[0]);

          if ($isString && $isString == 'ROLE_SUPER_ADMIN') {
          $this->idGimnasio = 0;
          $this->cuotas = 0;
          } else {$session->set('moduloNombreSeleccionado', $entity->__toString());
          if (!is_null($token->getUser()->getGimnasios())) {
          $this->negocio = $token->getUser()->getGimnasios()[0]->getId();
          }
          $this->cuotas = count($this->em->getRepository('SistemaGymBundle:Asistencia')->findAistenciaSinCuota($this->idGimnasio));
          } */
    }

    public function onKernelResponse(FilterResponseEvent $event) {
        // ladybug_dump_die(2);
        $request = $event->getRequest();
        // ladybug_dump_die($token->getUser()->getConfiguraciones());
        if (!is_null($this->lenguaje)) {

            $request->setLocale($this->lenguaje);
            $request->getSession()->set('_locale', $this->lenguaje);
        }

        if (!is_null($this->modulo)) {
            $request->getSession()->set('moduloSeleccionado', $this->modulo);
            $request->getSession()->set('moduloNombreSeleccionado', $this->moduloNombreSeleccionado);
        }
    }

    public function onKernelRequest(\Symfony\Component\HttpKernel\Event\GetResponseEvent $event) {
        /* if ($event->getRequestType() !== \Symfony\Component\HttpKernel\HttpKernelInterface::MASTER_REQUEST) {
          return;
          }

          $session = $event->getRequest()->getSession();
          $metadataBag = $session->getMetadataBag();

          $lastUsed = $metadataBag->getLastUsed();


          if ($lastUsed === null) {
          // the session was created just now
          return;
          } */
    }

}

<?php

namespace Tecspro\ModuloBundle\Service;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of VariablesSession
 *
 * @author rodrigo
 */
class VariablesSession {

    public function isModuloSeleccionado($request) {
        $session = $request->getSession();
        $modulo = $session->get('moduloSeleccionado', null);

        if (!$modulo) {
            //$session->getFlashBag()->add('danger', 'Debe seleccionar un modulo primero');
            $resultado = false;
        } else {
            $resultado = true;
        }
        $array['resultado'] = $resultado;
        $array['modulo'] = $modulo;
        return $array;
    }

}

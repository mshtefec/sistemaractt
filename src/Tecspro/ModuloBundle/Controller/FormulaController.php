<?php

namespace Tecspro\ModuloBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Tecspro\ModuloBundle\Entity\Formula;
use Tecspro\ModuloBundle\Form\FormulaType;
use Tecspro\ModuloBundle\Form\FormulaFilterType;
use A2lix\I18nDoctrineBundle\Annotation\I18nDoctrine;

/**
 * Formula controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/formula")
 */
class FormulaController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Tecspro/ModuloBundle/Resources/config/Formula.yml',
    );

    /**
     * Lists all Formula entities.
     *
     * @Route("/", name="admin_formula")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->config['filterType'] = new FormulaFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Formula entity.
     *
     * @Route("/", name="admin_formula_create")
     * @Method("POST")
     * @Template("ModuloBundle:Formula:new.html.twig")
     */
    public function createAction() {
        $this->config['newType'] = new FormulaType();
        $response = parent::createAction();

        return $response;
    }

    /**
     * Displays a form to create a new Formula entity.
     *
     * @Route("/new", name="admin_formula_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->config['newType'] = new FormulaType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Formula entity.
     *
     * @Route("/{id}", name="admin_formula_show", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Formula entity.
     *
     * @Route("/{id}/edit", name="admin_formula_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     * @I18nDoctrine
     */
    public function editAction($id) {
        $this->config['editType'] = new FormulaType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Formula entity.
     *
     * @Route("/{id}", name="admin_formula_update")
     * @Method("PUT")
     * @Template("ModuloBundle:Formula:edit.html.twig")
     * @I18nDoctrine
     */
    public function updateAction($id) {
        $this->config['editType'] = new FormulaType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a Formula entity.
     *
     * @Route("/{id}", name="admin_formula_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter Formula.
     *
     * @Route("/exporter/{format}", name="admin_formula_export")
     */
    public function getExporter($format) {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Autocomplete a Formula entity.
     *
     * @Route("/autocomplete-forms/get-grupo", name="Formula_autocomplete_grupo")
     */
    public function getAutocompleteGrupo() {
        $options = array(
            'repository' => "TecsproModuloBundle:Grupo",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Datatable Formula.
     *
     * @Route("/get-table/", name="admin_formula_table")
     */
    public function getDatatable() {
        $response = parent::getTable();

        return $response;
    }

}

<?php

namespace Tecspro\TablaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EficienciaProyeccion
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Tecspro\TablaBundle\Entity\EficienciaProyeccionRepository")
 */
class EficienciaProyeccion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="alturaCentroGravedadPeatonSobreCapoMetros", type="string", length=255)
     */
    private $alturaCentroGravedadPeatonSobreCapoMetros;

    /**
     * @var string
     *
     * @ORM\Column(name="minima", type="string", length=255)
     */
    private $minima;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set alturaCentroGravedadPeatonSobreCapoMetros
     *
     * @param string $alturaCentroGravedadPeatonSobreCapoMetros
     * @return EficienciaProyeccion
     */
    public function setAlturaCentroGravedadPeatonSobreCapoMetros($alturaCentroGravedadPeatonSobreCapoMetros)
    {
        $this->alturaCentroGravedadPeatonSobreCapoMetros = $alturaCentroGravedadPeatonSobreCapoMetros;

        return $this;
    }

    /**
     * Get alturaCentroGravedadPeatonSobreCapoMetros
     *
     * @return string 
     */
    public function getAlturaCentroGravedadPeatonSobreCapoMetros()
    {
        return $this->alturaCentroGravedadPeatonSobreCapoMetros;
    }

    /**
     * Set minima
     *
     * @param string $minima
     * @return EficienciaProyeccion
     */
    public function setMinima($minima)
    {
        $this->minima = $minima;

        return $this;
    }

    /**
     * Get minima
     *
     * @return string 
     */
    public function getMinima()
    {
        return $this->minima;
    }

  
}

<?php

namespace Tecspro\ARCBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Tecspro\ARCBundle\Entity\Vehiculo;
use Tecspro\ARCBundle\Form\VehiculoType;
use Tecspro\ARCBundle\Form\VehiculoFilterType;

/**
 * Vehiculo controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/vehiculo")
 */
class VehiculoController extends Controller
{
    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Tecspro/ARCBundle/Resources/config/Vehiculo.yml',
    );

    /**
     * Lists all Vehiculo entities.
     *
     * @Route("/", name="vehiculo")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->config['filterType'] = new VehiculoFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Vehiculo entity.
     *
     * @Route("/", name="vehiculo_create")
     * @Method("POST")
     * @Template("TecsproARCBundle:Vehiculo:new.html.twig")
     */
    public function createAction()
    {
        $this->config['newType'] = new VehiculoType();
        $response = parent::createAction();

        return $response;
    }

    /**
     * Displays a form to create a new Vehiculo entity.
     *
     * @Route("/new", name="vehiculo_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->config['newType'] = new VehiculoType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Vehiculo entity.
     *
     * @Route("/{id}", name="vehiculo_show", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Vehiculo entity.
     *
     * @Route("/{id}/edit", name="vehiculo_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->config['editType'] = new VehiculoType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Vehiculo entity.
     *
     * @Route("/{id}", name="vehiculo_update")
     * @Method("PUT")
     * @Template("TecsproARCBundle:Vehiculo:edit.html.twig")
     */
    public function updateAction($id)
    {
        $this->config['editType'] = new VehiculoType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a Vehiculo entity.
     *
     * @Route("/{id}", name="vehiculo_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter Vehiculo.
     *
     * @Route("/exporter/{format}", name="vehiculo_export")
     */
    public function getExporter($format)
    {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Autocomplete a Vehiculo entity.
     *
     * @Route("/autocomplete-forms/get-arcCampos", name="Vehiculo_autocomplete_arcCampos")
     */
    public function getAutocompleteARCCampos()
    {
        $options = array(
            'repository' => "TecsproARCBundle:ARCCampos",
            'field'      => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Datatable Vehiculo.
     *
     * @Route("/get-table/", name="vehiculo_table")
     */
    public function getDatatable()
    {
        $response = parent::getTable();

        return $response;
    }
 
}
<?php

namespace Tecspro\ModuloBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 * Categoria
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Tecspro\ModuloBundle\Entity\CategoriaRepository")
 */
class CategoriaTranslation implements \A2lix\I18nDoctrineBundle\Doctrine\Interfaces\OneLocaleInterface {

     use Translation;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     * @Type("string")
     */
    private $nombre;


    /**
     * Set nombre
     *
     * @param string $nombre
     * @return CategoriaTranslation
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }
}

<?php

namespace Tecspro\ARCBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * ARCCamposType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class ARCCamposType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('cantidadVehiculos', 'choice', array(
                    'label' => 'CamposARC.Cantidad Vehiculos',
                    'translation_domain' => 'TecsproARCBundle',
                    'choices' => array(
                        '1' => 1,
                        '2' => 2,
                    ),
                        )
                )
                ->add('fraccionDeTiempoAAnalizar', null, array(
                    'label' => 'CamposARC.Fraccion de tiempo a analizar',
                    'translation_domain' => 'TecsproARCBundle',
                        //'data' => 0.1  
                        )
                )
                ->add('imprimir', null, array(
                    'label' => 'CamposARC.Imprimir',
                    'translation_domain' => 'TecsproARCBundle',
                    'attr' => array(
                        'class' => "chArcImprimir"
                    )
                        )
                )
                ->add('vehiculos', 'collection', array(
                    'label' => false,
                    'translation_domain' => 'TecsproARCBundle',
                    'type' => new VehiculoType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'required' => true,
                    'by_reference' => false,
                    'prototype' => true,
                        )
                )
                ->add('formulasParameter', 'hidden', array(
                    'mapped' => false
                ))
                ->add('fechaProyecto', 'hidden', array(
                    'mapped' => false
                ))
                ->add('nombreProyecto', 'hidden', array(
                    'mapped' => false
                ))
                ->add('comentariosProyecto', 'hidden', array(
                    'mapped' => false
                ))
                ->add('partesInvolucradas', 'hidden', array(
                    'mapped' => false
                ))
                ->add('cerraProyecto', 'hidden', array(
                    'data' => false,
                    'mapped' => false
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Tecspro\ARCBundle\Entity\ARCCampos'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'tecspro_arcbundle_arccampos';
    }

}

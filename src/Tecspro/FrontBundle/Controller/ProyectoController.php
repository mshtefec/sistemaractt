<?php

namespace Tecspro\FrontBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Tecspro\FrontBundle\Entity\Proyecto;
use Tecspro\FrontBundle\Form\ProyectoType;
use Tecspro\FrontBundle\Form\ProyectoFilterType;
use Symfony\Component\HttpFoundation\JsonResponse;
use JMS\Serializer\SerializerBuilder;
use A2lix\I18nDoctrineBundle\Annotation\I18nDoctrine;

/**
 * Proyecto controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/proyecto")
 */
class ProyectoController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Tecspro/FrontBundle/Resources/config/Proyecto.yml',
    );

    /**
     * Lists all Proyecto entities.
     *
     * @Route("/", name="proyecto")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->config['filterType'] = new ProyectoFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Proyecto entity.
     *
     * @Route("/", name="proyecto_create")
     * @Method("POST")
     * @Template("TecsproFrontBundle:Proyecto:new.html.twig")
     */
    public function createAction() {
        $this->config['newType'] = new ProyectoType();
        $response = parent::createAction();

        return $response;
    }

    /**
     * Creates a new Proyecto entity.
     *
     * @Route("/create/by-ajax/", name="proyecto_create_ajax")
     * @Method("POST")
     */
    public function createAjaxAction() {

        $this->config['newType'] = new ProyectoType();
        $config = $this->getConfig();
        $request = $this->getRequest();

        $em = $this->getDoctrine()->getManager();

        $session = $request->getSession();

        $proyectoSession = $session->get('proyecto', null);

        if (is_null($proyectoSession)) {

            $entity = new $config['entity']();
            $entity->setUser($this->getUser());
        } else {

            $entity = $em->getRepository($config['repository'])->find($proyectoSession);
        }

        $form = $this->createCreateForm($config, $entity);
        $form->handleRequest($request);

        $resultado = false;
        if ($form->isValid()) {
            $formula = $request->request->all()["tecspro_frontbundle_proyecto"]["formulasParameter"];
            if ($formula !== "") {
                $arrayFormula = json_decode($formula, true);
                $this->get('formula')->guardarFormulasArrayEnProyecto($arrayFormula);
            }
            $session = $request->getSession();
            $moduloSession = $session->get('moduloSeleccionado', null);
            $moduloEntity = $em->getRepository('ModuloBundle:Modulo')->find($moduloSession);
            $entity->setModulo($moduloEntity);

            $em->persist($entity);
            $em->flush();

            $this->useACL($entity, 'create');
            //guardar en session
            $session->set('proyecto', $entity->getId());
            $session->set('proyectoNombre', $entity->getNombre());
            $this->get("formula")->guardarFormulasDeSessionEnProyecto();

            if (is_null($proyectoSession)) {

                $this->get('session')->getFlashBag()->add('success', 'flash.create.success');
            } else {

                $this->get('session')->getFlashBag()->add('success', 'flash.update.success');
            }
            $resultado = true;
        }


        // remove the form to return to the view
        unset($config['newType']);


        return $this->redirect($this->generateUrl('index'));
    }

    /**
     * Displays a form to create a new Proyecto entity.
     *
     * @Route("/new", name="proyecto_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->config['newType'] = new ProyectoType();
        $config = $this->getConfig();
        $request = $this->getRequest();
        $session = $request->getSession();
        $em = $this->getDoctrine()->getManager();
        $proyectoSession = $session->get('proyecto', null);
        if (is_null($proyectoSession)) {
            $entity = new $config['entity']();
        } else {

            $entity = $em->getRepository($config['repository'])->find($proyectoSession);
        }
        $form = $this->createCreateForm($config, $entity);

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a Proyecto entity.
     *
     * @Route("/{id}", name="proyecto_show", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Proyecto entity.
     *
     * @Route("/{id}/edit", name="proyecto_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->config['editType'] = new ProyectoType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Proyecto entity.
     *
     * @Route("/{id}", name="proyecto_update")
     * @Method("PUT")
     * @Template("TecsproFrontBundle:Proyecto:edit.html.twig")
     */
    public function updateAction($id) {
        $this->config['editType'] = new ProyectoType();
        $config = $this->getConfig();
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
        }
        $this->useACL($entity, 'update');
        $deleteForm = $this->createDeleteForm($config, $id);
        $editForm = $this->createEditForm($config, $entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');
            $session = $request->getSession();
            $session->set('proyectoNombre', $entity->getNombre());
            $nextAction = $editForm->get('saveAndAdd')->isClicked() ?
                    $this->generateUrl($config['new']) :
                    $this->generateUrl($config['show'], array('id' => $id))
            ;

            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        // remove the form to return to the view
        unset($config['editType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Proyecto entity.
     *
     * @Route("/{id}", name="proyecto_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {

        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Deletes a Proyecto entity.
     *
     * @Route("/delete/{id}", name="proyecto_delete_byajax", options={"expose"=true})
     */
    public function deleteByAjaxAction($id) {
        $config = $this->getConfig();

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository($config['repository'])->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
        }

        $em->remove($entity);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'flash.delete.success');
        $request = $this->getRequest();
        $session = $request->getSession();
        $proyectoSession = $session->get('proyecto', null);
        if ($proyectoSession == $id) {
            $session->remove('formulas');
            $session->remove('proyecto');
            $session->remove('proyectoNombre');
        }
        return $this->redirect($this->generateUrl('index'), 301);
    }

    /**
     * Exporter Proyecto.
     *
     * @Route("/exporter/{format}", name="proyecto_export")
     */
    public function getExporter($format) {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Autocomplete a Proyecto entity.
     *
     * @Route("/autocomplete-forms/get-partesInvolucradas", name="Proyecto_autocomplete_partesInvolucradas")
     */
    public function getAutocompleteParteInvolucrada() {

        $options = array(
            'repository' => "TecsproFrontBundle:ParteInvolucrada",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Datatable Proyecto.
     *
     * @Route("/get-table/", name="proyecto_table")
     */
    public function getDatatable() {
        $response = parent::getTable();

        return $response;
    }

    /**
     * Create query.
     * @return Doctrine\ORM\QueryBuilder $queryBuilder
     */
    protected function createQuery() {
        $em = $this->getDoctrine()->getManager();
        $config = $this->getConfig();
        $qb = $em->createQueryBuilder();
        $user = $this->getUser();
        $qb
                ->select('a.id', 'a.fecha', 'a.nombre')
                ->from($config['repository'], 'a')
                ->join('a.user', 'u')
                ->where('u.id = ' . $user->getId())
        ;

        $tipoArray[] = array(
            'column' => 1,
            'type' => 'datetime',
            'format' => 'd/m/Y'
        );

        $array = array(
            'query' => $qb,
            'tipoArray' => $tipoArray
        );

        return $array;
    }

    /**
     * Creates a form to create a entity.
     *
     * @param array $config
     * @param $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createCreateForm($config, $entity) {

        $language = $this->getRequest()->getSession()->get('_locale');

        $form = $this->createForm($config['newType'], $entity, array(
            'action' => '',
            'method' => 'POST',
            'empty_data' => $language,
            'attr' => array(
                'id' => 'form'
            )
        ));

        $form
                ->add('save', 'submit', array(
                    'translation_domain' => 'TecsproFrontBundle',
                    'label' => 'Guardar Proyecto',
                    'attr' => array(
                        'class' => 'btn btn-success pull-right',
                    ),
                ))
        ;

        return $form;
    }

    /**
     * Deletes a Proyecto entity.
     *
     * @Route("/proyecto/proyecto-nuevo", name="proyecto_nuevo")
     */
    public function ProyectoNuevoAction() {
        $request = $this->getRequest();
        $session = $request->getSession();


        $session->remove('formulas');
        $session->remove('proyecto');
        $session->remove('proyectoNombre');


        return $this->redirect($this->generateUrl('inicio_plantilla'), 301);
    }

    /**
     *
     * @Route("/proyecto/select-proyecto/{proyecto}", name="proyecto_select", options={"expose"=true})
     * @I18nDoctrine
     */
    public function ProyectoSelectAction($proyecto) {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        $session = $request->getSession();

        $entity = $em->getRepository('TecsproFrontBundle:Proyecto')->find($proyecto);
        $formulas = array();
        $idFormulas = array();
        //busco las id de formulas para buscarlas en la bd
        if (is_null($entity) == false && is_null($entity->getFormulas()) == false) {
            foreach ($entity->getFormulas() as $formulaJson) {

                $idFormulas[] = $formulaJson['idFormula'];
            }
        }

        //
        if (count($idFormulas) > 0) {
            //verificar si existen las formulas sino informar que el archivo esta corrupto
            //borro valores repetidos 
            $idFormulas = array_unique($idFormulas);

            $formulasBd = $em->getRepository('ModuloBundle:Formula')->existe($idFormulas);

            //$formulabdkey = formulas de la base de datos con la key= id de la formula
            //$formulasActualizada= formulas actualizada con los valores de usuario y valores del sistema

            $formulabdkey = array();
            $formulaActualizada = array();
            foreach ($formulasBd as $formulabd) {
                $formulabdkey[$formulabd->getId()] = $formulabd;
            }

            $serializer = SerializerBuilder::create()->build();

            foreach ($entity->getFormulas() as $key => $_formulaJson) {

                //deserializo para setear los valores de las variables
                $formulaJson = $_formulaJson[0];
                $valoresJson = json_decode($formulaJson["valores"], true);
                //deserializo para setear los valores de las ecucaciones

                $ecuacionesJson = json_decode($formulaJson["ecuaciones"], true);
                $arrayEcuacionJson = array();
                foreach ($ecuacionesJson as $ecujson) {
                    $arrayEcuacionJson[$ecujson["id"]] = $ecujson;
                }
                //seteo formula
                // $formula = clone $formulabdkey[$formulaJson["idFormula"]];
                $formula = clone $formulabdkey[$_formulaJson["idFormula"]];
                if ($formula->getAutoincremental() == false) {
                    foreach ($formula->getVariables() as $variable) {
                        foreach ($valoresJson as $var) {
                            if ($variable->getId() === $var["id"]) {
                                if ($var['autoincrementar'] == "true") {
                                    $variable->setParticiones($var['particiones']);
                                    $variable->setValorDesde($var['valor_desde']);
                                    $variable->setValorHasta($var['valor_hasta']);
                                    if (isset($var['valor']))
                                        $variable->setValor($var['valor']);
                                    $variable->setAutoincrementar(true);
                                } else {
                                    $variable->setParticiones(0);
                                    $variable->setValorDesde(0);
                                    $variable->setValorHasta(0);
                                    if (isset($var['valor']))
                                        $variable->setValor(array($var['valor']));
                                    $variable->setAutoincrementar(false);
                                }
                            }
                        }
                    }
                }else {

                    $newVariablePlantilla = clone $formula->getVariables()[0];
                    $formula->getVariables()->clear();

                    foreach ($valoresJson as $var) {
                        $newVariable = clone $newVariablePlantilla;

                        if ($var['autoincrementar'] == "true") {
                            $newVariable->setParticiones($var['particiones']);
                            $newVariable->setValorDesde($var['valor_desde']);
                            $newVariable->setValorHasta($var['valor_hasta']);
                            if (isset($var['valor']))
                                $newVariable->setValor($var['valor']);
                            $newVariable->setAutoincrementar(true);
                        } else {
                            $newVariable->setParticiones(0);
                            $newVariable->setValorDesde(0);
                            $newVariable->setValorHasta(0);
                            if (isset($var['valor']))
                                $newVariable->setValor(array($var['valor']));
                            $newVariable->setAutoincrementar(false);
                        }

                        $formula->addVariable($newVariable);
                    }
                }
                foreach ($formula->getEcuacion() as $ecuacion) {
                    if (isset($arrayEcuacionJson[$ecuacion->getId()]["resultado_maximo"]))
                        $ecuacion->setResultadoMaximo($arrayEcuacionJson[$ecuacion->getId()]["resultado_maximo"]);

                    if (isset($arrayEcuacionJson[$ecuacion->getId()]["resultado_minimo"]))
                        $ecuacion->setResultadoMinimo($arrayEcuacionJson[$ecuacion->getId()]["resultado_minimo"]);

                    if (isset($arrayEcuacionJson[$ecuacion->getId()]["resultado_medio"]))
                        $ecuacion->setResultadoMedio($arrayEcuacionJson[$ecuacion->getId()]["resultado_medio"]);

                    if (isset($arrayEcuacionJson[$ecuacion->getId()]["resultado"]))
                        $ecuacion->setResultado($arrayEcuacionJson[$ecuacion->getId()]["resultado"]);

                    if (isset($arrayEcuacionJson[$ecuacion->getId()]["unidad_seleccionada"]))
                        $ecuacion->setUnidadSeleccionada($arrayEcuacionJson[$ecuacion->getId()]["unidad_seleccionada"]);
                }
                $formula->setImprimir($_formulaJson["imprimir"]);
                if (isset($formulaJson["nombrePersonalizado"]))
                    $formula->setNombrePersonalizado($formulaJson["nombrePersonalizado"]);

                //deserializo para setear los valores de las v);
                $json = $serializer->serialize($formula, 'json');

                $array['idFormula'] = $formula->getId();
                $array['nombre'] = $serializer->serialize($formula->getTranslations(), 'json');
                $array['formula'] = $json;
                $array['imprimir'] = $_formulaJson["imprimir"];
                $array['orden'] = $formula->getGrupo()->getOrden();
                $array['key'] = $key;

                $formulas[] = $array;
            }
            $session->set('key', $formulas[count($formulas) - 1]["key"]);
            $session->set('formulas', $formulas);


            $repuesta = true;
        } else {
            $session->remove('formulas');

            $repuesta = true;
        }
        $session->set('proyecto', $proyecto);
        $session->set('proyectoNombre', $entity->getNombre());
        $session->set('moduloSeleccionado', $entity->getModulo()->getId());
        $session->set('moduloNombreSeleccionado', $entity->getModulo()->getNombre());

        return $this->redirect($this->generateUrl('index'), 301);
    }

    /**
     * Deletes a Proyecto entity.
     *
     * @Route("/delete-proyectos-seleccionados/", name="proyecto_seleccionados_delete_byajax", options={"expose"=true})
     */
    public function deleteProyectosSeleccionadosByAjaxAction() {
        $config = $this->getConfig();
        $request = $this->getRequest();
        $idProyectos = $request->query->get('idProyecto', null);
        $em = $this->getDoctrine()->getManager();
        $proyectos = $em->getRepository($config['repository'])->findInArrayId($idProyectos);

        $this->get('session')->getFlashBag()->add('success', 'flash.delete.success');

        $session = $request->getSession();
        $proyectoSession = $session->get('proyecto', null);

        foreach ($proyectos as $proyecto) {
            if ($proyectoSession == $proyecto->getId()) {
                $session->remove('formulas');
                $session->remove('proyecto');
                $session->remove('proyectoNombre');
            }
            $em->remove($proyecto);
        }
        $em->flush();

        $response = new JsonResponse();
        $response->setData(true);

        return $response;
    }

}

<?php

namespace Tecspro\VehiculoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * MotocicletaType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class MotocicletaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('marca')
            ->add('modelo')
            ->add('anio')
            ->add('cilindrada')
            ->add('largo')
            ->add('ancho')
            ->add('alto')
            ->add('distanciaEntreEjes')
            ->add('pesoVacio')
            ->add('comentario')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Tecspro\VehiculoBundle\Entity\Motocicleta'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'tecspro_vehiculobundle_motocicleta';
    }
}

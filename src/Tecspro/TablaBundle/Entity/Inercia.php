<?php

namespace Tecspro\TablaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Inercia
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Tecspro\TablaBundle\Entity\InerciaRepository")
 */
class Inercia
{
     use \A2lix\I18nDoctrineBundle\Doctrine\ORM\Util\Translatable;
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="batalla", type="string", length=255, nullable=true)
     */
    private $batalla;

    /**
     * @var string
     *
     * @ORM\Column(name="unobatallaMenor2P896", type="string", length=255, nullable=true)
     */
    private $unobatallaMenor2P896;

    /**
     * @var string
     *
     * @ORM\Column(name="unobatallaMenor2P642", type="string", length=255, nullable=true)
     */
    private $unobatallaMenor2P642;

     /**
     * @var string
     *
     * @ORM\Column(name="unoDesviacionEstandar", type="string", length=255, nullable=true)
     */
    private $unoDesviacionEstandar;

       /**
     * @var string
     *
     * @ORM\Column(name="dosbatallaMayor2P896", type="string", length=255, nullable=true)
     */
    private $dosbatallaMayor2P896;

      /**
     * @var string
     *
     * @ORM\Column(name="dosDesviacionEstandar", type="string", length=255, nullable=true)
     */
    private $dosDesviacionEstandar;


    /**
     * @var string
     *
     * @ORM\Column(name="dosbatallaMayor2P642", type="string", length=255, nullable=true)
     */
    private $dosbatallaMayor2P642;

    /**
     * @var string
     *
     * @ORM\Column(name="unobatallaMenor2P931", type="string", length=255, nullable=true)
     */
    private $unobatallaMenor2P931;

    /**
     * @var string
     *
     * @ORM\Column(name="dosbatallaMayor2P931", type="string", length=255, nullable=true)
     */
    private $dosbatallaMayor2P931;
    protected $translations;
    /**
     * Constructor
     */
    public function __construct() {
        $this->translations = new \Doctrine\Common\Collections\ArrayCollection();
    }
    


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set batalla
     *
     * @param string $batalla
     * @return Inercia
     */
    public function setBatalla($batalla)
    {
        $this->batalla = $batalla;

        return $this;
    }

    /**
     * Get batalla
     *
     * @return string 
     */
    public function getBatalla()
    {
        return $this->batalla;
    }


    /**
     * Set unobatallaMenor2P896
     *
     * @param string $unobatallaMenor2P896
     * @return Inercia
     */
    public function setUnobatallaMenor2P896($unobatallaMenor2P896)
    {
        $this->unobatallaMenor2P896 = $unobatallaMenor2P896;

        return $this;
    }

    /**
     * Get unobatallaMenor2P896
     *
     * @return string 
     */
    public function getUnobatallaMenor2P896()
    {
        return $this->unobatallaMenor2P896;
    }

    /**
     * Set unobatallaMenor2P642
     *
     * @param string $unobatallaMenor2P642
     * @return Inercia
     */
    public function setUnobatallaMenor2P642($unobatallaMenor2P642)
    {
        $this->unobatallaMenor2P642 = $unobatallaMenor2P642;

        return $this;
    }

    /**
     * Get unobatallaMenor2P642
     *
     * @return string 
     */
    public function getUnobatallaMenor2P642()
    {
        return $this->unobatallaMenor2P642;
    }

    /**
     * Set unoDesviacionEstandar
     *
     * @param string $unoDesviacionEstandar
     * @return Inercia
     */
    public function setUnoDesviacionEstandar($unoDesviacionEstandar)
    {
        $this->unoDesviacionEstandar = $unoDesviacionEstandar;

        return $this;
    }

    /**
     * Get unoDesviacionEstandar
     *
     * @return string 
     */
    public function getUnoDesviacionEstandar()
    {
        return $this->unoDesviacionEstandar;
    }

    /**
     * Set dosbatallaMayor2P896
     *
     * @param string $dosbatallaMayor2P896
     * @return Inercia
     */
    public function setDosbatallaMayor2P896($dosbatallaMayor2P896)
    {
        $this->dosbatallaMayor2P896 = $dosbatallaMayor2P896;

        return $this;
    }

    /**
     * Get dosbatallaMayor2P896
     *
     * @return string 
     */
    public function getDosbatallaMayor2P896()
    {
        return $this->dosbatallaMayor2P896;
    }

    /**
     * Set dosDesviacionEstandar
     *
     * @param string $dosDesviacionEstandar
     * @return Inercia
     */
    public function setDosDesviacionEstandar($dosDesviacionEstandar)
    {
        $this->dosDesviacionEstandar = $dosDesviacionEstandar;

        return $this;
    }

    /**
     * Get dosDesviacionEstandar
     *
     * @return string 
     */
    public function getDosDesviacionEstandar()
    {
        return $this->dosDesviacionEstandar;
    }

    /**
     * Set dosbatallaMayor2P642
     *
     * @param string $dosbatallaMayor2P642
     * @return Inercia
     */
    public function setDosbatallaMayor2P642($dosbatallaMayor2P642)
    {
        $this->dosbatallaMayor2P642 = $dosbatallaMayor2P642;

        return $this;
    }

    /**
     * Get dosbatallaMayor2P642
     *
     * @return string 
     */
    public function getDosbatallaMayor2P642()
    {
        return $this->dosbatallaMayor2P642;
    }

    /**
     * Set unobatallaMenor2P931
     *
     * @param string $unobatallaMenor2P931
     * @return Inercia
     */
    public function setUnobatallaMenor2P931($unobatallaMenor2P931)
    {
        $this->unobatallaMenor2P931 = $unobatallaMenor2P931;

        return $this;
    }

    /**
     * Get unobatallaMenor2P931
     *
     * @return string 
     */
    public function getUnobatallaMenor2P931()
    {
        return $this->unobatallaMenor2P931;
    }

    /**
     * Set dosbatallaMayor2P931
     *
     * @param string $dosbatallaMayor2P931
     * @return Inercia
     */
    public function setDosbatallaMayor2P931($dosbatallaMayor2P931)
    {
        $this->dosbatallaMayor2P931 = $dosbatallaMayor2P931;

        return $this;
    }

    /**
     * Get dosbatallaMayor2P931
     *
     * @return string 
     */
    public function getDosbatallaMayor2P931()
    {
        return $this->dosbatallaMayor2P931;
    }
}

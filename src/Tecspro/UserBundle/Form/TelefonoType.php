<?php

namespace Tecspro\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Regex;

class TelefonoType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tipo', 'choice', array(
                'label_attr' => array('class' => 'col-lg-1 col-md-1 col-sm-1'),
                'choices' => $this->getTipo()
            ))
            ->add('numero', null, array(
                'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                'constraints' => new Regex(array('pattern' => '/\d/' , 'htmlPattern' => '/\d/')),
            ))
            // ->add('persona')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Tecspro\UserBundle\Entity\Telefono'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'Tecspro_Userbundle_telefono';
    }

    private function getTipo()
    {
        return array(
            'personal' => 'Personal',
            'laboral'  => 'Laboral',
        );
    }
}

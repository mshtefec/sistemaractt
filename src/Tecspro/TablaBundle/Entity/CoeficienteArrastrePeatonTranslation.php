<?php

namespace Tecspro\TablaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 * Categoria
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Tecspro\TablaBundle\Entity\CoeficienteArrastrePeatonRepository")
 */
class CoeficienteArrastrePeatonTranslation implements \A2lix\I18nDoctrineBundle\Doctrine\Interfaces\OneLocaleInterface {

      use \Tecspro\ModuloBundle\Entity\Translation;

    
  

    /**
     * @var string
     *
     * @ORM\Column(name="trayectoria", type="string", length=255, nullable=true)
     */
    private $trayectoria;

    /**
     * @var string
     *
     * @ORM\Column(name="Notas", type="string", length=255, nullable=true)
     */
    private $notas;


    /**
     * Set trayectoria
     *
     * @param string $trayectoria
     * @return CoeficienteArrastrePeatonTranslation
     */
    public function setTrayectoria($trayectoria)
    {
        $this->trayectoria = $trayectoria;

        return $this;
    }

    /**
     * Get trayectoria
     *
     * @return string 
     */
    public function getTrayectoria()
    {
        return $this->trayectoria;
    }

    /**
     * Set notas
     *
     * @param string $notas
     * @return CoeficienteArrastrePeatonTranslation
     */
    public function setNotas($notas)
    {
        $this->notas = $notas;

        return $this;
    }

    /**
     * Get notas
     *
     * @return string 
     */
    public function getNotas()
    {
        return $this->notas;
    }
}

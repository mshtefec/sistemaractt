<?php

namespace Tecspro\ModuloBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Exclude;

/**
 * Ecuacion
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Ecuacion {

    use \A2lix\I18nDoctrineBundle\Doctrine\ORM\Util\Translatable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Type("integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="expresion", type="text")
     * @Type("string")
     */
    private $expresion;

    /**
     * @ORM\OneToMany(targetEntity="UnidadResultadoOrden", mappedBy="ecuacion",cascade={"all"})
     * @Type("ArrayCollection<Tecspro\ModuloBundle\Entity\UnidadResultadoOrden>")
     */
    private $unidadResultado;

    /**
     * @var string
     *
     * @ORM\Column(name="resultadoMaximo", type="string", length=255,nullable=true)
     * @Type("string")
     */
    private $resultadoMaximo;

    /**
     * @var string
     *
     * @ORM\Column(name="resultadoMinimo", type="string", length=255,nullable=true)
     * @Type("string")
     */
    private $resultadoMinimo;

    /**
     * @var string
     *
     * @ORM\Column(name="resultadoMedio", type="string", length=255,nullable=true)
     * @Type("string")
     */
    private $resultadoMedio;

    /**
     * @ORM\Column(name="resultado", type="array")
     * @Type("array")
     */
    private $resultado;

    /**
     * @ORM\ManyToOne(targetEntity="Formula", inversedBy="ecuacion")
     * @ORM\JoinColumn(name="formula_id", referencedColumnName="id")
     * @Exclude
     */
    private $formula;

    /**
     * @var integer
     *
     * @ORM\Column(name="unidadSeleccionada", type="integer")
     * @Type("integer")
     */
    private $unidadSeleccionada;

    /**
     * @Type("ArrayCollection<Tecspro\ModuloBundle\Entity\EcuacionTranslation>")
     */
    protected $translations;

    /**
     * @Assert\True(message = "Las ecuaciones solo tienen que tener una unidad chequeada como principal")
     */
    public function isPrincipal() {
        $contador = 0;
        foreach ($this->unidadResultado as $unidad) {
            if ($unidad->getPrincipal())
                $contador++;

            if ($contador > 1)
                break;
        }
        if ($contador > 1) {
            $res = false;
        } else {
            $res = true;
        }
        return $res;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set resultadoMaximo
     *
     * @param string $resultadoMaximo
     * @return Ecuacion
     */
    public function setResultadoMaximo($resultadoMaximo) {
        $this->resultadoMaximo = $resultadoMaximo;

        return $this;
    }

    /**
     * Get resultadoMaximo
     *
     * @return string 
     */
    public function getResultadoMaximo() {
        return $this->resultadoMaximo;
    }

    /**
     * Set resultadoMinimo
     *
     * @param string $resultadoMinimo
     * @return Ecuacion
     */
    public function setResultadoMinimo($resultadoMinimo) {
        $this->resultadoMinimo = $resultadoMinimo;

        return $this;
    }

    /**
     * Get resultadoMinimo
     *
     * @return string 
     */
    public function getResultadoMinimo() {
        return $this->resultadoMinimo;
    }

    /**
     * Set resultado
     *
     * @param array $resultado
     * @return Ecuacion
     */
    public function setResultado($resultado) {
        $this->resultado = $resultado;

        return $this;
    }

    /**
     * Get resultado
     *
     * @return array 
     */
    public function getResultado() {
        return $this->resultado;
    }

    /**
     * Set formula
     *
     * @param \Tecspro\ModuloBundle\Entity\Formula $formula
     * @return Ecuacion
     */
    public function setFormula(\Tecspro\ModuloBundle\Entity\Formula $formula = null) {
        $this->formula = $formula;

        return $this;
    }

    /**
     * Get formula
     *
     * @return \Tecspro\ModuloBundle\Entity\Formula 
     */
    public function getFormula() {
        return $this->formula;
    }

    /**
     * Set expresion
     *
     * @param string $expresion
     * @return Ecuacion
     */
    public function setExpresion($expresion) {
        $this->expresion = $expresion;

        return $this;
    }

    /**
     * Get expresion
     *
     * @return string 
     */
    public function getExpresion() {
        return $this->expresion;
    }

    /**
     * Set resultadoMedio
     *
     * @param string $resultadoMedio
     * @return Ecuacion
     */
    public function setResultadoMedio($resultadoMedio) {
        $this->resultadoMedio = $resultadoMedio;

        return $this;
    }

    /**
     * Get resultadoMedio
     *
     * @return string 
     */
    public function getResultadoMedio() {
        return $this->resultadoMedio;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->unidadResultado = new \Doctrine\Common\Collections\ArrayCollection();
        $this->translations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add unidadResultado
     *
     * @param \Tecspro\ModuloBundle\Entity\Unidad $unidadResultado
     * @return Ecuacion
     */
    public function addUnidadResultado(\Tecspro\ModuloBundle\Entity\UnidadResultadoOrden $unidadResultado) {
        $unidadResultado->setEcuacion($this);
        $this->unidadResultado[] = $unidadResultado;

        return $this;
    }

    /**
     * Set unidadSeleccionada
     *
     * @param integer $unidadSeleccionada
     * @return Ecuacion
     */
    public function setUnidadSeleccionada($unidadSeleccionada) {
        $this->unidadSeleccionada = $unidadSeleccionada;

        return $this;
    }

    /**
     * Get unidadSeleccionada
     *
     * @return integer 
     */
    public function getUnidadSeleccionada() {
        return $this->unidadSeleccionada;
    }

    /**
     * Remove unidadResultado
     *
     * @param \Tecspro\ModuloBundle\Entity\UnidadResultadoOrden $unidadResultado
     */
    public function removeUnidadResultado(\Tecspro\ModuloBundle\Entity\UnidadResultadoOrden $unidadResultado) {
        $this->unidadResultado->removeElement($unidadResultado);
    }

    /**
     * Get unidadResultado
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUnidadResultado() {
        return $this->unidadResultado;
    }

}

<?php

namespace Tecspro\UserBundle\services;

use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\Routing\RouterInterface;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Mailer\MailerInterface;

class Email {

    protected $mailer;
    protected $router;
    protected $templating;

    public function __construct($mailer, RouterInterface $router, EngineInterface $templating) {
        $this->mailer = $mailer;
        $this->router = $router;
        $this->templating = $templating;
    }

    public function sendResettingSessionMessage(\FOS\UserBundle\Model\UserInterface $user) {

        $url = $this->router->generate('User_resetting_request_session_send_token', array('token' => $user->getConfirmationTokenSession()), true);
        $rendered = $this->templating->render('UserBundle:email:sessionReseting.html.twig', array(
            'user' => $user,
            'confirmationUrl' => $url
        ));
        $this->sendEmailMessage($rendered, "soporte@ractt.com", $user->getEmail());
    }

    /**
     * @param string $renderedTemplate
     * @param string $toEmail
     */
    protected function sendEmailMessage($renderedTemplate, $fromEmail, $toEmail) {
        // Render the email, use the first line as the subject, and the rest as the body
        $renderedLines = explode("\n", trim($renderedTemplate));
        $subject = $renderedLines[0];
        $body = implode("\n", array_slice($renderedLines, 1));

        $message = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom($fromEmail)
                ->setTo($toEmail)
                ->setBody($body);

        $this->mailer->send($message);
    }

}

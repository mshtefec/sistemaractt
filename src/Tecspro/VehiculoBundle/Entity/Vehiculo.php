<?php

namespace Tecspro\VehiculoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vehiculo
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Vehiculo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="marca", type="string", length=255)
     */
    protected $marca;

    /**
     * @var string
     *
     * @ORM\Column(name="modelo", type="string", length=255)
     */
    protected $modelo;

    /**
     * @var integer
     *
     * @ORM\Column(name="anio", type="integer")
     */
    protected $anio;

    /**
     * @var string
     *
     * @ORM\Column(name="cilindrada", type="string", length=255)
     */
    protected $cilindrada;

    /**
     * @var float
     *
     * @ORM\Column(name="largo", type="float")
     */
    protected $largo;

    /**
     * @var float
     *
     * @ORM\Column(name="ancho", type="float")
     */
    protected $ancho;

    /**
     * @var float
     *
     * @ORM\Column(name="alto", type="float")
     */
    protected $alto;

    /**
     * @var string
     *
     * @ORM\Column(name="distanciaEntreEjes", type="string", length=255)
     */
    protected $distanciaEntreEjes;

    /**
     * @var string
     *
     * @ORM\Column(name="pesoVacio", type="string", length=255)
     */
    protected $pesoVacio;

    /**
     * @var string
     *
     * @ORM\Column(name="comentario", type="string", length=255)
     */
    protected $comentario;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set marca
     *
     * @param string $marca
     * @return Vehiculo
     */
    public function setMarca($marca)
    {
        $this->marca = $marca;

        return $this;
    }

    /**
     * Get marca
     *
     * @return string 
     */
    public function getMarca()
    {
        return $this->marca;
    }

    /**
     * Set modelo
     *
     * @param string $modelo
     * @return Vehiculo
     */
    public function setModelo($modelo)
    {
        $this->modelo = $modelo;

        return $this;
    }

    /**
     * Get modelo
     *
     * @return string 
     */
    public function getModelo()
    {
        return $this->modelo;
    }

    /**
     * Set anio
     *
     * @param integer $anio
     * @return Vehiculo
     */
    public function setAnio($anio)
    {
        $this->anio = $anio;

        return $this;
    }

    /**
     * Get anio
     *
     * @return integer 
     */
    public function getAnio()
    {
        return $this->anio;
    }

    /**
     * Set cilindrada
     *
     * @param string $cilindrada
     * @return Vehiculo
     */
    public function setCilindrada($cilindrada)
    {
        $this->cilindrada = $cilindrada;

        return $this;
    }

    /**
     * Get cilindrada
     *
     * @return string 
     */
    public function getCilindrada()
    {
        return $this->cilindrada;
    }

    /**
     * Set largo
     *
     * @param float $largo
     * @return Vehiculo
     */
    public function setLargo($largo)
    {
        $this->largo = $largo;

        return $this;
    }

    /**
     * Get largo
     *
     * @return float 
     */
    public function getLargo()
    {
        return $this->largo;
    }

    /**
     * Set ancho
     *
     * @param float $ancho
     * @return Vehiculo
     */
    public function setAncho($ancho)
    {
        $this->ancho = $ancho;

        return $this;
    }

    /**
     * Get ancho
     *
     * @return float 
     */
    public function getAncho()
    {
        return $this->ancho;
    }

    /**
     * Set alto
     *
     * @param float $alto
     * @return Vehiculo
     */
    public function setAlto($alto)
    {
        $this->alto = $alto;

        return $this;
    }

    /**
     * Get alto
     *
     * @return float 
     */
    public function getAlto()
    {
        return $this->alto;
    }

    /**
     * Set distanciaEntreEjes
     *
     * @param string $distanciaEntreEjes
     * @return Vehiculo
     */
    public function setDistanciaEntreEjes($distanciaEntreEjes)
    {
        $this->distanciaEntreEjes = $distanciaEntreEjes;

        return $this;
    }

    /**
     * Get distanciaEntreEjes
     *
     * @return string 
     */
    public function getDistanciaEntreEjes()
    {
        return $this->distanciaEntreEjes;
    }

    /**
     * Set pesoVacio
     *
     * @param string $pesoVacio
     * @return Vehiculo
     */
    public function setPesoVacio($pesoVacio)
    {
        $this->pesoVacio = $pesoVacio;

        return $this;
    }

    /**
     * Get pesoVacio
     *
     * @return string 
     */
    public function getPesoVacio()
    {
        return $this->pesoVacio;
    }

    /**
     * Set comentario
     *
     * @param string $comentario
     * @return Vehiculo
     */
    public function setComentario($comentario)
    {
        $this->comentario = $comentario;

        return $this;
    }

    /**
     * Get comentario
     *
     * @return string 
     */
    public function getComentario()
    {
        return $this->comentario;
    }
}

/* global Translator */

function renderPanelVariable(key, imagen, referencia, formulaSeleccionada, plantilla) {
    console.log(3123123);
//var source = $("#" + plantilla).html();
//var template = Handlebars.compile(source);
    Handlebars.registerHelper('image', function (uploaddir, file) {

        var result = '<img class="media-object" src="' + location.host + 'media/cache/upscale800x600/uploads/imagenes/' + file + '">';
        return new Handlebars.SafeString(result);
    });
    Handlebars.registerHelper('file', function (translation) {
        var file_path = "";
        var uploaddir = "";
        var result = "";
        if (translation instanceof Array) {
            for ($i = 0; $i < translation.length; $i++) {
                if (translation[$i].locale === Translator.locale) {
                    uploaddir = translation[$i]["upload_dir"];
                    file_path = translation[$i]["file_path"];
                }
            }
        } else {
            uploaddir = translation[Translator.locale]["upload_dir"];
            file_path = translation[Translator.locale]["file_path"];
        }

        if (typeof file_path !== "undefined" && typeof uploaddir !== "undefined") {
            result = '<a href="../' + uploaddir + "/" + file_path + '" target="_blank" class="btn btn-info"><span class="glyphicon glyphicon-comment"></span> Referencia</a>';
        }
        return new Handlebars.SafeString(result);
    }
    );
    metodosHandlebars();
    $form = JSON.parse(formulaSeleccionada);


    var html = $templatePanelVariables($form);
    $("#panelVariable").html(html);
    metodos();
    mensajespopup();
    botonAccion(key);
}
function metodos() {
    $(".guardar").change(function () {

        if ($form.autoincremental === false) {
            $vari = $(this).data('idvariable');
            $tipo = $(this).data('tipo');

            $autoincrementar = false;
            for ($i = 0; $i < $form.variables.length; $i++) {
                if ($form.variables[$i].id === $vari) {
                    if ($tipo === "valor") {
                        $arr = [$(this).val()];
                        $form.variables[$i].valor = $arr;
                    } else if ($tipo === "valorDesde") {
                        $form.variables[$i].valor_desde = $(this).val();
                        $autoincrementar = true;
                    } else if ($tipo === "valorHasta") {
                        $form.variables[$i].valor_hasta = $(this).val();
                        $autoincrementar = true;
                    } else if ($tipo === "particiones") {
                        $form.variables[$i].particiones = parseInt($(this).val());
                        $autoincrementar = true;
                    }
                    $form.variables[$i].autoincrementar = $autoincrementar;
                }
            }
        } else {
            index = $(this).data('idvariableindex');
            $autoincrementar = false;

            if ($tipo === "valor") {
                $arr = [$(this).val()];
                $form.variables[index].valor = $arr;
            } else if ($tipo === "valorDesde") {
                $form.variables[index].valor_desde = $(this).val();
                $autoincrementar = true;
            } else if ($tipo === "valorHasta") {
                $form.variables[index].valor_hasta = $(this).val();
                $autoincrementar = true;
            } else if ($tipo === "particiones") {
                $form.variables[index].particiones = parseInt($(this).val());
                $autoincrementar = true;
            }
            $form.variables[index].autoincrementar = $autoincrementar;


        }
        guardarForm();
    });
    $(".soloNumeros").keypress(function (e) {
        var key = window.Event ? e.which : e.keyCode

        return (key >= 48 && key <= 57 || key === 46 || key === 8 || key === 0 || key === 13);
    });
    $('.button-checkbox').each(function () {

// Settings
        var $widget = $(this),
                $button = $widget.find('button'),
                $checkbox = $widget.find('input:checkbox'),
                color = $button.data('color'),
                settings = {
                    on: {
                        icon: 'glyphicon glyphicon-check'
                    },
                    off: {
                        icon: 'glyphicon glyphicon-unchecked'
                    }
                };
        // Event Handlers
        $button.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });
        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');
            // Set the button's state
            $button.data('state', (isChecked) ? "on" : "off");
            // Set the button's icon
            $button.find('.state-icon')
                    .removeClass()
                    .addClass('state-icon ' + settings[$button.data('state')].icon);
            // Update the button's color
            if (isChecked) {
                $button
                        .removeClass('btn-default')
                        .addClass('btn-' + color + ' active');
            } else {
                $button
                        .removeClass('btn-' + color + ' active')
                        .addClass('btn-default');
            }
        }

        // Initialization
        function init() {

            updateDisplay();
            // Inject the icon if applicable
            if ($button.find('.state-icon').length == 0) {
                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
            }
        }
        init();
    });
}

function mostrarTotales() {
//Muestro los resulados
    for ($e = 0; $e < ecuaciones.length; $e++) {

        cambiarUnidad(ecuaciones[$e].id, $("#listaUnidadEcuacion" + ecuaciones[$e].id).val());
        /*$liHtml = "";
         if (typeof ($form.ecuacion[$i].resultado) !== "undefined") {
         if (parseInt($form.ecuacion[$i].resultado.length) > 1) {
         $liHtml = $liHtml + '<li class="list-group-item"><span class="badge">' + $form.ecuacion[$i].resultado_medio + '</span> Resultado Medio</li>';
         $liHtml = $liHtml + '<li class="list-group-item"><span class="badge">' + $form.ecuacion[$i].resultado_minimo + '</span> Resultado Minimo</li>';
         $liHtml = $liHtml + '<li class="list-group-item"><span class="badge">' + $form.ecuacion[$i].resultado_maximo + '</span> Resultado Maximo</li>';
         
         } else {
         $liHtml = '<li class="list-group-item"><span class="badge">' + $form.ecuacion[$i].resultado[0] + '</span> Resultado</li>';
         
         }
         }
         $("#listarResultadoEcuacion" + $form.ecuacion[$i].id).html($liHtml);*/
    }
}

function cambiarUnidad($_ecuacion, $_unidad) {
    for ($i = 0; $i < $form.ecuacion.length; $i++) {
        $liHtml = "";
        if ($form.ecuacion[$i].id === $_ecuacion) {
            for ($j = 0; $j < $form.ecuacion[$i].unidad_resultado.length; $j++) {
                if ($form.ecuacion[$i].unidad_resultado[$j].unidad.id === parseInt($_unidad)) {
                    if ($form.ecuacion[$i].unidad_resultado[$j].principal === true) {
                        for ($l = 0; $l < ecuaciones.length; $l++) {
                            if ($form.ecuacion[$i].id === ecuaciones[$l].id) {
                                if (ecuaciones[$l].autoincremental === true) {
                                    $calResultadoMedio = ecuaciones[$l].resultadoMedio;
                                    $calResultadoMinimo = ecuaciones[$l].resultadoMinimo;
                                    $calResultadoMaximo = ecuaciones[$l].resultadoMaximo;
                                    $liHtml = $liHtml + '<li class="list-group-item"><span class="badge">' + $calResultadoMedio + '</span> Resultado Medio</li>';
                                    $liHtml = $liHtml + '<li class="list-group-item"><span class="badge">' + $calResultadoMinimo + '</span> Resultado Minimo</li>';
                                    $liHtml = $liHtml + '<li class="list-group-item"><span class="badge">' + $calResultadoMaximo + '</span> Resultado Maximo</li>';
                                } else {
                                    $calResultado = ecuaciones[$l].resultado;
                                    $liHtml = '<li class="list-group-item"><span class="badge">' + $calResultado + '</span> Resultado</li>';
                                }
                            }
                        }
                    } else {
                        for ($l = 0; $l < ecuaciones.length; $l++) {
                            if ($form.ecuacion[$i].id === ecuaciones[$l].id) {
                                if (ecuaciones[$l].autoincremental === true) {

                                    $calResultadoMedio = math.round(math.eval(ecuaciones[$l].resultadoMedio + $form.ecuacion[$i].unidad_resultado[$j].unidad.operacion), 2);
                                    $calResultadoMinimo = math.round(math.eval(ecuaciones[$l].resultadoMinimo + $form.ecuacion[$i].unidad_resultado[$j].unidad.operacion), 2);
                                    $calResultadoMaximo = math.round(math.eval(ecuaciones[$l].resultadoMaximo + $form.ecuacion[$i].unidad_resultado[$j].unidad.operacion), 2);
                                    $liHtml = $liHtml + '<li class="list-group-item"><span class="badge">' + $calResultadoMedio + '</span> Resultado Medio</li>';
                                    $liHtml = $liHtml + '<li class="list-group-item"><span class="badge">' + $calResultadoMinimo + '</span> Resultado Minimo</li>';
                                    $liHtml = $liHtml + '<li class="list-group-item"><span class="badge">' + $calResultadoMaximo + '</span> Resultado Maximo</li>';
                                } else {
                                    $calResultado = math.round(math.eval(ecuaciones[$l].resultado + $form.ecuacion[$i].unidad_resultado[$j].unidad.operacion), 2);
                                    $liHtml = '<li class="list-group-item"><span class="badge">' + $calResultado + '</span> Resultado</li>';
                                }
                            }
                        }
                    }
                    $("#listarResultadoEcuacion" + $form.ecuacion[$i].id).html($liHtml);
                }
            }
        }
    }
}
function metodosHandlebars() {

    Handlebars.registerHelper('ifmayornumb', function (variable, numb, options) {
        if (variable > numb) {
            return options.fn(this);
        } else {
            return options.inverse(this);
        }
    });
    Handlebars.registerHelper('ifmayor0', function (conditional, options) {
        if (conditional > 0) {
            return options.fn(this);
        }
    });
    Handlebars.registerHelper('traducir', function (variable, catalogo, options) {
        return  Translator.trans(variable, {}, catalogo);
    });
    Handlebars.registerHelper('traducirEntity', function (translation, variable, options) {

        if (translation instanceof Array) {
            for ($i = 0; $i < translation.length; $i++) {
                if (translation[$i].locale === Translator.locale) {
                    $respuesta = translation[$i][variable];
                }
            }
        } else {
            //$respuesta = translation[0][variable];
            $respuesta = translation[Translator.locale][variable];
        }
        return  $respuesta;
    });
    Handlebars.registerHelper('ifautoincrementarfalse', function (conditional, options) {

        if (conditional === false) {
            return options.fn(this);
        }
    });
    Handlebars.registerHelper('ifautoincrementartrue', function (conditional, options) {

        if (conditional === true) {
            return options.fn(this);
        } else {
            return options.inverse(this);
        }
    });
    Handlebars.registerHelper('ifCond', function (v1, v2, options) {

        if (v1 === v2) {
            return options.fn(this);
        } else {
            return options.inverse(this);
        }
    });
}
function botonAccion(key) {
    console.log(44);
    $(".txtUnidad").change(function () {
        $ecuacion = $(this).data("ecuacion");
        $val = $(this).val();
        cambiarUnidad($ecuacion, $val);
        /* $.ajax({
         type: "GET",
         url: Routing.generate('Formula_unidad_seleccionada'),
         data: {key: key, ecuacion: $ecuacion, val: $val},
         success: function (array) {
         
         cambiarUnidad($ecuacion, $val);
         },
         error: function (data) {
         mostrarNotifiacion("Alerta", "Disculpe, existió un problema", "error");
         
         }
         });*/
    });
    $(".btnCalcular").click(function () {
        console.log(233);
        calcular();
        $(".lblGuardado").removeClass("label-success");
        $(".lblGuardado").addClass("label-warning");
        $(".lblGuardado").html("calculo sin guardar");
        $form.sincronizado = false;
        guardarForm();
    });
    $(".btnGuardar").click(function () {
        console.log(2);
        $lst = $(this);
        $(this).attr("disabled", true);
        var session = [];
        var txtUnidad = [];
        $(".txtUnidad").each(function (index) {
            $unidad = {ecuacion: $(this).data("ecuacion"), unidadSeleccionada: $(this).val()};
            txtUnidad.push($unidad);
        });
        $form.sincronizado = false;
        $fomString = JSON.stringify($formSession);
        console.log($fomString);
        guardarForm();
        session.push(ecuaciones);
        session.push(variables);
        session.push(key);
        session.push(txtUnidad);
        session.push($fomString);
        //guarda las variables en session
        $.ajax({
            // la URL para la petición
            url: Routing.generate('Formula_session'),
            // la información a enviar
            // (también es posible utilizar una cadena de datos)
            data: {formula: session},
            // especifica si será una petición POST o GET
            type: 'GET',
            // el tipo de información que se espera de respuesta
            dataType: 'json',
            // código a ejecutar si la petición es satisfactoria;
            // la respuesta es pasada como argumento a la función
            success: function (json) {
                $form = JSON.parse(json.formula['formula']);
                $index = findInFormSessionIndex(key);
                $formSession[$index] = json.formula;
                //$formSession = json.formulasSession;
                //$(".txtUnidad").trigger("change");
                $(".lblGuardado").removeClass("label-warning");
                $(".lblGuardado").addClass("label-success");
                $(".lblGuardado").html("calculo guardado");
                mostrarNotifiacion("Alerta", "calculo guardado", "success");
                $($lst).removeAttr("disabled");
                if (json.nuevoProyecto === true) {
                    location.reload();
                }

            },
            // código a ejecutar si la petición falla;
            // son pasados como argumentos a la función
            // el objeto de la petición en crudo y código de estatus de la petición
            error: function (xhr, status) {
                alert('Disculpe, existió un problema');
                $($lst).removeAttr("disabled");
            },
        });
    });
    $(".btnImportar").click(function () {
        $variableSeleccionada = $(this).data("idvariable");
        importarResultado(key);
    });
    $(".btn-add").click(function () {

        var $formGroup = $('.rowAutoincremental tr').first();
        var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
        var $formGroupClone = $formGroup.clone();
        //.attr("id", "#blue");
        //cambio id 
        variable = $formGroupClone.children('td').first().children('div').first().children('div').first().children().children('input').data("idvariable");
        $contadorAnterior = contadorVariables - 1;
        //variables
        $formGroupClone.children('td').first().children('div').first().children('div').first().children().children('input').removeClass("txtVariables" + variable + 0);
        $formGroupClone.children('td').first().children('div').first().children('div').first().children().children('input').addClass("txtVariables" + variable + "" + contadorVariables);
        $formGroupClone.children('td').first().children('div').first().children('div').first().attr("id", "variable" + variable + "" + contadorVariables);
        $formGroupClone.children('td').first().children('div').first().children('div').first().attr("style", "display:block;");
        $formGroupClone.children('td').first().children('div').first().children('div').next().attr("id", "autoincremental" + variable + "" + contadorVariables);
        $formGroupClone.children('td').first().children('div').first().children('div').next().attr("style", "display:none;");
        //txtParticion

        $formGroupClone.children('td').first().children('div').first().children('div').next().children('div').first().children("div").next().children("select").attr("id", "txtParticion" + variable + "" + contadorVariables);
        //txtminimo
        $formGroupClone.children('td').first().children('div').first().children('div').next().children('div').next().children("input").attr("id", "txtValorDesde" + variable + "" + contadorVariables);
        $formGroupClone.children('td').first().children('div').first().children('div').next().children('div').next().children("input").removeClass("txtVariables" + variable + 0);
        $formGroupClone.children('td').first().children('div').first().children('div').next().children('div').next().children("input").addClass("txtVariables" + variable + "" + contadorVariables);
        //txtmaximo
        $formGroupClone.children('td').first().children('div').first().children('div').next().children('div').next().next().children("input").attr("id", "txtValorHasta" + variable + "" + contadorVariables);
        $formGroupClone.children('td').first().children('div').first().children('div').next().children('div').next().next().children("input").removeClass("txtVariables" + variable + 0);
        $formGroupClone.children('td').first().children('div').first().children('div').next().children('div').next().next().children("input").addClass("txtVariables" + variable + "" + contadorVariables);
        //idvariableindex

        $formGroupClone.children('td').first().children('div').first().children('div').first().children("div").children("input").attr("data-idvariableindex", contadorVariables);
        //acciones
        $formGroupClone.children('td').first().children('div').first().children('div').first().children().children("span").children().first().attr("data-index", contadorVariables);
        $formGroupClone.children('td').first().children('div').first().children('div').next().children('div').next().next().next().children().first().attr("data-index", contadorVariables);
        $formGroupClone.children('td').next().children('a').first().attr("data-variable", variable + "" + contadorVariables);
        $formGroupClone.children('td').next().children('a').first().attr("data-index", contadorVariables);
        $formGroupClone.children('td').next().children('a').next().attr("data-variable", variable + "" + contadorVariables);
        $formGroupClone.children('td').next().children('a').next().attr("data-idvariable", variable + "" + contadorVariables);
        contadorVariables++;
        $formGroupClone.find('input').val('');
        $formGroupClone.find('.concept').text('Phone');
        //agrego a form 
        $nuevo = $form.variables[0];
        $form.variables.push($nuevo);
        //$formGroupClone.insertAfter($formGroup);
        $('.rowAutoincremental').append($formGroupClone);
        var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
        if ($multipleFormGroup.data('max') <= countFormGroup($multipleFormGroup)) {
            $lastFormGroupLast.find('.btn-add').attr('disabled', true);
        }
        $(".btnImportar").click(function () {
            $variableSeleccionada = $(this).data("idvariable");
            importarResultado(key);
        });
        $(".guardar").change(function () {
            $tipo = $(this).data('tipo');
            index = $(this).data('idvariableindex');
            $autoincrementar = false;

            if ($tipo === "valor") {
                $arr = [$(this).val()];
                $form.variables[index].valor = $arr;
            } else if ($tipo === "valorDesde") {
                $form.variables[index].valor_desde = $(this).val();
                $autoincrementar = true;
            } else if ($tipo === "valorHasta") {
                $form.variables[index].valor_hasta = $(this).val();
                $autoincrementar = true;
            } else if ($tipo === "particiones") {
                $form.variables[index].particiones = parseInt($(this).val());
                $autoincrementar = true;
            }
            $form.variables[index].autoincrementar = $autoincrementar;



            //}
            guardarForm();
        });
        $form.sincronizado = false;
        guardarForm();
    });
}
function btnAutoincremental($_variable) {
    $variable = $_variable.getAttribute("data-variable");
    $index = $_variable.getAttribute("data-index");
    //solamente ahi 2 variables que pueden ser autoincrementar
    $contador = 1;
    $(".panelAutoincrementar").each(function (index) {
        if ($(this).is(':hidden') === false) {
            $contador++;
        }
    });
    if ($contador < 3) {


//oculto la variable y coloco el panel de variables autoincrementar
        $("#variable" + $variable).toggle();
        $("#autoincremental" + $variable).toggle();
    } else if ($("#autoincremental" + $variable).is(':hidden') === false) {

        $("#variable" + $variable).toggle();
        $("#autoincremental" + $variable).toggle();
    }
    $form.variables[$index].autoincremental = $("#variable" + $variable).is(':hidden');
}

function mensajespopup() {
    $(".mensajes").on('keyup', function () {
        var mensaje = $(this);
        var stength = $(this).data("content");
        var popover = mensaje.attr('data-content', stength).data('bs.popover');
        //popover.setContent();
        //popover.$tip.addClass(popover.options.placement).removeClass('danger success info warning primary').addClass(pclass);

    });
    $('input[data-toggle="popover"]').popover({
        placement: 'top',
        trigger: 'focus'
    });
}

function renderPanelDerecho(formulas) {
//var sourceSession = $("#hbPanelGrupoSeleccionados").html();
// var templateSession = Handlebars.compile(sourceSession);
    var htmlSession = $templatePanelFormulasSeleccionadas(formulas);
    $("#panelGrupoSeleccionados").html(htmlSession);
    if (formulas !== null && formulas.length !== 0) {
        $(".activeEcuacion").removeClass("activeEcuacion");
        $(".iconEcuacion").remove();
        $lst = $("#panelGrupoSeleccionados a").last();
        $($lst).html("<i class='menu-icon iconEcuacion fa fa-check bg-primary'></i>" + $($lst).html());
        $($lst).addClass("activeEcuacion");
    }
    metodosPanelDerecho();
}

function findInFormSession($_key) {
    if (typeof $formSession.length !== "undefined") {
        for ($i = 0; $i < $formSession.length; $i++) {
            if ($formSession[$i].key === parseInt($_key)) {
                key = $_key;
                return $formSession[$i];
            }
        }
    } /*else {
     $un = 0;
     while (typeof $formSession[$un] !== " undefined") {
     if ($formSession[$un].key === parseInt($_key)) {
     key = $_key;
     return $formSession[$i];
     }
     $un++;
     }
     }*/
}
function findInFormSessionIndex($_key) {
    if (typeof $formSession.length !== "undefined") {
        for ($i = 0; $i < $formSession.length; $i++) {
            if ($formSession[$i].key === parseInt($_key)) {
                key = $_key;
                return $i;
            }
        }
    } /*else {
     $un = 0;
     while (typeof $formSession[$un] !== " undefined") {
     if ($formSession[$un].key === parseInt($_key)) {
     key = $_key;
     return $formSession[$i];
     }
     $un++;
     }
     }*/
}

function guardarVariablesEnSession() {
    $(".guardar").each(function (index) {
        for ($i = 0; $i < $form.variables.length; $i++) {
            if ($form.variables[$i].id == $(this).data("idvariable")) {
                if ($(this).is(":visible")) {
                    $form.variables[$i].valor = $(this).val();
                } else {
                    if ($(this).data("tipo") === "valor_desde") {
                        $form.variables[$i].valor_desde = $(this).val();
                    } else {
                        $form.variables[$i].valor_hasta = $(this).val();
                    }
                }
            }
        }
    });
    $formTemp = JSON.stringify($form);
    for ($i = 0; $i < $formSession.length; $i++) {
        if ($formSession[$i].key === key) {
            $formSession[$i].formula = $formTemp;
        }
    }
}

function guardarForm() {
    $formTemp = JSON.stringify($form);
    for ($i = 0; $i < $formSession.length; $i++) {
        if ($formSession[$i].key === key) {
            $formSession[$i].formula = $formTemp;
        }
    }
}
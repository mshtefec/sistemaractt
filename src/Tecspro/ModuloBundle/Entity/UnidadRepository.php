<?php

namespace Tecspro\ModuloBundle\Entity;

use Doctrine\ORM\EntityRepository;

class UnidadRepository extends EntityRepository {

    public function findByNombre($term) {

        return $this->createQueryBuilder('n')
                        ->select('n')
                        ->Join('n.translations','t')
                        ->where('t.nombre LIKE :texto')
                        ->setParameter('texto', '%' . $term. '%')
                        ->addOrderBy('t.nombre', 'asc')
                        ->getQuery()
                        ->getResult()
        ;
    }

}

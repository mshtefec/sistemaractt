<?php

namespace Tecspro\VehiculoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;

/**
 * AutoFilterType filtro.
 * @author Nombre Apellido <name@gmail.com>
 */
class AutoFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('categorizacion', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('trocha', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('marca', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('modelo', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('anio', 'filter_number_range', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('cilindrada', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('largo', 'filter_number_range', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('ancho', 'filter_number_range', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('alto', 'filter_number_range', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('distanciaEntreEjes', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('pesoVacio', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('comentario', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
        ;

        $listener = function(FormEvent $event)
        {
            // Is data empty?
            foreach ((array)$event->getForm()->getData() as $data) {
                if ( is_array($data)) {
                    foreach ($data as $subData) {
                        if (!empty($subData)) {
                            return;
                        }
                    }
                } else {
                    if (!empty($data)) {
                        return;
                    }    
                }
            }
            $event->getForm()->addError(new FormError('Filter empty'));
        };
        $builder->addEventListener(FormEvents::POST_SUBMIT, $listener);
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Tecspro\VehiculoBundle\Entity\Auto'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'tecspro_vehiculobundle_autofiltertype';
    }
}

<?php

namespace Tecspro\ARCBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * VehiculoType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class VehiculoType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nombre', null, array(
                    'label' => 'VehiculoARC.Nombre',
                    'translation_domain' => 'TecsproARCBundle',
                    'attr' => array(
                        'class' => 'txtNombreArc'
                    )
                        )
                )
                ->add('peaton', null, array(
                    'label' => 'VehiculoARC.Peaton',
                    'translation_domain' => 'TecsproARCBundle',
                    'attr' => array(
                        'class' => 'txtPatonArc'
                    ))
                )
                ->add('movimientoPrevioALaColision', 'choice', array(
                    'attr' => array(
                        'class' => 'txtMovimientoPrevioALaColisionArc'
                    ),
                    'choices' => array(
                        'MRU' => 'MRU',
                        'MRU+MRV' => 'MRU+MRV',
                    ),
                    'label' => 'VehiculoARC.Movimiento previo a la colision',
                    'translation_domain' => 'TecsproARCBundle',
                        )
                )
                ->add('velocidadInicial', null, array(
                    'label' => 'VehiculoARC.Velocidad inicial',
                    'translation_domain' => 'TecsproARCBundle',
                    'attr' => array(
                        'class' => 'txtVelocidadInicialArc'
                    ),
                        )
                )
                ->add('unidadVelocidadInicial', 'choice', array(
                    'attr' => array(
                        'class' => 'txtUnidadVelocidadInicialArc'
                    ),
                    'choices' => array(
                        "m/s" => "m/s",
                        "Km/h" => "Km/h",
                    ),
                    'label' => 'VehiculoARC.Unidad velocidad inicial',
                    'translation_domain' => 'TecsproARCBundle',
                        )
                )
                ->add('velocidadImpacto', null, array(//
                    'label' => 'VehiculoARC.Velocidad impacto',
                    'translation_domain' => 'TecsproARCBundle',
                    'attr' => array(
                        'class' => 'txtVelocidadImpactoArc'
                    ),
                        )
                )
                ->add('unidadVelocidadImpacto', 'choice', array(//
                    'choices' => array(
                        "m/s" => "m/s",
                        "Km/h" => "Km/h",
                    ),
                    'label' => 'VehiculoARC.Unidad velocidad impacto',
                    'translation_domain' => 'TecsproARCBundle',
                    'attr' => array(
                        'class' => 'txtUnidadVelocidadImpactoArc'
                    ),
                        )
                )
                ->add('tiempoReaccion', null, array(//
                    'label' => 'VehiculoARC.Tiempo reaccion',
                    'translation_domain' => 'TecsproARCBundle',
                    'attr' => array(
                        'class' => 'txtTiempoReaccionArc'
                    ),
                        )
                )
                ->add('desaceleracion', null, array(//
                    'attr' => array(
                        'class' => 'txtDesaceleracionArc'
                    ),
                    'label' => 'VehiculoARC.Desaceleracion',
                    'translation_domain' => 'TecsproARCBundle',
                        )
                )
                ->add('tiempoPreimpactoAConsiderar', null, array(//
                    // 'disabled' => true,
                    'label' => 'VehiculoARC.Tiempo pre-impacto a considerar',
                    'translation_domain' => 'TecsproARCBundle',
                     'attr' => array(
                        'class' => 'txtTiempoPreimpactoAConsiderarArc'
                    ),
                        )
                )
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Tecspro\ARCBundle\Entity\Vehiculo'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'tecspro_arcbundle_vehiculo';
    }

}

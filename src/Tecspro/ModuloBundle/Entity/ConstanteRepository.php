<?php

namespace Tecspro\ModuloBundle\Entity;

use Doctrine\ORM\EntityRepository;

class ConstanteRepository extends EntityRepository {

    public function findByNombre($term) {

        return $this->createQueryBuilder('n')
                        ->select('n')
                        ->join('n.translations', 't')
                        ->where('t.nombre LIKE :texto')
                        ->setParameter('texto', '%' . $term. '%')
                        ->addOrderBy('t.nombre', 'asc')
                        ->getQuery()
                        ->getResult()
        ;
    }

}

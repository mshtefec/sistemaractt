function renderFormula($formula, json, $orden) {
    $arrayKey = [];
    key = 1;
    $(".btnEcuacionSeleccionada").each(function (index) {
        $arrayKey.push($(this).data('key'));
    });
    if ($arrayKey.length > 0)
        key = Math.max.apply(null, $arrayKey) + 1;


    $formula = {formula: json, idFormula: $formula, key: key, nombre: json.translations, imprimir: json.imprimir, orden: $orden};

    $formSession.push($formula);
    renderPanelVariable(key, '', '', json, "hbPanelVariable");
    renderPanelDerecho($formSession);
    //($formSession);
    metodosGenerales();
    return key;
}

function metodosGenerales() {
    $(".btnImprimirCheck").click(function () {
        $.ajax({
            // la URL para la petición
            url: Routing.generate('Formula_boton_imprimir'),
            // la información a enviar
            // (también es posible utilizar una cadena de datos)
            data: {estado: $(this).siblings('input').is(":checked"), key: key},
            // especifica si será una petición POST o GET
            type: 'GET',
            // el tipo de información que se espera de respuesta
            dataType: 'json',
            // código a ejecutar si la petición es satisfactoria;
            // la respuesta es pasada como argumento a la función
            success: function (json) {
                if (json.resultado === true) {
                    $index = findInFormSessionIndex(key);
                    $formSession[$index].imprimir = json.valor;
                    if (json.valor === true) {
                        $(".iconImprimirli" + parseInt(key)).removeClass("fa-square-o");
                        $(".iconImprimirli" + parseInt(key)).addClass("fa-check-square-o");
                    } else {
                        $(".iconImprimirli" + parseInt(key)).removeClass("fa-check-square-o");
                        $(".iconImprimirli" + parseInt(key)).addClass("fa-square-o");
                    }
                    $form.imprimir = json.valor;
                    guardarForm();
                }
            },
            // código a ejecutar si la petición falla;
            // son pasados como argumentos a la función
            // el objeto de la petición en crudo y código de estatus de la petición
            error: function (xhr, status) {
                mostrarNotifiacion("Alerta", "Disculpe, existió un problema", "error");
                return 0;
            },
        });
    });
}





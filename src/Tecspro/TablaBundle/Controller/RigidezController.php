<?php

namespace Tecspro\TablaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Tecspro\TablaBundle\Entity\Rigidez;
use Tecspro\TablaBundle\Form\RigidezType;
use Tecspro\TablaBundle\Form\RigidezFilterType;

/**
 * Rigidez controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/rigidez")
 */
class RigidezController extends Controller
{
    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Tecspro/TablaBundle/Resources/config/Rigidez.yml',
    );

    /**
     * Lists all Rigidez entities.
     *
     * @Route("/", name="admin_rigidez")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->config['filterType'] = new RigidezFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Rigidez entity.
     *
     * @Route("/", name="admin_rigidez_create")
     * @Method("POST")
     * @Template("TecsproTablaBundle:Rigidez:new.html.twig")
     */
    public function createAction()
    {
        $this->config['newType'] = new RigidezType();
        $response = parent::createAction();

        return $response;
    }

    /**
     * Displays a form to create a new Rigidez entity.
     *
     * @Route("/new", name="admin_rigidez_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->config['newType'] = new RigidezType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Rigidez entity.
     *
     * @Route("/{id}", name="admin_rigidez_show", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Rigidez entity.
     *
     * @Route("/{id}/edit", name="admin_rigidez_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->config['editType'] = new RigidezType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Rigidez entity.
     *
     * @Route("/{id}", name="admin_rigidez_update")
     * @Method("PUT")
     * @Template("TecsproTablaBundle:Rigidez:edit.html.twig")
     */
    public function updateAction($id)
    {
        $this->config['editType'] = new RigidezType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a Rigidez entity.
     *
     * @Route("/{id}", name="admin_rigidez_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter Rigidez.
     *
     * @Route("/exporter/{format}", name="admin_rigidez_export")
     */
    public function getExporter($format)
    {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Datatable Rigidez.
     *
     * @Route("/get-table/", name="admin_rigidez_table")
     */
    public function getDatatable()
    {
        $response = parent::getTable();

        return $response;
    }
 
}
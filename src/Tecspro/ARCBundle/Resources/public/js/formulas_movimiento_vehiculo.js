var circulando = 0;
var desacelerando = 1;
var colicionado = 2;

function calcular_velocidad() {

    var cantidad_vehiculos = jQuery('#tecspro_arcbundle_arccampos_cantidadVehiculos').val();
    var tipo_movimiento_previo = $(".txtMovimientoPrevioALaColisionArc")[0].value;//jQuery('#tecspro_arcbundle_arccampos_vehiculos_undefined_movimientoPrevioALaColision').val();
    //if (typeof tipo_movimiento_previo === "undefined")
    //    tipo_movimiento_previo = jQuery('#tecspro_arcbundle_arccampos_vehiculos_0_movimientoPrevioALaColision').val();


    var tablas;

    if (cantidad_vehiculos == 1) {

        if (tipo_movimiento_previo == 'MRU') {

            tabla = calcular_mru_vehiculo_1();
        } else {


            tabla = calcular_mrv_vehiculo_1();
        }
    } else {

        var tipo_movimiento_previo2 = $(".txtMovimientoPrevioALaColisionArc")[1].value;
        if (tipo_movimiento_previo == 'MRU') {

            if (tipo_movimiento_previo2 == 'MRU') {

                tabla = calcular_mru_y_mru_vehiculo_2();
            } else {

                tabla = calcular_mru_y_mrv_vehiculo_2();
            }

        } else {

            if (tipo_movimiento_previo2 == 'MRU') {

                tabla = calcular_mrv_y_mru_vehiculo_2();
            } else {

                tabla = calcular_mrv_y_mrv_vehiculo_2();
            }
        }
    }

    if (cantidad_vehiculos == 1) {

        var html = $templateARC(tabla);
        $("#tabla_resultados").html(html);
    } else {

        var html = $templateARC_2(tabla);
        $("#tabla_resultados").html(html);
    }
}

function calcular_mru_vehiculo_1() {

    var tiempo_preimpacto = $(".txtTiempoPreimpactoAConsiderarArc")[0].value;//jQuery('#tecspro_arcbundle_arccampos_vehiculos_undefined_tiempoPreimpactoAConsiderar').val();

    var unidad_velocidad_inicial = $(".txtUnidadVelocidadInicialArc")[0].value;


    var velocidad_inicial = $(".txtVelocidadInicialArc")[0].value;

    var fraccion_de_tiempo = jQuery('#tecspro_arcbundle_arccampos_fraccionDeTiempoAAnalizar').val();

    var unidad_velocidad = 1;
    var unidad_posicion = 1;

    var intervalo = parseFloat(fraccion_de_tiempo);
    var tiempo_final = parseFloat(tiempo_preimpacto);

    if (unidad_velocidad_inicial !== "m/s") {

        unidad_velocidad = 1 / 3.6;
        unidad_posicion = 3.6;
    }

    var velocidad_con_unidad = unidad_velocidad * velocidad_inicial;
    var velocidad_resultante = velocidad_con_unidad * 3.6;
    var momento = 0;
    var posicion = 0;
    var tiempo_actual = 0.0;

    var tabla = new Array([]);

    while (tiempo_actual <= tiempo_final) {

        posicion = (tiempo_actual * velocidad_inicial) / unidad_posicion;

        tabla[momento] = [];

        if ((posicion == 0) && (tiempo_actual == 0)) {

            tabla[momento] = {
                tiempo: 0,
                posicion: 0,
                velocidad: velocidad_resultante,
                estado: colicionado
            };
        } else {

            tabla[momento] = {
                tiempo: (parseFloat(tiempo_actual)).toFixed(2),
                posicion: (parseFloat(posicion)).toFixed(2),
                velocidad: (parseFloat(velocidad_resultante)).toFixed(2),
                estado: circulando
            };
        }

        tiempo_actual = tiempo_actual + intervalo;

        momento++;
    }
    return tabla;
}

function calcular_mrv_vehiculo_1() {

    var fraccion_de_tiempo = parseFloat(jQuery('#tecspro_arcbundle_arccampos_fraccionDeTiempoAAnalizar').val());
    var desaceleracion = parseFloat($(".txtDesaceleracionArc")[0].value);

    var tiempo_de_reaccion = parseFloat($(".txtTiempoReaccionArc")[0].value);

    var velocidad_inicial = parseFloat($(".txtVelocidadInicialArc")[0].value);

    var velocidad_impacto = parseFloat($(".txtVelocidadImpactoArc")[0].value);
    var unidad_de_inicio = $(".txtUnidadVelocidadInicialArc")[0].value;
    var unidad_de_impacto = $(".txtUnidadVelocidadImpactoArc")[0].value;

    var KMS = 1 / 3.6;

    var velocidad_inicial_unidad = unidad_de_inicio == 'm/s' ?
            (parseFloat(velocidad_inicial)) : (parseFloat(velocidad_inicial) * KMS);

    var velocidad_impacto_unidad = unidad_de_impacto == 'm/s' ?
            (parseFloat(velocidad_impacto)) : (parseFloat(velocidad_impacto) * KMS);

    var tabla = [];

    if (velocidad_inicial_unidad < velocidad_impacto_unidad) {

        alert('La velocidad de impacto no puede ser mayor a la velocidad inicial.');
    } else {

        var friccion = parseFloat((9.81 * desaceleracion)).toFixed(2);
        var tiempo_final = parseFloat(((velocidad_inicial_unidad - velocidad_impacto_unidad) / friccion).toFixed(2));
        var tiempo_total = tiempo_final;

        if (tiempo_de_reaccion !== '') {

            tiempo_total = tiempo_final + parseFloat(tiempo_de_reaccion.toFixed(2));
        }

        var tiempo = 0.0;
        var flag = true;
        var momento = 0;

        while (tiempo <= tiempo_total) {

            if (flag && (tiempo > tiempo_final) && (tiempo_final > 0)) {

                flag = false;

                var registro = mrv(tiempo_final, velocidad_inicial_unidad, velocidad_impacto_unidad, friccion, tiempo_final);

                tabla[momento] = {
                    tiempo: (parseFloat(tiempo_final)).toFixed(2),
                    posicion: (parseFloat(registro[0])).toFixed(2),
                    velocidad: (parseFloat(registro[1])).toFixed(2),
                    estado: registro[2]
                }
                momento++;
            }

            var registro = mrv(tiempo, velocidad_inicial_unidad, velocidad_impacto_unidad, friccion, tiempo_final);

            if (registro[2] == 'Colisionado') {

                registro[1] = parseFloat(registro[1]).toFixed(2);
            }

            tabla[momento] = {
                tiempo: (parseFloat(tiempo)).toFixed(2),
                posicion: (parseFloat(registro[0])).toFixed(2),
                velocidad: (parseFloat(registro[1])).toFixed(2),
                estado: registro[2]
            }
            momento++;

            tiempo = tiempo + fraccion_de_tiempo;
        }
    }

    return tabla;
}

function calcular_mru_y_mru_vehiculo_2() {

    var tiempo_preimpacto_1 = $(".txtTiempoPreimpactoAConsiderarArc")[0].value;
    var velocidad_inicial_1 = $(".txtVelocidadInicialArc")[0].value;
    var unidad_velocidad_inicial_1 = $(".txtUnidadVelocidadInicialArc")[0].value;
    var tiempo_preimpacto_2 = $(".txtTiempoPreimpactoAConsiderarArc")[1].value;
    var velocidad_inicial_2 = $(".txtVelocidadInicialArc")[1].value;
    var unidad_velocidad_inicial_2 = $(".txtUnidadVelocidadInicialArc")[1].value;
    var fraccion_de_tiempo = jQuery('#tecspro_arcbundle_arccampos_fraccionDeTiempoAAnalizar').val();


    var tiempo_impacto_2 = 0.0;
    var tiempo_impacto_1 = 0.0;

    if (velocidad_inicial_1 == '') {

        alert("La velocidad inicial para el vehiculo 1, es obligatoria para el calculo.");
    } else if (velocidad_inicial_2 == '') {

        alert("La velocidad inicial para el vehiculo 2, es obligatoria para el calculo.");
    } else {

        if (tiempo_preimpacto_1 !== '') {

            tiempo_impacto_1 = tiempo_preimpacto_1;
        }

        if (tiempo_preimpacto_2 !== '') {

            tiempo_impacto_2 = tiempo_preimpacto_2;
        }
        var KMS = 1 / 3.6;

        var tiempo_actual = 0.0;
        var momento = 0;

        var velocidad_vehiculo_1 = unidad_velocidad_inicial_1 == 'm/s' ?
                (parseFloat(velocidad_inicial_1)) : (parseFloat(velocidad_inicial_1) * KMS);

        var velocidad_vehiculo_2 = unidad_velocidad_inicial_2 == 'm/s' ?
                (parseFloat(velocidad_inicial_2)) : (parseFloat(velocidad_inicial_2) * KMS);

        var intervalo = parseFloat(fraccion_de_tiempo);

        if (tiempo_impacto_1 < tiempo_impacto_2) {

            tiempo_impacto_1 = tiempo_impacto_2;
        }

        var tabla_resultados = [];

        var valores_movimiento_rectilineo_uniforme = Array();
        var valores_movimiento_rectilineo_variado = Array();

        while (tiempo_actual <= tiempo_impacto_1) {

            var valores_movimiento_rectilineo_uniforme = mru(tiempo_actual, velocidad_vehiculo_1, 0.0);
            var valores_movimiento_rectilineo_variado = mru(tiempo_actual, velocidad_vehiculo_2, 0.0);

            tabla_resultados[momento] = {
                tiempo: (parseFloat(tiempo_actual)).toFixed(2),
                posicion_1: (parseFloat(valores_movimiento_rectilineo_uniforme[0])).toFixed(2),
                velocidad_1: (parseFloat(valores_movimiento_rectilineo_uniforme[1])).toFixed(2),
                estado_1: valores_movimiento_rectilineo_uniforme[2],
                posicion_2: (parseFloat(valores_movimiento_rectilineo_variado[0])).toFixed(2),
                velocidad_2: (parseFloat(valores_movimiento_rectilineo_variado[1])).toFixed(2),
                estado_2: valores_movimiento_rectilineo_variado[2]
            };
            momento++;
            tiempo_actual += intervalo;
        }
    }

    return tabla_resultados;
}

function calcular_mru_y_mrv_vehiculo_2() {

    var unidad_velocidad_inicial_1 = $(".txtUnidadVelocidadInicialArc")[0].value;

    var velocidad_inicial_1 = parseFloat($(".txtVelocidadInicialArc")[0].value);

    var tiempo_preimpacto_1 = parseFloat($(".txtTiempoPreimpactoAConsiderarArc")[0].value);
    var unidad_velocidad_inicial_2 = $(".txtUnidadVelocidadInicialArc")[1].value;
    var unidad_velocidad_impacto_2 = $(".txtUnidadVelocidadImpactoArc")[1].value;
    var velocidad_inicial_2 = parseFloat($(".txtVelocidadInicialArc")[1].value);
    var velocidad_impacto_2 = parseFloat($(".txtVelocidadImpactoArc")[1].value);
    var desaceleracion_2 = parseFloat($(".txtDesaceleracionArc")[1].value);
    var tiempo_de_reaccion_2 = parseFloat($(".txtTiempoReaccionArc")[1].value);

    var fraccion_de_tiempo = jQuery('#tecspro_arcbundle_arccampos_fraccionDeTiempoAAnalizar').val();
    var intervalo = parseFloat(fraccion_de_tiempo);

    if (velocidad_inicial_1 == '') {

        alert("La velocidad incial para el vehiculo 1, es obligatoria.");
    } else if (velocidad_inicial_2 == '') {

        alert("La velocidad incial para el vehiculo 2, es obligatoria.");
    } else {

        var tiempo_actual = 0;
        var momento = 0;

        var KMS = 1 / 3.6;

        var velocidad_inicial_vehiculo_1 = unidad_velocidad_inicial_1 == 'm/s' ?
                (parseFloat(velocidad_inicial_1)) : (parseFloat(velocidad_inicial_1) * KMS);

        var velocidad_inicial_vehiculo_2 = unidad_velocidad_inicial_2 == 'm/s' ?
                (parseFloat(velocidad_inicial_2)) : (parseFloat(velocidad_inicial_2) * KMS);

        var velocidad_impacto_vehiculo_2 = unidad_velocidad_impacto_2 == 'm/s' ?
                (parseFloat(velocidad_impacto_2)) : (parseFloat(velocidad_impacto_2) * KMS);

        if (velocidad_inicial_vehiculo_2 < velocidad_impacto_vehiculo_2) {

            alert("La velocidad de impacto no puede ser mayor a la velocidad inicial.");
        } else {

            var friccion = 9.81 * desaceleracion_2;
            var tiempo_total = 0.0;

            if (tiempo_preimpacto_1 !== '') {

                tiempo_total = tiempo_preimpacto_1;
            }

            var tiempo_final = (velocidad_inicial_vehiculo_2 - velocidad_impacto_vehiculo_2) / friccion;

            var tiempo_impacto;

            if (tiempo_total > tiempo_final) {

                tiempo_impacto = tiempo_total;
            } else {

                tiempo_impacto = tiempo_final;
                if (tiempo_de_reaccion_2 !== '') {

                    tiempo_impacto = tiempo_final + tiempo_de_reaccion_2;
                }
            }


            var momento_desaceleracion = true;
            var momento_colision = true;

            var tabla_resultados = [];

            while (tiempo_actual <= tiempo_impacto) {

                var resultados_vehiculo_1 = Array();
                var resultados_vehiculo_2 = Array();

                if (momento_colision && (tiempo_actual > tiempo_total) && (tiempo_total > 0.0)) {

                    momento_colision = false;
                    resultados_vehiculo_1 = mru(tiempo_total, velocidad_inicial_vehiculo_1, 0.0);
                    resultados_vehiculo_2 = mrv(tiempo_total, velocidad_inicial_vehiculo_2, friccion, tiempo_final);

                    tabla_resultados[momento] = {
                        tiempo: (parseFloat(tiempo_total)).toFixed(2),
                        posicion_1: (parseFloat(resultados_vehiculo_1[0])).toFixed(2),
                        velocidad_1: (parseFloat(resultados_vehiculo_1[1])).toFixed(2),
                        estado_1: resultados_vehiculo_1[2],
                        posicion_2: (parseFloat(resultados_vehiculo_2[0])).toFixed(2),
                        velocidad_2: (parseFloat(resultados_vehiculo_2[1])).toFixed(2),
                        estado_2: resultados_vehiculo_2[2]
                    };
                    momento++;
                }

                if (momento_desaceleracion && (tiempo_actual > tiempo_final) && (tiempo_final > 0.0)) {

                    momento_desaceleracion = false;
                    resultados_vehiculo_1 = mru(tiempo_final, velocidad_inicial_vehiculo_1, 0.0);

                    resultados_vehiculo_2 = mrv(
                            tiempo_final,
                            velocidad_inicial_vehiculo_2,
                            velocidad_impacto_vehiculo_2,
                            friccion,
                            tiempo_final
                            );

                    tabla_resultados[momento] = {
                        tiempo: (parseFloat(tiempo_final)).toFixed(2),
                        posicion_1: (parseFloat(resultados_vehiculo_1[0])).toFixed(2),
                        velocidad_1: (parseFloat(resultados_vehiculo_1[1])).toFixed(2),
                        estado_1: resultados_vehiculo_1[2],
                        posicion_2: (parseFloat(resultados_vehiculo_2[0])).toFixed(2),
                        velocidad_2: (parseFloat(resultados_vehiculo_2[1])).toFixed(2),
                        estado_2: resultados_vehiculo_2[2]
                    };
                    momento++;
                }

                resultados_vehiculo_1 = mru(tiempo_actual, velocidad_inicial_vehiculo_1, 0.0);

                resultados_vehiculo_2 = mrv(
                        tiempo_actual,
                        velocidad_inicial_vehiculo_2,
                        velocidad_impacto_vehiculo_2,
                        friccion,
                        tiempo_final
                        );

                tabla_resultados[momento] = {
                    tiempo: (parseFloat(tiempo_actual)).toFixed(2),
                    posicion_1: (parseFloat(resultados_vehiculo_1[0])).toFixed(2),
                    velocidad_1: (parseFloat(resultados_vehiculo_1[1])).toFixed(2),
                    estado_1: resultados_vehiculo_1[2],
                    posicion_2: (parseFloat(resultados_vehiculo_2[0])).toFixed(2),
                    velocidad_2: (parseFloat(resultados_vehiculo_2[1])).toFixed(2),
                    estado_2: resultados_vehiculo_2[2]
                };

                momento++;
                tiempo_actual += intervalo;
            }
        }
    }

    return tabla_resultados;
}

function calcular_mrv_y_mru_vehiculo_2() {

    // Datos del vehiculo con MRV.
    var velocidad_inicial_1 = parseFloat($(".txtVelocidadInicialArc")[0].value);
    var unidad_velocidad_inicial_1 = $(".txtUnidadVelocidadInicialArc")[0].value;
    var velocidad_impacto_1 = parseFloat($(".txtVelocidadImpactoArc")[0].value);
    var unidad_velocidad_impacto_1 = $(".txtUnidadVelocidadImpactoArc")[0].value;

    var tiempo_preimpacto_1 = parseFloat($(".txtTiempoPreimpactoAConsiderarArc")[0].value);
    var tiempo_de_reaccion_1 = parseFloat($(".txtTiempoReaccionArc")[0].value);
    var desaceleracion_1 = parseFloat($(".txtDesaceleracionArc")[0].value);
    // Datos del vehiculo con MRU.
    var velocidad_inicial_2 = parseFloat($(".txtVelocidadInicialArc")[1].value);
    var unidad_velocidad_inicial_2 = $(".txtUnidadVelocidadInicialArc")[1].value;

    var tiempo_preimpacto_2 = parseFloat($(".txtTiempoPreimpactoAConsiderarArc")[1].value);
    // Fraccion de tiempo.
    var fraccion_de_tiempo = jQuery('#tecspro_arcbundle_arccampos_fraccionDeTiempoAAnalizar').val();
    var intervalo = parseFloat(fraccion_de_tiempo);

    //if (isNaN(tiempo_de_reaccion_2))
    //    velocidad_inicial_1 = parseFloat(jQuery('#tecspro_arcbundle_arccampos_vehiculos_undefined_velocidadInicial').val());

    var tabla = [];

    if (velocidad_inicial_1 == '') {

        alert("La velocidad incial para el vehiculo 1, es obligatoria.");
    } else if (velocidad_inicial_2 == '') {

        alert("La velocidad incial para el vehiculo 2, es obligatoria.");
    } else {

        var KMS = 1 / 3.6;

        var tiempo_actual = 0.0; //tiempo_actual
        var momento = 0; //momento

        var velocidad_inicial_vehiculo_1 = unidad_velocidad_inicial_1 == 'm/s' ? //velocidad_inicial_vehiculo_1
                (parseFloat(velocidad_inicial_1)) : (parseFloat(velocidad_inicial_1) * KMS);

        var velocidad_inicial_vehiculo_2 = unidad_velocidad_inicial_2 == 'm/s' ? //velocidad_inicial_vehiculo_2
                (parseFloat(velocidad_inicial_2)) : (parseFloat(velocidad_inicial_2) * KMS);

        var velocidad_impacto_vehiculo_2 = unidad_velocidad_impacto_1 == 'm/s' ? //velocidad_impacto_vehiculo_2
                (parseFloat(velocidad_impacto_1)) : (parseFloat(velocidad_impacto_1) * KMS);

        var friccion = 9.81 * desaceleracion_1; //friccion
        var tiempo_total = tiempo_de_reaccion_1; // tiempo_total

        if (velocidad_inicial_vehiculo_1 < velocidad_impacto_vehiculo_2) {

            alert('La veocidad de impacto no puede ser mayor a la velocidad inicial.');
        } else if (tiempo_preimpacto_2 !== '') {

            tiempo_total = tiempo_preimpacto_2;
        }

        var tiempo_final = parseFloat((velocidad_inicial_vehiculo_1 - velocidad_impacto_vehiculo_2) / friccion).toFixed(2); //tiempo_final
        var tiempo_impacto; //tiempo_impacto

        if (tiempo_final > tiempo_total) {

            tiempo_impacto = tiempo_total;
            if (tiempo_de_reaccion_1) {

                tiempo_impacto = parseFloat(tiempo_final + tiempo_de_reaccion_1).toFixed(2);
            }
        } else {

            tiempo_impacto = tiempo_total;
        }

        var colision_vehiculo_2 = true; //colision_vehiculo_2
        var desaceleracion_vehiculo_2 = true; //desaceleracion_vehiculo_2

        var tabla_resultados = [];

        while (tiempo_actual <= tiempo_impacto) {

            var resultados_vehiculo_1;
            var resultados_vehiculo_2;

            if (colision_vehiculo_2 && (tiempo_actual > tiempo_final) && (tiempo_final > 0.0)) {

                colision_vehiculo_2 = false;
                resultados_vehiculo_1 = mrv(tiempo_final, velocidad_inicial_vehiculo_1, velocidad_impacto_vehiculo_2, friccion, tiempo_final);
                resultados_vehiculo_2 = mru(tiempo_final, velocidad_inicial_vehiculo_2, 0.0);

                tabla_resultados[momento] = {
                    tiempo: (parseFloat(tiempo_final)).toFixed(2),
                    posicion_1: (parseFloat(resultados_vehiculo_1[0])).toFixed(2),
                    velocidad_1: (parseFloat(resultados_vehiculo_1[1])).toFixed(2),
                    estado_1: resultados_vehiculo_1[2],
                    posicion_2: (parseFloat(resultados_vehiculo_2[0])).toFixed(2),
                    velocidad_2: (parseFloat(resultados_vehiculo_2[1])).toFixed(2),
                    estado_2: resultados_vehiculo_2[2]
                };
                momento++;
            }

            if (desaceleracion_vehiculo_2 && (tiempo_actual > tiempo_total) && (tiempo_total > 0.0)) {

                desaceleracion_vehiculo_2 = false;
                resultados_vehiculo_1 = mrv(tiempo_total, velocidad_inicial_vehiculo_1, velocidad_impacto_vehiculo_2, friccion, tiempo_final);
                resultados_vehiculo_2 = mru(tiempo_total, velocidad_inicial_vehiculo_2, 0.0);
                tabla_resultados[momento] = {
                    tiempo: (parseFloat(tiempo_total)).toFixed(2),
                    posicion_1: (parseFloat(resultados_vehiculo_1[0])).toFixed(2),
                    velocidad_1: (parseFloat(resultados_vehiculo_1[1])).toFixed(2),
                    estado_1: resultados_vehiculo_1[2],
                    posicion_2: (parseFloat(resultados_vehiculo_2[0])).toFixed(2),
                    velocidad_2: (parseFloat(resultados_vehiculo_2[1])).toFixed(2),
                    estado_2: resultados_vehiculo_2[2]
                }
                momento++;
            }

            resultados_vehiculo_1 = mrv(tiempo_actual, velocidad_inicial_vehiculo_1, velocidad_impacto_vehiculo_2, friccion, tiempo_final);
            resultados_vehiculo_2 = mru(tiempo_actual, velocidad_inicial_vehiculo_2, 0.0);

            tabla_resultados[momento] = {
                tiempo: (parseFloat(tiempo_actual)).toFixed(2),
                posicion_1: (parseFloat(resultados_vehiculo_1[0])).toFixed(2),
                velocidad_1: (parseFloat(resultados_vehiculo_1[1])).toFixed(2),
                estado_1: resultados_vehiculo_1[2],
                posicion_2: (parseFloat(resultados_vehiculo_2[0])).toFixed(2),
                velocidad_2: (parseFloat(resultados_vehiculo_2[1])).toFixed(2),
                estado_2: resultados_vehiculo_2[2]
            }
            momento++;
            tiempo_actual += intervalo;
        }
    }

    return tabla_resultados;
}

function calcular_mrv_y_mrv_vehiculo_2() {


// Datos del vehiculo_1 con MRV.
    var velocidad_inicial_1 = parseFloat($(".txtVelocidadInicialArc")[0].value);
    var unidad_velocidad_inicial_1 = $(".txtUnidadVelocidadInicialArc")[0].value;
    var velocidad_impacto_1 = parseFloat($(".txtVelocidadImpactoArc")[0].value);
    var unidad_velocidad_impacto_1 = $(".txtUnidadVelocidadImpactoArc")[0].value;
    var tiempo_preimpacto_1 = parseFloat($(".txtTiempoPreimpactoAConsiderarArc")[0].value);
    var tiempo_de_reaccion_1 = parseFloat($(".txtTiempoReaccionArc")[0].value);
    var desaceleracion_1 = parseFloat($(".txtDesaceleracionArc")[0].value);
// Datos del vehiculo_2 con MRV.
    var velocidad_inicial_2 = parseFloat($(".txtVelocidadInicialArc")[1].value);
    var unidad_velocidad_inicial_2 = $(".txtUnidadVelocidadInicialArc")[1].value;
    var velocidad_impacto_2 = parseFloat($(".txtVelocidadImpactoArc")[1].value);
    var unidad_velocidad_impacto_2 = $(".txtUnidadVelocidadImpactoArc")[1].value;
    var tiempo_preimpacto_2 = parseFloat($(".txtTiempoPreimpactoAConsiderarArc")[1].value);
    var tiempo_de_reaccion_2 = parseFloat($(".txtTiempoReaccionArc")[1].value);
    var desaceleracion_2 = parseFloat($(".txtDesaceleracionArc")[1].value);
// Friccion.
    var fraccion_de_tiempo = jQuery('#tecspro_arcbundle_arccampos_fraccionDeTiempoAAnalizar').val();
    var intervalo = parseFloat(fraccion_de_tiempo);

    var tabla = [];

    if (desaceleracion_1 == 0 || velocidad_impacto_1 == 0.0) {

        alert("La desaceleracion y la velocidad de impacto del vehiculo 1 son obligatorios");
    } else if (desaceleracion_2 == 0 || velocidad_impacto_2 == 0.0) {

        alert("La desaceleracion y la velocidad de impacto del vehiculo 2 son obligatorios");
    } else {

        var KMS = 1 / 3.6;

        var num = 0.0;

        var num2 = 0;

        var num3 = unidad_velocidad_inicial_1 == 'm/s' ?
                (parseFloat(velocidad_inicial_1)) : (parseFloat(velocidad_inicial_1) * KMS);

        var num4 = unidad_velocidad_inicial_2 == 'm/s' ?
                (parseFloat(velocidad_inicial_2)) : (parseFloat(velocidad_inicial_2) * KMS);

        var num5 = unidad_velocidad_impacto_1 == 'm/s' ?
                (parseFloat(velocidad_impacto_1)) : (parseFloat(velocidad_impacto_1) * KMS);

        var num6 = unidad_velocidad_impacto_2 == 'm/s' ?
                (parseFloat(velocidad_impacto_2)) : (parseFloat(velocidad_impacto_2) * KMS);


        if (num4 < num6) {

            alert("La velocidad de impacto del vehiculo 1 no puede ser mayor a la velocidad inicial del mismo.");
        } else if (num3 < num5) {

            alert("La velocidad de impacto del vehiculo 2 no puede ser mayor a la velocidad inicial del mismo.");
        } else {

            var num7 = 9.81 * desaceleracion_1;
            var num8 = 9.81 * desaceleracion_2;
            var num9 = parseFloat(((num3 - num5) / num7).toFixed(2));
            var num10 = parseFloat(((num4 - num6) / num8).toFixed(2));
            var num11 = 0;


            if (num9 > num10) {

                num11 = num9;
                if (tiempo_de_reaccion_1 !== 0.0) {

                    num11 = parseFloat(num9 + tiempo_de_reaccion_1);
                }
            } else {

                num11 = num10;
                if (tiempo_de_reaccion_2 !== 0.0) {
                    num11 = parseFloat(num10 + tiempo_de_reaccion_2);
                }
            }

            var flag = true;
            var flag2 = true;

            while (num <= num11) {
                var array;
                var array2;

                if (flag && (num > num9) && (num9 > 0.0)) {

                    flag = false;
                    array = mrv(num9, num3, num5, num9);
                    array2 = mrv(num9, num4, num6, num8, num10);
                    tabla[num2] = {
                        tiempo: (parseFloat(num9)).toFixed(2),
                        posicion_1: (parseFloat(array[0])).toFixed(2),
                        velocidad_1: (parseFloat(array[1])).toFixed(2),
                        estado_1: array[2],
                        posicion_2: (parseFloat(array2[0])).toFixed(2),
                        velocidad_2: (parseFloat(array2[1])).toFixed(2),
                        estado_2: array2[2]
                    }
                    num2++;
                }

                if (flag2 && (num > num10) && (num10 > 0.0)) {

                    flag2 = false;
                    array = mrv(num10, num3, num5, num7, num9);
                    array2 = mrv(num10, num4, num6, num8, num10);
                    tabla[num2] = {
                        tiempo: (parseFloat(num10)).toFixed(2),
                        posicion_1: (parseFloat(array[0])).toFixed(2),
                        velocidad_1: (parseFloat(array[1])).toFixed(2),
                        estado_1: array[2],
                        posicion_2: (parseFloat(array2[0])).toFixed(2),
                        velocidad_2: (parseFloat(array2[1])).toFixed(2),
                        estado_2: array2[2]
                    }
                    num2++;
                }

                array = mrv(num, num3, num5, num7, num9);
                array2 = mrv(num, num4, num6, num8, num10);
                tabla[num2] = {
                    tiempo: (parseFloat(num)).toFixed(2),
                    posicion_1: (parseFloat(array[0])).toFixed(2),
                    velocidad_1: (parseFloat(array[1])).toFixed(2),
                    estado_1: array[2],
                    posicion_2: (parseFloat(array2[0])).toFixed(2),
                    velocidad_2: (parseFloat(array2[1])).toFixed(2),
                    estado_2: array2[2]
                }
                num2++;
                num += intervalo;
            }
        }
    }

    return tabla;
}

function mru(tiempo, velocidad) {

    tabla = Array();

    var posicion = (tiempo * velocidad).toFixed(2);

    if (tiempo == 0.0 && posicion == 0) {
        tabla =
                [
                    posicion,
                    (3.6 * velocidad).toFixed(2),
                    colicionado
                ];
    } else {

        tabla =
                [
                    posicion,
                    (3.6 * velocidad).toFixed(2),
                    circulando
                ];
    }

    return tabla;
}

function mrv(tiempo, vi, vf, friccion, tiempo_final) {

    var posicion = ((tiempo * vi) - (friccion * 0.5 * tiempo * tiempo)).toFixed(2);
    var estado = '';
    var velocidad = 0;

    if (tiempo <= tiempo_final) {

        if (tiempo == 0.0 && posicion == 0.0) {

            estado = colicionado;

            velocidad = 3.6 * (vf + (friccion * tiempo)).toFixed(2);

            tabla = [
                posicion,
                velocidad,
                estado
            ];
        } else {

            estado = desacelerando;

            if (tiempo == tiempo_final) {

                velocidad = (3.6 * vi).toFixed(2);
            } else {

                velocidad = (3.6 * (vf + friccion * tiempo)).toFixed(2);
            }

            tabla = [
                posicion,
                velocidad,
                estado
            ];
        }
    } else {

        estado = circulando;

        tabla = [
            tiempo * vi,
            3.6 * vi,
            estado
        ];
    }

    return tabla;
}
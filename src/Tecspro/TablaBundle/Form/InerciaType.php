<?php

namespace Tecspro\TablaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * InerciaType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class InerciaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('translations', 'a2lix_translations', array(
                    'required' => true,
                    'fields' => array(
                        'vehiculo' => array(
                            'field_type' => 'choice',
                            'locale_options' => array(
                                'es' => array(
                                    'label' => 'Vehículos Español',
                                ),
                                'pt' => array(
                                    'label' => 'Vehículos Portugues',
                                )
                            ),
                            'choices'  => array(
                                'es' => array(
                                    'VehiculosPasajeros' => 'Vehículos de pasajeros',
                                    'Pick Ups' => 'Pick Ups',
                                    'Multipropósito' => 'Multipropósito',
                                    'Vans- Mini Vans' => 'Vans- Mini Vans',
                                ),
                                 'pt' => array(
                                    'Veículos Passagerios' => 'Veículos Passagerios',
                                    'Pick Ups' => 'Pick Ups',
                                    'Multipropósito' => 'Multipropósito',
                                    'Vans- Mini Vans' => 'Vans- Mini Vans',
                                ),
                            ),
                        ),
                        'indicador' => array(
                            'field_type' => 'choice',
                            'locale_options' => array(
                                'es' => array(
                                    'label' => 'Indicador Español',
                                ),
                                'pt' => array(
                                    'label' => 'Indicador Portugues',
                                )
                            ),
                            'choices'  => array(
                                'es' => array(
                                    'INERCIA' => 'INERCIA',
                                    'Cabeceo [kg.m2]' => 'Cabeceo [kg.m2]',
                                    'Guiñada [kg.m2]' => 'Guiñada [kg.m2]',
                                    'Rólido [kg.m2]' => 'Rólido [kg.m2]',
                                ),
                                 'pt' => array(
                                      'INÉRCIA' => 'INÉRCIA',
                                    'Pitch [kg.m2]' => 'Pitch [kg.m2]',
                                    'Yaw [kg.m2]' => 'Yaw [kg.m2]',
                                    'Roll  [kg.m2]' => 'Roll  [kg.m2]',
                                ),
                                 )
                        ),
                           'categoria' => array(
                            'field_type' => 'choice',
                            'locale_options' => array(
                                'es' => array(
                                    'label' => 'Categoria Español',
                                ),
                                'pt' => array(
                                    'label' => 'Categoria Portugues',
                                )
                            ),
                            'choices'  => array(
                                'es' => array(
                                    'Categoria 1 Batalla = 2.05-2.41 m' => 'Categoria 1 Batalla = 2.05-2.41 m',
                                    'Categoria 2 Batalla = 2.41-2.58 m' => 'Categoria 2 Batalla = 2.41-2.58 m' ,
                                    'Categoria 3 Batalla = 2.58-2.80 m' => 'Categoria 3 Batalla = 2.58-2.80 m',
                                    'Categoria 4 Batalla = 2.80-2.98 m' => 'Categoria 4 Batalla = 2.80-2.98 m',
                                    'Categoria 5 Batalla 0 mayores a 2.98 m' => 'Categoria 5 Batalla 0 mayores a 2.98 m',
                                ),
                                 'pt' => array(
                                      'Categoria 1 Distância entre eixos  = 2.05-2.41 m' => 'Categoria 1 Distância entre eixos  = 2.05-2.41 m',
                                    'Categoria 2 Distância entre eixos  = 2.41-2.58 m' => 'Categoria 2 Distância entre eixos  = 2.41-2.58 m' ,
                                    'Categoria 3 Distância entre eixos  = 2.58-2.80 m' => 'Categoria 3 Distância entre eixos  = 2.58-2.80 m',
                                    'Categoria 4 Distância entre eixos  = 2.80-2.98 m' => 'Categoria 4 Distância entre eixos  = 2.80-2.98 m',
                                    'Categoria 5 Distância entre eixos 0 maiores a 2.98' => 'Categoria 5 Distância entre eixos 0 maiores a 2.98 ',
                                ),
                                 )
                        ),
                    )
                ))
            ->add('batalla')
            ->add('unobatallaMenor2P896')
            ->add('unobatallaMenor2P642')
            ->add('unoDesviacionEstandar')
            ->add('dosbatallaMayor2P896')
            ->add('dosDesviacionEstandar')
            ->add('dosbatallaMayor2P642')
            ->add('unobatallaMenor2P931')
            ->add('dosbatallaMayor2P931')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Tecspro\TablaBundle\Entity\Inercia'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'tecspro_tablabundle_inercia';
    }
}

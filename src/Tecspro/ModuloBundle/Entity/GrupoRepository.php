<?php

namespace Tecspro\ModuloBundle\Entity;

use Doctrine\ORM\EntityRepository;

class GrupoRepository extends EntityRepository {

    public function findGrupoJoinFormula($id) {

        return $this->createQueryBuilder('a')
                        ->select('a,f,e,v,u')
                        ->leftJoin('a.formulas', 'f')
                        ->leftJoin('f.ecuacion', 'e')
                        ->leftJoin('f.variables', 'v')
                        ->leftJoin('e.unidadResultado', 'u')
                        ->where('a.id= :id')
                        ->setParameter('id', $id)
                        ->getQuery()
                        ->getOneOrNullResult()
        ;
    }

    public function findUltimoOrdenByCategoria($categoria) {


        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
                ->select('max(a.orden)+1')
                ->from('Tecspro\ModuloBundle\Entity\Grupo', 'a')
                ->leftJoin('a.categoria', 'c')
                ->where('c.id= :cat')
                ->setParameter('cat', $categoria)
        ;
        return $qb
                        ->getQuery()
                        ->getOneOrNullResult()
        ;
    }

}

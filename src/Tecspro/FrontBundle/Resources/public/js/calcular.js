function calcular() {
    variables = [];
    ecuaciones = [];
    expresiones = [];
    var autoincrementalBoolean = false;
    //obtengo todas las variables
    try {
        $(".txtVariables").each(function (index) {
            var temp = "";
            //pregunto si ahy variables autoincremental
            if ($(this).is(":visible")) {

                $form.variables[index].valor = [$(this).val()];
                var variable = {simbolo: $(this).data('simbolo'), valor: parseFloat($(this).val()), autoincrementar: false};
            } else {
                autoincrementalBoolean = true;
                if ($form.autoincremental === true) {
                    var particion = parseInt($("#txtParticion" + $(this).data("idvariable") + $(this).data("idvariableindex")).val());
                    var valorDesde = parseFloat($("#txtValorDesde" + $(this).data("idvariable") + $(this).data("idvariableindex")).val());
                    var valorHasta = parseFloat($("#txtValorHasta" + $(this).data("idvariable") + $(this).data("idvariableindex")).val());
                } else {
                    var particion = parseInt($("#txtParticion" + $(this).data("idvariable")).val());
                    var valorDesde = parseFloat($("#txtValorDesde" + $(this).data("idvariable")).val());
                    var valorHasta = parseFloat($("#txtValorHasta" + $(this).data("idvariable")).val());
                }

                var minimo = 0;
                var maximo = 0;
                var sumador;
                if (valorDesde <= valorHasta) {
                    minimo = valorDesde;
                    maximo = valorHasta;
                } else {
                    minimo = valorHasta;
                    maximo = valorDesde;
                }

                temp = "(maximo-minimo)/particion";
                temp = temp.replace("maximo", maximo);
                temp = temp.replace("minimo", minimo);
                temp = temp.replace("particion", particion - 1);
                sumador = parseFloat(math.eval(temp));
                var tabla = [];
                tabla.push(minimo);
                for ($i = 1; $i < particion - 1; $i++) {
                    tabla.push(tabla[$i - 1] + sumador);
                }
                tabla.push(maximo);
                $form.variables[index].valor = tabla;
                var variable = {simbolo: $(this).data('simbolo'), autoincrementar: true,
                    partcion: particion, minimo: minimo, maximo: maximo, tabla: tabla};
            }
            variables.push(variable);
        });
        if ($form.autoincremental === true) {
            //si es autoincremental
            var expresion = "";
            var tablaEcuacion;
            var tablaTemp;
            var tablaIncremento = [];
            var tablaFinal = [];
            for ($f = 0; $f < $form.ecuacion.length; $f++) {
                tablaEcuacion = [];
                for ($i = 0; $i < variables.length; $i++) {
                    expresion = $form.ecuacion[$f].expresion.split(";")[0];
                    if (variables[$i].autoincrementar === false) {
                        expresion = expresion.replace(variables[$i].simbolo, variables[$i].valor);
                        tablaEcuacion.push(expresion);
                    } else {

                        tablaTemp = reemplazarSimbolosPorVariable(expresion, variables[$i]);
                        tablaIncremento.push(tablaTemp);
                    }
                }
                ecuaciones.push({id: $form.ecuacion[$f].id, expresion: $form.ecuacion[$f].expresion, ecuaciones: tablaEcuacion, tablaIncremento: tablaIncremento,
                    tablaFinal: [], resultadoMedio: null, resultadoMaximo: null, resultadoMinimo: null, ecuacionesResultado: [], resultado: null, autoincremental: false});
            }

            if (autoincrementalBoolean === true) {
                var resultado;
                var resultadoMedio = 0;
                var sumatoria = "";
                for ($e = 0; $e < ecuaciones.length; $e++) {
                    sumatoria = "";
                    resultadoMedio = 0;
                    ecuaciones[$e].autoincremental = true;
                    for ($p = 0; $p < ecuaciones[$e].ecuaciones.length; $p++) {
                        sumatoria += "(" + ecuaciones[$e].ecuaciones[$p] + ")+";
                    }

                    var arrayTemplate = [];
                    for ($p = 0; $p < ecuaciones[$e].tablaIncremento.length; $p++) {
                        for ($j = 0; $j < ecuaciones[$e].tablaIncremento[$p].length; $j++) {
                            if ($p === 0) {
                                ecuaciones[$e].tablaFinal.push(sumatoria + "(" + ecuaciones[$e].tablaIncremento[$p][$j] + ")");
                            } else {
                                if ($j === 0) {
                                    arrayTemplate = ecuaciones[$e].tablaFinal;
                                    ecuaciones[$e].tablaFinal = [];
                                    //temp=ecuaciones[$e].tablaFinal[$i] + "+(" + ecuaciones[$e].tablaIncremento[$p][$j] + ")"
                                    //console.log(temp);
                                    //ecuaciones[$e].tablaFinal.push(temp);

                                    for ($i = 0; $i < arrayTemplate.length; $i++) {
                                        ecuaciones[$e].tablaFinal.push(arrayTemplate[$i] + "+" + ecuaciones[$e].tablaIncremento[$p][$j]);
                                    }
                                } else {
                                    for ($i = 0; $i < arrayTemplate.length; $i++) {
                                        ecuaciones[$e].tablaFinal.push(arrayTemplate[$i] + "+" + ecuaciones[$e].tablaIncremento[$p][$j]);
                                    }
                                }
                            }
                        }

                    }
                    var operacion = ecuaciones[$e].expresion.split(";")[1];
                    for ($i = 0; $i < ecuaciones[$e].tablaFinal.length; $i++) {

                        resultado = math.round(math.eval("(" + ecuaciones[$e].tablaFinal[$i] + ")" + operacion), 2);
                        resultadoMedio = resultadoMedio + resultado;
                        ecuaciones[$e].ecuacionesResultado.push(resultado);
                    }
                    /*resultado = math.eval(ecuaciones[$e].ecuaciones[$p]);
                     
                     resultado = resultado + resultado;
                     ecuaciones[$e].ecuacionesResultado.push(resultado);*/
                    //calcular resultado medio
                    ecuaciones[$e].resultadoMedio = math.round(math.eval(resultadoMedio / ecuaciones[$e].tablaFinal.length), 2);
                    //calcular resultado minimo
                    ecuaciones[$e].resultadoMinimo = Math.min.apply(null, ecuaciones[$e].ecuacionesResultado);
                    //calcular resultado maximo       
                    ecuaciones[$e].resultadoMaximo = Math.max.apply(null, ecuaciones[$e].ecuacionesResultado);
                }
            } else {
                var sumatoria = "";
                var cant = 0;
                for ($l = 0; $l < ecuaciones.length; $l++) {
                    cant = parseInt(ecuaciones[$l].ecuaciones.length);
                    sumatoria = ""
                    for ($p = 0; $p < cant; $p++) {
                        sumatoria += "(" + ecuaciones[$l].ecuaciones[$p] + ")";
                        if ($p !== cant - 1)
                            sumatoria += "+"
                    }

                    sumatoria = "(" + sumatoria + ")" + ecuaciones[$l].expresion.split(";")[1];
                    $resultado = math.round(math.eval(sumatoria), 2);
                    ecuaciones[$l].resultado = $resultado;
                }
            }


        } else {
            //si no es autoincremental

            //obtengo las ecuaciones y reemplazo por los valores
            for ($f = 0; $f < $form.ecuacion.length; $f++) {
                var expresion = $form.ecuacion[$f].expresion;
                for ($i = 0; $i < variables.length; $i++) {
                    if (variables[$i].autoincrementar === false) {

                        if (variables[$i].simbolo.indexOf("[") !== -1) {
                            var re = new RegExp("\\" + variables[$i].simbolo, "g");
                        } else {
                            var re = new RegExp(variables[$i].simbolo, "g");
                        }
                        expresion = expresion.replace(re, variables[$i].valor);
                    }
                }

                expresiones.push({id: $form.ecuacion[$f].id, expresion: expresion});
            }


            //si alguna variable es autoincremental
            if (autoincrementalBoolean === true) {

                var tablaEcuacion;
                var id;
                var expresion;
                var resultadoMedio;
                var resultadoMinimoMaximo;
                for ($l = 0; $l < expresiones.length; $l++) {

                    id = expresiones[$l].id;
                    expresion = expresiones[$l].expresion;
                    tablaEcuacion = reemplazarSimbolos(expresion, variables);
                    //ecuaciones.push(tablaEcuacion);

                    ecuaciones.push({id: id, expresion: expresion, ecuaciones: tablaEcuacion,
                        resultadoMedio: null, resultadoMaximo: null, resultadoMinimo: null, ecuacionesResultado: [], autoincremental: true});
                }

                //obtener el mayor resultado y el menor
                var resultado;
                var resultadoMedio = 0;
                for ($e = 0; $e < ecuaciones.length; $e++) {
                    resultadoMedio = 0;
                    for ($p = 0; $p < ecuaciones[$e].ecuaciones.length; $p++) {
                        resultado = math.round(math.eval(ecuaciones[$e].ecuaciones[$p]), 2);
                        resultadoMedio = resultadoMedio + resultado;
                        ecuaciones[$e].ecuacionesResultado.push(resultado);
                    }

                    //calcular resultado medio
                    ecuaciones[$e].resultadoMedio = math.round(math.eval(resultadoMedio / ecuaciones[$e].ecuaciones.length), 2);
                    //calcular resultado minimo
                    ecuaciones[$e].resultadoMinimo = Math.min.apply(null, ecuaciones[$e].ecuacionesResultado);
                    //calcular resultado maximo       
                    ecuaciones[$e].resultadoMaximo = Math.max.apply(null, ecuaciones[$e].ecuacionesResultado);
                }
            } else {
                for ($l = 0; $l < expresiones.length; $l++) {

                    id = expresiones[$l].id;
                    expresion = expresiones[$l].expresion;
                    $resultado = math.round(math.eval(expresion), 2);
                    ecuaciones.push({id: id, expresion: expresion, resultado: $resultado, autoincremental: false});
                }
            }
        }

        $ecuacionesTemp[key] = ecuaciones;
        $f = findInFormSession(key);
        $f.ecuacionesTemp = ecuaciones;
        for ($a = 0; $a < $form.ecuacion.length; $a++) {
      
            if (typeof ecuaciones[$a].resultadoMaximo !== "undefined" && ecuaciones[$a].resultadoMaximo !== null) {
                $form.ecuacion[$a].resultado_maximo = ecuaciones[$a].resultadoMaximo.toString();
            } else {
                $form.ecuacion[$a].resultado_maximo = "";
            }

            if (typeof ecuaciones[$a].resultadoMedio !== "undefined" && ecuaciones[$a].resultadoMedio !== null) {
                $form.ecuacion[$a].resultado_medio = ecuaciones[$a].resultadoMedio.toString();
            } else {
                $form.ecuacion[$a].resultado_medio = "";
            }

            if (typeof ecuaciones[$a].resultadoMinimo !== "undefined" && ecuaciones[$a].resultadoMinimo !== null) {
                $form.ecuacion[$a].resultado_minimo = ecuaciones[$a].resultadoMinimo.toString();
            } else {
                $form.ecuacion[$a].resultado_minimo = "";
            }

            if (typeof ecuaciones[$a].resultado !== "undefined" && ecuaciones[$a].resultado !== null) {
                $form.ecuacion[$a].resultado = [ecuaciones[$a].resultado.toString()];
            } else {
                $form.ecuacion[$a].resultado = ecuaciones[$a].ecuacionesResultado;
            }
        }
        guardarForm();
        mostrarTotales();
    } catch (error) {
        if ($("#panelVariable").html() !== "") {
            for ($i = 0; $i < $form.ecuacion.length; $i++) {
                $text = "<li class='list-group-item'>" +
                        "<span class='badge'>Error</span>" +
                        "Resultado" +
                        "</li>";
                $(".resultado" + $form.ecuacion[$i].id).html($text);
            }
            variables = [];
            ecuaciones = [];
            expresiones = [];
            $ecuacionesTemp[key] = ecuaciones;
            $f = findInFormSession(key);
            $f.ecuacionesTemp = ecuaciones;
            guardarForm();
        }

    }

}


function reemplazarSimbolos(_expresion, variables) {
    var array = [];
    var arrayTemp = [];
    var temp = "";
    var temp2 = "";
    var primerVariableAutoincremental = 0;
    var expresion = _expresion;
    //variables
    for ($i = 0; $i < variables.length; $i++) {
//pregunto solo por las de autoincrementar
        if (variables[$i].autoincrementar === true) {
            primerVariableAutoincremental++;
            for ($j = 0; $j < variables[$i].tabla.length; $j++) {
                if (primerVariableAutoincremental === 1) {
                    temp = expresion;
                    if (variables[$i].simbolo.indexOf("[") !== -1) {
                        var re = new RegExp("\\" + variables[$i].simbolo, "g");
                    } else {
                        var re = new RegExp(variables[$i].simbolo, "g");
                    }

                    expresion = expresion.replace(re, variables[$i].tabla[$j]);
                    arrayTemp.push(expresion);
                    expresion = temp;
                } else {
                    for ($e = 0; $e < arrayTemp.length; $e++) {
                        temp2 = arrayTemp[$e];
                        if (variables[$i].simbolo.indexOf("[") !== -1) {
                            var re = new RegExp("\\" + variables[$i].simbolo, "g");
                        } else {
                            var re = new RegExp(variables[$i].simbolo, "g");
                        }


                        temp2 = temp2.replace(re, variables[$i].tabla[$j]);
                        array.push(temp2);
                    }
                }
            }
        }
    }
    if (primerVariableAutoincremental === 1) {
        return arrayTemp;
    } else {
        return array;
    }
}
function reemplazarSimbolosPorVariable(_expresion, variable) {
    var array = [];
    var temp = "";
    var expresion = _expresion;
    for ($j = 0; $j < variable.tabla.length; $j++) {
        temp = expresion;
        if (variable.simbolo.indexOf("[") !== -1) {
            var re = new RegExp("\\" + variable.simbolo, "g");
        } else {
            var re = new RegExp(variable.simbolo, "g");
        }

        temp = temp.replace(re, variable.tabla[$j]);
        array.push(temp);
    }


    return array;
}
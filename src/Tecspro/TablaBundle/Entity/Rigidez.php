<?php

namespace Tecspro\TablaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rigidez
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Tecspro\TablaBundle\Entity\RigidezRepository")
 */
class Rigidez {

    use \A2lix\I18nDoctrineBundle\Doctrine\ORM\Util\Translatable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="desvicacionEstandar", type="string", length=255, nullable=true)
     */
    private $desvicacionEstandar;

    /**
     * @var string
     *
     * @ORM\Column(name="batalla", type="string", length=255, nullable=true)
     */
    private $batalla;

    /**
     * @var string
     *
     * @ORM\Column(name="categoria1", type="string", length=255, nullable=true)
     */
    private $categoria1;

    /**
     * @var string
     *
     * @ORM\Column(name="desviacionEstandar1", type="string", length=255, nullable=true)
     */
    private $desviacionEstandar1;

    /**
     * @var string
     *
     * @ORM\Column(name="categoria2", type="string", length=255, nullable=true)
     */
    private $categoria2;

    /**
     * @var string
     *
     * @ORM\Column(name="desviacionEstandar2", type="string", length=255, nullable=true)
     */
    private $desviacionEstandar2;

    /**
     * @var string
     *
     * @ORM\Column(name="ladoCategoria", type="string", length=255, nullable=true)
     */
    private $ladoCategoria;
    protected $translations;

    /**
     * Constructor
     */
    public function __construct() {
        $this->translations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set desvicacionEstandar
     *
     * @param string $desvicacionEstandar
     * @return Rigidez
     */
    public function setDesvicacionEstandar($desvicacionEstandar) {
        $this->desvicacionEstandar = $desvicacionEstandar;

        return $this;
    }

    /**
     * Get desvicacionEstandar
     *
     * @return string 
     */
    public function getDesvicacionEstandar() {
        return $this->desvicacionEstandar;
    }

    /**
     * Set batalla
     *
     * @param string $batalla
     * @return Rigidez
     */
    public function setBatalla($batalla) {
        $this->batalla = $batalla;

        return $this;
    }

    /**
     * Get batalla
     *
     * @return string 
     */
    public function getBatalla() {
        return $this->batalla;
    }

    /**
     * Set categoria1
     *
     * @param string $categoria1
     * @return Rigidez
     */
    public function setCategoria1($categoria1) {
        $this->categoria1 = $categoria1;

        return $this;
    }

    /**
     * Get categoria1
     *
     * @return string 
     */
    public function getCategoria1() {
        return $this->categoria1;
    }

    /**
     * Set desviacionEstandar1
     *
     * @param string $desviacionEstandar1
     * @return Rigidez
     */
    public function setDesviacionEstandar1($desviacionEstandar1) {

        $this->desviacionEstandar1 = $desviacionEstandar1;

        return $this;
    }

    /**
     * Get desviacionEstandar1
     *
     * @return string 
     */
    public function getDesviacionEstandar1() {
        return $this->desviacionEstandar1;
    }

    /**
     * Set categoria2
     *
     * @param string $categoria2
     * @return Rigidez
     */
    public function setCategoria2($categoria2) {
        $this->categoria2 = $categoria2;

        return $this;
    }

    /**
     * Get categoria2
     *
     * @return string 
     */
    public function getCategoria2() {
        return $this->categoria2;
    }

    /**
     * Set desviacionEstandar2
     *
     * @param string $desviacionEstandar2
     * @return Rigidez
     */
    public function setDesviacionEstandar2($desviacionEstandar2) {
        $this->desviacionEstandar2 = $desviacionEstandar2;

        return $this;
    }

    /**
     * Get desviacionEstandar2
     *
     * @return string 
     */
    public function getDesviacionEstandar2() {
        return $this->desviacionEstandar2;
    }

    /**
     * Set ladoCategoria
     *
     * @param string $ladoCategoria
     * @return Rigidez
     */
    public function setLadoCategoria($ladoCategoria) {
        $this->ladoCategoria = $ladoCategoria;

        return $this;
    }

    /**
     * Get ladoCategoria
     *
     * @return string 
     */
    public function getLadoCategoria() {
        return $this->ladoCategoria;
    }

}

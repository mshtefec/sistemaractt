<?php

namespace Tecspro\ARCBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * ResultadoARCType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class ResultadoARCType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tiempo')
            ->add('posicion')
            ->add('velocidad')
            ->add('estado')
            ->add('arcCampos', 'select2', array(
                'class' => 'Tecspro\ARCBundle\Entity\ARCCampos',
                'url'   => 'ResultadoARC_autocomplete_arcCampos',
                'configs' => array(
                    'multiple' => false,//required true or false
                    'width'    => 'off',
                ),
                'attr' => array(
                    'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                )
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Tecspro\ARCBundle\Entity\ResultadoARC'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'tecspro_arcbundle_resultadoarc';
    }
}

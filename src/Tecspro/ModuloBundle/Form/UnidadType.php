<?php

namespace Tecspro\ModuloBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * UnidadType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class UnidadType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('simbolo')
                ->add('translations', 'a2lix_translations', array(
                    'required' => true,
                    'fields' => array(
                        'nombre' => array(
                            'field_type' => 'text',
                            'locale_options' => array(
                                'es' => array(
                                    'label' => 'Nombre Español',
                                ),
                                'pt' => array(
                                    'label' => 'Nombre Portugues',
                                )
                            )
                        ),
                        'magnitud' => array(
                            'field_type' => 'text',
                            'locale_options' => array(
                                'es' => array(
                                    'label' => 'Magnitud Español',
                                ),
                                'pt' => array(
                                    'label' => 'Magnitud Portugues',
                                )
                            )
                        )
                    )
                ))
                ->add('operacion')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Tecspro\ModuloBundle\Entity\Unidad'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'tecspro_modulobundle_unidad';
    }

}

<?php

namespace Tecspro\ARCBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Tecspro\ARCBundle\Entity\ARCCampos;
use Tecspro\ARCBundle\Form\ARCCamposType;
use Tecspro\ARCBundle\Form\ARCCamposFilterType;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * ARCCampos controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/arccampos")
 */
class ARCCamposController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Tecspro/ARCBundle/Resources/config/ARCCampos.yml',
    );

    /**
     * Lists all ARCCampos entities.
     *
     * @Route("/", name="arccampos")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->config['filterType'] = new ARCCamposFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new ARCCampos entity.
     *
     * @Route("/", name="arccampos_create")
     * @Method("POST")
     * @Template("TecsproFrontBundle:Default:index.html.twig")
     */
    public function createAction() {
        
        $proyecto = $this->get('formula')->getProyectoActual(true);
        $entity = $proyecto->getArcCampos();

        $nuevo = false;

        $this->config['newType'] = new ARCCamposType();
        $config = $this->getConfig();
        $request = $this->getRequest();
        $session = $request->getSession();
        if (is_null($entity)) {

            $entity = new $config['entity']();
            $nuevo = true;
        } else {
            $arrayTempVehiculos = array();
            foreach ($entity->getVehiculos() as $vehiculo) {
                $arrayTempVehiculos[] = $vehiculo->getId();
            }
        }

        $form = $this->createCreateForm($config, $entity);
 
        $form->handleRequest($request);
        // ladybug_dump_die($entity->getVehiculos());
        if ($form->isValid()) {
            //datos del proyecto
            $nombreProyecto = $request->request->all()["tecspro_arcbundle_arccampos"]["nombreProyecto"];
            $fechaProyecto = $request->request->all()["tecspro_arcbundle_arccampos"]["fechaProyecto"];
            $comentarioProyecto = $request->request->all()["tecspro_arcbundle_arccampos"]["comentariosProyecto"];
            $parteInvolucradasProyecto = json_decode($request->request->all()["tecspro_arcbundle_arccampos"]["partesInvolucradas"], true);
            $cerrarProyecto = $request->request->all()["tecspro_arcbundle_arccampos"]["cerraProyecto"];

            $formula = $request->request->all()["tecspro_arcbundle_arccampos"]["formulasParameter"];

            if ($formula !== "") {

                $arrayFormula = json_decode($formula, true);
                $this->get('formula')->guardarFormulasArrayEnProyecto($arrayFormula);
            }
            $em = $this->getDoctrine()->getManager();
            if ($nombreProyecto != "") {
                $proyecto->setNombre($nombreProyecto);
                $session->set('proyectoNombre', $nombreProyecto);
            }
            $proyecto->setFecha(\DateTime::createFromFormat('d/m/Y', $fechaProyecto));
            $proyecto->setComentario($comentarioProyecto);

            if (count($parteInvolucradasProyecto) > 0) {
                $contador = -1;
                foreach ($parteInvolucradasProyecto as $key => $pi) {
                    $contador++;
                    $existe = $proyecto->getPartesInvolucradas()->get($key);

                    if (is_null($existe)) {
                        $parteInvolucradaEntity = new \Tecspro\FrontBundle\Entity\ParteInvolucrada();
                        $parteInvolucradaEntity->setNombre($pi);
                        $parteInvolucradaEntity->setProyecto($proyecto);
                        $em->persist($parteInvolucradaEntity);
                    } else {

                        $proyecto->getPartesInvolucradas()->get($key)->setNombre($pi);
                    }
                }

                if ($contador != count($proyecto->getPartesInvolucradas())) {

                    for ($i = $contador; $i < count($proyecto->getPartesInvolucradas()); $i++) {
                        $proyecto->removePartesInvolucrada($proyecto->getPartesInvolucradas()->get($i));
                    }
                }
            }

            $em->persist($proyecto);

            if (is_null($entity->getProyecto())) {

                $entity->setProyecto($proyecto);
            }

            if ($nuevo) {
                // ladybug_dump_die($entity);
                $em->persist($entity);
                $em->flush();
                $this->useACL($entity, 'create');
                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('proyectoGuardado', array(), 'TecsproARCBundle'));
            } else {
                /*  $em->clear();
                  ladybug_dump_die($entity->getVehiculos()); */
                // ladybug_dump_die($entity->getVehiculos());

                $em->flush();
                $this->useACL($entity, 'update');
                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('proyectoGuardado', array(), 'TecsproARCBundle'));
            }
            if ($cerrarProyecto) {
                $session->remove('formulas');
                $session->remove('proyecto');
                $session->remove('proyectoNombre');
            }
            return $this->redirectToRoute('inicio_plantilla');
        }

        if ($nuevo) {

            $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');
        } else {

            $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');
        }


        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new ARCCampos entity.
     *
     * @Route("/new", name="arccampos_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {

        $proyecto = $this->get('formula')->getProyectoActual(false);

        if (is_null($proyecto)) {

            $this->config['newType'] = new ARCCamposType();
            $config = $this->getConfig();
            $entity = new ARCCampos();

            $vehiculo = new \Tecspro\ARCBundle\Entity\Vehiculo();
            $entity->addVehiculo($vehiculo);

            $entity->setFraccionDeTiempoAAnalizar("0.2");

            $form = $this->createCreateForm($config, $entity);

            // remove the form to return to the view
            unset($config['newType']);

            return array(
                'config' => $config,
                'entity' => $entity,
                'form' => $form->createView(),
            );
        } else {

            if (is_null($proyecto->getArcCampos())) {

                $this->config['newType'] = new ARCCamposType();
                $config = $this->getConfig();
                $entity = new ARCCampos();
                $entity->setFraccionDeTiempoAAnalizar("0.2");

                $form = $this->createCreateForm($config, $entity);

                // remove the form to return to the view
                unset($config['newType']);

                return array(
                    'config' => $config,
                    'entity' => $entity,
                    'form' => $form->createView(),
                );
            } else {

                $this->config['newType'] = new ARCCamposType();
                $config = $this->getConfig();
                $entity = $proyecto->getArcCampos();

                $form = $this->createCreateForm($config, $entity);

                // remove the form to return to the view
                unset($config['newType']);

                return array(
                    'config' => $config,
                    'entity' => $entity,
                    'form' => $form->createView(),
                );
            }
        }
    }

    /**
     * Finds and displays a ARCCampos entity.
     *
     * @Route("/{id}", name="arccampos_show", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Creates a form to create a entity.
     *
     * @param array $config
     * @param $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createCreateForm($config, $entity) {

        $form = $this->createForm($config['newType'], $entity, array(
            'action' => $this->generateUrl($config['create']),
            'method' => 'POST',
        ));

        $form
                ->add('save', 'submit', array(
                    'translation_domain' => 'MWSimpleAdminCrudBundle',
                    'label' => 'views.new.save',
                    'attr' => array(
                        'class' => 'form-control btn-success',
                        'col' => 'col-lg-2',
                    ),
                ))
        ;

        return $form;
    }

    /**
     * Displays a form to edit an existing ARCCampos entity.
     *
     * @Route("/{id}/edit", name="arccampos_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->config['editType'] = new ARCCamposType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing ARCCampos entity.
     *
     * @Route("/{id}", name="arccampos_update")
     * @Method("PUT")
     * @Template("TecsproARCBundle:ARCCampos:edit.html.twig")
     */
    public function updateAction($id) {
        $this->config['editType'] = new ARCCamposType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a ARCCampos entity.
     *
     * @Route("/{id}", name="arccampos_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter ARCCampos.
     *
     * @Route("/exporter/{format}", name="arccampos_export")
     */
    public function getExporter($format) {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Autocomplete a ARCCampos entity.
     *
     * @Route("/autocomplete-forms/get-resultadosARC", name="ARCCampos_autocomplete_resultadosARC")
     */
    public function getAutocompleteResultadoARC() {
        $options = array(
            'repository' => "TecsproARCBundle:ResultadoARC",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a ARCCampos entity.
     *
     * @Route("/autocomplete-forms/get-vehiculos", name="ARCCampos_autocomplete_vehiculos")
     */
    public function getAutocompleteVehiculo() {
        $options = array(
            'repository' => "TecsproARCBundle:Vehiculo",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Datatable ARCCampos.
     *
     * @Route("/get-table/", name="arccampos_table")
     */
    public function getDatatable() {
        $response = parent::getTable();

        return $response;
    }

}

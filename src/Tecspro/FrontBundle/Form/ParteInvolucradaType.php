<?php

namespace Tecspro\FrontBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ParteInvolucradaType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nombre', 'text', array(
                    "label" => 'Nombre',
                    "required" => true,
                    'translation_domain' => 'TecsproFrontBundle',
                    'attr' => array(
                        'class' => 'txtParteInvolucrada'
                    )
                        )
                )
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Tecspro\FrontBundle\Entity\ParteInvolucrada'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'tecspro_frontbundle_parteinvolucrada';
    }

}

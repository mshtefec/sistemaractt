<?php

namespace Tecspro\TablaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * RigidezType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class RigidezType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('translations', 'a2lix_translations', array(
                    'required' => true,
                    'fields' => array(
                        'vehiculo' => array(
                            'field_type' => 'choice',
                            'locale_options' => array(
                                'es' => array(
                                    'label' => 'Vehiculos Español',
                                ),
                                'pt' => array(
                                    'label' => 'Vehiculos Portugues',
                                )
                            ),
                            'choices' => array(
                                'es' => array(
                                    'VehiculosPasajeros' => 'Vehiculos de pasajeros (Turismos)',
                                    'Pick Ups' => 'Pick Ups',
                                    'Multipropositos' => 'Multipropositos',
                                    'Vans- Mini Vans' => 'Vans- Mini Vans',
                                ),
                                'pt' => array(
                                    'Veículos Passagerios' => 'Veículos Passagerios',
                                    'Pick Ups' => 'Pick Ups',
                                    'Multipropositos' => 'Multipropositos',
                                    'Vans- Mini Vans' => 'Vans- Mini Vans',
                                ),
                            ),
                        ),
                        'tipo' => array(
                            'field_type' => 'choice',
                            'locale_options' => array(
                                'es' => array(
                                    'label' => 'Tipo Español',
                                ),
                                'pt' => array(
                                    'label' => 'Tipo Portugues',
                                )
                            ),
                            'choices' => array(
                                'es' => array(
                                    'Coeficiente de Rigidez para McHenry' => 'Coeficiente de Rigidez para McHenry',
                                    'Coeficiente de Rigidez para Prassad' => 'Coeficiente de Rigidez para Prassad',
                                ),
                                'pt' => array(
                                    'Coeficiente de Rigidez para McHenry' => 'Coeficiente de Rigidez para McHenry',
                                    'Coeficiente de Rigidez para Prassad' => 'Coeficiente de Rigidez para Prassad',
                                ),
                            )
                        ),
                        'categoria' => array(
                            'field_type' => 'choice',
                            'locale_options' => array(
                                'es' => array(
                                    'label' => 'Categoria Español',
                                ),
                                'pt' => array(
                                    'label' => 'Categoria Portugues',
                                )
                            ),
                            'choices' => array(
                                'es' => array(
                                    'Categoria 1 Batalla = 2.05-2.41 m' => 'Categoria 1 Batalla = 2.05-2.41 m',
                                    'Categoria 2 Batalla = 2.41-2.58 m' => 'Categoria 2 Batalla = 2.41-2.58 m',
                                    'Categoria 3 Batalla = 2.58-2.80 m' => 'Categoria 3 Batalla = 2.58-2.80 m',
                                    'Categoria 4 Batalla = 2.80-2.98 m' => 'Categoria 4 Batalla = 2.80-2.98 m',
                                    'Categoria 5 Batalla 0 mayores a 2.98 m' => 'Categoria 5 Batalla 0 mayores a 2.98 m',
                                ),
                                'pt' => array(
                                    'Categoria 1 Distância entre eixos  = 2.05-2.41 m' => 'Categoria 1 Distância entre eixos  = 2.05-2.41 m',
                                    'Categoria 2 Distância entre eixos  = 2.41-2.58 m' => 'Categoria 2 Distância entre eixos  = 2.41-2.58 m',
                                    'Categoria 3 Distância entre eixos  = 2.58-2.80 m' => 'Categoria 3 Distância entre eixos  = 2.58-2.80 m',
                                    'Categoria 4 Distância entre eixos  = 2.80-2.98 m' => 'Categoria 4 Distância entre eixos  = 2.80-2.98 m',
                                    'Categoria 5 Distância entre eixos 0 maiores a 2.98' => 'Categoria 5 Distância entre eixos 0 maiores a 2.98 ',
                                ),
                            )
                        ),
                        'lado' => array(
                            'field_type' => 'choice',
                            'locale_options' => array(
                                'es' => array(
                                    'label' => 'Lado Español',
                                ),
                                'pt' => array(
                                    'label' => 'Lado Portugues',
                                )
                            ),
                            'choices' => array(
                                'es' => array(
                                    'Frontal' => 'Frontal',
                                    'Trasero' => 'Trasero',
                                    'Lateral' => 'Lateral',
                                ),
                                'pt' => array(
                                    'Frontal' => 'Frontal',
                                    'Traseiro' => 'Traseiro',
                                    'Lateral' => 'Lateral',
                                ),
                            )
                        ),
                    )
                ))
                ->add('ladoCategoria', 'choice', array(
                    'choices' => array(
                        'A [N/cm]' => 'A [N/cm]',
                        'B [N/cm2]' => 'B [N/cm2]',
                        'G [N]' => 'G [N]',
                        'do [N0,5]' => 'do [N0,5]',
                        'd1 [N0,5/m]' => 'd1 [N0,5/m]',
                    ),
                    // *this line is important*
                    'choices_as_values' => true,
                ))
                ->add('desvicacionEstandar')
                ->add('batalla')
                ->add('categoria1')
                ->add('desviacionEstandar1')
                ->add('categoria2')
                ->add('desviacionEstandar2')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Tecspro\TablaBundle\Entity\Rigidez'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'tecspro_tablabundle_rigidez';
    }

}

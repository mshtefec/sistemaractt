<?php

namespace Tecspro\FrontBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ParteInvolucrada
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Tecspro\FrontBundle\Entity\ParteInvolucradaRepository")
 */
class ParteInvolucrada {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\ManyToOne(targetEntity="proyecto", inversedBy="partesInvolucradas")
     * @ORM\JoinColumn(name="proyecto_id", referencedColumnName="id")
     */
    private $proyecto;

    public function __toString() {
        return $this->nombre;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return ParteInvolucrada
     */
    public function setNombre($nombre) {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre() {
        return $this->nombre;
    }

    /**
     * Set proyecto
     *
     * @param \Tecspro\FrontBundle\Entity\proyecto $proyecto
     * @return ParteInvolucrada
     */
    public function setProyecto(\Tecspro\FrontBundle\Entity\proyecto $proyecto = null) {
        $this->proyecto = $proyecto;

        return $this;
    }

    /**
     * Get proyecto
     *
     * @return \Tecspro\FrontBundle\Entity\proyecto 
     */
    public function getProyecto() {
        return $this->proyecto;
    }

}

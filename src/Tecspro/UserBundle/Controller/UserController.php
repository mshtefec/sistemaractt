<?php

namespace Tecspro\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Tecspro\UserBundle\Entity\User;
use Tecspro\UserBundle\Form\UserType;
use Tecspro\UserBundle\Form\UserFilterType;
use Exporter\Source\DoctrineORMQuerySourceIterator;
use Exporter\Source\ArraySourceIterator;
use Exporter\Handler;
use Symfony\Component\HttpFoundation\Response;

/**
 * User controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/superadmin/user")
 */
class UserController extends Controller {

    private $ROLE_CLIENTE = 'ROLE_CLIENTE';
    private $ROLE_PRUEBA = 'ROLE_PRUEBA';
    private $ROLE_SUPERADMIN = 'ROLE_SUPERADMIN';
    private $ROLE_ADMIN = 'ROLE_ADMIN';

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Tecspro/UserBundle/Resources/config/User.yml',
    );

    /**
     * Lists all User entities.
     *
     * @Route("/", name="superadmin_user")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {

        $this->config['filterType'] = new UserFilterType();
        $request = $this->getRequest();
        $session = $request->getSession();
        $session->set("filtroUser", true);
        $config = $this->getConfig();

        return array(
            'config' => $config,
        );
    }

    /**
     * Lists all User entities.
     *
     * @Route("/disabled", name="superadmin_user_disabled")
     * @Method("GET")
     * @Template()
     */
    public function indexDisabledAction() {

        $this->config['filterType'] = new UserFilterType();
        $request = $this->getRequest();
        $session = $request->getSession();
        $session->set("filtroUser", false);
        $config = $this->getConfig();

        return array(
            'config' => $config,
        );
    }

    /**
     * Creates a new User entity.
     *
     * @Route("/", name="superadmin_user_create")
     * @Method("POST")
     * @Template("UserBundle:User:new.html.twig")
     */
    public function createAction() {

        $this->config['newType'] = new UserType();
        //default controller adminCrud

        $config = $this->getConfig();
        $request = $this->getRequest();
        $entity = new $config['entity']();
        $form = $this->createCreateForm($config, $entity);
        $em = $this->getDoctrine()->getManager();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $entity->setFechaInicio(date_time_set($entity->getFechaInicio(), 23, 59, 59));
            if ($entity->hasRole($this->ROLE_CLIENTE) || $entity->hasRole($this->ROLE_PRUEBA)) {

                $dia_de_inicio = clone $entity->getFechaInicio();

                $dia_de_expiracion = $dia_de_inicio->modify('+' . $entity->getPlanDeCuenta() . ' days');

                $entity->setExpiresAt($dia_de_expiracion);
            }

            foreach ($entity->getTelefonos() as $telefono) {

                $telefono->setUser($entity);
            }

            foreach ($entity->getDomicilios() as $domicilio) {

                $domicilio->setUser($entity);
            }
            $user = $this->getUser();
            $entity->setParent($user);

            if (count($entity->getModulos()) <= 0) {
                $this->get('session')->getFlashBag()->add('danger', 'Debe Tener Modulos selecionados');
            } else if (count($entity->getRoles()) <= 0) {
                $this->get('session')->getFlashBag()->add('danger', 'Debe Tener Roles selecionados');
            } else {
                //agregado secure password
                $this->setSecurePassword($entity);

                $modulo = $entity->getModulos()->last()->getId();
                $entity->setItemConfiguraciones("ulitmoModulo", $modulo);
                //  ladybug_dump_die($user->getConfiguraciones());
                //fin
                $em->persist($entity);
                $em->flush();
                $this->useACL($entity, 'create');

                $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

                $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $entity->getId()));
                $superadmin = $em->getRepository('UserBundle:User')->find(1);
               
                $message = \Swift_Message::newInstance()
                        ->setSubject('Nuevo Usuario')
                        ->setFrom($user->getEmail())
                        ->setTo($superadmin->getEmail())
                        ->setBody(
                        $this->renderView(
                                // app/Resources/views/Emails/registration.html.twig
                                'UserBundle:email:usuarioCreado.html.twig', array('entity' => $entity)
                        ), 'text/html'
                        )
                ;
                $this->get('mailer')->send($message);

                return $this->redirect($nextAction);
            }
        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new User entity.
     *
     * @Route("/new", name="superadmin_user_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->config['newType'] = new UserType();
        $config = $this->getConfig();
        $entity = new $config['entity']();
        $form = $this->createCreateForm($config, $entity);

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a entity.
     *
     * @param array $config
     * @param $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createCreateForm($config, $entity) {
        $form = $this->createForm($config['newType'], $entity, array(
            'action' => $this->generateUrl($config['create']),
            'method' => 'POST',
        ));

        $form
                ->add('save', 'submit', array(
                    'translation_domain' => 'MWSimpleAdminCrudBundle',
                    'label' => 'views.new.save',
                    'attr' => array(
                        'class' => 'btn btn-success'
                    ),
                ))
                ->add('saveAndAdd', 'submit', array(
                    'translation_domain' => 'MWSimpleAdminCrudBundle',
                    'label' => 'views.new.saveAndAdd',
                    'attr' => array(
                        'class' => 'btn btn-primary',
                    ),
                ))
        ;

        return $form;
    }

    /**
     * Creates a form to edit a entity.
     *
     * @param array $config
     * @param $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createEditForm($config, $entity) {

        $current_role_name = '';

        if ($this->get('security.authorization_checker')->isGranted($this->ROLE_SUPERADMIN)) {

            $current_role_name = 'super_admin';
        }

        $form = $this->createForm($config['editType'], $entity, array(
            'action' => $this->generateUrl($config['update'], array('id' => $entity->getId())),
            'method' => 'PUT',
            'allow_extra_fields' => $current_role_name,
        ));

        $form
                ->add('save', 'submit', array(
                    'translation_domain' => 'MWSimpleAdminCrudBundle',
                    'label' => 'views.new.save',
                    'attr' => array(
                        'class' => 'form-control btn-success',
                        'col' => 'col-lg-2',
                    ),
                ))
                ->add('saveAndAdd', 'submit', array(
                    'translation_domain' => 'MWSimpleAdminCrudBundle',
                    'label' => 'views.new.saveAndAdd',
                    'attr' => array(
                        'class' => 'form-control btn-primary',
                        'col' => 'col-lg-3',
                    ),
                ))
        ;

        return $form;
    }

    /**
     * Finds and displays a User entity.
     *
     * @Route("/{id}", name="superadmin_user_show", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {

        $config = $this->getConfig();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
        }
        $this->useACL($entity, 'show');
        $deleteForm = $this->createDeleteForm($config, $id);

        return array(
            'config' => $config,
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/{id}/edit", name="superadmin_user_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->config['editType'] = new UserType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing User entity.
     *
     * @Route("/{id}", name="superadmin_user_update")
     * @Method("PUT")
     * @Template("UserBundle:User:edit.html.twig")
     */
    public function updateAction($id) {
        $this->config['editType'] = new UserType();

        $config = $this->getConfig();
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->find($id);

        $fecha_de_inicio = $entity->getFechaInicio();

        $plan_de_cuenta = $entity->getPlanDeCuenta();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
        }
        $this->useACL($entity, 'update');
        //pass anterior para comparar
        $passwordOld = $entity->getPassword();
        //fin
        $deleteForm = $this->createDeleteForm($config, $id);
        $editForm = $this->createEditForm($config, $entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            if (count($entity->getModulos()) <= 0) {
                $this->get('session')->getFlashBag()->add('danger', 'Debe Tener Modulos selecionados');
            } else if (count($entity->getRoles()) <= 0) {
                $this->get('session')->getFlashBag()->add('danger', 'Debe Tener Roles selecionados');
            } else {
                $entity->setFechaInicio(date_time_set($entity->getFechaInicio(), 23, 59, 59));
                if ($this->get('security.authorization_checker')->isGranted($this->ROLE_SUPERADMIN)) {
                    if ($entity->hasRole($this->ROLE_CLIENTE) || $entity->hasRole($this->ROLE_PRUEBA)) {
                        $dia_de_inicio = clone $entity->getFechaInicio();

                        $dia_de_expiracion = $dia_de_inicio->modify('+' . $entity->getPlanDeCuenta() . ' days');

                        $entity->setExpiresAt($dia_de_expiracion);
                    }
                } else {

                    $entity->setFechaInicio($fecha_de_inicio);

                    $entity->setPlanDeCuenta($plan_de_cuenta);
                }

                foreach ($entity->getTelefonos() as $telefono) {

                    $telefono->setUser($entity);
                }

                foreach ($entity->getDomicilios() as $domicilio) {

                    $domicilio->setUser($entity);
                }



                $resetSessiones = '0';
                if (isset($request->request->all()['tecspro_userbundle_user']['ResetearSessiones'])) {
                    $resetSessiones = $request->request->all()['tecspro_userbundle_user']['ResetearSessiones'];
                }
                if (count($entity->getModulos()) > 0) {
                    //agregado secure password
                    if (!is_null($entity->getPassword())) {

                        //Si pass es diferente actualizo
                        $this->setSecurePassword($entity);
                    } else {
                        $entity->setPassword($passwordOld);
                    }
                    if ($resetSessiones == "1") {
                        $entity->setSessiones(array());
                    }


                    //fin
                    $em->flush();
                    $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

                    $nextAction = $editForm->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $id));
                    return $this->redirect($nextAction);
                } else {
                    $this->get('session')->getFlashBag()->add('danger', 'Debe Tener Modulos selecionados');
                }
            }
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        // remove the form to return to the view
        unset($config['editType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a User entity.
     *
     * @Route("/{id}", name="superadmin_user_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $config = $this->getConfig();
        $request = $this->getRequest();
        $form = $this->createDeleteForm($config, $id);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository($config['repository'])->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
            }
            $tieneChildren = false;
            if ($entity->hasRole($this->ROLE_ADMIN)) {
                if (count($entity->getChildren()) <= 0) {
                    $tieneChildren = false;
                } else {
                    $tieneChildren = true;
                }
            } else {
                $tieneChildren = false;
            }

            if ($tieneChildren) {
                $entity->setEnabled(false);
                $em->persist($entity);
                $em->flush();
                $this->get('session')->getFlashBag()->add('success', 'El usuario Tiene clientes relacionados, el usuario se encuenta ahora inactivo', 'flash.delete.success');
            } else {
                $em->remove($entity);
                $em->flush();
                $this->get('session')->getFlashBag()->add('success', 'flash.delete.success');
            }
        }

        return $this->redirect($this->generateUrl($config['index']));
    }

    /**
     * Datatable Formula.
     *
     * @Route("/get-table/{estado}", name="superadmin_user_table")
     */
    public function getDatatable($estado) {
        $response = parent::getTable($this->createQueryWithParameter($estado));

        return $response;
    }

    /**
     * Create query.
     * @return Doctrine\ORM\QueryBuilder $queryBuilder
     */
    private function createQueryWithParameter($estado) {
        $em = $this->getDoctrine()->getManager();
        $config = $this->getConfig();
        $qb = $em->createQueryBuilder();
        if ($this->get('security.authorization_checker')->isGranted($this->ROLE_SUPERADMIN)) {
            $qb
                    ->select('a.id', 'a.username', "a.email", 'a.fechaCreacion', 'a.plan_de_cuenta', "GROUP_CONCAT(r.name SEPARATOR ',') as roles")
                    ->from($config['repository'], 'a')
                    ->join('a.user_roles', 'r')
                    ->where('a.enabled = ' . $estado)
                    ->groupBy('a.id');
            ;
        } else {
            $user = $this->getUser();

            $qb
                    ->select('a.id', 'a.username', "a.email", 'a.fechaCreacion', 'a.plan_de_cuenta', "GROUP_CONCAT(r.name SEPARATOR ',') as roles")
                    ->from($config['repository'], 'a')
                    ->join('a.user_roles', 'r')
                    ->join('a.parent', 'p')
                    ->where("a.enabled = " . $estado)
                    ->andWhere("p.id = " . $user->getId())
                    ->groupBy('a.id');
            ;
        }


        $tipoArray[] = array(
            'column' => 3,
            'type' => 'datetime',
            'format' => 'd/m/Y'
        );

        $array = array(
            'query' => $qb,
            'tipoArray' => $tipoArray,
            'concatBoolean' => true,
            'oneToMany' => true,
        );

        return $array;
    }

    private function getTipoFormat($array, $index) {
        $type = 'string';
        $format = '';
        foreach ($array as $a) {
            if ($a['column'] == $index) {
                if (isset($a['type'])) {
                    $type = $a['type'];
                }
                if (isset($a['format'])) {
                    $format = $a['format'];
                }
                break;
            }
        }

        return array($type, $format);
    }

    /**
     * Exporter User.
     *
     * @Route("/exporter/{format}", name="superadmin_user_export")
     */
    public function getExporter($format) {
        $request = $this->getRequest();
        $session = $request->getSession();
        $usuariosActivos = $session->get("filtroUser", true);
        $config = $this->getConfig();
        $queryBuilder = $this->createQuery($config['repository']);
        $campos = array();
        //array
        foreach ($config['fieldsindex'] as $key => $value) {
            //if is defined and true
            if (!empty($value['export']) && $value['export']) {
                if ($value['type'] == 'ONE_TO_ONE' || $value['type'] == 'ONE_TO_ONE' || $value['type'] == 'MANY_TO_ONE') {
                    $campos[] = $value['alias'] . '.' . $value['name'] . ' as ' . str_replace(" ", "_", $value['label']);
                } elseif ($value['type'] == 'ONE_TO_MANY') {
                    $campos[] = "GROUP_CONCAT(user_roles.name SEPARATOR ',')" . ' as ' . str_replace(" ", "_", $value['label']);
                } elseif ($value['type'] == 'datetime' || $value['type'] == 'datetimetz') {
                    $campos[] = "DATE_FORMAT(" . $key . ", '%d/%m/%Y %H:%s')" . ' as ' . str_replace(" ", "_", $value['label']);
                } elseif ($value['type'] == 'date') {
                    $campos[] = "DATE_FORMAT(" . $key . ", '%d/%m/%Y')" . ' as ' . str_replace(" ", "_", $value['label']);
                } elseif ($value['type'] == 'time') {
                    $campos[] = "DATE_FORMAT(" . $key . ", '%H:%s')" . ' as ' . str_replace(" ", "_", $value['label']);
                } else {
                    $campos[] = $key . ' as ' . str_replace(" ", "_", $value['label']);
                }
            }
        }

        $queryBuilder['query']->select($campos);
        $queryBuilder['query']->where("a.enabled = :ena");
        $queryBuilder['query']->setParameter("ena", $usuariosActivos);
        $array = $queryBuilder['query']->getQuery()->getArrayResult();

        // Pick a format to export to
        //$format = 'csv';
        // Set Content-Type
        switch ($format) {
            case 'xls':
                $content_type = 'application/vnd.ms-excel';
                break;
            case 'json':
                $content_type = 'application/json';
                break;
            case 'csv':
                $content_type = 'text/csv';
                break;
            default:
                $content_type = 'text/csv';
                break;
        }
        // Location to Export this to
        $export_to = 'php://output';
        // Data to export
        //$exporter_source = new DoctrineORMQuerySourceIterator($query, $campos, 'Y-m-d H:i:s');      
        $exporter_source = new ArraySourceIterator($array);
        // Get an Instance of the Writer
        $exporter_writer = '\Exporter\Writer\\' . ucfirst($format) . 'Writer';
        $exporter_writer = new $exporter_writer($export_to);
        // Generate response
        $response = new Response();
        // Set headers
        $response->headers->set('Cache-Control', 'must-revalidate, post-check=0, pre-check=0');
        $response->headers->set('Content-type', $content_type);
        $response->headers->set('Expires', 0);
        //$response->headers->set('Content-length', filesize($filename));
        $response->headers->set('Pragma', 'public');
        // Send headers before outputting anything
        $response->sendHeaders();
        // Export to the format
        Handler::create($exporter_source, $exporter_writer)->export();

        return $response;
    }

    /**
     * Autocomplete a User entity.
     *
     * @Route("/autocomplete-forms/get-user_roles", name="User_autocomplete_user_roles")
     */
    public function getAutocompleteRole() {
        $role_name = '';

        if ($this->isGranted($this->ROLE_SUPERADMIN)) {

            $role_name = array($this->ROLE_ADMIN, $this->ROLE_CLIENTE, $this->ROLE_PRUEBA);
        } elseif ($this->isGranted($this->ROLE_ADMIN)) {

            $role_name = array($this->ROLE_CLIENTE, $this->ROLE_PRUEBA);
        }

        $request = $this->getRequest();
        $term = $request->query->get('q', null);

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('UserBundle:Role')->findBy(array('name' => $role_name));

        $array = array();
        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->__toString(),
            );
        }

        //ladybug_dump_die($array);

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /** Secure password */
    private function setSecurePassword($entity) {
        $factory = $this->get('security.encoder_factory');
        $encoder = $factory->getEncoder($entity);
        $password = $encoder->encodePassword($entity->getPassword(), $entity->getSalt());
        $entity->setPassword($password);
    }

    /**
     * Autocomplete a User entity.
     *
     * @Route("/autocomplete-forms/get-modulos", name="User_autocomplete_modulos")
     */
    public function getAutocompleteModulo() {
        $request = $this->getRequest();
        $term = $request->query->get('q', null);

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ModuloBundle:Modulo')->findAll();

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->__toString(),
            );
        }

        //ladybug_dump_die($array);

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

}

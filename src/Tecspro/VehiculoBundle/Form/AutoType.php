<?php

namespace Tecspro\VehiculoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * AutoType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class AutoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('categorizacion')
            ->add('trocha')
            ->add('marca')
            ->add('modelo')
            ->add('anio')
            ->add('cilindrada')
            ->add('largo')
            ->add('ancho')
            ->add('alto')
            ->add('distanciaEntreEjes')
            ->add('pesoVacio')
            ->add('comentario')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Tecspro\VehiculoBundle\Entity\Auto'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'tecspro_vehiculobundle_auto';
    }
}

<?php

namespace Tecspro\ModuloBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 * Categoria
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Tecspro\ModuloBundle\Entity\CategoriaRepository")
 */
class UnidadTranslation implements \A2lix\I18nDoctrineBundle\Doctrine\Interfaces\OneLocaleInterface {

    use Translation;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     * @Type("string")
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="magnitud", type="string", length=255)
     * @Type("string")
     */
    private $magnitud;

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return UnidadTranslation
     */
    public function setNombre($nombre) {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre() {
        return $this->nombre;
    }


    /**
     * Set magnitud
     *
     * @param string $magnitud
     * @return UnidadTranslation
     */
    public function setMagnitud($magnitud)
    {
        $this->magnitud = $magnitud;

        return $this;
    }

    /**
     * Get magnitud
     *
     * @return string 
     */
    public function getMagnitud()
    {
        return $this->magnitud;
    }
}

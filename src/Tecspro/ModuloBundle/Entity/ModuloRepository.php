<?php

namespace Tecspro\ModuloBundle\Entity;

use Doctrine\ORM\EntityRepository;

class ModuloRepository extends EntityRepository {

    public function findByUser($user) {
        return $this->createQueryBuilder('a')
                        ->select('a')
                        ->join('a.users', 'u')
                        ->where('u.id= :id')
                        ->setParameter('id', $user)
                        ->getQuery()
                        ->getResult()
        ;
    }

}

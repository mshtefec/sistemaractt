<?php

namespace Tecspro\TablaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Tecspro\TablaBundle\Entity\TablaFriccion;
use Tecspro\TablaBundle\Form\TablaFriccionType;
use Tecspro\TablaBundle\Form\TablaFriccionFilterType;

/**
 * TablaFriccion controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/tablafriccion")
 */
class TablaFriccionController extends Controller
{
    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Tecspro/TablaBundle/Resources/config/TablaFriccion.yml',
    );

    /**
     * Lists all TablaFriccion entities.
     *
     * @Route("/", name="admin_tablafriccion")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->config['filterType'] = new TablaFriccionFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new TablaFriccion entity.
     *
     * @Route("/", name="admin_tablafriccion_create")
     * @Method("POST")
     * @Template("TecsproTablaBundle:TablaFriccion:new.html.twig")
     */
    public function createAction()
    {
        $this->config['newType'] = new TablaFriccionType();
        $response = parent::createAction();

        return $response;
    }

    /**
     * Displays a form to create a new TablaFriccion entity.
     *
     * @Route("/new", name="admin_tablafriccion_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->config['newType'] = new TablaFriccionType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a TablaFriccion entity.
     *
     * @Route("/{id}", name="admin_tablafriccion_show", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing TablaFriccion entity.
     *
     * @Route("/{id}/edit", name="admin_tablafriccion_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->config['editType'] = new TablaFriccionType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing TablaFriccion entity.
     *
     * @Route("/{id}", name="admin_tablafriccion_update")
     * @Method("PUT")
     * @Template("TecsproTablaBundle:TablaFriccion:edit.html.twig")
     */
    public function updateAction($id)
    {
        $this->config['editType'] = new TablaFriccionType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a TablaFriccion entity.
     *
     * @Route("/{id}", name="admin_tablafriccion_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter TablaFriccion.
     *
     * @Route("/exporter/{format}", name="admin_tablafriccion_export")
     */
    public function getExporter($format)
    {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Datatable TablaFriccion.
     *
     * @Route("/get-table/", name="admin_tablafriccion_table")
     */
    public function getDatatable()
    {
        $response = parent::getTable();

        return $response;
    }
 
}
<?php

namespace Tecspro\ModuloBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

/**
 * UnidadType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class VariablesType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('translations', 'a2lix_translations', array(
                    'required_locales' => array('es', 'pt'),
                    'fields' => array(
                        'nombre' => array(
                            'field_type' => 'text',
                            'locale_options' => array(
                                'es' => array(
                                    'label' => 'Nombre Español',
                                ),
                                'pt' => array(
                                    'label' => 'Nombre Portugues',
                                )
                            )
                        )
                    )
                ))
                ->add('simbolo', null, array(
                    'attr' => array(
                        'class' => 'variable'
                    )
                ))
                ->add('unidad', 'entity', array(
                    'class' => 'ModuloBundle:Unidad',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                                ->orderBy('u.id', 'ASC');
                    },
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Tecspro\ModuloBundle\Entity\Variable',
            'cascade_validation' => true,
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'tecspro_modulobundle_variable';
    }

}

<?php

namespace Tecspro\TablaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * TablaFriccionType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class TablaFriccionType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('translations', 'a2lix_translations', array(
                    'required' => true,
                    'fields' => array(
                        'caracteristicasSuperficie' => array(
                            'field_type' => 'choice',
                            'locale_options' => array(
                                'es' => array(
                                    'label' => 'Características Superficie Español',
                                ),
                                'pt' => array(
                                    'label' => 'Características Superficie Portugues',
                                )
                            ),
                            'choices'  => array(
                                'es' => array(
                                    'CONCRETO' => 'CONCRETO',
                                    'ASFALTO o ALQUITRÁN' => 'ASFALTO o ALQUITRÁN',
                                    'ADOQUÍN' => 'ADOQUÍN',
                                    'PIEDRA COMPACTA' => 'PIEDRA COMPACTA',
                                    'GRAVA' => 'GRAVA',
                                    'ESCORIAS' => 'ESCORIAS',
                                    'PIEDRA'  => 'PIEDRA',
                                    'HIELO' => 'HIELO',
                                    'NIEVE' => 'NIEVE',
                                    'REJA METÁLICA' => 'REJA METÁLICA',
                                ),
                                 'pt' => array(
                                    'CONCRETO' => 'CONCRETO',
                                    'ASFALTO o ALQUITRÁN' => 'ASFALTO o ALQUITRÁN',
                                    'REVESTIMENTO DE PEDRA' => 'REVESTIMENTO DE PEDRA',
                                    'PEDRA COMPACTA' => 'PEDRA COMPACTA',
                                    'ESTRADA DE CASCALHO' => 'ESTRADA DE CASCALHO',
                                    'ESCÓRIAS' => 'ESCÓRIAS',
                                    'PEDRA' => 'PEDRA',
                                    'GELO' => 'GELO',
                                    'NEVE' => 'NEVE',
                                    'REDE METÁLICA' => 'REDE METÁLICA',
                                ),
                            ),
                        ),
                        'tipo' => array(
                            'field_type' => 'choice',
                            'locale_options' => array(
                                'es' => array(
                                    'label' => 'Tipo Español',
                                ),
                                'pt' => array(
                                    'label' => 'Tipo Portugues',
                                )
                            ),
                            'choices'  => array(
                                'es' => array(
                                    'Nuevo – liso' => 'Nuevo – liso',
                                    'Usado' => 'Usado',
                                    'Pulimentado por el tránsito' => 'Pulimentado por el tránsito',
                                    'Con exceso de alquitrán' => 'Con exceso de alquitrán',
                                    'Apisonada con riego asfáltico' => 'Apisonada con riego asfáltico',
                                    'Suelta' => 'Suelta',
                                    'Compactadas' => 'Compactadas',
                                    'Machacada' => 'Machacada',
                                    'Liso' => 'Liso',
                                    'Con ranuras' => 'Con ranuras',
                                ),
                                 'pt' => array(
                                    'NOVO - LISO' => 'NOVO - LISO',
                                    'Usado' => 'Usado',
                                    'Gastado pelo trânsito' => 'Gastado pelo trânsito',
                                    'Com excesso de alquitrán' => 'Com excesso de alquitrán',
                                    'Achatada com rego asfáltico' => 'Achatada com rego asfáltico',
                                    'Solto' => 'Solto',
                                    'Achatada' => 'Achatada',
                                    'Esmagada' => 'Esmagada',
                                    'Liso' => 'Liso',
                                    'Com ranhuera' => 'Com ranhueras',
                                ),
                                 )
                        )
                    )
                ))
                ->add('secaMenos50kmhr')
                ->add('secaMas50Kmhr')
                ->add('humerdadMenos50Kmhr')
                ->add('humedadMas50Kmhr')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Tecspro\TablaBundle\Entity\TablaFriccion'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'tecspro_tablabundle_tablafriccion';
    }

}

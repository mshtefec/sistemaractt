<?php

namespace Tecspro\TablaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PropiedadTabla
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Tecspro\TablaBundle\Entity\PropiedadTablaRepository")
 */
class PropiedadTabla {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="fuente", type="string", length=255)
     */
    private $fuente;

    /**
     * @var string
     *
     * @ORM\Column(name="tabla", type="string", length=255)
     */
    private $tabla;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set fuente
     *
     * @param string $fuente
     * @return PropiedadTabla
     */
    public function setFuente($fuente) {
        $this->fuente = $fuente;

        return $this;
    }

    /**
     * Get fuente
     *
     * @return string 
     */
    public function getFuente() {
        return $this->fuente;
    }

    /**
     * Set tabla
     *
     * @param string $tabla
     * @return PropiedadTabla
     */
    public function setTabla($tabla) {
        $this->tabla = $tabla;

        return $this;
    }

    /**
     * Get tabla
     *
     * @return string 
     */
    public function getTabla() {
        return $this->tabla;
    }

}

<?php

namespace Tecspro\VehiculoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Tecspro\VehiculoBundle\Entity\Motocicleta;
use Tecspro\VehiculoBundle\Form\MotocicletaType;
use Tecspro\VehiculoBundle\Form\MotocicletaFilterType;

/**
 * Motocicleta controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/motocicleta")
 */
class MotocicletaController extends Controller
{
    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Tecspro/VehiculoBundle/Resources/config/Motocicleta.yml',
    );

    /**
     * Lists all Motocicleta entities.
     *
     * @Route("/", name="admin_motocicleta")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->config['filterType'] = new MotocicletaFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Motocicleta entity.
     *
     * @Route("/", name="admin_motocicleta_create")
     * @Method("POST")
     * @Template("VehiculoBundle:Motocicleta:new.html.twig")
     */
    public function createAction()
    {
        $this->config['newType'] = new MotocicletaType();
        $response = parent::createAction();

        return $response;
    }

    /**
     * Displays a form to create a new Motocicleta entity.
     *
     * @Route("/new", name="admin_motocicleta_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->config['newType'] = new MotocicletaType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Motocicleta entity.
     *
     * @Route("/{id}", name="admin_motocicleta_show", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Motocicleta entity.
     *
     * @Route("/{id}/edit", name="admin_motocicleta_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->config['editType'] = new MotocicletaType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Motocicleta entity.
     *
     * @Route("/{id}", name="admin_motocicleta_update")
     * @Method("PUT")
     * @Template("VehiculoBundle:Motocicleta:edit.html.twig")
     */
    public function updateAction($id)
    {
        $this->config['editType'] = new MotocicletaType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a Motocicleta entity.
     *
     * @Route("/{id}", name="admin_motocicleta_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter Motocicleta.
     *
     * @Route("/exporter/{format}", name="admin_motocicleta_export")
     */
    public function getExporter($format)
    {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Datatable Motocicleta.
     *
     * @Route("/get-table/", name="admin_motocicleta_table")
     */
    public function getDatatable()
    {
        $response = parent::getTable();

        return $response;
    }
 
}
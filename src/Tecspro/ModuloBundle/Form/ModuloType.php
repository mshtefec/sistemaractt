<?php

namespace Tecspro\ModuloBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * ModuloType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class ModuloType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('translations', 'a2lix_translations', array(
                    'required' => true,
                    'fields' => array(
                        'nombre' => array(
                            'field_type' => 'text',
                            'locale_options' => array(
                                'es' => array(
                                    'label' => 'Nombre Español',
                                ),
                                'pt' => array(
                                    'label' => 'Nombre Portugues',
                                )
                            )
                        )
                    )
                ))
                ->add('ArcIsActive', null, array(
                    'label' => 'Campos ARC activos?',
                    'translation_domain' => 'ModuloBundle',
                        )
                )
                ->add('tablas', 'choice', array(
                    'required' => false,
                    'multiple' => true,
                    'choices' => array(
                        'Fricción' => "TablaFriccion",
                        'Momento de Inercia' => "Inercia",
                        'Centro Gravedad' => "CentroGravedad",
                        'Rigidez' => "Rigidez",
                        'Coeficiente de Arrastre de peatón' => "CoeficienteArrastrePeaton",
                        'Eficiencia de Proyección' => "EficienciaProyeccion",
                    ),
                    // *this line is important*
                    'choices_as_values' => true,
                ))


        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Tecspro\ModuloBundle\Entity\Modulo'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'tecspro_modulobundle_modulo';
    }

}

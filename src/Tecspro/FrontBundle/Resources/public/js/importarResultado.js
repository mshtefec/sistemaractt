/* global Translator */

function importarResultado($key) {
    importar();
    var htmlSession = $templateFormImportarFormula($arrayImportar);
    $("#importarResultado").html(htmlSession);
    $("#modalImportar").modal('show');
    metodoImportar();
}

function armarArrayImportar() {
    for ($i = 0; $i < $formSession.length; $i++) {
        if ($formSession[$i]["key"] !== key) {
            $formjson = JSON.parse($formSession[$i]["formula"]);
            for ($j = 0; $j < $formjson.ecuacion.length; $j++) {
                for ($e = 0; $e < $formjson.ecuacion.length; $e++) {

                }
            }
        }
    }
}
function metodoImportar() {
    $(".btnAceptarImportar").click(function () {
        $variables = [];
        $(".btnSelectResultadoImportActive").each(function (index) {
            if ($(this).hasClass("active")) {
                $variables.push($(this).data("valor"));
            }
        });
        $($(".txtVariables" + $variableSeleccionada)[1]).val(Math.min.apply(null, $variables));
        $($(".txtVariables" + $variableSeleccionada)[2]).val(Math.max.apply(null, $variables));
        $($(".txtVariables" + $variableSeleccionada)[1]).change();
        $($(".txtVariables" + $variableSeleccionada)[2]).change();
        console.log($($(".txtVariables" + $variableSeleccionada)[1]));
        console.log($($(".txtVariables" + $variableSeleccionada)[2]));
        $("#variable" + $variableSeleccionada).hide();
        $("#autoincremental" + $variableSeleccionada).show();
        $("#modalImportar").modal('hide');
    });
    $(".btnAtrasImportar").click(function () {
        var htmlSession = $templateFormImportarFormula($arrayImportar);
        $("#importarResultado").html(htmlSession);
        $("#modalImportar").modal('show');
        metodoImportar();
    });
    $(".btnSelectResultadoImportActive").click(function () {
        if ($(this).hasClass("active")) {
            $(this).toggleClass("active");
        } else {
            $contador = 1;
            $(".btnSelectResultadoImportActive").each(function (index) {
                if ($(this).hasClass("active")) {
                    $contador++;
                }
            });
            if ($contador <= 2) {
                $(this).toggleClass("active");
            }
        }
    });
    $(".btnSelectResultadoImport").click(function () {
        if ($(this).data("tipo") === "ecuaciones") {
            $ecuaciones = findInArrayImportar($(this).data("key"));
            htmlSession = $templateFormImportarEcuaciones($ecuaciones);
            $("#importarResultado").html(htmlSession);
            metodoImportar();
        } else if ($(this).data("tipo") === "autoincremetar") {
            $ecuacion = findEcuacionInArrayImportar($(this).data("key"), $(this).data("idecuacion"));
            htmlSession = $templateFormImportarAutoicremental($ecuacion);
            $("#importarResultado").html(htmlSession);
            metodoImportar();
        } else if ($(this).data("tipo") === "simple") {
            $("#autoincremental" + $variableSeleccionada).hide();
            $("#variable" + $variableSeleccionada).show();
            $(".txtVariables" + $variableSeleccionada).first().val($(this).data("valor"));
            $(".txtVariables" + $variableSeleccionada).first().change();
            $("#modalImportar").modal('hide');
        } else if ($(this).data("tipo") === "seleccionUnidad") {
            if ($(this).data("autoicremental") === true) {
                $arrayTem = {};
                $ecuaciones = findEcuacionInArrayImportar($(this).data("key"), $(this).data("idecuacion"));
                $simbolo = $(this).data("simbolo");
                $unidadTemp = [];
                $arrayTem = jQuery.extend(true, {}, $ecuaciones);
                for ($j = 0; $j < $arrayTem.unidades.length; $j++) {
                    if ($arrayTem.unidades[$j].simbolo === $simbolo) {
                        $arrayTem.resultadoMedio = $arrayTem.unidades[$j].resultado;
                        $arrayTem.resultadoMinimo = $arrayTem.unidades[$j].resultadoMinimo;
                        $arrayTem.resultadoMaximo = $arrayTem.unidades[$j].resultadoMaximo;
                        $temp = $arrayTem.unidad;
                        $arrayTem.unidad = $arrayTem.unidades[$j].simbolo;
                        $arrayTem.unidades[$j].simbolo = $temp;
                    }
                }

                htmlSession = $templateFormImportarAutoicremental($arrayTem);
                $("#importarResultado").html(htmlSession);
                metodoImportar();
            } else {
                $("#autoincremental" + $variableSeleccionada).hide();
                $("#variable" + $variableSeleccionada).show();
                $(".txtVariables" + $variableSeleccionada).first().val($(this).data("valor"));
                $(".txtVariables" + $variableSeleccionada).first().change();
                $("#modalImportar").modal('hide');
            }
        }

    });
}



function findInArrayImportar(key) {
    for ($i = 0; $i < $arrayImportar.length; $i++) {
        if ($arrayImportar[$i].key === key) {
            return $arrayImportar[$i];
        }
    }


}
function findEcuacionInArrayImportar(key, ecuacion) {

    for ($i = 0; $i < $arrayImportar.length; $i++) {
        if ($arrayImportar[$i].key === key) {
            for ($j = 0; $j < $arrayImportar[$i].ecuaciones.length; $j++) {
                if ($arrayImportar[$i].ecuaciones[$j].idEcuacion === parseInt(ecuacion)) {
                    return $arrayImportar[$i].ecuaciones[$j];
                }
            }
        }
    }
}

function importar() {

    $arrayImportar = [];
    $arrayEcuaicion = [];
    for ($i = 0; $i < $formSession.length; $i++) {
        if ($formSession[$i]["key"] !== key) {
            formu = "";
            if (typeof $formSession[$i]["formula"] !== "object") {
                try {
                    formu = JSON.parse($formSession[$i]["formula"]);
                } catch ($e) {
                    formu = "";
                }
            } else {
                formu = $formSession[$i]["formula"];
            }

            if (formu !== "") {
                $arrayEcuaicion = [];

                for ($j = 0; $j < formu.ecuacion.length; $j++) {

                    if ((typeof formu.ecuacion[$j].resultado !== "undefined" && formu.ecuacion[$j].resultado.length > 0)) {
                        $ecuacion = {key: 0, idEcuacion: 0, ecuacion: "", autoincrementar: false, resultadoMedio: 0, resultadoMaximo: 0, resultadoMinimo: 0, resultado: 0, unidad: "", unidades: []};
                        $ecuacionNom = "";
                        if (formu.ecuacion[$j].translations instanceof Array) {
                            for ($t = 0; $t < formu.ecuacion[$j].translations.length; $t++) {
                                if (formu.ecuacion[$j].translations[$t]["locale"] === Translator.locale) {
                                    $ecuacionNom = formu.ecuacion[$j].translations[$t]["nombre"];
                                }
                            }
                        } else {
                            $ecuacionNom = formu.ecuacion[$j].translations[Translator.locale]["nombre"];
                        }

                        $ecuacion.ecuacion = $ecuacionNom;
                        $ecuacion.idEcuacion = formu.ecuacion[$j].id;
                        $ecuacion.key = $formSession[$i]["key"];

                        if ((typeof formu.ecuacion[$j].resultado_medio !== "undefined") && formu.ecuacion[$j].resultado_medio !== "") {
                            $ecuacion.autoincrementar = true;
                            $ecuacion.resultadoMedio = formu.ecuacion[$j].resultado_medio;
                            $ecuacion.resultadoMaximo = formu.ecuacion[$j].resultado_maximo;
                            $ecuacion.resultadoMinimo = formu.ecuacion[$j].resultado_minimo;
                        } else {
                            $ecuacion.resultado = formu.ecuacion[$j].resultado[0];
                        }
                        for ($u = 0; $u < formu.ecuacion[$j].unidad_resultado.length; $u++) {
                            if (formu.ecuacion[$j].unidad_resultado[$u].unidad.id === formu.ecuacion[$j].unidad_seleccionada) {

                                $ecuacion.unidad = formu.ecuacion[$j].unidad_resultado[$u].unidad.simbolo;
                            } else {
                                $operacion = formu.ecuacion[$j].unidad_resultado[$u].unidad.operacion;
                                $resultadoMinimo = 0;
                                $resultadoMaximo = 0;
                                if ($ecuacion.autoincrementar === false) {
                                    $resultado = math.round(math.eval($ecuacion.resultado + $operacion), 2);
                                } else {

                                    $resultadoMinimo = math.round(math.eval($ecuacion.resultadoMinimo + $operacion), 2);
                                    $resultado = math.round(math.eval($ecuacion.resultadoMedio + $operacion), 2);
                                    $resultadoMaximo = math.round(math.eval($ecuacion.resultadoMaximo + $operacion), 2);
                                }
                                $ecuacion.unidades.push({simbolo: formu.ecuacion[$j].unidad_resultado[$u].unidad.simbolo,
                                    resultado: $resultado, resultadoMinimo: $resultadoMinimo, resultadoMaximo: $resultadoMaximo});
                            }
                        }

                        $arrayEcuaicion.push($ecuacion);
                    }
                }

// $ecuacion.unidades.push({unidad: formu.ecuacion[$j].unidad_resultado[$e].unidad.simbolo, operacion: formu.ecuacion[$j].unidad_resultado[$e].unidad.operacion});
                if ($arrayEcuaicion.length > 0) {
                    $formulaNom = "";
                    if (formu.translations instanceof Array) {
                        for ($t = 0; $t < formu.translations.length; $t++) {
                            if (formu.translations[$t]["locale"] === Translator.locale) {
                                $formulaNom = formu.translations[$t]["nombre"];
                            }
                        }
                    } else {
//$respuesta = translation[0][variable];
                        $formulaNom = formu.translations[Translator.locale]["nombre"];
                    }

                    $arrayImportar.push({key: $formSession[$i]["key"], formula: $formulaNom, ecuaciones: $arrayEcuaicion, cantEcuaciones: $arrayEcuaicion.length});
                }
            }
        }
    }
}
function importar2() {
    $arrayImportar = [];
    $arrayEcuaicion = [];
    for ($i = 0; $i < $formSession.length; $i++) {
        if ($formSession[$i]["key"] !== key) {
            formu = "";
            try {
                formu = JSON.parse($formSession[$i]["formula"]);
            } catch ($e) {
                formu = "";
            }

            if (formu !== "") {
                $arrayEcuaicion = [];
                //que este guardado o que este en temporal
                if (typeof $ecuacionesTemp[$formSession[$i]["key"]] !== "undefined") {
                    for ($k = 0; $k < $ecuacionesTemp[$formSession[$i]["key"]].length; $k++) {
                        $ecuacion = {key: 0, idEcuacion: 0, ecuacion: "", autoincrementar: false, resultadoMedio: 0, resultadoMaximo: 0, resultadoMinimo: 0, resultado: 0, unidad: "", unidades: []};
                        $ecuacionNom = "";
                        if (formu.ecuacion[$k].translations instanceof Array) {
                            for ($t = 0; $t < formu.ecuacion[$k].translations.length; $t++) {
                                if (formu.ecuacion[$k].translations[$t]["locale"] === Translator.locale) {
                                    $ecuacionNom = formu.ecuacion[$k].translations[$t]["nombre"];
                                }
                            }
                        } else {
                            $ecuacionNom = formu.ecuacion[$k].translations[Translator.locale]["nombre"];
                        }

                        $ecuacion.ecuacion = $ecuacionNom;
                        $ecuacion.key = $formSession[$i]["key"];
                        $ecuacion.idEcuacion = $ecuacionesTemp[$formSession[$i]["key"]][$k].id;
                        if ($ecuacionesTemp[$formSession[$i]["key"]][$k]["autoincremental"] === true) {
                            $ecuacion.autoincrementar = true;
                            $ecuacion.resultadoMedio = $ecuacionesTemp[$formSession[$i]["key"]][$k]["resultadoMedio"];
                            $ecuacion.resultadoMaximo = $ecuacionesTemp[$formSession[$i]["key"]][$k]["resultadoMaximo"];
                            $ecuacion.resultadoMinimo = $ecuacionesTemp[$formSession[$i]["key"]][$k]["resultadoMinimo"];
                        } else {
                            $ecuacion.resultado = $ecuacionesTemp[$formSession[$i]["key"]][$k]["resultado"];
                        }

                        for ($u = 0; $u < formu.ecuacion[$k].unidad_resultado.length; $u++) {
                            if (formu.ecuacion[$k].unidad_resultado[$u].unidad.id === formu.ecuacion[$k].unidad_seleccionada) {
                                $ecuacion.unidad = formu.ecuacion[$k].unidad_resultado[$u].unidad.simbolo;
                            } else {
                                $operacion = formu.ecuacion[$k].unidad_resultado[$u].unidad.operacion;
                                $resultadoMinimo = 0;
                                $resultadoMaximo = 0;
                                if ($ecuacion.autoincrementar === false) {
                                    $resultado = math.round(math.eval($ecuacion.resultado + $operacion), 2);
                                } else {

                                    $resultadoMinimo = math.round(math.eval($ecuacion.resultadoMinimo + $operacion), 2);
                                    $resultado = math.round(math.eval($ecuacion.resultadoMedio + $operacion), 2);
                                    $resultadoMaximo = math.round(math.eval($ecuacion.resultadoMaximo + $operacion), 2);
                                }
                                $ecuacion.unidades.push({simbolo: formu.ecuacion[$k].unidad_resultado[$u].unidad.simbolo,
                                    resultado: $resultado, resultadoMinimo: $resultadoMinimo, resultadoMaximo: $resultadoMaximo});
                            }
                        }
                        $arrayEcuaicion.push($ecuacion);
                    }
                } else if ((typeof formu.ecuacion[0].resultado !== "undefined")) {

                    for ($j = 0; $j < formu.ecuacion.length; $j++) {
                        $ecuacion = {key: 0, idEcuacion: 0, ecuacion: "", autoincrementar: false, resultadoMedio: 0, resultadoMaximo: 0, resultadoMinimo: 0, resultado: 0, unidad: "", unidades: []};
                        $ecuacionNom = "";
                        if (formu.ecuacion[$j].translations instanceof Array) {
                            for ($t = 0; $t < formu.ecuacion[$j].translations.length; $t++) {
                                if (formu.ecuacion[$j].translations[$t]["locale"] === Translator.locale) {
                                    $ecuacionNom = formu.ecuacion[$j].translations[$t]["nombre"];
                                }
                            }
                        } else {
                            $ecuacionNom = formu.ecuacion[$j].translations[Translator.locale]["nombre"];
                        }

                        $ecuacion.ecuacion = $ecuacionNom;
                        $ecuacion.idEcuacion = formu.ecuacion[$j].id;
                        $ecuacion.key = $formSession[$i]["key"];
                        if ((typeof formu.ecuacion[$j].resultadoMedio !== "undefined") && formu.ecuacion[$j].resultadoMedio !== "") {
                            $ecuacion.autoincrementar = true;
                            $ecuacion.resultadoMedio = formu.ecuacion[$j].resultado_medio;
                            $ecuacion.resultadoMaximo = formu.ecuacion[$j].resultado_maximo;
                            $ecuacion.resultadoMinimo = formu.ecuacion[$j].resultado_minimo;
                        } else {
                            $ecuacion.resultado = formu.ecuacion[$j].resultado[0];
                        }
                        for ($u = 0; $u < formu.ecuacion[$j].unidad_resultado.length; $u++) {
                            if (formu.ecuacion[$j].unidad_resultado[$u].unidad.id === formu.ecuacion[$j].unidad_seleccionada) {

                                $ecuacion.unidad = formu.ecuacion[$j].unidad_resultado[$u].unidad.simbolo;
                            } else {
                                $operacion = formu.ecuacion[$j].unidad_resultado[$u].unidad.operacion;
                                $resultadoMinimo = 0;
                                $resultadoMaximo = 0;
                                if ($ecuacion.autoincrementar === false) {
                                    $resultado = math.round(math.eval($ecuacion.resultado + $operacion), 2);
                                } else {

                                    $resultadoMinimo = math.round(math.eval($ecuacion.resultadoMinimo + $operacion), 2);
                                    $resultado = math.round(math.eval($ecuacion.resultadoMedio + $operacion), 2);
                                    $resultadoMaximo = math.round(math.eval($ecuacion.resultadoMaximo + $operacion), 2);
                                }
                                $ecuacion.unidades.push({simbolo: formu.ecuacion[$j].unidad_resultado[$u].unidad.simbolo,
                                    resultado: $resultado, resultadoMinimo: $resultadoMinimo, resultadoMaximo: $resultadoMaximo});
                            }
                        }

                        $arrayEcuaicion.push($ecuacion);
                    }
                }

// $ecuacion.unidades.push({unidad: formu.ecuacion[$j].unidad_resultado[$e].unidad.simbolo, operacion: formu.ecuacion[$j].unidad_resultado[$e].unidad.operacion});
                if ($arrayEcuaicion.length > 0) {
                    $formulaNom = "";
                    if (formu.translations instanceof Array) {
                        for ($t = 0; $t < formu.translations.length; $t++) {
                            if (formu.translations[$t]["locale"] === Translator.locale) {
                                $formulaNom = formu.translations[$t]["nombre"];
                            }
                        }
                    } else {
//$respuesta = translation[0][variable];
                        $formulaNom = formu.translations[Translator.locale]["nombre"];
                    }

                    $arrayImportar.push({key: $formSession[$i]["key"], formula: $formulaNom, ecuaciones: $arrayEcuaicion, cantEcuaciones: $arrayEcuaicion.length});
                }
            }
        }
    }
}



<?php

namespace Tecspro\ModuloBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Tecspro\ModuloBundle\Form\GrupoType;
use Tecspro\ModuloBundle\Form\EcuacionType;
use Tecspro\ModuloBundle\Form\ReferenciaType;
use Tecspro\ModuloBundle\Form\VariablesType;

/**
 * FormulaType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class FormulaType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('translations', 'a2lix_translations', array(
                    'required_locales' => array('es', 'pt'),
                    'fields' => array(
                        'nombre' => array(
                            'field_type' => 'text',
                            'locale_options' => array(
                                'es' => array(
                                    'label' => 'Nombre Español',
                                ),
                                'pt' => array(
                                    'label' => 'Nombre Portugues',
                                )
                            ),
                        ),
                        'file' => array(
                            'field_type' => 'file',
                            // 'file_path' => 'webPath',
                            'required' => false,
                            'locale_options' => array(
                                'es' => array(
                                    'label' => 'Referencia Español',
                                ),
                                'pt' => array(
                                    'label' => 'Referencia Portugues',
                                )
                            )
                        ),
                        'filePath' => array(
                            'field_type' => 'hidden',
                        )
                    )
                ))
                ->add('autoincremental', null, array(
                    'label' => 'Autoincremental',
                    'required' => false,
                    'label_attr' => array(
                        'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                    ),
                    'attr' => array(
                        'col' => "col-lg-6 col-md-6 col-sm-12 col-xs-12",
                    ),
                ))
                ->add('publicar', null, array(
                    'label' => 'Publicar',
                    'required' => false,
                    'label_attr' => array(
                        'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                    ),
                    'attr' => array(
                        'col' => "col-lg-6 col-md-6 col-sm-12 col-xs-12",
                    ),
                ))
                ->add('file', 'mws_field_file', array(
                    'required' => false,
                    'file_path' => 'webPath',
                    'label' => 'Image',
                        //'show_path' => true
                ))
                ->add('variables', 'collection', array(
                    'type' => new VariablesType(),
                    'label' => false,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'prototype_name' => '__nameembed__',
                    'by_reference' => false,
                ))
                ->add('ecuacion', 'collection', array(
                    'type' => new EcuacionType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'required' => true,
                    'by_reference' => false,
                    'prototype_name' => '__nameembed__',
                    'label' => false,
                ))
                ->add('orden')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Tecspro\ModuloBundle\Entity\Formula',
            'cascade_validation' => true,
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'tecspro_modulobundle_formula';
    }

}

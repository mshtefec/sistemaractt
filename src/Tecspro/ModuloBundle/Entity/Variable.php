<?php

namespace Tecspro\ModuloBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Exclude;
/**
 * Variable
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Variable {

    use \A2lix\I18nDoctrineBundle\Doctrine\ORM\Util\Translatable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Type("integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="simbolo", type="string", length=255)
     *  @Type("string")
     */
    private $simbolo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="autoincrementar", type="boolean")
     *  @Type("boolean")
     */
    private $autoincrementar;

    /**
     * @var integer
     *
     * @ORM\Column(name="particiones", type="integer")
     * @Type("integer")
     */
    private $particiones;

    /**
     * @var string
     *
     * @ORM\Column(name="valorDesde", type="decimal")
     * @Type("float")
     */
    private $valorDesde;

    /**
     * @var string
     *
     * @ORM\Column(name="valorHasta", type="decimal")
     * @Type("float")
     */
    private $valorHasta;

    /**
     * @ORM\ManyToOne(targetEntity="Unidad",cascade={"persist"})
     * @ORM\JoinColumn(name="unidad_id", referencedColumnName="id")
     * @Type("Tecspro\ModuloBundle\Entity\Unidad")
     */
    private $unidad;

    /**
     * @ORM\ManyToOne(targetEntity="Formula", inversedBy="variables",cascade={"persist"})
     * @ORM\JoinColumn(name="formula_id", referencedColumnName="id")
     * @Exclude
     */
    private $formula;

    /**
     * @Type("array")
     */
    private $valor;

    /**
     * @Type("ArrayCollection<Tecspro\ModuloBundle\Entity\VariableTranslation>")
     */
    protected $translations;

    public function __construct() {
        $this->autoincrementar = false;
        $this->particiones = 0;
        $this->valorDesde = 0;
        $this->valorHasta = 0;
        $this->translations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set simbolo
     *
     * @param string $simbolo
     * @return Variable
     */
    public function setSimbolo($simbolo) {
        $this->simbolo = $simbolo;

        return $this;
    }

    /**
     * Get simbolo
     *
     * @return string 
     */
    public function getSimbolo() {
        return $this->simbolo;
    }

    /**
     * Set unidad
     *
     * @param string $unidad
     * @return Variable
     */
    public function setUnidad($unidad) {
        $this->unidad = $unidad;

        return $this;
    }

    /**
     * Get unidad
     *
     * @return string 
     */
    public function getUnidad() {
        return $this->unidad;
    }

    /**
     * Set autoincrementar
     *
     * @param boolean $autoincrementar
     * @return Variable
     */
    public function setAutoincrementar($autoincrementar) {
        $this->autoincrementar = $autoincrementar;

        return $this;
    }

    /**
     * Get autoincrementar
     *
     * @return boolean 
     */
    public function getAutoincrementar() {
        return $this->autoincrementar;
    }

    /**
     * Set particiones
     *
     * @param integer $particiones
     * @return Variable
     */
    public function setParticiones($particiones) {
        $this->particiones = $particiones;

        return $this;
    }

    /**
     * Get particiones
     *
     * @return integer 
     */
    public function getParticiones() {
        return $this->particiones;
    }

    /**
     * Set valorDesde
     *
     * @param string $valorDesde
     * @return Variable
     */
    public function setValorDesde($valorDesde) {
        $this->valorDesde = $valorDesde;

        return $this;
    }

    /**
     * Get valorDesde
     *
     * @return string 
     */
    public function getValorDesde() {
        return $this->valorDesde;
    }

    /**
     * Set valorHasta
     *
     * @param string $valorHasta
     * @return Variable
     */
    public function setValorHasta($valorHasta) {
        $this->valorHasta = $valorHasta;

        return $this;
    }

    /**
     * Get valorHasta
     *
     * @return string 
     */
    public function getValorHasta() {
        return $this->valorHasta;
    }

    /**
     * Set formula
     *
     * @param \Tecspro\ModuloBundle\Entity\Formula $formula
     * @return Variable
     */
    public function setFormula(\Tecspro\ModuloBundle\Entity\Formula $formula = null) {
        $this->formula = $formula;

        return $this;
    }

    /**
     * Get formula
     *
     * @return \Tecspro\ModuloBundle\Entity\Formula 
     */
    public function getFormula() {
        return $this->formula;
    }

    /**
     * Set valor
     *
     * @param array $valor
     * @return Variable
     */
    public function setValor($valor) {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return array 
     */
    public function getValor() {
        return $this->valor;
    }

}

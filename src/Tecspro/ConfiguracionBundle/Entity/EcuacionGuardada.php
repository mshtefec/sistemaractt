<?php

namespace Tecspro\ConfiguracionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EcuacionGuardada
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class EcuacionGuardada
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ecuacion", type="string", length=255)
     */
    private $ecuacion;

    /**
     * @var string
     *
     * @ORM\Column(name="unidadResultado", type="decimal")
     */
    private $unidadResultado;

    /**
     * @var string
     *
     * @ORM\Column(name="resultadoMaximo", type="decimal")
     */
    private $resultadoMaximo;

    /**
     * @var string
     *
     * @ORM\Column(name="resultadoMinimo", type="decimal")
     */
    private $resultadoMinimo;

    /**
     * @var string
     *
     * @ORM\Column(name="resultado", type="decimal")
     */
    private $resultado;

    /**
     * @var \Tecspro\ConfiguracionBundle\Entity\FormulaGuardada
     *
     * @ORM\ManyToOne(targetEntity="Tecspro\ConfiguracionBundle\Entity\FormulaGuardada", inversedBy="ecuacionGuardada")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="formulaGuardada_id", referencedColumnName="id")
     * })
     */
    private $formulaGuardada;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ecuacion
     *
     * @param string $ecuacion
     * @return EcuacionGuardada
     */
    public function setEcuacion($ecuacion)
    {
        $this->ecuacion = $ecuacion;

        return $this;
    }

    /**
     * Get ecuacion
     *
     * @return string 
     */
    public function getEcuacion()
    {
        return $this->ecuacion;
    }

    /**
     * Set unidadResultado
     *
     * @param string $unidadResultado
     * @return EcuacionGuardada
     */
    public function setUnidadResultado($unidadResultado)
    {
        $this->unidadResultado = $unidadResultado;

        return $this;
    }

    /**
     * Get unidadResultado
     *
     * @return string 
     */
    public function getUnidadResultado()
    {
        return $this->unidadResultado;
    }

    /**
     * Set resultadoMaximo
     *
     * @param string $resultadoMaximo
     * @return EcuacionGuardada
     */
    public function setResultadoMaximo($resultadoMaximo)
    {
        $this->resultadoMaximo = $resultadoMaximo;

        return $this;
    }

    /**
     * Get resultadoMaximo
     *
     * @return string 
     */
    public function getResultadoMaximo()
    {
        return $this->resultadoMaximo;
    }

    /**
     * Set resultadoMinimo
     *
     * @param string $resultadoMinimo
     * @return EcuacionGuardada
     */
    public function setResultadoMinimo($resultadoMinimo)
    {
        $this->resultadoMinimo = $resultadoMinimo;

        return $this;
    }

    /**
     * Get resultadoMinimo
     *
     * @return string 
     */
    public function getResultadoMinimo()
    {
        return $this->resultadoMinimo;
    }

    /**
     * Set resultado
     *
     * @param string $resultado
     * @return EcuacionGuardada
     */
    public function setResultado($resultado)
    {
        $this->resultado = $resultado;

        return $this;
    }

    /**
     * Get resultado
     *
     * @return string 
     */
    public function getResultado()
    {
        return $this->resultado;
    }
}

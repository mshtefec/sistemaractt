<?php

namespace Tecspro\TablaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 * TablaFriccion
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Tecspro\TablaBundle\Entity\TablaFriccionRepository")
 */
class TablaFriccion {
    
    use \A2lix\I18nDoctrineBundle\Doctrine\ORM\Util\Translatable;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="secaMenos50kmhr", type="string", length=255, nullable=true)
     */
    private $secaMenos50kmhr;

    /**
     * @var string
     *
     * @ORM\Column(name="secaMas50Kmhr", type="string", length=255, nullable=true)
     */
    private $secaMas50Kmhr;

    /**
     * @var string
     *
     * @ORM\Column(name="HumerdadMenos50Kmhr", type="string", length=255, nullable=true)
     */
    private $humerdadMenos50Kmhr;

    /**
     * @var string
     *
     * @ORM\Column(name="humedadMas50Kmhr", type="string", length=255, nullable=true)
     */
    private $humedadMas50Kmhr;
   

    protected $translations;

    
     /**
     * Constructor
     */
    public function __construct() {
        $this->translations = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set secaMenos50kmhr
     *
     * @param string $secaMenos50kmhr
     * @return TablaFriccion
     */
    public function setSecaMenos50kmhr($secaMenos50kmhr)
    {
        $this->secaMenos50kmhr = $secaMenos50kmhr;

        return $this;
    }

    /**
     * Get secaMenos50kmhr
     *
     * @return string 
     */
    public function getSecaMenos50kmhr()
    {
        return $this->secaMenos50kmhr;
    }

    /**
     * Set secaMas50Kmhr
     *
     * @param string $secaMas50Kmhr
     * @return TablaFriccion
     */
    public function setSecaMas50Kmhr($secaMas50Kmhr)
    {
        $this->secaMas50Kmhr = $secaMas50Kmhr;

        return $this;
    }

    /**
     * Get secaMas50Kmhr
     *
     * @return string 
     */
    public function getSecaMas50Kmhr()
    {
        return $this->secaMas50Kmhr;
    }

    /**
     * Set humerdadMenos50Kmhr
     *
     * @param string $humerdadMenos50Kmhr
     * @return TablaFriccion
     */
    public function setHumerdadMenos50Kmhr($humerdadMenos50Kmhr)
    {
        $this->humerdadMenos50Kmhr = $humerdadMenos50Kmhr;

        return $this;
    }

    /**
     * Get humerdadMenos50Kmhr
     *
     * @return string 
     */
    public function getHumerdadMenos50Kmhr()
    {
        return $this->humerdadMenos50Kmhr;
    }

    /**
     * Set humedadMas50Kmhr
     *
     * @param string $humedadMas50Kmhr
     * @return TablaFriccion
     */
    public function setHumedadMas50Kmhr($humedadMas50Kmhr)
    {
        $this->humedadMas50Kmhr = $humedadMas50Kmhr;

        return $this;
    }

    /**
     * Get humedadMas50Kmhr
     *
     * @return string 
     */
    public function getHumedadMas50Kmhr()
    {
        return $this->humedadMas50Kmhr;
    }


}

<?php

namespace Tecspro\VehiculoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Tecspro\VehiculoBundle\Entity\Auto;
use Tecspro\VehiculoBundle\Form\AutoType;
use Tecspro\VehiculoBundle\Form\AutoFilterType;

/**
 * Auto controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/auto")
 */
class AutoController extends Controller
{
    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Tecspro/VehiculoBundle/Resources/config/Auto.yml',
    );

    /**
     * Lists all Auto entities.
     *
     * @Route("/", name="admin_auto")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->config['filterType'] = new AutoFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Auto entity.
     *
     * @Route("/", name="admin_auto_create")
     * @Method("POST")
     * @Template("VehiculoBundle:Auto:new.html.twig")
     */
    public function createAction()
    {
        $this->config['newType'] = new AutoType();
        $response = parent::createAction();

        return $response;
    }

    /**
     * Displays a form to create a new Auto entity.
     *
     * @Route("/new", name="admin_auto_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->config['newType'] = new AutoType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Auto entity.
     *
     * @Route("/{id}", name="admin_auto_show", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Auto entity.
     *
     * @Route("/{id}/edit", name="admin_auto_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->config['editType'] = new AutoType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Auto entity.
     *
     * @Route("/{id}", name="admin_auto_update")
     * @Method("PUT")
     * @Template("VehiculoBundle:Auto:edit.html.twig")
     */
    public function updateAction($id)
    {
        $this->config['editType'] = new AutoType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a Auto entity.
     *
     * @Route("/{id}", name="admin_auto_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter Auto.
     *
     * @Route("/exporter/{format}", name="admin_auto_export")
     */
    public function getExporter($format)
    {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Datatable Auto.
     *
     * @Route("/get-table/", name="admin_auto_table")
     */
    public function getDatatable()
    {
        $response = parent::getTable();

        return $response;
    }
 
}
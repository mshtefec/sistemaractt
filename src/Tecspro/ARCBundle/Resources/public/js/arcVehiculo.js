/**
 *  Documento se carga on ready y bindea las funciones
 * 
 * @return void
 */
jQuery(document).ready(function () {
    collectionArcVehiculo = jQuery('.vehiculos');

    jQuery('.add-arcVehiculo-form').click(function (e) {
        e.preventDefault()
        addForm(collectionArcVehiculo, jQuery('.vehiculos'));
    });
    jQuery('.vehiculos').delegate('.delete-form', 'click', function (e) {
        // prevent the link from creating a "#" on  the URL
        e.preventDefault();
        // remove the li for the tag form
        jQuery(this).closest('.rowremove').remove();
    });

});
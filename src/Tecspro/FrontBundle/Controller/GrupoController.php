<?php

namespace Tecspro\FrontBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Tecspro\ModuloBundle\Entity\Modulo;
use Tecspro\ModuloBundle\Form\ModuloType;
use Tecspro\ModuloBundle\Form\ModuloFilterType;
use Symfony\Component\HttpFoundation\JsonResponse;
use JMS\Serializer\SerializerBuilder;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Dompdf\Dompdf;
use Symfony\Component\Process\Process;

/**
 * @Route("/")
 */
class GrupoController extends Controller {

    private $MRU = 'MRU';
    private $MRUMRV = 'MRU+MRV';
    private $circulando = 0;
    private $desacelerando = 1;
    private $colicionado = 2;
    private $mS = 'm/s';
    private $kmH = 'Km/h';
    private $ES = 0;
    private $PT = 1;

    /**
     * @Route("/lista-grupos", name="lista_grupos")
     */
    public function listaGruposAction() {
        $request = $this->getRequest();
        $moduloSeleccionado = $this->get('variableSession')->isModuloSeleccionado($request);
        $entities = null;
        if ($moduloSeleccionado['resultado'] == true) {
            $em = $this->getDoctrine()->getManager();
            $entities = $em->getRepository('ModuloBundle:Categoria')->findCategoriaByModulo($moduloSeleccionado['modulo']);
        }
        return $this->render(
                        'TecsproFrontBundle:Form:macro-list-grupos-by-categoria.html.twig', array('entities' => $entities)
        );
    }

    /**
     * @Route("/lista-grupos-seleccionados", name="lista_grupos_seleccionados")
     */
    public function listaGruposSeleccionadoAction() {
        $request = $this->getRequest();
        $session = $request->getSession();
        $formulaSession = $session->get('formulas', null);

        return $this->render(
                        'TecsproFrontBundle:Form:macro-modal-grupo-seleccionados.html.twig', array('entities' => $formulaSession)
        );
    }

    /**
     * @Route("/imprimir-ecuacion/", name="imprimir_grupos", options={"expose"=true})
     */
    public function imprimirAction() {
        $request = $this->getRequest();
        $session = $request->getSession();
        $em = $this->getDoctrine()->getManager();
        $proyectoSession = $session->get('proyecto', null);

        $_formSession = $request->request->all()["formSession"];
        $type = $request->request->all()["type"];
        $arc = $request->request->all()["arc"];

        $formulaSession = json_decode($_formSession, true);

        $entity = null;

        //buscar datos de proyecto
        if (is_null($proyectoSession) == false) {
            $entity = $em->getRepository('TecsproFrontBundle:Proyecto')->find($proyectoSession);
        }

        if ($type == "docx" || $type == "odt" || $type == "xlsx" || $type == "pptx" || $type == "pdf") {
            $phpWord = $this->formatearTexto($formulaSession, $entity, $arc, $request->getLocale());
            $properties = $phpWord->getDocInfo();
            $properties->setTitle('Rutina de cómputos para el análisis de las colisiones');
            $tmp = $this->container->getParameter("kernel.root_dir") . "/../web/uploads/tmp/" . $this->getUser()->getId();
            if (!file_exists($tmp)) {

                mkdir($tmp, 0755);
            }

            if ($type == "docx") {

                $titulo = "Rutina_de_computos";

                $pdfNombre = $titulo . ".docx";
                $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');

                //$objWriter->setTempDir($this->container->getParameter("kernel.root_dir") . "/../web/uploads/tmp/");
                $objWriter->save($tmp . "/" . $pdfNombre);
                $contentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
            } else if ($type == "odt") {

                $titulo = "Rutina_de_computos";
                $pdfNombre = $titulo . ".docx";

                $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
                $objWriter->save($tmp . "/" . $pdfNombre);
                $contentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";

                $pdfNombre = "Rutina_de_computos.odt";

                $convertidor = $this->container->getParameter("kernel.root_dir") . "/../src/AppBundle/" . "DC.py";

                $archivo_docx = $this->container->getParameter("kernel.root_dir") . "/../web/uploads/tmp/" . $this->getUser()->getId() . "/Rutina_de_computos.docx";

                $archivo_odt = $this->container->getParameter("kernel.root_dir") . "/../web/uploads/tmp/" . $this->getUser()->getId() . "/Rutina_de_computos.odt";

                $command = $convertidor . " " . $archivo_docx . " " . $archivo_odt;

                $process = new Process("/usr/bin/python " . $command);

                $process->run();

                //ladybug_dump_die($this->container->getParameter("kernel.root_dir"));
            } else if ($type == "pdf") {

                $titulo = "Rutina_de_computos";
                $pdfNombre = $titulo . ".docx";

                $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
                $objWriter->save($tmp . "/" . $pdfNombre);
                $contentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";

                $pdfNombre = "Rutina_de_computos.pdf";

                $convertidor = $this->container->getParameter("kernel.root_dir") . "/../src/AppBundle/" . "DC.py";

                $archivo_docx = $this->container->getParameter("kernel.root_dir") . "/../web/uploads/tmp/" . $this->getUser()->getId() . "/Rutina_de_computos.docx";

                $archivo_odt = $this->container->getParameter("kernel.root_dir") . "/../web/uploads/tmp/" . $this->getUser()->getId() . "/Rutina_de_computos.pdf";

                $command = $convertidor . " " . $archivo_docx . " " . $archivo_odt;

                $process = new Process("/usr/bin/python " . $command);

                $process->run();
            }

            $response = new JsonResponse();
            $response->setData(array(
                'archivo' => $this->getUser()->getId() . "/" . $pdfNombre
            ));

            return $response;
            //$response->setContent(file_put_contents($this->container->getParameter("kernel.root_dir") . "/../web/uploads/tmp/" . $titulo,$titulo));
            //   return $response;
        } else {
            $response = new JsonResponse();
            $response->setData(false);

            return $response;
        }
    }

    private function formatearTexto($entities, $proyecto, $arc, $_locale) {

        $locale = $this->ES;

        if ($_locale == "pt") {
            $locale = $this->PT;
        }

        $serializer = SerializerBuilder::create()->build();

        $table_style = array(
            'unit' => \PhpOffice\PhpWord\Style\Table::WIDTH_PERCENT,
            'width' => 100 * 50,
            'borderSize' => 6,
            'borderColor' => '999999',
            'cellMargin' => 80,
        );
        $table_style_auto = array(
            'unit' => \PhpOffice\PhpWord\Style\Table::WIDTH_AUTO,
            'width' => 100 * 50,
            'borderSize' => 6,
            'borderColor' => '999999',
            'cellMargin' => 80,
        );

        $styleSombreado = array('bgColor' => 'cccccc');
        $styleSuccess = array('bgColor' => 'DFF0D8');
        $styleWarning = array('bgColor' => 'FCF8E3');
        $styleDanger = array('bgColor' => 'F2DEDE');
        $cellResultado = array('size' => 8);

        $phpWord = new \PhpOffice\PhpWord\PhpWord();

        $phpWord->setDefaultFontName('Microsoft Sans Serif');
        $phpWord->setDefaultParagraphStyle(
                array(
                    'spaceLine' => \PhpOffice\PhpWord\Shared\Converter::INCH_TO_POINT,
                    'spacing' => 38,
                )
        );
        /* Note: any element you append to a document must reside inside of a Section. */

// Adding an empty Section to the document...
        $section = $phpWord->addSection();
        $section->addText(
                htmlspecialchars(
                        $this->get('translator')->trans('Rutina_de_computos_para_el_analisis_de_las_colisiones', array(), 'TecsproFrontBundle')
                ), array('bold' => true), array('align' => 'center')
        );

        $section->addText(
                htmlspecialchars(
                        $this->get('translator')->trans('Detalle_de_los_modelos_y_valores_de_sus_variables_adoptados_para_la_estimacion_de_energias_y_velocidades', array(), 'TecsproFrontBundle')
                )
        );

        if (is_null($proyecto)) {
            $proyectoNombre = "";
            $proyectoFecha = (new \DateTime())->format("d/m/Y");
            $proyectoComentario = "";
        } else {
            $proyectoNombre = $proyecto->getNombre();
            $proyectoFecha = $proyecto->getFecha()->format("d/m/Y");
            $proyectoComentario = $proyecto->getComentario();
        }
        $section->addText(
                htmlspecialchars(
                        $this->get('translator')->trans('Nombre_del_caso', array(), 'TecsproFrontBundle') . ": " . $proyectoNombre
                )
        );
        $section->addText(
                htmlspecialchars(
                        $this->get('translator')->trans('fecha', array(), 'TecsproFrontBundle') . ": " . $proyectoFecha
                )
        );

        $section->addText(
                htmlspecialchars(
                        $this->get('translator')->trans('Parte_Involucrada', array(), 'TecsproFrontBundle') . ": "
                )
        );
        $contador = 1;
        if (is_null($proyecto) == false) {
            foreach ($proyecto->getPartesInvolucradas() as $parteInvolucrada) {
                $section->addText(
                        htmlspecialchars(
                                $contador . "° " . $this->get('translator')->trans('Parte_Involucrada', array(), 'TecsproFrontBundle') . ": " . $parteInvolucrada
                        )
                );
                $contador++;
            }
            $section->addTextBreak(1);
        }

        $section->addText(
                htmlspecialchars(
                        $this->get('translator')->trans('Comentarios', array(), 'TecsproFrontBundle')
                ), array('underline' => "single", 'bold' => true)
        );



        $section->addText(
                htmlspecialchars(
                        $proyectoComentario
                )
        );
        $section->addTextBreak(1);

        $contador = 1;

        foreach ($entities as $entityss) {
            if (is_array($entityss["formula"])) {
                $stringFormula = $serializer->serialize($entityss["formula"], 'json');
            } else {
                $stringFormula = $entityss["formula"];
            }

            $formula = $serializer->deserialize($stringFormula, 'Tecspro\ModuloBundle\Entity\Formula', 'json');
           
            $section->addText(
                    htmlspecialchars(
                            $contador . ". " . $formula->getTranslations()->get($locale)->getNombre() . "\n"
                    ), array('bold' => true)
            );

            if (is_null($formula->getNombrePersonalizado()) == false && $formula->getNombrePersonalizado() != "") {
                $section->addText(
                        htmlspecialchars(
                                $formula->getNombrePersonalizado() . "\n"
                        )
                );
            }

            //imagen
            if (is_null($formula->getWebPath()) == false) {

                $section->addImage(
                        $formula->getWebPath()
                );
            }

            $section->addTextBreak(1);

            //descripcion de las variables

            $tablaDescripcion = $section->addTable($table_style);

            $tablaDescripcion->addRow();
            $tablaDescripcion->addCell(2000, $styleSombreado)->addText(htmlspecialchars($this->get('translator')->trans('Nombre_de_la_variable', array(), 'TecsproFrontBundle')));
            $tablaDescripcion->addCell(2000, $styleSombreado)->addText(htmlspecialchars($this->get('translator')->trans('Magnitud', array(), 'TecsproFrontBundle')));
            $tablaDescripcion->addCell(2000, $styleSombreado)->addText(htmlspecialchars($this->get('translator')->trans('Nombre_de_la_Unidad', array(), 'TecsproFrontBundle')));
            $tablaDescripcion->addCell(2000, $styleSombreado)->addText(htmlspecialchars($this->get('translator')->trans('Simbolo', array(), 'TecsproFrontBundle')));

            $section->addTextBreak(1);
            $section->addText(
                    htmlspecialchars(
                            $this->get('translator')->trans('Valores_Ingresados', array(), 'TecsproFrontBundle') . ":"
                    ), array('bold' => true)
            );

            $tablaIngresados = $section->addTable($table_style);
            $tablaIngresados->addRow();
            $tablaIngresados->addCell(2000, $styleSombreado)->addText(htmlspecialchars($this->get('translator')->trans('Variable', array(), 'TecsproFrontBundle')));
            $tablaIngresados->addCell(2000, $styleSombreado)->addText(htmlspecialchars($this->get('translator')->trans('Valor', array(), 'TecsproFrontBundle')));
            $tablaIngresados->addCell(2000, $styleSombreado)->addText(htmlspecialchars($this->get('translator')->trans('Unidad', array(), 'TecsproFrontBundle')));

            //constantes
            $columnas = array();
            $filas = array();
            $autoincrementar = 0;
            if ($formula->getAutoincremental()) {
                foreach ($formula->getVariables() as $key2 => $variable) {
                    $index = $key2 + 1;
                    //descripcion
                    $tablaDescripcion->addRow();
                    $tablaDescripcion->addCell(2000)->addText(htmlspecialchars($variable->getSimbolo() . $index . " " . $variable->getTranslations()->get($locale)->getNombre()));
                    $tablaDescripcion->addCell(2000)->addText(htmlspecialchars($variable->getUnidad()->getTranslations()->get($locale)->getMagnitud()));
                    $tablaDescripcion->addCell(2000)->addText(htmlspecialchars($variable->getUnidad()->getTranslations()->get($locale)->getNombre()));
                    $tablaDescripcion->addCell(2000)->addText(htmlspecialchars($variable->getUnidad()->getSimbolo()));

                    //valores
                    $tablaIngresados->addRow();
                    $tablaIngresados->addCell(2000)->addText(htmlspecialchars($variable->getSimbolo() . $index . " " . $variable->getTranslations()->get($locale)->getNombre()));

                    if ($variable->getAutoincrementar()) {
                        $autoincrementar++;
                        if ($autoincrementar == 1) {
                            $tablaIngresados->addCell(2000)->addText(htmlspecialchars($variable->getSimbolo() . $index . " (Filas)"));
                            $columnas = array(
                                'variable' => $variable->getSimbolo() . $index,
                                'valores' => $variable->getValor()
                            );
                        } else if ($autoincrementar == 2) {
                            $tablaIngresados->addCell(2000)->addText(htmlspecialchars($variable->getSimbolo() . $index . " (Columnas)"));
                            $filas = array(
                                'variable' => $variable->getSimbolo() . $index,
                                'valores' => $variable->getValor()
                            );
                        }
                    } else {
                        if (is_array($variable->getValor()[0])) {
                            $tablaIngresados->addCell(2000)->addText(htmlspecialchars($variable->getValor()[0][0]));
                        } else {
                            $tablaIngresados->addCell(2000)->addText(htmlspecialchars($variable->getValor()[0]));
                        }
                    }
                    $tablaIngresados->addCell(2000)->addText(htmlspecialchars($variable->getUnidad()->getSimbolo()));
                }
            } else {
                foreach ($formula->getVariables() as $key2 => $variable) {

                    //descripcion
                    $tablaDescripcion->addRow();
                    $tablaDescripcion->addCell(2000)->addText(htmlspecialchars($variable->getSimbolo() . " " . $variable->getTranslations()->get($locale)->getNombre()));
                    $tablaDescripcion->addCell(2000)->addText(htmlspecialchars($variable->getUnidad()->getTranslations()->get($locale)->getMagnitud()));
                    $tablaDescripcion->addCell(2000)->addText(htmlspecialchars($variable->getUnidad()->getTranslations()->get($locale)->getNombre()));
                    $tablaDescripcion->addCell(2000)->addText(htmlspecialchars($variable->getUnidad()->getSimbolo()));
                    
                    //valores
                    $tablaIngresados->addRow();
                    $tablaIngresados->addCell(2000)->addText(htmlspecialchars($variable->getSimbolo() . " " . $variable->getTranslations()->get($locale)->getNombre()));

                    if ($variable->getAutoincrementar()) {
                        $autoincrementar++;
                        if ($autoincrementar == 1) {
                            $tablaIngresados->addCell(2000)->addText(htmlspecialchars($variable->getSimbolo() . " (Filas)"));
                            $columnas = array(
                                'variable' => $variable->getSimbolo(),
                                'valores' => $variable->getValor()
                            );
                        } else if ($autoincrementar == 2) {
                            $tablaIngresados->addCell(2000)->addText(htmlspecialchars($variable->getSimbolo() . " (Columnas)"));
                            $filas = array(
                                'variable' => $variable->getSimbolo(),
                                'valores' => $variable->getValor()
                            );
                        }
                    } else {
                        if (is_array($variable->getValor()[0])) {
                            $tablaIngresados->addCell(2000)->addText(htmlspecialchars($variable->getValor()[0][0]));
                        } else {

                            $tablaIngresados->addCell(2000)->addText(htmlspecialchars($variable->getValor()[0]));
                        }
                    }
                    $tablaIngresados->addCell(2000)->addText(htmlspecialchars($variable->getUnidad()->getSimbolo()));
                }
            }
            $section->addTextBreak(1);
            $cantidadEcuacion = count($formula->getEcuacion());

            foreach ($formula->getEcuacion() as $ecuacion) {
                $lbResultado = $this->get('translator')->trans('Resultado', array(), 'TecsproFrontBundle');
                $lbResultadoMinimo = $this->get('translator')->trans('Resultado_Minimo', array(), 'TecsproFrontBundle');
                $lbResultadoMaximo = $this->get('translator')->trans('Resultado_Maximo', array(), 'TecsproFrontBundle');
                $lbResultadoMedio = $this->get('translator')->trans('Resultado_Medio', array(), 'TecsproFrontBundle');
                if ($cantidadEcuacion >= 2) {
                    $lbResultado = $lbResultado . " " . $ecuacion->getTranslations()->get($locale)->getNombre();
                    $lbResultadoMinimo = $lbResultadoMinimo . " " . $ecuacion->getTranslations()->get($locale)->getNombre();
                    $lbResultadoMaximo = $lbResultadoMaximo . " " . $ecuacion->getTranslations()->get($locale)->getNombre();
                    $lbResultadoMedio = $lbResultadoMedio . " " . $ecuacion->getTranslations()->get($locale)->getNombre();
                }
                if (is_null($ecuacion->getResultado()) == false) {
                    if (count($ecuacion->getResultado()) > 1) {
                        foreach ($ecuacion->getUnidadResultado() as $unidad) {
                            $section->addTextBreak(1);
                            $section->addText(
                                    htmlspecialchars(
                                            $this->get('translator')->trans('Resultados_en', array(), 'TecsproFrontBundle') . ' en ' . $unidad->getUnidad()->getSimbolo()
                                    ), array('bold' => true)
                            );

                            if ($autoincrementar == 2) {
                                $tablaResultado = $section->addTable($table_style);
                                $tablaResultado->addRow();
                                $tablaResultado->addCell(1000, $styleSombreado)->addText(htmlspecialchars("#"), $cellResultado);
                                for ($i = 0; $i < count($filas["valores"]); $i++) {
                                    $tablaResultado->addCell(1000, $styleSombreado)->addText(htmlspecialchars($filas["variable"] . " = " . round($filas["valores"][$i], 2)), $cellResultado);
                                }
                            } else {
                                $tablaResultado = $section->addTable($table_style_auto);
                                $tablaResultado->addRow();
                                $tablaResultado->addCell(1000, $styleSombreado)->addText(htmlspecialchars("#"), $cellResultado);
                                $tablaResultado->addCell(2000, $styleSombreado)->addText(htmlspecialchars($this->get('translator')->trans('Resultados', array(), 'TecsproFrontBundle')), $cellResultado);
                            }
                            $cantidadColumnas = count($columnas["valores"]);
                            $j = -1;
                            for ($i = 0; $i < $cantidadColumnas; $i++) {
                                $tablaResultado->addRow();
                                $tablaResultado->addCell(1000, $styleSombreado)->addText(htmlspecialchars($columnas["variable"] . " = " . round($columnas["valores"][$i], 2)), $cellResultado);

                                if ($autoincrementar == 2) {
                                    for ($f = 0; $f < count($filas["valores"]); $f++) {
                                        $j++;
                                        if ($unidad->getPrincipal()) {
                                            $tablaResultado->addCell(1000)->addText(htmlspecialchars(round($ecuacion->getResultado()[$j], 2)), $cellResultado);
                                        } else {
                                            eval("\$res =" . $ecuacion->getResultado()[$j] . $unidad->getUnidad()->getOperacion() . ";");
                                            $tablaResultado->addCell(1000)->addText(htmlspecialchars(round($res, 2)), $cellResultado);
                                        }
                                    }
                                } else {
                                    if ($unidad->getPrincipal()) {
                                        $tablaResultado->addCell(1000)->addText(htmlspecialchars(round($ecuacion->getResultado()[$i], 2)), $cellResultado);
                                    } else {
                                        eval("\$res =" . $ecuacion->getResultado()[$i] . $unidad->getUnidad()->getOperacion() . ";");
                                        $tablaResultado->addCell(1000)->addText(htmlspecialchars(round($res, 2)), $cellResultado);
                                    }
                                }
                            }
                        }

                        foreach ($ecuacion->getUnidadResultado() as $unidad) {
                            $section->addTextBreak(1);
                            if ($unidad->getPrincipal()) {
                                $res = $ecuacion->getResultadoMedio();
                                $resMinimo = $ecuacion->getResultadoMinimo();
                                $resMaximo = $ecuacion->getResultadoMaximo();
                            } else {
                                eval("\$res =" . $ecuacion->getResultadoMedio() . $unidad->getUnidad()->getOperacion() . ";");
                                eval("\$resMinimo =" . $ecuacion->getResultadoMinimo() . $unidad->getUnidad()->getOperacion() . ";");
                                eval("\$resMaximo =" . $ecuacion->getResultadoMaximo() . $unidad->getUnidad()->getOperacion() . ";");
                            }
                            $res = round($res, 2);
                            $resMinimo = round($resMinimo, 2);
                            $resMaximo = round($resMaximo, 2);
                            $section->addText(
                                    htmlspecialchars(
                                            $lbResultadoMedio . ": " . $res . " " . $unidad->getUnidad()->getSimbolo()
                                    ), array('bold' => true)
                            );
                            $section->addText(
                                    htmlspecialchars(
                                            $lbResultadoMinimo . ": " . $resMinimo . " " . $unidad->getUnidad()->getSimbolo()
                                    ), array('bold' => true)
                            );
                            $section->addText(
                                    htmlspecialchars(
                                            $lbResultadoMaximo . ": " . $resMaximo . " " . $unidad->getUnidad()->getSimbolo()
                                    ), array('bold' => true)
                            );
                        }
                        $section->addTextBreak(1);
                    } else {
                        foreach ($ecuacion->getUnidadResultado() as $unidad) {
                            $res = 0;
                            if (count($ecuacion->getResultado()) > 0) {
                                if ($unidad->getPrincipal()) {
                                    $res = $ecuacion->getResultado()[0];
                                } else {

                                    eval("\$res =" . $ecuacion->getResultado()[0] . $unidad->getUnidad()->getOperacion() . ";");
                                }
                            }
                            $res = round($res, 2);

                            $section->addText(
                                    htmlspecialchars(
                                            $lbResultado . ": " . $res . " " . $unidad->getUnidad()->getSimbolo()
                                    ), array('bold' => true)
                            );
                        }
                        $section->addTextBreak(1);
                    }
                }
            }
            $contador++;
        }
        //arc
        if ($arc != "") {
            $section->addText(
                    htmlspecialchars(
                            $this->get('translator')->trans('arc.tituloArcImprimir', array(), 'TecsproFrontBundle')
                    ), array('bold' => true)
            );
            $arcArray = json_decode($arc, true);

            foreach ($arcArray["vehiculos"] as $key => $vehiculo) {
                $tipo = 'vehículo';

                if (isset($vehiculo['peaton']) && $vehiculo['peaton'] == true) {
                    $tipo = $this->get('translator')->trans('arc.peaton', array(), 'TecsproFrontBundle');
                }
                $contador = $key + 1;
                $section->addText(
                        htmlspecialchars(
                                "$tipo $contador: " . $vehiculo['nombre']
                        )
                );

                //imagen  
                if ($vehiculo['movimientoPrevioColision'] == $this->MRU) {
                    $img = "bundles/tecsproarc/img/mru.png";
                } else {
                    $img = "bundles/tecsproarc/img/mrv.png";
                }


                $section->addImage(
                        $img
                );


                $tiempo = str_replace("variable", "$tipo " . $contador, $this->get('translator')->trans('arc.tiempoDescripcion', array(), 'TecsproFrontBundle'));
                $posicion = str_replace("variable", "$tipo " . $contador, $this->get('translator')->trans('arc.posicionDescripcion', array(), 'TecsproFrontBundle'));
                $velocidad = str_replace("variable", "$tipo " . $contador, $this->get('translator')->trans('arc.velocidadDescripcion', array(), 'TecsproFrontBundle'));



                if ($vehiculo['unidadVInicial'] == $this->mS) {
                    $unidadVinicial = $this->get('translator')->trans('arc.unidadms', array(), 'TecsproFrontBundle');
                } else {
                    $unidadVinicial = $this->get('translator')->trans('arc.unidadkm', array(), 'TecsproFrontBundle');
                }



                $tablaDescripcion = $section->addTable($table_style);
                $tablaDescripcion->addRow();
                $tablaDescripcion->addCell(2000, $styleSombreado)->addText(htmlspecialchars($this->get('translator')->trans('arc.NombreVariable', array(), 'TecsproFrontBundle')));
                $tablaDescripcion->addCell(2000, $styleSombreado)->addText(htmlspecialchars($this->get('translator')->trans('arc.Columna', array(), 'TecsproFrontBundle')));
                $tablaDescripcion->addCell(2000, $styleSombreado)->addText(htmlspecialchars($this->get('translator')->trans('arc.NombreUnidad', array(), 'TecsproFrontBundle')));
                $tablaDescripcion->addCell(2000, $styleSombreado)->addText(htmlspecialchars($this->get('translator')->trans('arc.simbolo', array(), 'TecsproFrontBundle')));

                $tablaDescripcion->addRow();
                $tablaDescripcion->addCell(1000)->addText(htmlspecialchars($tiempo));
                $tablaDescripcion->addCell(1000)->addText(htmlspecialchars(1));
                $tablaDescripcion->addCell(1000)->addText(htmlspecialchars($this->get('translator')->trans('arc.tiempoUnidad', array(), 'TecsproFrontBundle')));
                $tablaDescripcion->addCell(1000)->addText(htmlspecialchars("[s]"));

                $tablaDescripcion->addRow();
                $tablaDescripcion->addCell(1000)->addText(htmlspecialchars($posicion));
                $tablaDescripcion->addCell(1000)->addText(htmlspecialchars(2));
                $tablaDescripcion->addCell(1000)->addText(htmlspecialchars($this->get('translator')->trans('arc.posicionUnidad', array(), 'TecsproFrontBundle')));
                $tablaDescripcion->addCell(1000)->addText(htmlspecialchars("[m]"));

                $tablaDescripcion->addRow();
                $tablaDescripcion->addCell(1000)->addText(htmlspecialchars($velocidad));
                $tablaDescripcion->addCell(1000)->addText(htmlspecialchars(3));
                $tablaDescripcion->addCell(1000)->addText(htmlspecialchars($unidadVinicial));
                $tablaDescripcion->addCell(1000)->addText(htmlspecialchars($vehiculo['unidadVInicial']));

                //variables    
                if ($vehiculo['movimientoPrevioColision'] == $this->MRUMRV) {

                    if ($vehiculo['unidadVImpacto'] == $this->mS) {
                        $unidadVimpacto = $this->get('translator')->trans('arc.unidadms', array(), 'TecsproFrontBundle');
                    } else {
                        $unidadVimpacto = $this->get('translator')->trans('arc.unidadkm', array(), 'TecsproFrontBundle');
                    }

                    $tablaDescripcion->addRow();
                    $tablaDescripcion->addCell(1000)->addText(htmlspecialchars($this->get('translator')->trans('arc.coeficiente', array(), 'TecsproFrontBundle')));
                    $tablaDescripcion->addCell(1000)->addText(htmlspecialchars("n/a"));
                    $tablaDescripcion->addCell(1000)->addText(htmlspecialchars(""));
                    $tablaDescripcion->addCell(1000)->addText(htmlspecialchars(""));

                    $tablaDescripcion->addRow();
                    $tablaDescripcion->addCell(1000)->addText(htmlspecialchars($this->get('translator')->trans('arc.velocidadImpactoDescripcion', array(), 'TecsproFrontBundle')));
                    $tablaDescripcion->addCell(1000)->addText(htmlspecialchars(4));
                    $tablaDescripcion->addCell(1000)->addText(htmlspecialchars($unidadVimpacto));
                    $tablaDescripcion->addCell(1000)->addText(htmlspecialchars($vehiculo['unidadVImpacto']));

                    $tablaDescripcion->addRow();
                    $tablaDescripcion->addCell(1000)->addText(htmlspecialchars($this->get('translator')->trans('arc.tiempoFrenado', array(), 'TecsproFrontBundle')));
                    $tablaDescripcion->addCell(1000)->addText(htmlspecialchars(5));
                    $tablaDescripcion->addCell(1000)->addText(htmlspecialchars($this->get('translator')->trans('arc.tiempoUnidad', array(), 'TecsproFrontBundle')));
                    $tablaDescripcion->addCell(1000)->addText(htmlspecialchars("[s]"));
                }
            }

            $section->addTextBreak(1);
            //tabla
            if (count($arcArray["tabla"]) > 0) {


                $tablaArc = $section->addTable($table_style);

                if (count($arcArray["vehiculos"]) == 1) {
                    $tablaArc->addRow();
                    $tablaArc->addCell(2000, $styleSombreado)->addText(htmlspecialchars($this->get('translator')->trans('arc.Tiempo', array(), 'TecsproFrontBundle')));
                    $tablaArc->addCell(2000, $styleSombreado)->addText(htmlspecialchars($this->get('translator')->trans('arc.Posicion', array(), 'TecsproFrontBundle')));
                    $tablaArc->addCell(2000, $styleSombreado)->addText(htmlspecialchars($this->get('translator')->trans('arc.Velocidad', array(), 'TecsproFrontBundle')));
                    $tablaArc->addCell(2000, $styleSombreado)->addText(htmlspecialchars($this->get('translator')->trans('arc.Estado', array(), 'TecsproFrontBundle')));

                    foreach ($arcArray["tabla"] as $row) {
                        $tablaArc->addRow();

                        if ($row['estado'] == $this->circulando) {
                            $tablaArc->addCell(2000, $styleSuccess)->addText(htmlspecialchars($row['tiempo']));
                            $tablaArc->addCell(2000, $styleSuccess)->addText(htmlspecialchars($row['posicion']));
                            $tablaArc->addCell(2000, $styleSuccess)->addText(htmlspecialchars($row['velocidad']));
                            $tablaArc->addCell(2000, $styleSuccess)->addText(htmlspecialchars($row['estado']));
                        } else if ($row['estado'] == $this->desacelerando) {
                            $tablaArc->addCell(2000, $styleWarning)->addText(htmlspecialchars($row['tiempo']));
                            $tablaArc->addCell(2000, $styleWarning)->addText(htmlspecialchars($row['posicion']));
                            $tablaArc->addCell(2000, $styleWarning)->addText(htmlspecialchars($row['velocidad']));
                            $tablaArc->addCell(2000, $styleWarning)->addText(htmlspecialchars($row['estado']));
                        } else if ($row['estado'] == $this->colicionado) {
                            $tablaArc->addCell(2000, $styleDanger)->addText(htmlspecialchars($row['tiempo']));
                            $tablaArc->addCell(2000, $styleDanger)->addText(htmlspecialchars($row['posicion']));
                            $tablaArc->addCell(2000, $styleDanger)->addText(htmlspecialchars($row['velocidad']));
                            $tablaArc->addCell(2000, $styleDanger)->addText(htmlspecialchars($row['estado']));
                        }
                    }
                } else {
                    $tablaArc->addRow();
                    $tablaArc->addCell(2000, $styleSombreado)->addText(htmlspecialchars($this->get('translator')->trans('arc.Tiempo', array(), 'TecsproFrontBundle')));
                    $tablaArc->addCell(2000, $styleSombreado)->addText(htmlspecialchars($this->get('translator')->trans('arc.Posicion', array(), 'TecsproFrontBundle') . " 1"));
                    $tablaArc->addCell(2000, $styleSombreado)->addText(htmlspecialchars($this->get('translator')->trans('arc.Velocidad', array(), 'TecsproFrontBundle') . " 1"));
                    $tablaArc->addCell(2000, $styleSombreado)->addText(htmlspecialchars($this->get('translator')->trans('arc.Estado', array(), 'TecsproFrontBundle') . " 1"));
                    $tablaArc->addCell(2000, $styleSombreado)->addText(htmlspecialchars($this->get('translator')->trans('arc.Posicion', array(), 'TecsproFrontBundle') . " 2"));
                    $tablaArc->addCell(2000, $styleSombreado)->addText(htmlspecialchars($this->get('translator')->trans('arc.Velocidad', array(), 'TecsproFrontBundle') . " 2"));
                    $tablaArc->addCell(2000, $styleSombreado)->addText(htmlspecialchars($this->get('translator')->trans('arc.Estado', array(), 'TecsproFrontBundle') . " 2"));

                    foreach ($arcArray["tabla"] as $row) {
                        $tablaArc->addRow();

                        $estado = "estado_1";
                        $posicion = "posicion_1";
                        $velocidad = "velocidad_1";

                        if ($row[$estado] == $this->circulando) {
                            $tablaArc->addCell(2000, $styleSuccess)->addText(htmlspecialchars($row['tiempo']));
                            $tablaArc->addCell(2000, $styleSuccess)->addText(htmlspecialchars($row[$posicion]));
                            $tablaArc->addCell(2000, $styleSuccess)->addText(htmlspecialchars($row[$velocidad]));
                            $tablaArc->addCell(2000, $styleSuccess)->addText(htmlspecialchars($row[$estado]));
                        } else if ($row[$estado] == $this->desacelerando) {
                            $tablaArc->addCell(2000, $styleWarning)->addText(htmlspecialchars($row['tiempo']));
                            $tablaArc->addCell(2000, $styleWarning)->addText(htmlspecialchars($row[$posicion]));
                            $tablaArc->addCell(2000, $styleWarning)->addText(htmlspecialchars($row[$velocidad]));
                            $tablaArc->addCell(2000, $styleWarning)->addText(htmlspecialchars($row[$estado]));
                        } else if ($row[$estado] == $this->colicionado) {
                            $tablaArc->addCell(2000, $styleDanger)->addText(htmlspecialchars($row['tiempo']));
                            $tablaArc->addCell(2000, $styleDanger)->addText(htmlspecialchars($row[$posicion]));
                            $tablaArc->addCell(2000, $styleDanger)->addText(htmlspecialchars($row[$velocidad]));
                            $tablaArc->addCell(2000, $styleDanger)->addText(htmlspecialchars($row[$estado]));
                        }

                        $estado = "estado_2";
                        $posicion = "posicion_2";
                        $velocidad = "velocidad_2";

                        if ($row[$estado] == $this->circulando) {
                            $tablaArc->addCell(2000, $styleSuccess)->addText(htmlspecialchars($row[$posicion]));
                            $tablaArc->addCell(2000, $styleSuccess)->addText(htmlspecialchars($row[$velocidad]));
                            $tablaArc->addCell(2000, $styleSuccess)->addText(htmlspecialchars($row[$estado]));
                        } else if ($row[$estado] == $this->desacelerando) {
                            $tablaArc->addCell(2000, $styleWarning)->addText(htmlspecialchars($row[$posicion]));
                            $tablaArc->addCell(2000, $styleWarning)->addText(htmlspecialchars($row[$velocidad]));
                            $tablaArc->addCell(2000, $styleWarning)->addText(htmlspecialchars($row[$estado]));
                        } else if ($row[$estado] == $this->colicionado) {
                            $tablaArc->addCell(2000, $styleDanger)->addText(htmlspecialchars($row[$posicion]));
                            $tablaArc->addCell(2000, $styleDanger)->addText(htmlspecialchars($row[$velocidad]));
                            $tablaArc->addCell(2000, $styleDanger)->addText(htmlspecialchars($row[$estado]));
                        }
                    }
                }
            }
        }
        $section->addTextBreak(1);
        $section->addText(
                htmlspecialchars(
                        $this->get('translator')->trans('firma', array(), 'TecsproFrontBundle')
        ));
        //$section->addLink('http://www.ractt.com/', htmlspecialchars('www.ractt.com', ENT_COMPAT, 'UTF-8'), 'myOwnLinkStyle');


        return $phpWord;
    }

}

<?php

namespace Tecspro\TablaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 * Categoria
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Tecspro\TablaBundle\Entity\RigidezRepository")
 */
class RigidezTranslation implements \A2lix\I18nDoctrineBundle\Doctrine\Interfaces\OneLocaleInterface {

      use \Tecspro\ModuloBundle\Entity\Translation;

    
     /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=255)
     */
    private $tipo;
        /**
     * @var string
     *
     * @ORM\Column(name="vehiculo", type="string", length=255, nullable=true)
     */
    private $vehiculo;

    /**
     * @var string
     *
     * @ORM\Column(name="categoria", type="string", length=255, nullable=true)
     */
    private $categoria;

    /**
     * @var string
     *
     * @ORM\Column(name="lado", type="string", length=255)
     */
    private $lado;


    

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return RigidezTranslation
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set vehiculo
     *
     * @param string $vehiculo
     * @return RigidezTranslation
     */
    public function setVehiculo($vehiculo)
    {
        $this->vehiculo = $vehiculo;

        return $this;
    }

    /**
     * Get vehiculo
     *
     * @return string 
     */
    public function getVehiculo()
    {
        return $this->vehiculo;
    }

    /**
     * Set categoria
     *
     * @param string $categoria
     * @return RigidezTranslation
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return string 
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Set lado
     *
     * @param string $lado
     * @return RigidezTranslation
     */
    public function setLado($lado)
    {
        $this->lado = $lado;

        return $this;
    }

    /**
     * Get lado
     *
     * @return string 
     */
    public function getLado()
    {
        return $this->lado;
    }
}

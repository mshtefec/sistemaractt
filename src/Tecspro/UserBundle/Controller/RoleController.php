<?php

namespace Tecspro\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Tecspro\UserBundle\Entity\Role;
use Tecspro\UserBundle\Form\RoleType;
use Tecspro\UserBundle\Form\RoleFilterType;

/**
 * Role controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/superadmin/role")
 */
class RoleController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Tecspro/UserBundle/Resources/config/Role.yml',
    );

    /**
     * Lists all Role entities.
     *
     * @Route("/", name="superadmin_role")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->config['filterType'] = new RoleFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Role entity.
     *
     * @Route("/", name="superadmin_role_create")
     * @Method("POST")
     * @Template("UserBundle:Role:new.html.twig")
     */
    public function createAction() {
        $this->config['newType'] = new RoleType();
        $response = parent::createAction();

        return $response;
    }

    /**
     * Displays a form to create a new Role entity.
     *
     * @Route("/new", name="superadmin_role_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->config['newType'] = new RoleType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Role entity.
     *
     * @Route("/{id}", name="superadmin_role_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Role entity.
     *
     * @Route("/{id}/edit", name="superadmin_role_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->config['editType'] = new RoleType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Role entity.
     *
     * @Route("/{id}", name="superadmin_role_update")
     * @Method("PUT")
     * @Template("UserBundle:Role:edit.html.twig")
     */
    public function updateAction($id) {
        $this->config['editType'] = new RoleType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a Role entity.
     *
     * @Route("/{id}", name="superadmin_role_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter Role.
     *
     * @Route("/exporter/{format}", name="superadmin_role_export")
     */
    public function getExporter($format) {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Datatable Formula.
     *
     * @Route("/get-table/", name="superadmin_role_table")
     */
    public function getDatatable() {
        $response = parent::getTable($this->createQuery());

        return $response;
    }

}

<?php

namespace Tecspro\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Translation\Loader\YamlFileLoader;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Translation\Loader\ArrayLoader;
use Symfony\Component\HttpFoundation\JsonResponse;
use A2lix\I18nDoctrineBundle\Annotation\I18nDoctrine;
use Symfony\Component\Yaml\Yaml;
use JMS\Serializer\SerializerBuilder;

class TablaController extends Controller {

    private $tablaFriccion = 'TablaFriccion';
    private $tablaInercia = 'Inercia';
    private $tablaCoeficienteArrastrePeaton = 'CoeficienteArrastrePeaton';
    private $tablaEficienciaProyeccion = 'EficienciaProyeccion';
    private $tablaRigidez = 'Rigidez';
    private $tablaCentroGravedad = 'CentroGravedad';

    /**
     * @Route("/tablas/", name="tablas_list")
     * @Template()
     */
    public function tablasListaAction() {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $modulo = $request->getSession()->get('moduloSeleccionado', null);
        $locale = $request->getSession()->get('_locale');
        $es = $this->container->get('twig')->getGlobals()["es"];
        $pt = $this->container->get('twig')->getGlobals()["pt"];

        $array = array();

        if (is_null($modulo) == false) {

            $moduloEntity = $em->getRepository('ModuloBundle:Modulo')->find($modulo);

            foreach ($moduloEntity->getTablas() as $tabla) {

                $config = $this->ObtenerConfiguracionTabla($tabla);

                if ($locale == $es) {

                    $nombreTabla = $config['config']['entityNameEs'];
                } elseif ($locale == $pt) {
                    $nombreTabla = $config['config']['entityNamePt'];
                }


                $array[] = array(
                    'id' => $tabla,
                    'tabla' => $nombreTabla
                );
            }
        }

        return array(
            'tablas' => $array,
        );
    }

    /**
     * @Route("/tablas/select", name="tablas_select", options={"expose"=true})
     */
    public function tablasSelectAction() {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $tabla = $request->get('tabla', null);
        $es = $this->container->get('twig')->getGlobals()["es"];
        $pt = $this->container->get('twig')->getGlobals()["pt"];

        $arrayConfig = $this->ObtenerConfiguracionTabla($tabla);
        $config = $arrayConfig['config'];

        //columna

        $select = array();
        $columnasArray = array();
        $indexColumn = array();
        $contador = 0;
        $tieneFiltro = false;

        $locale = $request->getSession()->get('_locale');


        if ($locale == $es) {
            $nombreTabla = $config['entityNameEs'];
        } elseif ($locale == $pt) {
            $nombreTabla = $config['entityNamePt'];
        }


        foreach ($config["fieldsindexFront"] as $key => $col) {
            if ($col["visible"] == false) {
                $indexColumn[] = $contador;
            }
            $columnName = '';

            if ($locale == $es) {
                $columnName = $col["labelEs"];
            } elseif ($locale == $pt) {
                $columnName = $col["labelPt"];
            }
            $columnasArray[] = array(
                'columna' => $columnName,
                'filtro' => $col["filtro"],
            );
            if (isset($col["alias"])) {
                $select[] = $col["alias"] . "." . $col["name"];
            } else {
                $select[] = $config["alias"] . "." . $col["name"];
            }
            $contador++;
        }
        foreach ($columnasArray as $key => $col) {
            if ($col['filtro'] == true) {
                $tieneFiltro = true;
                break;
            }
        }

        $tablaArray = $em->getRepository($arrayConfig['repository'])->findAllArray($select);
        $propiedad = $em->getRepository("TecsproTablaBundle:PropiedadTabla")->findByName($tabla);

        $fuente = "";
        if (is_null($propiedad) == false) {
            $fuente = $propiedad->getFuente();
        }
        //datos
        $datosArray = array();
        foreach ($tablaArray as $dato) {
            $registo = array();
            foreach ($dato as $d) {
                $registo[] = array(
                    'dato' => $d
                );
            }
            $datosArray[] = array(
                'registros' => $registo
            );
        }
        /* $array = array(
          "data" => $tablaArray
          ); */
        $resultado = array(
            'id' => $tabla,
            'tabla' => $nombreTabla,
            'columnas' => $columnasArray,
            'datos' => $datosArray,
            'indexHide' => $indexColumn,
            'tieneFiltro' => $tieneFiltro,
            'fuente' => $fuente
        );

        $response = new JsonResponse();
        $response->setData($resultado);

        return $response;
    }

    /**
     * @Route("/tabla-sistema/seleccionar", name="tablaSistema_guardarSession", options={"expose"=true})
     */
    public function tablaGuardarSessionAction() {
        $request = $this->getRequest();
        $tabla = $request->query->get("tabla");
        $session = $request->getSession();
        $session->set('tablaSistema', $tabla);
        $response = new JsonResponse();
        $response->setData(true);

        return $response;
    }

    /**
     * @Route("/tabla-sistema/eliminar", name="tablaSistema_eliminarSession", options={"expose"=true})
     */
    public function tablaEliminarSessionAction() {
        $request = $this->getRequest();
        $tabla = $request->query->get("tabla");
        $session = $request->getSession();
        $session->remove('tablaSistema');
        $response = new JsonResponse();
        $response->setData(true);

        return $response;
    }

    private function ObtenerConfiguracionTabla($tabla) {
        if ($tabla == $this->tablaFriccion) {
            $yml = 'Tecspro/TablaBundle/Resources/config/TablaFriccion.yml';
            $repository = 'TecsproTablaBundle:TablaFriccion';
        } elseif ($tabla == $this->tablaInercia) {
            $yml = 'Tecspro/TablaBundle/Resources/config/Inercia.yml';
            $repository = 'TecsproTablaBundle:Inercia';
        } elseif ($tabla == $this->tablaCoeficienteArrastrePeaton) {
            $yml = 'Tecspro/TablaBundle/Resources/config/CoeficienteArrastrePeaton.yml';
            $repository = 'TecsproTablaBundle:CoeficienteArrastrePeaton';
        } elseif ($tabla == $this->tablaEficienciaProyeccion) {
            $yml = 'Tecspro/TablaBundle/Resources/config/EficienciaProyeccion.yml';
            $repository = 'TecsproTablaBundle:EficienciaProyeccion';
        } elseif ($tabla == $this->tablaRigidez) {
            $yml = 'Tecspro/TablaBundle/Resources/config/Rigidez.yml';
            $repository = 'TecsproTablaBundle:Rigidez';
        } elseif ($tabla == $this->tablaCentroGravedad) {
            $yml = 'Tecspro/TablaBundle/Resources/config/CentroGravedad.yml';
            $repository = 'TecsproTablaBundle:CentroGravedad';
        }

        $config = $this->getConfig($yml);
        return array(
            'repository' => $repository,
            'config' => $config
        );
    }

    private function getConfig($yml) {
        $configs = Yaml::parse(file_get_contents($this->get('kernel')->getRootDir() . '/../src/' . $yml));
        foreach ($configs as $key => $value) {
            $config[$key] = $value;
        }


        return $config;
    }

}

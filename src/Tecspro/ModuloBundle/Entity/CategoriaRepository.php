<?php

namespace Tecspro\ModuloBundle\Entity;

use Doctrine\ORM\EntityRepository;

class CategoriaRepository extends EntityRepository {

    public function findCategoriaByModulo($modulo) {

        return $this->createQueryBuilder('a')
                        ->select('a,m,g')
                        ->leftJoin('a.modulo', 'm')
                        ->leftJoin('a.grupos', 'g')
                        ->leftJoin('g.formulas', 'f')
                        ->where('m.id= :mod and f.publicar = true')
                        ->setParameter('mod', $modulo)
                        ->orderBy('a.orden', 'asc')
                        ->addOrderBy('g.orden', 'asc')
                        ->getQuery()
                        ->getResult()
        ;
    }

    public function findUltimoOrden($modulo) {

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
                ->select('max(a.orden)+1')
                ->from('Tecspro\ModuloBundle\Entity\Categoria', 'a')
                ->join('a.modulo', 'm')
                ->where('m.id = :mo')
                ->setParameter('mo', $modulo)
        ;

        return $qb
                        ->getQuery()
                        ->getOneOrNullResult()
        ;
    }

}

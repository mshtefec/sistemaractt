<?php

namespace Tecspro\TablaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * PropiedadTablaType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class PropiedadTablaType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('tabla', 'choice', array(
                    'multiple' => false,
                    'choices' => array(
                        'Fricción' => "TablaFriccion",
                        'Momento de Inercia' => "Inercia",
                        'Centro Gravedad' => "CentroGravedad",
                        'Rigidez' => "Rigidez",
                        'Coeficiente de Arrastre de peatón' => "CoeficienteArrastrePeaton",
                        'Eficiencia de Proyección' => "EficienciaProyeccion",
                    ),
                    // *this line is important*
                    'choices_as_values' => true,
                ))
                ->add('fuente')

        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Tecspro\TablaBundle\Entity\PropiedadTabla'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'tecspro_tablabundle_propiedadtabla';
    }

}

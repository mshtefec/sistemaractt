<?php

namespace Tecspro\ModuloBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Tecspro\ModuloBundle\Form\GrupoType;
use Tecspro\ModuloBundle\Form\EcuacionType;

/**
 * FormulaType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class ReferenciaType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('idioma', 'choice', array(
                    'choices' => array('es' => 'Español', 'pt' => 'Portugues'),
                    'choices_as_values' => false,
                ))
                ->add('file', 'mws_field_file', array(
                    'required' => false,
                    'file_path' => 'webPath',
                    'label' => 'Image',
                        //'show_path' => true
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Tecspro\ModuloBundle\Entity\Referencia'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'tecspro_modulobundle_referencia';
    }

}

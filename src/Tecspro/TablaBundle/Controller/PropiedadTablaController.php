<?php

namespace Tecspro\TablaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Tecspro\TablaBundle\Entity\PropiedadTabla;
use Tecspro\TablaBundle\Form\PropiedadTablaType;
use Tecspro\TablaBundle\Form\PropiedadTablaFilterType;
use A2lix\I18nDoctrineBundle\Annotation\I18nDoctrine;

/**
 * PropiedadTabla controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/propiedadTabla")
 */
class PropiedadTablaController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Tecspro/TablaBundle/Resources/config/PropiedadTabla.yml',
    );

    /**
     * Lists all PropiedadTabla entities.
     *
     * @Route("/", name="admin_propiedadTabla")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->config['filterType'] = new PropiedadTablaFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new PropiedadTabla entity.
     *
     * @Route("/", name="admin_propiedadTabla_create")
     * @Method("POST")
     * @Template("TecsproTablaBundle:PropiedadTabla:new.html.twig")
     */
    public function createAction() {
        $this->config['newType'] = new PropiedadTablaType();
        $response = parent::createAction();

        return $response;
    }

    /**
     * Displays a form to create a new PropiedadTabla entity.
     *
     * @Route("/new", name="admin_propiedadTabla_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->config['newType'] = new PropiedadTablaType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a PropiedadTabla entity.
     *
     * @Route("/{id}", name="admin_propiedadTabla_show", options={"expose"=true})
     * @Method("GET")
     * @Template()
     * @I18nDoctrine
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing PropiedadTabla entity.
     *
     * @Route("/{id}/edit", name="admin_propiedadTabla_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->config['editType'] = new PropiedadTablaType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing PropiedadTabla entity.
     *
     * @Route("/{id}", name="admin_propiedadTabla_update")
     * @Method("PUT")
     * @Template("TecsproTablaBundle:PropiedadTabla:edit.html.twig")
     */
    public function updateAction($id) {
        $this->config['editType'] = new PropiedadTablaType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a PropiedadTabla entity.
     *
     * @Route("/{id}", name="admin_propiedadTabla_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter PropiedadTabla.
     *
     * @Route("/exporter/{format}", name="admin_propiedadTabla_export")
     */
    public function getExporter($format) {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Datatable PropiedadTabla.
     *
     * @Route("/get-table/", name="admin_propiedadTabla_table")
     */
    public function getDatatable() {
        $response = parent::getTable();

        return $response;
    }

}

<?php

namespace Tecspro\FrontBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProyectoType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $languaje = 'es';

        if ($options['empty_data'] == 'pt') {

            $languaje = 'pt';
        } elseif ($options['empty_data'] == 'es') {

            $languaje = 'es';
        }


        $builder
                ->add('nombre', 'text', array(
                    "label" => 'Nombre',
                    "required" => true,
                    'translation_domain' => 'TecsproFrontBundle',
                    'attr' => array(
                        'class' => 'txtNombre'
                    )
                        )
                )
                ->add('comentario', 'text', array(
                    "label" => 'Comentarios',
                    "required" => false,
                    'translation_domain' => 'TecsproFrontBundle',
                    'attr' => array(
                        'class' => 'txtComentario'
                    )
                        )
                )
                ->add('fecha', 'bootstrapdatetime', array(
                    'language' => $languaje,
                    "label" => 'Fecha',
                    "required" => true,
                    'translation_domain' => 'TecsproFrontBundle',
                    'attr' => array(
                        'class' => 'txtFecha'
                    )
                        )
                )
                ->add('partesInvolucradas', 'collection', array(
                    'type' => new ParteInvolucradaType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'required' => true,
                    'by_reference' => false,
                    'prototype' => true,
                    'prototype_name' => '__name__',
                    'label' => false
                ))
                ->add('formulasParameter', 'hidden', array(
                    'mapped' => false
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Tecspro\FrontBundle\Entity\Proyecto'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'tecspro_frontbundle_proyecto';
    }

}

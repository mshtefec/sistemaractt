<?php

namespace Tecspro\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Tecspro\UserBundle\Entity\Domicilio;
use Tecspro\UserBundle\Form\DomicilioType;
use Tecspro\UserBundle\Form\DomicilioFilterType;

/**
 * Domicilio controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/domicilio")
 */
class DomicilioController extends Controller
{
    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Tecspro/UserBundle/Resources/config/Domicilio.yml',
    );

    /**
     * Lists all Domicilio entities.
     *
     * @Route("/", name="admin_domicilio")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->config['filterType'] = new DomicilioFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Domicilio entity.
     *
     * @Route("/", name="admin_domicilio_create")
     * @Method("POST")
     * @Template("UserBundle:Domicilio:new.html.twig")
     */
    public function createAction()
    {
        $this->config['newType'] = new DomicilioType();
        $response = parent::createAction();

        return $response;
    }

    /**
     * Displays a form to create a new Domicilio entity.
     *
     * @Route("/new", name="admin_domicilio_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->config['newType'] = new DomicilioType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Domicilio entity.
     *
     * @Route("/{id}", name="admin_domicilio_show", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Domicilio entity.
     *
     * @Route("/{id}/edit", name="admin_domicilio_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->config['editType'] = new DomicilioType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Domicilio entity.
     *
     * @Route("/{id}", name="admin_domicilio_update")
     * @Method("PUT")
     * @Template("UserBundle:Domicilio:edit.html.twig")
     */
    public function updateAction($id)
    {
        $this->config['editType'] = new DomicilioType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a Domicilio entity.
     *
     * @Route("/{id}", name="admin_domicilio_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter Domicilio.
     *
     * @Route("/exporter/{format}", name="admin_domicilio_export")
     */
    public function getExporter($format)
    {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Datatable Domicilio.
     *
     * @Route("/get-table/", name="admin_domicilio_table")
     */
    public function getDatatable()
    {
        $response = parent::getTable();

        return $response;
    }
 
}
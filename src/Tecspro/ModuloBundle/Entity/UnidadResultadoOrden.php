<?php

namespace Tecspro\ModuloBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Exclude;

/**
 * UnidadResultadoOrden
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Tecspro\ModuloBundle\Entity\UnidadResultadoOrdenRepository")
 */
class UnidadResultadoOrden {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Type("integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Unidad",cascade={"persist"})
     * @ORM\JoinColumn(name="unidad_id", referencedColumnName="id")
     * @Type("Tecspro\ModuloBundle\Entity\Unidad")
     */
    private $unidad;

    /**
     * @ORM\ManyToOne(targetEntity="Ecuacion", inversedBy="unidadResultado")
     * @ORM\JoinColumn(name="ecuacion_id", referencedColumnName="id")
     * @Exclude
     */
    private $ecuacion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="principal", type="boolean")
     * @Type("boolean")
     */
    protected $principal;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set unidad
     *
     * @param \Tecspro\ModuloBundle\Entity\Unidad $unidad
     * @return UnidadResultadoOrden
     */
    public function setUnidad(\Tecspro\ModuloBundle\Entity\Unidad $unidad = null) {
        $this->unidad = $unidad;

        return $this;
    }

    /**
     * Get unidad
     *
     * @return \Tecspro\ModuloBundle\Entity\Unidad 
     */
    public function getUnidad() {
        return $this->unidad;
    }

    /**
     * Set ecuacion
     *
     * @param \Tecspro\ModuloBundle\Entity\Ecuacion $ecuacion
     * @return UnidadResultadoOrden
     */
    public function setEcuacion(\Tecspro\ModuloBundle\Entity\Ecuacion $ecuacion = null) {
        $this->ecuacion = $ecuacion;

        return $this;
    }

    /**
     * Get ecuacion
     *
     * @return \Tecspro\ModuloBundle\Entity\Ecuacion 
     */
    public function getEcuacion() {
        return $this->ecuacion;
    }

    /**
     * Set principal
     *
     * @param boolean $principal
     * @return UnidadResultadoOrden
     */
    public function setPrincipal($principal) {
        $this->principal = $principal;

        return $this;
    }

    /**
     * Get principal
     *
     * @return boolean 
     */
    public function getPrincipal() {
        return $this->principal;
    }

}

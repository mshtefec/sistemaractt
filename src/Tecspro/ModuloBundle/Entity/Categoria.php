<?php

namespace Tecspro\ModuloBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Exclude;

/**
 * Categoria
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Tecspro\ModuloBundle\Entity\CategoriaRepository")
 */
class Categoria {

    use \A2lix\I18nDoctrineBundle\Doctrine\ORM\Util\Translatable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Type("integer")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="orden", type="integer")
     * @Type("integer")
     */
    private $orden;

    /**
     * @ORM\ManyToOne(targetEntity="Modulo", inversedBy="categorias")
     * @ORM\JoinColumn(name="modulo_id", referencedColumnName="id")
     * @Exclude
     */
    private $modulo;

    /**
     * @ORM\OneToMany(targetEntity="Grupo", mappedBy="categoria" , cascade={"all"})
     * @Exclude
     */
    private $grupos;

    /**
     * @Type("ArrayCollection<Tecspro\ModuloBundle\Entity\CategoriaTranslation>")
     */
    protected $translations;

    /**
     * Constructor
     */
    public function __construct() {
        $this->grupos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->translations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString() {
        return $this->getNombre();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set orden
     *
     * @param integer $orden
     * @return Categoria
     */
    public function setOrden($orden) {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return integer 
     */
    public function getOrden() {
        return $this->orden;
    }

    /**
     * Set modulo
     *
     * @param \Tecspro\ModuloBundle\Entity\Modulo $modulo
     * @return Categoria
     */
    public function setModulo(\Tecspro\ModuloBundle\Entity\Modulo $modulo = null) {
        $this->modulo = $modulo;

        return $this;
    }

    /**
     * Get modulo
     *
     * @return \Tecspro\ModuloBundle\Entity\Modulo 
     */
    public function getModulo() {
        return $this->modulo;
    }

    /**
     * Add grupos
     *
     * @param \Tecspro\ModuloBundle\Entity\Grupo $grupos
     * @return Categoria
     */
    public function addGrupo(\Tecspro\ModuloBundle\Entity\Grupo $grupos) {
        $grupos->setCategoria($this);
        $this->grupos[] = $grupos;

        return $this;
    }

    /**
     * Remove grupos
     *
     * @param \Tecspro\ModuloBundle\Entity\Grupo $grupos
     */
    public function removeGrupo(\Tecspro\ModuloBundle\Entity\Grupo $grupos) {
        $this->grupos->removeElement($grupos);
    }

    /**
     * Get grupos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGrupos() {
        return $this->grupos;
    }

}

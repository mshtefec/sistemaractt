<?php

namespace Tecspro\UserBundle\EventListener;

use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Logout\LogoutHandlerInterface;

class TecsproLogoutHandler implements LogoutHandlerInterface {

    private $session;
    private $em;

    public function __construct(UserManagerInterface $userManager, \Doctrine\ORM\EntityManager $em) {
        $this->em = $em;
        $this->userManager = $userManager;
    }

    /**
     * @param Request        $request
     * @param Response       $response
     * @param TokenInterface $token
     */
    public function logout(Request $request, Response $response, TokenInterface $token) {
        $this->session = $request->getSession();
        $user = $token->getUser();
        $array = $user->getSessiones();

        if (isset($array[$this->session->getId()])) {
            unset($array[$this->session->getId()]);
            $user->setSessiones($array);
            $this->userManager->updateUser($user);
        }
        $this->session->invalidate();
        //$user->setSessionId(null);
    }

}

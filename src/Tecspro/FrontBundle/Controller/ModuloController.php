<?php

namespace Tecspro\FrontBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Tecspro\ModuloBundle\Entity\Modulo;
use Tecspro\ModuloBundle\Form\ModuloType;
use Tecspro\ModuloBundle\Form\ModuloFilterType;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/")
 */
class ModuloController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Tecspro/ModuloBundle/Resources/config/Modulo.yml',
    );

    /**
     * @Route("/", name="index")
     * @Template("TecsproFrontBundle:Default:index.html.twig")
     */
    public function indexAction() {

        return array('name' => null);
    }

    /**
     *
     * @Route("/seleccionar-modulo/", name="modulo_seleccionar_modulo")
     */
    public function seleccionarModulosAction() {

        //filtrar modulos por clientes
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        $entities = $em->getRepository('ModuloBundle:Modulo')->findByUser($user->getId());

        return $this->render(
                        'TecsproFrontBundle:Form:macro-modal-seleccionarmodulo.html.twig', array('entities' => $entities)
        );
    }

    /**
     *
     * @Route("/set-modulo/", name="modulo_setsession_modulo")
     */
    public function setearSessionModuloAction() {
        $config = $this->getConfig();
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $modulo = $request->request->get('modulo', null);
        $array = array();
        $entity = $em->getRepository($config['repository'])->find($modulo);

        if (!$entity) {
            $resultado = false;
        } else {
            $session = $request->getSession();
            $session->set('moduloSeleccionado', $modulo);
            $session->set('moduloNombreSeleccionado', $entity->getNombre());
            $resultado = true;
            $user = $this->getUser();
            $user->setItemConfiguraciones("ulitmoModulo", $modulo);
            $em->persist($user);
            $em->flush();
        }


        $array[] = array(
            'resultado' => $resultado,
        );


        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

}

<?php

namespace Tecspro\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * sessions
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Tecspro\UserBundle\Entity\sessionsRepository")
 */
class sessions {

    /**
     * @var string
     *
     * @ORM\Column(name="sess_id", type="string", length=255)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $sess_id;

    /**
     * @var blob
     *
     * @ORM\Column(name="sess_data", type="blob")
     */
    private $sess_data;

    /**
     * @var integer
     *
     * @ORM\Column(name="sess_time", type="integer")
     */
    private $sess_time;

    /**
     * @var integer
     *
     * @ORM\Column(name="sess_lifetime", type="integer")
     */
    private $sess_lifetime;

    /**
     * Get sess_id
     *
     * @return string 
     */
    public function getSessId() {
        return $this->sess_id;
    }

    /**
     * Set sess_data
     *
     * @param string $sessData
     * @return sessions
     */
    public function setSessData($sessData) {
        $this->sess_data = $sessData;

        return $this;
    }

    /**
     * Get sess_data
     *
     * @return string 
     */
    public function getSessData() {
        return $this->sess_data;
    }

    /**
     * Set sess_time
     *
     * @param integer $sessTime
     * @return sessions
     */
    public function setSessTime($sessTime) {
        $this->sess_time = $sessTime;

        return $this;
    }

    /**
     * Get sess_time
     *
     * @return integer 
     */
    public function getSessTime() {
        return $this->sess_time;
    }

    /**
     * Set sess_lifetime
     *
     * @param integer $sessLifetime
     * @return sessions
     */
    public function setSessLifetime($sessLifetime) {
        $this->sess_lifetime = $sessLifetime;

        return $this;
    }

    /**
     * Get sess_lifetime
     *
     * @return integer 
     */
    public function getSessLifetime() {
        return $this->sess_lifetime;
    }

}

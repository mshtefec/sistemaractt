<?php

namespace Tecspro\TablaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Tecspro\TablaBundle\Entity\Inercia;
use Tecspro\TablaBundle\Form\InerciaType;
use Tecspro\TablaBundle\Form\InerciaFilterType;
use A2lix\I18nDoctrineBundle\Annotation\I18nDoctrine;

/**
 * Inercia controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/inercia")
 */
class InerciaController extends Controller
{
    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Tecspro/TablaBundle/Resources/config/Inercia.yml',
    );

    /**
     * Lists all Inercia entities.
     *
     * @Route("/", name="admin_inercia")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->config['filterType'] = new InerciaFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Inercia entity.
     *
     * @Route("/", name="admin_inercia_create")
     * @Method("POST")
     * @Template("TecsproTablaBundle:Inercia:new.html.twig")
     */
    public function createAction()
    {
        $this->config['newType'] = new InerciaType();
        $response = parent::createAction();

        return $response;
    }

    /**
     * Displays a form to create a new Inercia entity.
     *
     * @Route("/new", name="admin_inercia_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->config['newType'] = new InerciaType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Inercia entity.
     *
     * @Route("/{id}", name="admin_inercia_show", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Inercia entity.
     *
     * @Route("/{id}/edit", name="admin_inercia_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->config['editType'] = new InerciaType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Inercia entity.
     *
     * @Route("/{id}", name="admin_inercia_update")
     * @Method("PUT")
     * @Template("TecsproTablaBundle:Inercia:edit.html.twig")
           * @I18nDoctrine
     */
    public function updateAction($id)
    {
        $this->config['editType'] = new InerciaType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a Inercia entity.
     *
     * @Route("/{id}", name="admin_inercia_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter Inercia.
     *
     * @Route("/exporter/{format}", name="admin_inercia_export")
     */
    public function getExporter($format)
    {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Datatable Inercia.
     *
     * @Route("/get-table/", name="admin_inercia_table")
     */
    public function getDatatable()
    {
        $response = parent::getTable();

        return $response;
    }
 
}
<?php

namespace Tecspro\ConfiguracionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VariableGuardada
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class VariableGuardada
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="variable", type="string", length=255)
     */
    private $variable;

    /**
     * @var string
     *
     * @ORM\Column(name="valor", type="decimal")
     */
    private $valor;

    /**
     * @var \Tecspro\ConfiguracionBundle\Entity\FormulaGuardada
     *
     * @ORM\ManyToOne(targetEntity="Tecspro\ConfiguracionBundle\Entity\FormulaGuardada", inversedBy="variableGuardada")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="formulaGuardada_id", referencedColumnName="id")
     * })
     */
    private $formulaGuardada;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set variable
     *
     * @param string $variable
     * @return VariableGuardada
     */
    public function setVariable($variable)
    {
        $this->variable = $variable;

        return $this;
    }

    /**
     * Get variable
     *
     * @return string 
     */
    public function getVariable()
    {
        return $this->variable;
    }

    /**
     * Set valor
     *
     * @param string $valor
     * @return VariableGuardada
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return string 
     */
    public function getValor()
    {
        return $this->valor;
    }
}

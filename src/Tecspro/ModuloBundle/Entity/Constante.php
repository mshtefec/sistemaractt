<?php

namespace Tecspro\ModuloBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 * Constante
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Tecspro\ModuloBundle\Entity\ConstanteRepository")
 */
class Constante {

    use \A2lix\I18nDoctrineBundle\Doctrine\ORM\Util\Translatable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Type("integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="valor",  type="string", length=255)
     * @Type("string")
     */
    private $valor;

    /**
     * @var string
     *
     * @ORM\Column(name="identificador",  type="string", length=255)
     * @Type("string")
     */
    private $identificador;

    /**
     * @ORM\ManyToOne(targetEntity="Unidad")
     * @ORM\JoinColumn(name="unidad_id", referencedColumnName="id")
     * @Type("Tecspro\ModuloBundle\Entity\Unidad")
     */
    private $unidad;

    /**
     * @Type("ArrayCollection<Tecspro\ModuloBundle\Entity\ConstanteTranslation>")
     */
    protected $translations;

    /**
     * Constructor
     */
    public function __construct() {
        $this->grupos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->translations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString() {
        return $this->getNombre();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set unidad
     *
     * @param \Tecspro\ModuloBundle\Entity\Unidad $unidad
     * @return Constante
     */
    public function setUnidad(\Tecspro\ModuloBundle\Entity\Unidad $unidad = null) {
        $this->unidad = $unidad;

        return $this;
    }

    /**
     * Get unidad
     *
     * @return \Tecspro\ModuloBundle\Entity\Unidad 
     */
    public function getUnidad() {
        return $this->unidad;
    }

    /**
     * Set valor
     *
     * @param string $valor
     * @return Constante
     */
    public function setValor($valor) {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return string 
     */
    public function getValor() {
        return $this->valor;
    }


    /**
     * Set identificador
     *
     * @param string $identificador
     * @return Constante
     */
    public function setIdentificador($identificador)
    {
        $this->identificador = $identificador;

        return $this;
    }

    /**
     * Get identificador
     *
     * @return string 
     */
    public function getIdentificador()
    {
        return $this->identificador;
    }
}

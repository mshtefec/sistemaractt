<?php

namespace Tecspro\TablaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Tecspro\TablaBundle\Entity\EficienciaProyeccion;
use Tecspro\TablaBundle\Form\EficienciaProyeccionType;
use Tecspro\TablaBundle\Form\EficienciaProyeccionFilterType;

/**
 * EficienciaProyeccion controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/eficienciaproyeccion")
 */
class EficienciaProyeccionController extends Controller
{
    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Tecspro/TablaBundle/Resources/config/EficienciaProyeccion.yml',
    );

    /**
     * Lists all EficienciaProyeccion entities.
     *
     * @Route("/", name="admin_eficienciaproyeccion")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->config['filterType'] = new EficienciaProyeccionFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new EficienciaProyeccion entity.
     *
     * @Route("/", name="admin_eficienciaproyeccion_create")
     * @Method("POST")
     * @Template("TecsproTablaBundle:EficienciaProyeccion:new.html.twig")
     */
    public function createAction()
    {
        $this->config['newType'] = new EficienciaProyeccionType();
        $response = parent::createAction();

        return $response;
    }

    /**
     * Displays a form to create a new EficienciaProyeccion entity.
     *
     * @Route("/new", name="admin_eficienciaproyeccion_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->config['newType'] = new EficienciaProyeccionType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a EficienciaProyeccion entity.
     *
     * @Route("/{id}", name="admin_eficienciaproyeccion_show", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing EficienciaProyeccion entity.
     *
     * @Route("/{id}/edit", name="admin_eficienciaproyeccion_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->config['editType'] = new EficienciaProyeccionType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing EficienciaProyeccion entity.
     *
     * @Route("/{id}", name="admin_eficienciaproyeccion_update")
     * @Method("PUT")
     * @Template("TecsproTablaBundle:EficienciaProyeccion:edit.html.twig")
     */
    public function updateAction($id)
    {
        $this->config['editType'] = new EficienciaProyeccionType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a EficienciaProyeccion entity.
     *
     * @Route("/{id}", name="admin_eficienciaproyeccion_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter EficienciaProyeccion.
     *
     * @Route("/exporter/{format}", name="admin_eficienciaproyeccion_export")
     */
    public function getExporter($format)
    {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Datatable EficienciaProyeccion.
     *
     * @Route("/get-table/", name="admin_eficienciaproyeccion_table")
     */
    public function getDatatable()
    {
        $response = parent::getTable();

        return $response;
    }
 
}
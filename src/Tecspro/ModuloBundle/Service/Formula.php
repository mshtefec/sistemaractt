<?php

namespace Tecspro\BackBundle\Services;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use JMS\Serializer\SerializerBuilder;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\SecurityContext;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Tecspro\ModuloBundle\Service;

/**
 * Description of formula
 *
 * @author rodrigo
 */
class Formula {

    private $em;
    private $session;

    public function __construct(\Symfony\Component\HttpFoundation\Session\Session $session, \Doctrine\ORM\EntityManager $em) {
        $this->em = $em;
        $this->session = $session;
        $this->session->start();
    }

    public function actualizarFormulas($formulas) {
        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();

        foreach ($formulas as $formula) {
            $json = $serializer->serialize($formula, 'json');
            $formula->setJson($json);
            $this->em->persist($formula);
        }
    }

}

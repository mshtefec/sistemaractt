<?php

namespace Tecspro\ConfiguracionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FomulaGuardada
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class FormulaGuardada
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="formula", type="string", length=255)
     */
    private $formula;

    /**
     * @ORM\OneToMany(targetEntity="Tecspro\ConfiguracionBundle\Entity\VariableGuardada", mappedBy="formulaGuardada")
     * */
    private $variableGuardada;

    /**
     * @ORM\OneToMany(targetEntity="Tecspro\ConfiguracionBundle\Entity\EcuacionGuardada", mappedBy="formulaGuardada")
     * */
    private $ecuacionGuardada;

    /**
     * @var \Tecspro\ConfiguracionBundle\Entity\ArchivoGuardado
     *
     * @ORM\ManyToOne(targetEntity="Tecspro\ConfiguracionBundle\Entity\ArchivoGuardado", inversedBy="formulaGuardada")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="archivoGuardado", referencedColumnName="id")
     * })
     */
    private $archivoGuardado;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set formula
     *
     * @param string $formula
     * @return FomulaGuardada
     */
    public function setFormula($formula)
    {
        $this->formula = $formula;

        return $this;
    }

    /**
     * Get formula
     *
     * @return string 
     */
    public function getFormula()
    {
        return $this->formula;
    }
}

<?php

namespace Tecspro\TablaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;

/**
 * RigidezFilterType filtro.
 * @author Nombre Apellido <name@gmail.com>
 */
class RigidezFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tipo', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('vehiculo', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('categoria', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('lado', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('desvicacionEstandar', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('batalla', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('categoria1', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('desviacionEstandar1', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('categoria2', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('desviacionEstandar2', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('fuente', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
        ;

        $listener = function(FormEvent $event)
        {
            // Is data empty?
            foreach ((array)$event->getForm()->getData() as $data) {
                if ( is_array($data)) {
                    foreach ($data as $subData) {
                        if (!empty($subData)) {
                            return;
                        }
                    }
                } else {
                    if (!empty($data)) {
                        return;
                    }    
                }
            }
            $event->getForm()->addError(new FormError('Filter empty'));
        };
        $builder->addEventListener(FormEvents::POST_SUBMIT, $listener);
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Tecspro\TablaBundle\Entity\Rigidez'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'tecspro_tablabundle_rigidezfiltertype';
    }
}

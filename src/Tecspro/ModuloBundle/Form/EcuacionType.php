<?php

namespace Tecspro\ModuloBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Tecspro\ModuloBundle\Form\GrupoType;
use Doctrine\ORM\EntityRepository;

/**
 * FormulaType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class EcuacionType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('translations', 'a2lix_translations', array(
                    'required_locales' => array('es', 'pt'),
                    'fields' => array(
                        'nombre' => array(
                            'field_type' => 'text',
                            'locale_options' => array(
                                'es' => array(
                                    'label' => 'Nombre Español',
                                ),
                                'pt' => array(
                                    'label' => 'Nombre Portugues',
                                )
                            )
                        )
                    )
                ))
                ->add('expresion', "textarea")
                ->add('unidadResultado', 'collection', array(
                    'type' => new UnidadResultadoOrdenType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'required' => true,
                    'by_reference' => false,
                    'prototype' => true,
                    'prototype_name' => '__nameembedsegundo__',
                    'label' => false
                ))
        /* ->add('unidadResultado', 'select2', array(
          'label' => false,
          'class' => 'ModuloBundle:UnidadResultadoOrden',
          'url' => 'autocomplete_get_grupo_unidad',
          'required' => false,
          'configs' => array(
          'multiple' => true, //es requerido true o false
          'width' => '100%'
          ),
          )) */
        /* ->add('unidadResultado')
          ->add('resultadoMaximo')
          ->add('resultadoMinimo')
          ->add('resultado') */
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Tecspro\ModuloBundle\Entity\Ecuacion',
            'cascade_validation' => true,
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'tecspro_modulobundle_ecuacion';
    }

}

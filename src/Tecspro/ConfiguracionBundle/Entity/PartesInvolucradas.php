<?php

namespace Tecspro\ConfiguracionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PartesInvolucradas
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class PartesInvolucradas
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var \Tecspro\ConfiguracionBundle\Entity\ArchivoGuardado
     *
     * @ORM\ManyToOne(targetEntity="Tecspro\ConfiguracionBundle\Entity\ArchivoGuardado", inversedBy="partesInvolucradas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="archivoGuardado", referencedColumnName="id")
     * })
     */
    private $archivoGuardado;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return PartesInvolucradas
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }
}

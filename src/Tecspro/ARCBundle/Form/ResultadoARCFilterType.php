<?php

namespace Tecspro\ARCBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;

/**
 * ResultadoARCFilterType filtro.
 * @author Nombre Apellido <name@gmail.com>
 */
class ResultadoARCFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tiempo', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('posicion', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('velocidad', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('estado', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
        ;

        $listener = function(FormEvent $event)
        {
            // Is data empty?
            foreach ((array)$event->getForm()->getData() as $data) {
                if ( is_array($data)) {
                    foreach ($data as $subData) {
                        if (!empty($subData)) {
                            return;
                        }
                    }
                } else {
                    if (!empty($data)) {
                        return;
                    }    
                }
            }
            $event->getForm()->addError(new FormError('Filter empty'));
        };
        $builder->addEventListener(FormEvents::POST_SUBMIT, $listener);
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Tecspro\ARCBundle\Entity\ResultadoARC'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'tecspro_arcbundle_resultadoarcfiltertype';
    }
}

-- MySQL dump 10.13  Distrib 5.5.46, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: doctos
-- ------------------------------------------------------
-- Server version	5.5.46-0+deb7u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `doctos`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `doctos` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `doctos`;

--
-- Table structure for table `ArchivoGuardado`
--

DROP TABLE IF EXISTS `ArchivoGuardado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ArchivoGuardado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime NOT NULL,
  `nombreProyecto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fechaIncidente` datetime NOT NULL,
  `comentario` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ArchivoGuardado`
--

LOCK TABLES `ArchivoGuardado` WRITE;
/*!40000 ALTER TABLE `ArchivoGuardado` DISABLE KEYS */;
/*!40000 ALTER TABLE `ArchivoGuardado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Auto`
--

DROP TABLE IF EXISTS `Auto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Auto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `marca` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `modelo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `anio` int(11) NOT NULL,
  `cilindrada` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `largo` double NOT NULL,
  `ancho` double NOT NULL,
  `alto` double NOT NULL,
  `distanciaEntreEjes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pesoVacio` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comentario` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `categorizacion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `trocha` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Auto`
--

LOCK TABLES `Auto` WRITE;
/*!40000 ALTER TABLE `Auto` DISABLE KEYS */;
/*!40000 ALTER TABLE `Auto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Categoria`
--

DROP TABLE IF EXISTS `Categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `modulo_id` int(11) DEFAULT NULL,
  `orden` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_CCE1908EC07F55F5` (`modulo_id`),
  CONSTRAINT `FK_CCE1908EC07F55F5` FOREIGN KEY (`modulo_id`) REFERENCES `Modulo` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Categoria`
--

LOCK TABLES `Categoria` WRITE;
/*!40000 ALTER TABLE `Categoria` DISABLE KEYS */;
INSERT INTO `Categoria` VALUES (1,2,1),(2,2,2),(3,2,3),(4,2,4),(5,2,5),(6,2,6),(7,2,7),(8,2,8),(9,2,9),(10,2,10),(11,2,11),(12,2,12);
/*!40000 ALTER TABLE `Categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CategoriaTranslation`
--

DROP TABLE IF EXISTS `CategoriaTranslation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CategoriaTranslation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_D68D9B182C2AC5D34180C698` (`translatable_id`,`locale`),
  KEY `IDX_D68D9B182C2AC5D3` (`translatable_id`),
  CONSTRAINT `FK_D68D9B182C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `Categoria` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CategoriaTranslation`
--

LOCK TABLES `CategoriaTranslation` WRITE;
/*!40000 ALTER TABLE `CategoriaTranslation` DISABLE KEYS */;
INSERT INTO `CategoriaTranslation` VALUES (1,1,'Matemáticas','es'),(2,1,'Matemáticas','pt'),(3,2,'Cinemática','es'),(4,2,'Cinemática','pt'),(5,3,'Trabajo-Energía','es'),(6,3,'Trabalho-Energia','pt'),(7,4,'Caída','es'),(8,4,'Queda','pt'),(9,5,'Roto-traslación','es'),(10,5,'Rota-translação','pt'),(11,6,'Momentum lineal','es'),(12,6,'Quantidade de movimiento','pt'),(13,7,'Atropello','es'),(14,7,'Atropelo','pt'),(15,8,'Motocicletas','es'),(16,8,'Motociletas','pt'),(17,9,'Hidroplaneo','es'),(18,9,'Hidroplanagem','pt'),(19,10,'Vuelcos','es'),(20,10,'Capotagem','pt'),(21,11,'Deformaciones','es'),(22,11,'Deformações','pt'),(23,12,'Personales','es'),(24,12,'Pessoal','pt');
/*!40000 ALTER TABLE `CategoriaTranslation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CentroGravedad`
--

DROP TABLE IF EXISTS `CentroGravedad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CentroGravedad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `batallaCategoria1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `batallaCategoria2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `batallaCategoria3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `batallaCategoria4` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `batallaCateogira5` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `altura` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desviacionEstandar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CentroGravedad`
--

LOCK TABLES `CentroGravedad` WRITE;
/*!40000 ALTER TABLE `CentroGravedad` DISABLE KEYS */;
/*!40000 ALTER TABLE `CentroGravedad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CentroGravedadTranslation`
--

DROP TABLE IF EXISTS `CentroGravedadTranslation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CentroGravedadTranslation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) DEFAULT NULL,
  `vehiculo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `indice` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `categoria` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_63DA611D2C2AC5D34180C698` (`translatable_id`,`locale`),
  KEY `IDX_63DA611D2C2AC5D3` (`translatable_id`),
  CONSTRAINT `FK_63DA611D2C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `CentroGravedad` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CentroGravedadTranslation`
--

LOCK TABLES `CentroGravedadTranslation` WRITE;
/*!40000 ALTER TABLE `CentroGravedadTranslation` DISABLE KEYS */;
/*!40000 ALTER TABLE `CentroGravedadTranslation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CoeficienteArrastrePeaton`
--

DROP TABLE IF EXISTS `CoeficienteArrastrePeaton`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CoeficienteArrastrePeaton` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `autor` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `factorArrastre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CoeficienteArrastrePeaton`
--

LOCK TABLES `CoeficienteArrastrePeaton` WRITE;
/*!40000 ALTER TABLE `CoeficienteArrastrePeaton` DISABLE KEYS */;
/*!40000 ALTER TABLE `CoeficienteArrastrePeaton` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CoeficienteArrastrePeatonTranslation`
--

DROP TABLE IF EXISTS `CoeficienteArrastrePeatonTranslation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CoeficienteArrastrePeatonTranslation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) DEFAULT NULL,
  `trayectoria` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Notas` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_83B2EC2A2C2AC5D34180C698` (`translatable_id`,`locale`),
  KEY `IDX_83B2EC2A2C2AC5D3` (`translatable_id`),
  CONSTRAINT `FK_83B2EC2A2C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `CoeficienteArrastrePeaton` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CoeficienteArrastrePeatonTranslation`
--

LOCK TABLES `CoeficienteArrastrePeatonTranslation` WRITE;
/*!40000 ALTER TABLE `CoeficienteArrastrePeatonTranslation` DISABLE KEYS */;
/*!40000 ALTER TABLE `CoeficienteArrastrePeatonTranslation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Constante`
--

DROP TABLE IF EXISTS `Constante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Constante` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unidad_id` int(11) DEFAULT NULL,
  `valor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E5E8C8CA9D01464C` (`unidad_id`),
  CONSTRAINT `FK_E5E8C8CA9D01464C` FOREIGN KEY (`unidad_id`) REFERENCES `Unidad` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Constante`
--

LOCK TABLES `Constante` WRITE;
/*!40000 ALTER TABLE `Constante` DISABLE KEYS */;
INSERT INTO `Constante` VALUES (1,3,'9.81');
/*!40000 ALTER TABLE `Constante` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ConstanteTranslation`
--

DROP TABLE IF EXISTS `ConstanteTranslation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ConstanteTranslation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_5045F8EF2C2AC5D34180C698` (`translatable_id`,`locale`),
  KEY `IDX_5045F8EF2C2AC5D3` (`translatable_id`),
  CONSTRAINT `FK_5045F8EF2C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `Constante` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ConstanteTranslation`
--

LOCK TABLES `ConstanteTranslation` WRITE;
/*!40000 ALTER TABLE `ConstanteTranslation` DISABLE KEYS */;
INSERT INTO `ConstanteTranslation` VALUES (1,1,'Aceleración de la gavedad','es'),(2,1,'Aceleração da gavedade','pt');
/*!40000 ALTER TABLE `ConstanteTranslation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Domicilio`
--

DROP TABLE IF EXISTS `Domicilio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Domicilio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `localidad` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provincia` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pais` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `User_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1965AC7268D3EA09` (`User_id`),
  CONSTRAINT `FK_1965AC7268D3EA09` FOREIGN KEY (`User_id`) REFERENCES `fos_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Domicilio`
--

LOCK TABLES `Domicilio` WRITE;
/*!40000 ALTER TABLE `Domicilio` DISABLE KEYS */;
/*!40000 ALTER TABLE `Domicilio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Ecuacion`
--

DROP TABLE IF EXISTS `Ecuacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Ecuacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `formula_id` int(11) DEFAULT NULL,
  `expresion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `resultadoMaximo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resultadoMinimo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resultadoMedio` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unidadSeleccionada` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_67461462A50A6386` (`formula_id`),
  CONSTRAINT `FK_67461462A50A6386` FOREIGN KEY (`formula_id`) REFERENCES `Formula` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Ecuacion`
--

LOCK TABLES `Ecuacion` WRITE;
/*!40000 ALTER TABLE `Ecuacion` DISABLE KEYS */;
INSERT INTO `Ecuacion` VALUES (1,1,'([S]/[t])',NULL,NULL,NULL,3),(2,2,'((V/3.6)*t)',NULL,NULL,NULL,1),(3,3,'([S]/[V/3.6])',NULL,NULL,NULL,2),(4,4,'(abs((([Vf]/3.6)^2-([Vo]/3.6)^2)/(2*[a])))',NULL,NULL,NULL,1),(5,5,'(((Vo/3.6)^2)/(19.62*µ))+((Vo/3.6)*tr)',NULL,NULL,NULL,1),(6,6,'((Vo/3.6)/(9.81*µ)+tr)',NULL,NULL,NULL,2),(7,7,'((a)^2+(b)^2)^0.5',NULL,NULL,NULL,6),(8,8,'(atan([a]/[b]))*57.2958',NULL,NULL,NULL,7),(9,9,'[M]*9.81*[µ]*[s]',NULL,NULL,NULL,10);
/*!40000 ALTER TABLE `Ecuacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EcuacionGuardada`
--

DROP TABLE IF EXISTS `EcuacionGuardada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EcuacionGuardada` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ecuacion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `unidadResultado` decimal(10,0) NOT NULL,
  `resultadoMaximo` decimal(10,0) NOT NULL,
  `resultadoMinimo` decimal(10,0) NOT NULL,
  `resultado` decimal(10,0) NOT NULL,
  `formulaGuardada_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_8DA9E75814484846` (`formulaGuardada_id`),
  CONSTRAINT `FK_8DA9E75814484846` FOREIGN KEY (`formulaGuardada_id`) REFERENCES `FormulaGuardada` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EcuacionGuardada`
--

LOCK TABLES `EcuacionGuardada` WRITE;
/*!40000 ALTER TABLE `EcuacionGuardada` DISABLE KEYS */;
/*!40000 ALTER TABLE `EcuacionGuardada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EcuacionTranslation`
--

DROP TABLE IF EXISTS `EcuacionTranslation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EcuacionTranslation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_63AE17FC2C2AC5D34180C698` (`translatable_id`,`locale`),
  KEY `IDX_63AE17FC2C2AC5D3` (`translatable_id`),
  CONSTRAINT `FK_63AE17FC2C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `Ecuacion` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EcuacionTranslation`
--

LOCK TABLES `EcuacionTranslation` WRITE;
/*!40000 ALTER TABLE `EcuacionTranslation` DISABLE KEYS */;
INSERT INTO `EcuacionTranslation` VALUES (1,1,'Velocidad constante','es'),(2,1,'Velocidade constante','pt'),(3,2,'Espacio con Vel. constante','es'),(4,2,'Espaços con Vel. constante','pt'),(5,3,'Tiempo con Vel. constante','es'),(6,3,'Tempo com Vel. constante','pt'),(7,4,'Distancia en desaceleración','es'),(8,4,'Distância retrógrado','pt'),(9,5,'Distancia de frenado','es'),(10,5,'istância de frenagem','pt'),(11,6,'Tiempo de parada','es'),(12,6,'Tempo para parar','pt'),(13,7,'Hipotenusa','es'),(14,7,'Hipotenusa','pt'),(15,8,'Ángulo por tangente','es'),(16,8,'Ângulo por tangente','pt'),(17,9,'Trabajo simple','es'),(18,9,'Trabalho simple','pt');
/*!40000 ALTER TABLE `EcuacionTranslation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EficienciaProyeccion`
--

DROP TABLE IF EXISTS `EficienciaProyeccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EficienciaProyeccion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alturaCentroGravedadPeatonSobreCapoMetros` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `minima` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EficienciaProyeccion`
--

LOCK TABLES `EficienciaProyeccion` WRITE;
/*!40000 ALTER TABLE `EficienciaProyeccion` DISABLE KEYS */;
/*!40000 ALTER TABLE `EficienciaProyeccion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Formula`
--

DROP TABLE IF EXISTS `Formula`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Formula` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grupo_id` int(11) DEFAULT NULL,
  `oculta` tinyint(1) NOT NULL,
  `file_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imprimir` tinyint(1) NOT NULL,
  `autoincremental` tinyint(1) NOT NULL,
  `orden` int(11) NOT NULL,
  `sincronizado` tinyint(1) NOT NULL,
  `json` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `IDX_A88C611D9C833003` (`grupo_id`),
  CONSTRAINT `FK_A88C611D9C833003` FOREIGN KEY (`grupo_id`) REFERENCES `Grupo` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Formula`
--

LOCK TABLES `Formula` WRITE;
/*!40000 ALTER TABLE `Formula` DISABLE KEYS */;
INSERT INTO `Formula` VALUES (1,1,0,'0c41d8c0aeb0214b7d685d7352b7dfa2bc504f27.png',1,0,1,0,NULL),(2,2,0,'7fd27bd945b78cd3d88cf782c00a2a82ad53e5d1.png',1,0,1,0,NULL),(3,3,0,'af3c32e4b462be85cee46aa28492d071105f5fe6.png',1,0,1,0,NULL),(4,4,0,'4d11b75a6afa8bc46699825163f73c9ee960f7e5.png',1,0,1,0,NULL),(5,5,0,'00717d42a20b7f6f970c2ef63303bc68ea7f2cc8.png',1,0,1,0,NULL),(6,6,0,'62ad48cf0d171ad24cc40e9d57d9580d00a977c1.png',1,0,1,0,NULL),(7,7,0,'4ed447b91b9287d473425fbc066d34f0fb062994.png',1,0,1,0,NULL),(8,8,0,'a7d89e4b6e7d993d8785cfa481e08b7607e805f2.png',1,0,1,0,NULL),(9,9,0,'8153664952e0c278a7afc88bc907c36bce4ba053.png',1,0,1,0,NULL);
/*!40000 ALTER TABLE `Formula` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FormulaGuardada`
--

DROP TABLE IF EXISTS `FormulaGuardada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FormulaGuardada` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `formula` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `archivoGuardado` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_9DBFD930151B1D74` (`archivoGuardado`),
  CONSTRAINT `FK_9DBFD930151B1D74` FOREIGN KEY (`archivoGuardado`) REFERENCES `ArchivoGuardado` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FormulaGuardada`
--

LOCK TABLES `FormulaGuardada` WRITE;
/*!40000 ALTER TABLE `FormulaGuardada` DISABLE KEYS */;
/*!40000 ALTER TABLE `FormulaGuardada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FormulaTranslation`
--

DROP TABLE IF EXISTS `FormulaTranslation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FormulaTranslation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_907B23582C2AC5D34180C698` (`translatable_id`,`locale`),
  KEY `IDX_907B23582C2AC5D3` (`translatable_id`),
  CONSTRAINT `FK_907B23582C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `Formula` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FormulaTranslation`
--

LOCK TABLES `FormulaTranslation` WRITE;
/*!40000 ALTER TABLE `FormulaTranslation` DISABLE KEYS */;
INSERT INTO `FormulaTranslation` VALUES (1,1,'Velocidad constante','91640d37f00d5756a6c34fa9504fdfcff490d63e.pdf','es'),(2,1,'Velocidade constante',NULL,'pt'),(3,2,'Espacio con Vel. cte.','4d7257d3bded98fb8e96e450193a38cf961d1c45.pdf','es'),(4,2,'Espaços con Vel. cte.',NULL,'pt'),(5,3,'Tiempo con Vel. cte.',NULL,'es'),(6,3,'Tempo com Vel. cte.',NULL,'pt'),(7,4,'Distancia en desaceleración',NULL,'es'),(8,4,'Distância retrógrado',NULL,'pt'),(9,5,'Distancia de frenado',NULL,'es'),(10,5,'Distância de frenagem',NULL,'pt'),(11,6,'Tiempo de parada',NULL,'es'),(12,6,'Tempo para parar',NULL,'pt'),(13,7,'Hipotenusa',NULL,'es'),(14,7,'Hipotenusa',NULL,'pt'),(15,8,'Ángulo por tangente',NULL,'es'),(16,8,'Ângulo por tangente',NULL,'pt'),(17,9,'Trabajo simple',NULL,'es'),(18,9,'Trabalho simple',NULL,'pt');
/*!40000 ALTER TABLE `FormulaTranslation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Grupo`
--

DROP TABLE IF EXISTS `Grupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Grupo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoria_id` int(11) DEFAULT NULL,
  `oculto` tinyint(1) NOT NULL,
  `orden` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_4DCFB4D73397707A` (`categoria_id`),
  CONSTRAINT `FK_4DCFB4D73397707A` FOREIGN KEY (`categoria_id`) REFERENCES `Categoria` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Grupo`
--

LOCK TABLES `Grupo` WRITE;
/*!40000 ALTER TABLE `Grupo` DISABLE KEYS */;
INSERT INTO `Grupo` VALUES (1,2,0,1),(2,2,0,2),(3,2,0,3),(4,2,0,4),(5,2,0,5),(6,2,0,6),(7,1,0,1),(8,1,0,2),(9,3,0,1);
/*!40000 ALTER TABLE `Grupo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GrupoTranslation`
--

DROP TABLE IF EXISTS `GrupoTranslation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GrupoTranslation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_466A07662C2AC5D34180C698` (`translatable_id`,`locale`),
  KEY `IDX_466A07662C2AC5D3` (`translatable_id`),
  CONSTRAINT `FK_466A07662C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `Grupo` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GrupoTranslation`
--

LOCK TABLES `GrupoTranslation` WRITE;
/*!40000 ALTER TABLE `GrupoTranslation` DISABLE KEYS */;
INSERT INTO `GrupoTranslation` VALUES (1,1,'Velocidad constante','es'),(2,1,'Velocidade constante','pt'),(3,2,'Espacio con Vel. constante','es'),(4,2,'Espaços con Vel. constante','pt'),(5,3,'Tiempo con Vel. cte.','es'),(6,3,'Tempo com Vel. cte.','pt'),(7,4,'Distancia en desaceleración','es'),(8,4,'Distância retrógrado','pt'),(9,5,'Distancia de frenado','es'),(10,5,'Distância de frenagem','pt'),(11,6,'Tiempo de parada','es'),(12,6,'Tempo para parar','pt'),(13,7,'Hipotenusa','es'),(14,7,'Hipotenusa','pt'),(15,8,'Ángulo por tangente','es'),(16,8,'Ângulo por tangente','pt'),(17,9,'Trabajo simple','es'),(18,9,'Trabalho simple','pt');
/*!40000 ALTER TABLE `GrupoTranslation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Inercia`
--

DROP TABLE IF EXISTS `Inercia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Inercia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `batalla` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unobatallaMenor2P896` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unobatallaMenor2P642` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unoDesviacionEstandar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dosbatallaMayor2P896` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dosDesviacionEstandar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dosbatallaMayor2P642` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unobatallaMenor2P931` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dosbatallaMayor2P931` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Inercia`
--

LOCK TABLES `Inercia` WRITE;
/*!40000 ALTER TABLE `Inercia` DISABLE KEYS */;
/*!40000 ALTER TABLE `Inercia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `InerciaTranslation`
--

DROP TABLE IF EXISTS `InerciaTranslation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `InerciaTranslation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) DEFAULT NULL,
  `vehiculo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `categoria` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `indicador` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_69C912832C2AC5D34180C698` (`translatable_id`,`locale`),
  KEY `IDX_69C912832C2AC5D3` (`translatable_id`),
  CONSTRAINT `FK_69C912832C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `Inercia` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `InerciaTranslation`
--

LOCK TABLES `InerciaTranslation` WRITE;
/*!40000 ALTER TABLE `InerciaTranslation` DISABLE KEYS */;
/*!40000 ALTER TABLE `InerciaTranslation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Modulo`
--

DROP TABLE IF EXISTS `Modulo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Modulo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ArcIsActive` tinyint(1) NOT NULL,
  `tablas` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Modulo`
--

LOCK TABLES `Modulo` WRITE;
/*!40000 ALTER TABLE `Modulo` DISABLE KEYS */;
INSERT INTO `Modulo` VALUES (2,1,'a:0:{}');
/*!40000 ALTER TABLE `Modulo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ModuloTranslation`
--

DROP TABLE IF EXISTS `ModuloTranslation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ModuloTranslation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_CFCBC62C2AC5D34180C698` (`translatable_id`,`locale`),
  KEY `IDX_CFCBC62C2AC5D3` (`translatable_id`),
  CONSTRAINT `FK_CFCBC62C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `Modulo` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ModuloTranslation`
--

LOCK TABLES `ModuloTranslation` WRITE;
/*!40000 ALTER TABLE `ModuloTranslation` DISABLE KEYS */;
INSERT INTO `ModuloTranslation` VALUES (3,2,'Accidentología','es'),(4,2,'Reconstrução de Acidentes de Trânsito','pt');
/*!40000 ALTER TABLE `ModuloTranslation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Motocicleta`
--

DROP TABLE IF EXISTS `Motocicleta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Motocicleta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `marca` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `modelo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `anio` int(11) NOT NULL,
  `cilindrada` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `largo` double NOT NULL,
  `ancho` double NOT NULL,
  `alto` double NOT NULL,
  `distanciaEntreEjes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pesoVacio` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comentario` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Motocicleta`
--

LOCK TABLES `Motocicleta` WRITE;
/*!40000 ALTER TABLE `Motocicleta` DISABLE KEYS */;
/*!40000 ALTER TABLE `Motocicleta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ParteInvolucrada`
--

DROP TABLE IF EXISTS `ParteInvolucrada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ParteInvolucrada` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proyecto_id` int(11) DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_8CAFFD2AF625D1BA` (`proyecto_id`),
  CONSTRAINT `FK_8CAFFD2AF625D1BA` FOREIGN KEY (`proyecto_id`) REFERENCES `Proyecto` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ParteInvolucrada`
--

LOCK TABLES `ParteInvolucrada` WRITE;
/*!40000 ALTER TABLE `ParteInvolucrada` DISABLE KEYS */;
/*!40000 ALTER TABLE `ParteInvolucrada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PartesInvolucradas`
--

DROP TABLE IF EXISTS `PartesInvolucradas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PartesInvolucradas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `archivoGuardado` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_46FD21EF151B1D74` (`archivoGuardado`),
  CONSTRAINT `FK_46FD21EF151B1D74` FOREIGN KEY (`archivoGuardado`) REFERENCES `ArchivoGuardado` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PartesInvolucradas`
--

LOCK TABLES `PartesInvolucradas` WRITE;
/*!40000 ALTER TABLE `PartesInvolucradas` DISABLE KEYS */;
/*!40000 ALTER TABLE `PartesInvolucradas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Pickups`
--

DROP TABLE IF EXISTS `Pickups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Pickups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoria` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alturaCentroGravedad` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `desviacionEstandar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Pickups`
--

LOCK TABLES `Pickups` WRITE;
/*!40000 ALTER TABLE `Pickups` DISABLE KEYS */;
/*!40000 ALTER TABLE `Pickups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PropiedadTabla`
--

DROP TABLE IF EXISTS `PropiedadTabla`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PropiedadTabla` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fuente` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tabla` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PropiedadTabla`
--

LOCK TABLES `PropiedadTabla` WRITE;
/*!40000 ALTER TABLE `PropiedadTabla` DISABLE KEYS */;
/*!40000 ALTER TABLE `PropiedadTabla` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Proyecto`
--

DROP TABLE IF EXISTS `Proyecto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Proyecto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `modulo_id` int(11) DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comentario` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fecha` date NOT NULL,
  `formulas` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:array)',
  PRIMARY KEY (`id`),
  KEY `IDX_96A460EFA76ED395` (`user_id`),
  KEY `IDX_96A460EFC07F55F5` (`modulo_id`),
  CONSTRAINT `FK_96A460EFA76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`),
  CONSTRAINT `FK_96A460EFC07F55F5` FOREIGN KEY (`modulo_id`) REFERENCES `Modulo` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Proyecto`
--

LOCK TABLES `Proyecto` WRITE;
/*!40000 ALTER TABLE `Proyecto` DISABLE KEYS */;
INSERT INTO `Proyecto` VALUES (1,1,2,'elpy','un pi','2016-04-19','N;'),(2,1,2,'el prot',NULL,'2016-04-19','N;'),(3,1,2,'Nuevo Proyecto 21/04/2016 01:20',NULL,'2016-04-21','N;');
/*!40000 ALTER TABLE `Proyecto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Referencia`
--

DROP TABLE IF EXISTS `Referencia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Referencia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `referencia_id` int(11) DEFAULT NULL,
  `idioma` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_8F4F1008778D91A2` (`referencia_id`),
  CONSTRAINT `FK_8F4F1008778D91A2` FOREIGN KEY (`referencia_id`) REFERENCES `Formula` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Referencia`
--

LOCK TABLES `Referencia` WRITE;
/*!40000 ALTER TABLE `Referencia` DISABLE KEYS */;
/*!40000 ALTER TABLE `Referencia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ResultadoARC`
--

DROP TABLE IF EXISTS `ResultadoARC`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ResultadoARC` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `arc_campos_id` int(11) DEFAULT NULL,
  `tiempo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `posicion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `velocidad` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_6860E367F64DB9A2` (`arc_campos_id`),
  CONSTRAINT `FK_6860E367F64DB9A2` FOREIGN KEY (`arc_campos_id`) REFERENCES `arc_campos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ResultadoARC`
--

LOCK TABLES `ResultadoARC` WRITE;
/*!40000 ALTER TABLE `ResultadoARC` DISABLE KEYS */;
/*!40000 ALTER TABLE `ResultadoARC` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Rigidez`
--

DROP TABLE IF EXISTS `Rigidez`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Rigidez` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `desvicacionEstandar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `batalla` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `categoria1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desviacionEstandar1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `categoria2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desviacionEstandar2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ladoCategoria` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Rigidez`
--

LOCK TABLES `Rigidez` WRITE;
/*!40000 ALTER TABLE `Rigidez` DISABLE KEYS */;
/*!40000 ALTER TABLE `Rigidez` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RigidezTranslation`
--

DROP TABLE IF EXISTS `RigidezTranslation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RigidezTranslation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) DEFAULT NULL,
  `tipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vehiculo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `categoria` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lado` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_5E407A52C2AC5D34180C698` (`translatable_id`,`locale`),
  KEY `IDX_5E407A52C2AC5D3` (`translatable_id`),
  CONSTRAINT `FK_5E407A52C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `Rigidez` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RigidezTranslation`
--

LOCK TABLES `RigidezTranslation` WRITE;
/*!40000 ALTER TABLE `RigidezTranslation` DISABLE KEYS */;
/*!40000 ALTER TABLE `RigidezTranslation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TablaFriccion`
--

DROP TABLE IF EXISTS `TablaFriccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TablaFriccion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `secaMenos50kmhr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `secaMas50Kmhr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HumerdadMenos50Kmhr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `humedadMas50Kmhr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TablaFriccion`
--

LOCK TABLES `TablaFriccion` WRITE;
/*!40000 ALTER TABLE `TablaFriccion` DISABLE KEYS */;
/*!40000 ALTER TABLE `TablaFriccion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TablaFriccionTranslation`
--

DROP TABLE IF EXISTS `TablaFriccionTranslation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TablaFriccionTranslation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) DEFAULT NULL,
  `caracteristicasSuperficie` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_A3049E3C2C2AC5D34180C698` (`translatable_id`,`locale`),
  KEY `IDX_A3049E3C2C2AC5D3` (`translatable_id`),
  CONSTRAINT `FK_A3049E3C2C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `TablaFriccion` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TablaFriccionTranslation`
--

LOCK TABLES `TablaFriccionTranslation` WRITE;
/*!40000 ALTER TABLE `TablaFriccionTranslation` DISABLE KEYS */;
/*!40000 ALTER TABLE `TablaFriccionTranslation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TablaInercia`
--

DROP TABLE IF EXISTS `TablaInercia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TablaInercia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `indicador` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batalla` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `desviacionEstandar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Categoria` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fuente` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TablaInercia`
--

LOCK TABLES `TablaInercia` WRITE;
/*!40000 ALTER TABLE `TablaInercia` DISABLE KEYS */;
/*!40000 ALTER TABLE `TablaInercia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TablaRigidez`
--

DROP TABLE IF EXISTS `TablaRigidez`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TablaRigidez` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lado` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `categoria1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `desviacionEstandarCategoria1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `categoria2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `desviacionEstandarCategoria2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fuente` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TablaRigidez`
--

LOCK TABLES `TablaRigidez` WRITE;
/*!40000 ALTER TABLE `TablaRigidez` DISABLE KEYS */;
/*!40000 ALTER TABLE `TablaRigidez` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Telefono`
--

DROP TABLE IF EXISTS `Telefono`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Telefono` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `tipo` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `numero` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_38916829A76ED395` (`user_id`),
  CONSTRAINT `FK_38916829A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Telefono`
--

LOCK TABLES `Telefono` WRITE;
/*!40000 ALTER TABLE `Telefono` DISABLE KEYS */;
/*!40000 ALTER TABLE `Telefono` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Unidad`
--

DROP TABLE IF EXISTS `Unidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Unidad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `simbolo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `magnitud` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `operacion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Unidad`
--

LOCK TABLES `Unidad` WRITE;
/*!40000 ALTER TABLE `Unidad` DISABLE KEYS */;
INSERT INTO `Unidad` VALUES (1,'[m]','Longitud','+0'),(2,'[seg]','Tiempo','+0'),(3,'[m/seg²]','Aceleración','+0'),(4,'[m/seg]','Velocidad','*3.6'),(5,'[km/hr]','Velocidad','/3.6'),(6,'[m²]','Longitud al cuadrado','+0'),(7,'[°]','Grados','+0'),(8,'Sin unidad','Sin unidad','+0'),(9,'[kg]','Masa','+0'),(10,'[J]','Trabajo','+0');
/*!40000 ALTER TABLE `Unidad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UnidadResultadoOrden`
--

DROP TABLE IF EXISTS `UnidadResultadoOrden`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UnidadResultadoOrden` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unidad_id` int(11) DEFAULT NULL,
  `ecuacion_id` int(11) DEFAULT NULL,
  `principal` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_5D838F9C9D01464C` (`unidad_id`),
  KEY `IDX_5D838F9C1A9F663E` (`ecuacion_id`),
  CONSTRAINT `FK_5D838F9C1A9F663E` FOREIGN KEY (`ecuacion_id`) REFERENCES `Ecuacion` (`id`),
  CONSTRAINT `FK_5D838F9C9D01464C` FOREIGN KEY (`unidad_id`) REFERENCES `Unidad` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UnidadResultadoOrden`
--

LOCK TABLES `UnidadResultadoOrden` WRITE;
/*!40000 ALTER TABLE `UnidadResultadoOrden` DISABLE KEYS */;
/*!40000 ALTER TABLE `UnidadResultadoOrden` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UnidadTranslation`
--

DROP TABLE IF EXISTS `UnidadTranslation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UnidadTranslation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_6EEBE7D22C2AC5D34180C698` (`translatable_id`,`locale`),
  KEY `IDX_6EEBE7D22C2AC5D3` (`translatable_id`),
  CONSTRAINT `FK_6EEBE7D22C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `Unidad` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UnidadTranslation`
--

LOCK TABLES `UnidadTranslation` WRITE;
/*!40000 ALTER TABLE `UnidadTranslation` DISABLE KEYS */;
INSERT INTO `UnidadTranslation` VALUES (1,1,'Distancia recorrida en metros','es'),(2,1,'Distancia recorrida en metros','pt'),(3,2,'Tiempo del movimiento','es'),(4,2,'Tiempo del movimiento','pt'),(5,3,'Aceleración de gravedad','es'),(6,3,'Aceleração da gravidade','pt'),(7,4,'Velocidad en m/seg','es'),(8,4,'Velocidade en m/seg','pt'),(9,5,'Velocidad en km/hr','es'),(10,5,'Velocidade em km/hr','pt'),(11,6,'Longitud al cuadrado','es'),(12,6,'Comprimento quadrado','pt'),(13,7,'Grado sexagesimal','es'),(14,7,'Grau sexagesimal','pt'),(15,8,'Sin unidad','es'),(16,8,'Sim unidade','pt'),(17,9,'Masa','es'),(18,9,'Massa','pt'),(19,10,'Joule','es'),(20,10,'Joule','pt');
/*!40000 ALTER TABLE `UnidadTranslation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Variable`
--

DROP TABLE IF EXISTS `Variable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Variable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unidad_id` int(11) DEFAULT NULL,
  `formula_id` int(11) DEFAULT NULL,
  `simbolo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `autoincrementar` tinyint(1) NOT NULL,
  `particiones` int(11) NOT NULL,
  `valorDesde` decimal(10,0) NOT NULL,
  `valorHasta` decimal(10,0) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_353BE5DB9D01464C` (`unidad_id`),
  KEY `IDX_353BE5DBA50A6386` (`formula_id`),
  CONSTRAINT `FK_353BE5DB9D01464C` FOREIGN KEY (`unidad_id`) REFERENCES `Unidad` (`id`),
  CONSTRAINT `FK_353BE5DBA50A6386` FOREIGN KEY (`formula_id`) REFERENCES `Formula` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Variable`
--

LOCK TABLES `Variable` WRITE;
/*!40000 ALTER TABLE `Variable` DISABLE KEYS */;
INSERT INTO `Variable` VALUES (1,1,1,'[S]',0,0,0,0),(2,2,1,'[t]',0,0,0,0),(3,5,2,'V',0,0,0,0),(4,2,2,'t',0,0,0,0),(5,1,3,'S',0,0,0,0),(6,5,3,'V',0,0,0,0),(7,5,4,'[Vf]',0,0,0,0),(8,5,4,'[Vo]',0,0,0,0),(9,3,4,'[a]',0,0,0,0),(10,5,5,'Vo',0,0,0,0),(11,3,5,'tr',0,0,0,0),(12,8,5,'µ',0,0,0,0),(13,5,6,'Vo',0,0,0,0),(14,2,6,'Vo',0,0,0,0),(15,8,6,'µ',0,0,0,0),(16,1,7,'a',0,0,0,0),(17,1,7,'b',0,0,0,0),(18,1,8,'[a]',0,0,0,0),(19,1,8,'[b]',0,0,0,0),(20,9,9,'[M]',0,0,0,0),(21,8,9,'[µ]',0,0,0,0),(22,1,9,'[s]',0,0,0,0);
/*!40000 ALTER TABLE `Variable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `VariableGuardada`
--

DROP TABLE IF EXISTS `VariableGuardada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VariableGuardada` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `variable` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valor` decimal(10,0) NOT NULL,
  `formulaGuardada_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_7376990D14484846` (`formulaGuardada_id`),
  CONSTRAINT `FK_7376990D14484846` FOREIGN KEY (`formulaGuardada_id`) REFERENCES `FormulaGuardada` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `VariableGuardada`
--

LOCK TABLES `VariableGuardada` WRITE;
/*!40000 ALTER TABLE `VariableGuardada` DISABLE KEYS */;
/*!40000 ALTER TABLE `VariableGuardada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `VariableTranslation`
--

DROP TABLE IF EXISTS `VariableTranslation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VariableTranslation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_AE0960642C2AC5D34180C698` (`translatable_id`,`locale`),
  KEY `IDX_AE0960642C2AC5D3` (`translatable_id`),
  CONSTRAINT `FK_AE0960642C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `Variable` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `VariableTranslation`
--

LOCK TABLES `VariableTranslation` WRITE;
/*!40000 ALTER TABLE `VariableTranslation` DISABLE KEYS */;
INSERT INTO `VariableTranslation` VALUES (1,1,'Distancia recorrida en metros','es'),(2,1,'Distância percorrida em metros','pt'),(3,2,'Tiempo del movimiento','es'),(4,2,'Tempo do movimento','pt'),(5,3,'Vel. constante en [km/kr]','es'),(6,3,'Vel. constante en [km/kr]','pt'),(7,4,'Tiempo a considerar','es'),(8,4,'Tempo para analise','pt'),(9,5,'Espacio con velocidad cte.','es'),(10,5,'Espaço com velocidade cte.','pt'),(11,6,'Velocidad constante','es'),(12,6,'Velocidade contante','pt'),(13,7,'Velocidad final','es'),(14,7,'Velocidade final','pt'),(15,8,'Velocidad inicial','es'),(16,8,'Velocidade de inicio','pt'),(17,9,'Aceleración','es'),(18,9,'Aceleração','pt'),(19,10,'Velocidad inicial','es'),(20,10,'Velocidade do inicio','pt'),(21,11,'Tiempo de reacción','es'),(22,11,'Tempo do reação','pt'),(23,12,'Coeficiente de fricción','es'),(24,12,'Atrito','pt'),(25,13,'Velocidad inicial','es'),(26,13,'Velocidade ao inicio','pt'),(27,14,'Tiempo de reacción','es'),(28,14,'Tempo de reação','pt'),(29,15,'Coeficiente de fricción','es'),(30,15,'Atrito','pt'),(31,16,'Cateto o lado','es'),(32,16,'Cateto','pt'),(33,17,'Cateto o lado','es'),(34,17,'Cateto','pt'),(35,18,'Cateto opuesto','es'),(36,18,'Cateto puesto','pt'),(37,19,'Cateto adyacente','es'),(38,19,'Cateto adyacente','pt'),(39,20,'Masa del vehículo','es'),(40,20,'Massa do veículo','pt'),(41,21,'Coeficiente de fricción','es'),(42,21,'Atrito','pt'),(43,22,'Distancia de fricción','es'),(44,22,'Distância de frenagem','pt');
/*!40000 ALTER TABLE `VariableTranslation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Vehiculo`
--

DROP TABLE IF EXISTS `Vehiculo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Vehiculo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `marca` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `modelo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `anio` int(11) NOT NULL,
  `cilindrada` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `largo` double NOT NULL,
  `ancho` double NOT NULL,
  `alto` double NOT NULL,
  `distanciaEntreEjes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pesoVacio` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comentario` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Vehiculo`
--

LOCK TABLES `Vehiculo` WRITE;
/*!40000 ALTER TABLE `Vehiculo` DISABLE KEYS */;
/*!40000 ALTER TABLE `Vehiculo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `VehiculosPasajeros`
--

DROP TABLE IF EXISTS `VehiculosPasajeros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VehiculosPasajeros` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `indice` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batallaCategoria1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batallaCategoria2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batallaCategoria3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batallaCategoria4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batallaCategoria5` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fuente` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `VehiculosPasajeros`
--

LOCK TABLES `VehiculosPasajeros` WRITE;
/*!40000 ALTER TABLE `VehiculosPasajeros` DISABLE KEYS */;
/*!40000 ALTER TABLE `VehiculosPasajeros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `arc_campos`
--

DROP TABLE IF EXISTS `arc_campos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `arc_campos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cantidad_vehiculos` int(11) NOT NULL,
  `fraccion_de_tiempo_a_analizar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imprimir` tinyint(1) NOT NULL,
  `proyecto_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_14E5E104F625D1BA` (`proyecto_id`),
  CONSTRAINT `FK_14E5E104F625D1BA` FOREIGN KEY (`proyecto_id`) REFERENCES `Proyecto` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `arc_campos`
--

LOCK TABLES `arc_campos` WRITE;
/*!40000 ALTER TABLE `arc_campos` DISABLE KEYS */;
/*!40000 ALTER TABLE `arc_campos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `arc_vehiculo`
--

DROP TABLE IF EXISTS `arc_vehiculo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `arc_vehiculo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `arc_campos_id` int(11) DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `peaton` tinyint(1) NOT NULL,
  `movimiento_previo_a_la_colision` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `velocidad_inicial` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `unidad_velocidad_inicial` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `velocidad_impacto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unidad_velocidad_impacto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tiempo_reaccion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desaceleracion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tiempo_preimpacto_a_considerar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_558FF584F64DB9A2` (`arc_campos_id`),
  CONSTRAINT `FK_558FF584F64DB9A2` FOREIGN KEY (`arc_campos_id`) REFERENCES `arc_campos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `arc_vehiculo`
--

LOCK TABLES `arc_vehiculo` WRITE;
/*!40000 ALTER TABLE `arc_vehiculo` DISABLE KEYS */;
/*!40000 ALTER TABLE `arc_vehiculo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ext_log_entries`
--

DROP TABLE IF EXISTS `ext_log_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ext_log_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `logged_at` datetime NOT NULL,
  `object_id` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `object_class` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `version` int(11) NOT NULL,
  `data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:array)',
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `log_class_lookup_idx` (`object_class`),
  KEY `log_date_lookup_idx` (`logged_at`),
  KEY `log_user_lookup_idx` (`username`),
  KEY `log_version_lookup_idx` (`object_id`,`object_class`,`version`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ext_log_entries`
--

LOCK TABLES `ext_log_entries` WRITE;
/*!40000 ALTER TABLE `ext_log_entries` DISABLE KEYS */;
INSERT INTO `ext_log_entries` VALUES (1,'create','2016-03-21 08:22:06','1','Tecspro\\UserBundle\\Entity\\User',1,'a:0:{}',NULL),(2,'create','2016-03-21 08:22:08','2','Tecspro\\UserBundle\\Entity\\User',1,'a:0:{}',NULL),(3,'create','2016-03-21 08:22:09','3','Tecspro\\UserBundle\\Entity\\User',1,'a:0:{}',NULL),(4,'remove','2016-03-21 09:33:38','8','Tecspro\\UserBundle\\Entity\\User',1,'N;','superadmin'),(5,'remove','2016-03-21 09:33:46','7','Tecspro\\UserBundle\\Entity\\User',1,'N;','superadmin'),(6,'remove','2016-03-21 09:34:45','4','Tecspro\\UserBundle\\Entity\\User',1,'N;','superadmin');
/*!40000 ALTER TABLE `ext_log_entries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fos_role`
--

DROP TABLE IF EXISTS `fos_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fos_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fos_role`
--

LOCK TABLES `fos_role` WRITE;
/*!40000 ALTER TABLE `fos_role` DISABLE KEYS */;
INSERT INTO `fos_role` VALUES (1,'ROLE_SUPERADMIN','SUPERADMIN'),(2,'ROLE_ADMIN','ADMIN'),(3,'ROLE_CLIENTE','CLIENTE');
/*!40000 ALTER TABLE `fos_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fos_user`
--

DROP TABLE IF EXISTS `fos_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fos_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `plan_de_cuenta` int(11) DEFAULT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `configuraciones` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `creador` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fos_user`
--

LOCK TABLES `fos_user` WRITE;
/*!40000 ALTER TABLE `fos_user` DISABLE KEYS */;
INSERT INTO `fos_user` VALUES (1,'superadmin','superadmin','superadmin@tecspro.com.ar','superadmin@tecspro.com.ar',1,'1utzz987v37ok040g0wso888og8sgg','$2y$13$1utzz987v37ok040g0wsoudmf8L.sD8kpeF2IlTm9E/9sp8lTFh.q','2016-04-22 11:56:34',0,0,NULL,NULL,NULL,'a:0:{}',0,NULL,NULL,NULL,'a:2:{s:12:\"ulitmoModulo\";i:2;s:8:\"lenguaje\";s:2:\"es\";}',''),(2,'admin','admin','admin@tecspro.com.ar','admin@tecspro.com.ar',1,'58gjiv4j3x4wcskcgk0o0kw04wkgkgc','$2y$13$58gjiv4j3x4wcskcgk0o0eCi1owbvzfYEndHo7kcS1u0mCK3bSXjS','2016-03-14 09:39:22',0,0,NULL,NULL,NULL,'a:0:{}',0,NULL,NULL,NULL,'a:0:{}',''),(3,'cliente','cliente','cliente@tecspro.com.ar','cliente@tecspro.com.ar',1,'bdu9wil38vkskk8cos0ko44g8kgk48w','$2y$13$bdu9wil38vkskk8cos0kouh0h1LVuwOm8WcUKbTqEMJeTlBSoTeA2','2016-04-18 21:30:15',0,0,NULL,NULL,NULL,'a:0:{}',0,NULL,365,'2016-03-10','a:1:{s:8:\"lenguaje\";s:2:\"pt\";}',''),(5,'Gustavo A. Enciso','gustavo a. enciso','encisoga@gmail.com','encisoga@gmail.com',1,'byy3byy9c80kcgo8044okwckgsgokwc','$2y$13$byy3byy9c80kcgo8044oku8h8LNszjjnj6oI8A2g2ODWoguUnz.u6','2016-03-18 20:37:25',0,0,'2016-03-19 00:00:00',NULL,NULL,'a:0:{}',0,NULL,5,'2016-03-14','a:0:{}','');
/*!40000 ALTER TABLE `fos_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fos_user_role`
--

DROP TABLE IF EXISTS `fos_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fos_user_role` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `IDX_1F9A8B42A76ED395` (`user_id`),
  KEY `IDX_1F9A8B42D60322AC` (`role_id`),
  CONSTRAINT `FK_1F9A8B42A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_1F9A8B42D60322AC` FOREIGN KEY (`role_id`) REFERENCES `fos_role` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fos_user_role`
--

LOCK TABLES `fos_user_role` WRITE;
/*!40000 ALTER TABLE `fos_user_role` DISABLE KEYS */;
INSERT INTO `fos_user_role` VALUES (1,1),(2,2),(3,3),(5,3);
/*!40000 ALTER TABLE `fos_user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grupo_constante`
--

DROP TABLE IF EXISTS `grupo_constante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grupo_constante` (
  `constante_id` int(11) NOT NULL,
  `grupo_id` int(11) NOT NULL,
  PRIMARY KEY (`constante_id`,`grupo_id`),
  KEY `IDX_8C5F91694FBA3AB7` (`constante_id`),
  KEY `IDX_8C5F91699C833003` (`grupo_id`),
  CONSTRAINT `FK_8C5F91694FBA3AB7` FOREIGN KEY (`constante_id`) REFERENCES `Grupo` (`id`),
  CONSTRAINT `FK_8C5F91699C833003` FOREIGN KEY (`grupo_id`) REFERENCES `Constante` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grupo_constante`
--

LOCK TABLES `grupo_constante` WRITE;
/*!40000 ALTER TABLE `grupo_constante` DISABLE KEYS */;
INSERT INTO `grupo_constante` VALUES (5,1),(9,1);
/*!40000 ALTER TABLE `grupo_constante` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_groups`
--

DROP TABLE IF EXISTS `users_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_groups` (
  `ecuacion_id` int(11) NOT NULL,
  `unidad_id` int(11) NOT NULL,
  PRIMARY KEY (`ecuacion_id`,`unidad_id`),
  KEY `IDX_FF8AB7E01A9F663E` (`ecuacion_id`),
  KEY `IDX_FF8AB7E09D01464C` (`unidad_id`),
  CONSTRAINT `FK_FF8AB7E01A9F663E` FOREIGN KEY (`ecuacion_id`) REFERENCES `Ecuacion` (`id`),
  CONSTRAINT `FK_FF8AB7E09D01464C` FOREIGN KEY (`unidad_id`) REFERENCES `Unidad` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_groups`
--

LOCK TABLES `users_groups` WRITE;
/*!40000 ALTER TABLE `users_groups` DISABLE KEYS */;
INSERT INTO `users_groups` VALUES (1,3),(1,5),(2,1),(3,2),(4,1),(5,1),(6,2),(7,6),(8,7),(9,10);
/*!40000 ALTER TABLE `users_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_modulo`
--

DROP TABLE IF EXISTS `users_modulo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_modulo` (
  `modulo_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`modulo_id`,`user_id`),
  KEY `IDX_E31641A6C07F55F5` (`modulo_id`),
  KEY `IDX_E31641A6A76ED395` (`user_id`),
  CONSTRAINT `FK_E31641A6A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_E31641A6C07F55F5` FOREIGN KEY (`modulo_id`) REFERENCES `Modulo` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_modulo`
--

LOCK TABLES `users_modulo` WRITE;
/*!40000 ALTER TABLE `users_modulo` DISABLE KEYS */;
INSERT INTO `users_modulo` VALUES (2,5);
/*!40000 ALTER TABLE `users_modulo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-22 20:16:32

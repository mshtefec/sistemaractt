<?php

namespace Tecspro\UserBundle\EventListener;

use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine;

class TecsproLoginListener {

    private $ROLE_ADMIN = 'ROLE_ADMIN';
    private $userManager;
    private $session;
    private $em;
    private $security;
    private $detectarNavegador;

    public function __construct(SessionInterface $session, UserManagerInterface $userManager, \Doctrine\ORM\EntityManager $em, $security, $detectarNavegador) {
        $this->em = $em;
        $this->session = $session;
        $this->userManager = $userManager;
        $this->security = $security;
        $this->detectarNavegador = $detectarNavegador;
    }

    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event) {

        $user = $event->getAuthenticationToken()->getUser();
      
        if ($this->security->isGranted($this->ROLE_ADMIN)) {
            $browser = $this->detectarNavegador->getBrowser($event->getRequest()->headers->get('user-agent'));
            $browserFull = $browser['name'] . $browser['version'] . '/' . $browser['platform'];
            $user->setLastLogin(new \DateTime());
            $user->setItemSession($this->session->getId(), array('Navegador' => $browserFull, 'fecha' => (new \DateTime)->format('d/m/Y H:i')));
            $this->userManager->updateUser($user);
            $this->session->start();
        } else {
            if (($user instanceof UserInterface) && (count($user->getCantidadSessiones()) > 0 && count($user->getSessiones()) < $user->getCantidadSessiones())) {
                $browser = $this->detectarNavegador->getBrowser($event->getRequest()->headers->get('user-agent'));
                $browserFull = $browser['name'] . $browser['version'] . '/' . $browser['platform'];
                $user->setLastLogin(new \DateTime());
                $user->setItemSession($this->session->getId(), array('Navegador' => $browserFull, 'fecha' => (new \DateTime)->format('d/m/Y H:i')));
                $this->userManager->updateUser($user);
                $this->session->start();
            } else {
                throw new AuthenticationException('Su cuenta esta en uso.');
            }
        }
    }

}

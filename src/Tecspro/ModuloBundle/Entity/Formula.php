<?php

namespace Tecspro\ModuloBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Exclude;

/**
 * Formula
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Tecspro\ModuloBundle\Entity\FormulaRepository")
 */
class Formula extends \MWSimple\Bundle\AdminCrudBundle\Entity\BaseFile {

    use \A2lix\I18nDoctrineBundle\Doctrine\ORM\Util\Translatable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Type("integer")
     */
    protected $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="oculta", type="boolean")
     * @Type("boolean")
     */
    protected $oculta;

    /**
     * @ORM\ManyToOne(targetEntity="Grupo", inversedBy="formulas")
     * @ORM\JoinColumn(name="grupo_id", referencedColumnName="id")
     * @Type("Tecspro\ModuloBundle\Entity\Grupo")
     */
    protected $grupo;

    /**
     * @ORM\OneToMany(targetEntity="Ecuacion", mappedBy="formula", cascade={"all"})
     * @Type("ArrayCollection<Tecspro\ModuloBundle\Entity\Ecuacion>")
     */
    protected $ecuacion;

    /**
     * @ORM\OneToMany(targetEntity="Referencia", mappedBy="formula", cascade={"all"})
     * @Type("ArrayCollection<Tecspro\ModuloBundle\Entity\Referencia>")
     */
    protected $referencias;

    /**
     * @ORM\OneToMany(targetEntity="Variable", mappedBy="formula", cascade={"all"})
     * @Type("ArrayCollection<Tecspro\ModuloBundle\Entity\Variable>")
     */
    protected $variables;

    /**
     * @var string
     * @Type("string")
     */
    protected $uploadDir = "uploads/imagenes";

    /**
     *
     * @ORM\Column(name="file_path", type="string", length=255, nullable=true)
     * @Type("string")
     */
    protected $filePath;

    /**
     * @Exclude
     */
    protected $file;

    /**
     * @ORM\Column(name="imprimir", type="boolean")
     * @Type("boolean")
     */
    protected $imprimir;

    /**
     * @ORM\Column(name="autoincremental", type="boolean")
     * @Type("boolean")
     */
    protected $autoincremental;

    /**
     * @Type("ArrayCollection<Tecspro\ModuloBundle\Entity\FormulaTranslation>")
     */
    protected $translations;

    /**
     * @var integer
     *
     * @ORM\Column(name="orden", type="integer")
     * @Type("integer")
     */
    private $orden;

    /**
     * @ORM\Column(name="sincronizado", type="boolean")
     * @Type("boolean")
     */
    protected $sincronizado;

    /**
     *
     * @ORM\Column(name="json", type="text", nullable=true)
     * @Exclude
     */
    protected $json;

    /**
     * @ORM\Column(name="publicar", type="boolean")
     * @Type("boolean")
     */
    protected $publicar;

    /**
     * @var string
     *
     * @ORM\Column(name="nombrePersonalizado", type="text", nullable=true)
     * @Type("string")
     */
    private $nombrePersonalizado;

    /**
     * Constructor
     */
    public function __construct() {
        $this->ecuacion = new \Doctrine\Common\Collections\ArrayCollection();
        $this->referencias = new \Doctrine\Common\Collections\ArrayCollection();
        $this->variables = new \Doctrine\Common\Collections\ArrayCollection();
        $this->oculta = false;
        $this->imprimir = true;
        $this->sincronizado = false;
        $this->publicar = false;
        $this->nombrePersonalizado = "";
        $this->translations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set oculta
     *
     * @param boolean $oculta
     * @return Formula
     */
    public function setOculta($oculta) {
        $this->oculta = $oculta;

        return $this;
    }

    /**
     * Get oculta
     *
     * @return boolean 
     */
    public function getOculta() {
        return $this->oculta;
    }

    /**
     * Set grupo
     *
     * @param \Tecspro\ModuloBundle\Entity\Grupo $grupo
     * @return Formula
     */
    public function setGrupo(\Tecspro\ModuloBundle\Entity\Grupo $grupo = null) {
        $this->grupo = $grupo;

        return $this;
    }

    /**
     * Get grupo
     *
     * @return \Tecspro\ModuloBundle\Entity\Grupo 
     */
    public function getGrupo() {
        return $this->grupo;
    }

    /**
     * Add referencias
     *
     * @param \Tecspro\ModuloBundle\Entity\Referencia $referencias
     * @return Formula
     */
    public function addReferencia(\Tecspro\ModuloBundle\Entity\Referencia $referencias) {
        $referencias->setFormula($this);
        $this->referencias[] = $referencias;

        return $this;
    }

    /**
     * Remove referencias
     *
     * @param \Tecspro\ModuloBundle\Entity\Referencia $referencias
     */
    public function removeReferencia(\Tecspro\ModuloBundle\Entity\Referencia $referencias) {
        $this->referencias->removeElement($referencias);
        $referencias->setFormula(null);
    }

    /**
     * Get referencias
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReferencias() {
        return $this->referencias;
    }

    /**
     * Add variables
     *
     * @param \Tecspro\ModuloBundle\Entity\Variable $variables
     * @return Formula
     */
    public function addVariable(\Tecspro\ModuloBundle\Entity\Variable $variables) {
        $variables->setFormula($this);
        $this->variables[] = $variables;

        return $this;
    }

    /**
     * Remove variables
     *
     * @param \Tecspro\ModuloBundle\Entity\Variable $variables
     */
    public function removeVariable(\Tecspro\ModuloBundle\Entity\Variable $variables) {
        $this->variables->removeElement($variables);
        $variables->setFormula(null);
    }

    /**
     * Get variables
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVariables() {
        return $this->variables;
    }

    /**
     * 
     * @param \Doctrine\Common\Collections\ArrayCollection $variables
     *   @return Formula
     */
    public function setVariables(\Doctrine\Common\Collections\ArrayCollection $variables) {
        $this->variables = $variables;
    }

    /**
     * 
     * @param \Doctrine\Common\Collections\ArrayCollection $ecuacion
     *   @return Formula
     */
    public function setEcuacion(\Doctrine\Common\Collections\ArrayCollection $ecuacion) {
        $this->ecuacion = $ecuacion;
    }

    /**
     * Add ecuacion
     *
     * @param \Tecspro\ModuloBundle\Entity\Ecuacion $ecuacion
     * @return Formula
     */
    public function addEcuacion(\Tecspro\ModuloBundle\Entity\Ecuacion $ecuacion) {
        $ecuacion->setFormula($this);
        $this->ecuacion[] = $ecuacion;

        return $this;
    }

    /**
     * Remove ecuacion
     *
     * @param \Tecspro\ModuloBundle\Entity\Ecuacion $ecuacion
     */
    public function removeEcuacion(\Tecspro\ModuloBundle\Entity\Ecuacion $ecuacion) {
        $this->ecuacion->removeElement($ecuacion);
        $ecuacion->setFormula(null);
    }

    /**
     * Get ecuacion
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEcuacion() {
        return $this->ecuacion;
    }

    /**
     * Set imprimir
     *
     * @param boolean $imprimir
     * @return Formula
     */
    public function setImprimir($imprimir) {
        $this->imprimir = $imprimir;

        return $this;
    }

    /**
     * Get imprimir
     *
     * @return boolean 
     */
    public function getImprimir() {
        return $this->imprimir;
    }

    /**
     * Set autoincremental
     *
     * @param boolean $autoincremental
     * @return Formula
     */
    public function setAutoincremental($autoincremental) {
        $this->autoincremental = $autoincremental;

        return $this;
    }

    /**
     * Get autoincremental
     *
     * @return boolean 
     */
    public function getAutoincremental() {
        return $this->autoincremental;
    }

    /**
     * Set orden
     *
     * @param integer $orden
     * @return Formula
     */
    public function setOrden($orden) {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return integer 
     */
    public function getOrden() {
        return $this->orden;
    }

    /**
     * Set sincronizado
     *
     * @param boolean $sincronizado
     * @return Formula
     */
    public function setSincronizado($sincronizado) {
        $this->sincronizado = $sincronizado;

        return $this;
    }

    /**
     * Get sincronizado
     *
     * @return boolean 
     */
    public function getSincronizado() {
        return $this->sincronizado;
    }

    /**
     * Set json
     *
     * @param string $json
     * @return Formula
     */
    public function setJson($json) {
        $this->json = $json;

        return $this;
    }

    /**
     * Get json
     *
     * @return string 
     */
    public function getJson() {
        return $this->json;
    }

    /**
     * Set publicar
     *
     * @param boolean $publicar
     * @return Formula
     */
    public function setPublicar($publicar) {
        $this->publicar = $publicar;

        return $this;
    }

    /**
     * Get publicar
     *
     * @return boolean 
     */
    public function getPublicar() {
        return $this->publicar;
    }

    /**
     * Set nombrePersonalizado
     *
     * @param string $nombrePersonalizado
     * @return Formula
     */
    public function setNombrePersonalizado($nombrePersonalizado) {
        $this->nombrePersonalizado = $nombrePersonalizado;

        return $this;
    }

    /**
     * Get nombrePersonalizado
     *
     * @return string 
     */
    public function getNombrePersonalizado() {
        return $this->nombrePersonalizado;
    }

}

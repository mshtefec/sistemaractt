<?php

namespace Tecspro\TablaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Tecspro\TablaBundle\Entity\CentroGravedad;
use Tecspro\TablaBundle\Form\CentroGravedadType;
use Tecspro\TablaBundle\Form\CentroGravedadFilterType;
use A2lix\I18nDoctrineBundle\Annotation\I18nDoctrine;

/**
 * CentroGravedad controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/centrogravedad")
 */
class CentroGravedadController extends Controller
{
    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Tecspro/TablaBundle/Resources/config/CentroGravedad.yml',
    );

    /**
     * Lists all CentroGravedad entities.
     *
     * @Route("/", name="admin_centrogravedad")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->config['filterType'] = new CentroGravedadFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new CentroGravedad entity.
     *
     * @Route("/", name="admin_centrogravedad_create")
     * @Method("POST")
     * @Template("TecsproTablaBundle:CentroGravedad:new.html.twig")
     */
    public function createAction()
    {
        $this->config['newType'] = new CentroGravedadType();
        $response = parent::createAction();

        return $response;
    }

    /**
     * Displays a form to create a new CentroGravedad entity.
     *
     * @Route("/new", name="admin_centrogravedad_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->config['newType'] = new CentroGravedadType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a CentroGravedad entity.
     *
     * @Route("/{id}", name="admin_centrogravedad_show", options={"expose"=true})
     * @Method("GET")
     * @Template()
          * @I18nDoctrine
     */
    public function showAction($id)
    {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing CentroGravedad entity.
     *
     * @Route("/{id}/edit", name="admin_centrogravedad_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->config['editType'] = new CentroGravedadType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing CentroGravedad entity.
     *
     * @Route("/{id}", name="admin_centrogravedad_update")
     * @Method("PUT")
     * @Template("TecsproTablaBundle:CentroGravedad:edit.html.twig")
      * @I18nDoctrine
     */
    public function updateAction($id)
    {
        $this->config['editType'] = new CentroGravedadType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a CentroGravedad entity.
     *
     * @Route("/{id}", name="admin_centrogravedad_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter CentroGravedad.
     *
     * @Route("/exporter/{format}", name="admin_centrogravedad_export")
     */
    public function getExporter($format)
    {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Datatable CentroGravedad.
     *
     * @Route("/get-table/", name="admin_centrogravedad_table")
     */
    public function getDatatable()
    {
        $response = parent::getTable();

        return $response;
    }
 
}
<?php

namespace Tecspro\FrontBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Proyecto
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Tecspro\FrontBundle\Entity\ProyectoRepository")
 */
class Proyecto {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="comentario", type="string", length=255, nullable=true)
     */
    private $comentario;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="date")
     */
    private $fecha;

    /**
     * @var integer
     *
     * @ORM\OneToMany(targetEntity="ParteInvolucrada", mappedBy="proyecto",cascade={"all"}, orphanRemoval=true)
     */
    private $partesInvolucradas;

    /**
     * @var \text
     *
     * @ORM\Column(name="formulas", type="array", nullable=true)
     */
    private $formulas;

    /**
     * @ORM\ManyToOne(targetEntity="Tecspro\UserBundle\Entity\User", inversedBy="proyectos")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Tecspro\ModuloBundle\Entity\Modulo", inversedBy="proyectos")
     * @ORM\JoinColumn(name="modulo_id", referencedColumnName="id")
     */
    private $modulo;

    /**
     * @ORM\OneToOne(targetEntity="Tecspro\ARCBundle\Entity\ARCCampos", mappedBy="proyecto",cascade={"all"}, orphanRemoval=true)
     */
    private $arc_campos;

    public function __construct() {
        $this->partesInvolucradas = new \Doctrine\Common\Collections\ArrayCollection();
        $this->fecha = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Proyecto
     */
    public function setNombre($nombre) {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre() {
        return $this->nombre;
    }

    /**
     * Set comentario
     *
     * @param string $comentario
     * @return Proyecto
     */
    public function setComentario($comentario) {
        $this->comentario = $comentario;

        return $this;
    }

    /**
     * Get comentario
     *
     * @return string 
     */
    public function getComentario() {
        return $this->comentario;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Proyecto
     */
    public function setFecha($fecha) {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha() {
        return $this->fecha;
    }

    /**
     * Set partesInvolucradas
     *
     * @param integer $partesInvolucradas
     * @return Proyecto
     */
    public function setPartesInvolucradas($partesInvolucradas) {
        $partesInvolucradas->setProyecto($this);
        $this->partesInvolucradas = $partesInvolucradas;

        return $this;
    }

    /**
     * Get partesInvolucradas
     *
     * @return integer 
     */
    public function getPartesInvolucradas() {
        return $this->partesInvolucradas;
    }

    /**
     * Add partesInvolucradas
     *
     * @param \Tecspro\FrontBundle\Entity\ParteInvolucrada $partesInvolucradas
     * @return Proyecto
     */
    public function addPartesInvolucrada(\Tecspro\FrontBundle\Entity\ParteInvolucrada $partesInvolucradas) {
        $partesInvolucradas->setProyecto($this);
        $this->partesInvolucradas[] = $partesInvolucradas;

        return $this;
    }

    /**
     * Remove partesInvolucradas
     *
     * @param \Tecspro\FrontBundle\Entity\ParteInvolucrada $partesInvolucradas
     */
    public function removePartesInvolucrada(\Tecspro\FrontBundle\Entity\ParteInvolucrada $partesInvolucradas) {
        $partesInvolucradas->setProyecto(null);
        $this->partesInvolucradas->removeElement($partesInvolucradas);
    }

    /**
     * Set user
     *
     * @param \Tecspro\UserBundle\Entity\User $user
     * @return Proyecto
     */
    public function setUser(\Tecspro\UserBundle\Entity\User $user = null) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Tecspro\UserBundle\Entity\User 
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Set formulas
     *
     * @param array $formulas
     * @return Proyecto
     */
    public function setFormulas($formulas) {
        $this->formulas = $formulas;

        return $this;
    }

    /**
     * Get formulas
     *
     * @return array 
     */
    public function getFormulas() {
        return $this->formulas;
    }

    /**
     * Adds/sets an element in the collection at the index / with the specified key.
     *
     * When the collection is a Map this is like put(key,value)/add(key,value).
     * When the collection is a List this is like add(position,value).
     *
     * @param mixed $key
     * @param mixed $value
     */
    public function setItemFormulas($key, $value) {
        $this->formulas[$key] = $value;
    }

    /**
     * Set modulo
     *
     * @param \Tecspro\ModuloBundle\Entity\Modulo $modulo
     * @return Proyecto
     */
    public function setModulo(\Tecspro\ModuloBundle\Entity\Modulo $modulo = null) {
        $this->modulo = $modulo;

        return $this;
    }

    /**
     * Get modulo
     *
     * @return \Tecspro\ModuloBundle\Entity\Modulo 
     */
    public function getModulo() {
        return $this->modulo;
    }

    /**
     * Set arc_campos
     *
     * @param \Tecspro\ARCBundle\Entity\ARCCampos $arcCampos
     * @return Proyecto
     */
    public function setArcCampos(\Tecspro\ARCBundle\Entity\ARCCampos $arcCampos = null) {
        $this->arc_campos = $arcCampos;

        return $this;
    }

    /**
     * Get arc_campos
     *
     * @return \Tecspro\ARCBundle\Entity\ARCCampos 
     */
    public function getArcCampos() {
        return $this->arc_campos;
    }

}

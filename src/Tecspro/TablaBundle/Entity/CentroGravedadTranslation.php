<?php

namespace Tecspro\TablaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 * Categoria
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Tecspro\TablaBundle\Entity\CentroGravedadRepository")
 */
class CentroGravedadTranslation implements \A2lix\I18nDoctrineBundle\Doctrine\Interfaces\OneLocaleInterface {

      use \Tecspro\ModuloBundle\Entity\Translation;

    
     /**
     * @var string
     *
     * @ORM\Column(name="vehiculo", type="string", length=255)
     */
    private $vehiculo;

    /**
     * @var string
     *
     * @ORM\Column(name="indice", type="string", length=255)
     */
    private $indice;
   /**
     * @var string
     *
     * @ORM\Column(name="categoria", type="string", length=255, nullable=true)
     */
    private $categoria;
 

    /**
     * Set vehiculo
     *
     * @param string $vehiculo
     * @return CentroGravedadTranslation
     */
    public function setVehiculo($vehiculo)
    {
        $this->vehiculo = $vehiculo;

        return $this;
    }

    /**
     * Get vehiculo
     *
     * @return string 
     */
    public function getVehiculo()
    {
        return $this->vehiculo;
    }

    /**
     * Set indice
     *
     * @param string $indice
     * @return CentroGravedadTranslation
     */
    public function setIndice($indice)
    {
        $this->indice = $indice;

        return $this;
    }

    /**
     * Get indice
     *
     * @return string 
     */
    public function getIndice()
    {
        return $this->indice;
    }

    /**
     * Set categoria
     *
     * @param string $categoria
     * @return CentroGravedadTranslation
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return string 
     */
    public function getCategoria()
    {
        return $this->categoria;
    }
}

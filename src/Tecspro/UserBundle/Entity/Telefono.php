<?php

namespace Tecspro\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Telefono
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Tecspro\UserBundle\Entity\TelefonoRepository")
 */
class Telefono {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=100, nullable=false)
     */
    private $tipo;

    /**
     * @var string
     *
     * @ORM\Column(name="numero", type="string", length=50, nullable=false)
     */
    private $numero;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Tecspro\UserBundle\Entity\User", inversedBy="telefonos")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     *
     */
    private $user;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set tipo
     *
     * @param  string   $tipo
     * @return Telefono
     */
    public function setTipo($tipo) {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo() {
        return $this->tipo;
    }

    /**
     * Set numero
     *
     * @param  string   $numero
     * @return Telefono
     */
    public function setNumero($numero) {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return string
     */
    public function getNumero() {
        return $this->numero;
    }

    /**
     * Set persona
     *
     * @param  \Tecspro\UserBundle\Entity\User $User
     * @return Telefono
     */
    public function setUser(\Tecspro\UserBundle\Entity\User $User = null) {
        $this->user = $User;

        return $this;
    }

    /**
     * Get persona
     *
     * @return \Tecspro\UserBundle\Entity\User
     */
    public function getUser() {
        return $this->user;
    }

}

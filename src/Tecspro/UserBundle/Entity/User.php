<?php

namespace Tecspro\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use FOS\UserBundle\Entity\User as BaseUser;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use FOS\MessageBundle\Model\ParticipantInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @Gedmo\Loggable
 * @ORM\Table(name="fos_user")
 * @ORM\Entity(repositoryClass="Tecspro\UserBundle\Entity\UserRepository")
 * @UniqueEntity("username")
 * @UniqueEntity("email")
 */
class User extends BaseUser implements ParticipantInterface {

    /**
     * @ORM\Id
     * @Gedmo\Versioned
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="plan_de_cuenta", type="integer", nullable=true)
     */
    protected $plan_de_cuenta;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_inicio", type="date", nullable=true)
     */
    private $fecha_inicio;

    /**
     * @ORM\ManyToMany(targetEntity="Role")
     * @ORM\JoinTable(name="fos_user_role")
     * @Assert\NotNull()
     */
    protected $user_roles;

    /**
     * @ORM\Column(name="cantidadSessiones", type="integer")
     */
    private $cantidadSessiones;

    /**
     * @ORM\OneToMany(targetEntity="Tecspro\FrontBundle\Entity\Proyecto", mappedBy="user",cascade={"all"})
     */
    private $proyectos;

    /**
     * @ORM\ManyToMany(targetEntity="Tecspro\ModuloBundle\Entity\Modulo", mappedBy="users", cascade={"persist"})
     * @Assert\NotNull()
     */
    private $modulos;

    /**
     * @var integer
     *
     * @ORM\OneToMany(targetEntity="Tecspro\UserBundle\Entity\Telefono", mappedBy="user", cascade={"all"}, orphanRemoval=true)
     * @Assert\Valid
     */
    protected $telefonos;

    /**
     *
     * @ORM\OneToMany(targetEntity="Tecspro\UserBundle\Entity\Domicilio", mappedBy="user", cascade={"all"}, orphanRemoval=true)
     * @Assert\Valid
     */
    protected $domicilios;

    /**
     * @var string
     *
     * @ORM\Column(name="configuraciones", type="array")
     */
    private $configuraciones;

    /**
     * @var string
     *
     * @ORM\Column(name="confirmationTokenSession", type="string", length=255, nullable=true)
     */
    private $confirmationTokenSession;

    /**
     * @var string
     *
     * @ORM\Column(name="passwordRequestedAtSession", type="datetime", nullable=true)
     */
    private $passwordRequestedAtSession;

    /**
     * @var string
     *
     * @ORM\Column(name="sessiones", type="array")
     */
    private $sessiones;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaCreacion", type="date")
     */
    private $fechaCreacion;

    /**
     * @ORM\OneToMany(targetEntity="User", mappedBy="parent")
     */
    private $children;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true)
     */
    private $parent;

    public function __construct() {
        parent::__construct();
        $this->children = new ArrayCollection();
        $this->user_roles = new ArrayCollection();
        $this->proyectos = new ArrayCollection();
        $this->logged = false;
        $this->modulos = new ArrayCollection();
        $this->telefonos = new ArrayCollection();
        $this->domicilios = new ArrayCollection();
        $this->sessiones = array();
        $this->fechaCreacion = new \DateTime();
        $this->plan_de_cuenta = 0;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    public function setCreatedBy(ParticipantInterface $Participant) {
        // …
    }

    public function isSessionRequestNonExpired($ttl) {
        return $this->getPasswordRequestedAtSession() instanceof \DateTime &&
                $this->getPasswordRequestedAtSession()->getTimestamp() + $ttl > time();
    }

    /**
     * Returns an ARRAY of Role objects with the default Role object appended.
     * @return array
     */
    public function getRoles() {
        $roles = $this->user_roles->toArray();
        return $roles;
    }

    /**
     * Returns the true ArrayCollection of Roles.
     * @return Doctrine\Common\Collections\ArrayCollection
     */
    public function getRolesCollection() {
        return $this->user_roles;
    }

    /**
     * Pass a string, get the desired Role object or null.
     * @param  string    $role
     * @return Role|null
     */
    public function getRole($role) {
        foreach ($this->getRoles() as $roleItem) {
            if ($role == $roleItem) {
                return $roleItem;
            }
        }

        return null;
    }

    /**
     * Pass a string, checks if we have that Role. Same functionality as getRole() except returns a real boolean.
     * @param  string  $role
     * @return boolean
     */
    public function hasRole($role) {
        if ($this->getRole($role)) {
            return true;
        }

        return false;
    }

    /**
     * Adds a Role OBJECT to the ArrayCollection. Can't type hint due to interface so throws Exception.
     * @throws Exception
     * @param  Role      $role
     */
    public function addRole($role) {
        if (!$role instanceof Role) {
            throw new \Exception("addRole takes a Role object as the parameter");
        }

        if (!$this->hasRole($role->getRole())) {
            $this->user_roles->add($role);
        }
    }

    /**
     * Pass a string, remove the Role object from collection.
     * @param string $role
     */
    public function removeRole($role) {
        $roleElement = $this->getRole($role);
        if ($roleElement) {
            $this->user_roles->removeElement($roleElement);
        }
    }

    /**
     * Pass an ARRAY of Role objects and will clear the collection and re-set it with new Roles.
     * Type hinted array due to interface.
     * @param array $user_roles Of Role objects.
     */
    public function setRoles(array $user_roles) {
        $this->user_roles->clear();
        foreach ($user_roles as $role) {
            $this->addRole($role);
        }
    }

    /**
     * Directly set the ArrayCollection of Roles. Type hinted as Collection which is the parent of (Array|Persistent)Collection.
     * @param Doctrine\Common\Collections\Collection $role
     */
    public function setRolesCollection(Collection $collection) {
        $this->user_roles = $collection;
    }

    /**
     * Add user_roles
     *
     * @param  \Tecspro\UserBundle\Entity\Role $userRoles
     * @return User
     */
    public function addUserRole(\Tecspro\UserBundle\Entity\Role $userRoles) {
        $this->user_roles[] = $userRoles;

        return $this;
    }

    /**
     * Remove user_roles
     *
     * @param \Tecspro\UserBundle\Entity\Role $userRoles
     */
    public function removeUserRole(\Tecspro\UserBundle\Entity\Role $userRoles) {
        $this->user_roles->removeElement($userRoles);
    }

    /**
     * Get user_roles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserRoles() {
        return $this->user_roles;
    }

    /**
     * Add proyectos
     *
     * @param \Tecspro\FrontBundle\Entity\Proyecto $proyectos
     * @return User
     */
    public function addProyecto(\Tecspro\FrontBundle\Entity\Proyecto $proyectos) {
        $this->proyectos[] = $proyectos;

        return $this;
    }

    /**
     * Remove proyectos
     *
     * @param \Tecspro\FrontBundle\Entity\Proyecto $proyectos
     */
    public function removeProyecto(\Tecspro\FrontBundle\Entity\Proyecto $proyectos) {
        $this->proyectos->removeElement($proyectos);
    }

    /**
     * Get proyectos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProyectos() {
        return $this->proyectos;
    }

    /**
     * Add modulos
     *
     * @param \Tecspro\ModuloBundle\Entity\Modulo $modulos
     * @return User
     */
    public function addModulo(\Tecspro\ModuloBundle\Entity\Modulo $modulos) {
        $modulos->addUser($this);
        $this->modulos[] = $modulos;

        return $this;
    }

    /**
     * Remove modulos
     *
     * @param \Tecspro\ModuloBundle\Entity\Modulo $modulos
     */
    public function removeModulo(\Tecspro\ModuloBundle\Entity\Modulo $modulos) {
        if (!$this->modulos->contains($modulos)) {
            return;
        }
        $modulos->removeUser($this);
        $this->modulos->removeElement($modulos);
    }

    /**
     * Get modulos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getModulos() {
        return $this->modulos;
    }

    /**
     * Set plan_de_cuenta
     *
     * @param integer $planDeCuenta
     * @return User
     */
    public function setPlanDeCuenta($planDeCuenta) {
        $this->plan_de_cuenta = $planDeCuenta;

        return $this;
    }

    /**
     * Get plan_de_cuenta
     *
     * @return integer 
     */
    public function getPlanDeCuenta() {
        return $this->plan_de_cuenta;
    }

    /**
     * Set fecha_inicio
     *
     * @param \DateTime $fechaInicio
     * @return User
     */
    public function setFechaInicio($fechaInicio) {
        $this->fecha_inicio = $fechaInicio;

        return $this;
    }

    /**
     * Get fecha_inicio
     *
     * @return \DateTime 
     */
    public function getFechaInicio() {
        return $this->fecha_inicio;
    }

    /**
     * Add telefonos
     *
     * @param \Tecspro\UserBundle\Entity\Telefono $telefonos
     * @return User
     */
    public function addTelefono(\Tecspro\UserBundle\Entity\Telefono $telefonos) {
        $this->telefonos[] = $telefonos;

        return $this;
    }

    /**
     * Remove telefonos
     *
     * @param \Tecspro\UserBundle\Entity\Telefono $telefonos
     */
    public function removeTelefono(\Tecspro\UserBundle\Entity\Telefono $telefonos) {
        $this->telefonos->removeElement($telefonos);
    }

    /**
     * Get telefonos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTelefonos() {
        return $this->telefonos;
    }

    /**
     * Add domicilios
     *
     * @param \Tecspro\UserBundle\Entity\Domicilio $domicilios
     * @return User
     */
    public function addDomicilio(\Tecspro\UserBundle\Entity\Domicilio $domicilios) {
        $this->domicilios[] = $domicilios;

        return $this;
    }

    /**
     * Remove domicilios
     *
     * @param \Tecspro\UserBundle\Entity\Domicilio $domicilios
     */
    public function removeDomicilio(\Tecspro\UserBundle\Entity\Domicilio $domicilios) {
        $this->domicilios->removeElement($domicilios);
    }

    /**
     * Get domicilios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDomicilios() {
        return $this->domicilios;
    }

    /**
     * Adds/sets an element in the collection at the index / with the specified key.
     *
     * When the collection is a Map this is like put(key,value)/add(key,value).
     * When the collection is a List this is like add(position,value).
     *
     * @param mixed $key
     * @param mixed $value
     */
    public function setItemConfiguraciones($key, $value) {
        $this->configuraciones[$key] = $value;
    }

    /**
     * Set configuraciones
     *
     * @param array $configuraciones
     * @return Configuracion
     */
    public function setConfiguraciones($configuraciones) {
        $this->configuraciones = $configuraciones;

        return $this;
    }

    /**
     * Get configuraciones
     *
     * @return array 
     */
    public function getConfiguraciones() {
        return $this->configuraciones;
    }

    /**
     * Set creador
     *
     * @param string $creador
     * @return User
     */
    public function setCreador($creador) {
        $this->creador = $creador;

        return $this;
    }

    /**
     * Get creador
     *
     * @return string 
     */
    public function getCreador() {
        return $this->creador;
    }

    /**
     * Set session_id
     *
     * @param string $sessionId
     * @return User
     */
    public function setSessionId($sessionId) {
        $this->session_id = $sessionId;

        return $this;
    }

    /**
     * Get session_id
     *
     * @return string 
     */
    public function getSessionId() {
        return $this->session_id;
    }

    /**
     * Set cantidadSessiones
     *
     * @param integer $cantidadSessiones
     * @return User
     */
    public function setCantidadSessiones($cantidadSessiones) {
        $this->cantidadSessiones = $cantidadSessiones;

        return $this;
    }

    /**
     * Get cantidadSessiones
     *
     * @return integer 
     */
    public function getCantidadSessiones() {
        return $this->cantidadSessiones;
    }

    public function setPasswordRequestedAtSession(\DateTime $date = null) {
        $this->passwordRequestedAtSession = $date;

        return $this;
    }

    /**
     * Gets the timestamp that the user requested a password reset.
     *
     * @return null|\DateTime
     */
    public function getPasswordRequestedAtSession() {
        return $this->passwordRequestedAtSession;
    }

    public function setConfirmationTokenSession($confirmationTokenSession) {
        $this->confirmationTokenSession = $confirmationTokenSession;

        return $this;
    }

    public function getConfirmationTokenSession() {
        return $this->confirmationTokenSession;
    }

    /**
     * Adds/sets an element in the collection at the index / with the specified key.
     *
     * When the collection is a Map this is like put(key,value)/add(key,value).
     * When the collection is a List this is like add(position,value).
     *
     * @param mixed $key
     * @param mixed $value
     */
    public function setItemSession($key, $value) {
        $this->sessiones[$key] = $value;
    }

    /**
     * Set sessiones
     *
     * @param array $sessiones
     * @return User
     */
    public function setSessiones($sessiones) {
        $this->sessiones = $sessiones;

        return $this;
    }

    /**
     * Get sessiones
     *
     * @return array 
     */
    public function getSessiones() {
        return $this->sessiones;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     * @return User
     */
    public function setFechaCreacion($fechaCreacion) {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime 
     */
    public function getFechaCreacion() {
        return $this->fechaCreacion;
    }

    /**
     * Add children
     *
     * @param \Tecspro\UserBundle\Entity\User $children
     * @return User
     */
    public function addChild(\Tecspro\UserBundle\Entity\User $children) {
        $children->setParent($this);
        $this->children[] = $children;

        return $this;
    }

    /**
     * Remove children
     *
     * @param \Tecspro\UserBundle\Entity\User $children
     */
    public function removeChild(\Tecspro\UserBundle\Entity\User $children) {
        $this->children->removeElement($children);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getChildren() {
        return $this->children;
    }

    /**
     * Set parent
     *
     * @param \Tecspro\UserBundle\Entity\User $parent
     * @return User
     */
    public function setParent(\Tecspro\UserBundle\Entity\User $parent = null) {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \Tecspro\UserBundle\Entity\User 
     */
    public function getParent() {
        return $this->parent;
    }

}

<?php
namespace Tecspro\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Role\RoleInterface;
use Tecspro\Bundle\ComunBundle\Services\Slugify;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_role")
 */
class Role implements RoleInterface, \Serializable
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    protected $name;

    /**
     * @ORM\Column(name="role_name", type="string", length=255)
     */
    protected $role_name;



    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function getRole()
    {
        return $this->getName();
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName()
    {
        $slugify = new Slugify();
        $this->name = "ROLE_" . $slugify->slugify($this->getRoleName());
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        /*
         * ! Don't serialize $users field !
         */

        return \serialize(array(
            $this->id,
            $this->name
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->name
        ) = \unserialize($serialized);
    }

    /**
     * Set role_name
     *
     * @param  string $roleName
     * @return Role
     */
    public function setRoleName($roleName)
    {
        $this->role_name = strtoupper($roleName);
        $this->setName();

        return $this;
    }

    /**
     * Get role_name
     *
     * @return string
     */
    public function getRoleName()
    {
        return $this->role_name;
    }
}

<?php

namespace Tecspro\ModuloBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UnidadResultadoOrdenType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('unidad', 'select2', array(
                    'label' => false,
                    'class' => 'ModuloBundle:Unidad',
                    'url' => 'autocomplete_get_grupo_unidad',
                    'required' => false,
                    'configs' => array(
                        'multiple' => false, //es requerido true o false
                        'width' => '100%'
                    ),
                ))
                ->add('principal', 'checkbox', array(
                    'label' => false,
                    'required' => false,
                    'attr' => array(
                        'class' => 'txtUnidadesPrincipales'
                    )
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Tecspro\ModuloBundle\Entity\UnidadResultadoOrden'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'tecspro_modulobundle_unidadresultadoorden';
    }

}

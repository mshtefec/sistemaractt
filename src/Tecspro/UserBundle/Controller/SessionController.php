<?php

namespace Tecspro\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\RedirectResponse;

class SessionController extends Controller {

    const SESSION_EMAIL = 'fos_user_send_resetting_email/email';

    /**
     * Autocomplete a User entity.
     *
     * @Route("/reset/sessiones", name="User_resetting_request_session")
     * @Template("UserBundle:Session:resetSessiones.html.twig")
     */
    public function resetSessionesModulo() {

        return array();
    }

    /**
     * Autocomplete a User entity.
     *
     * @Route("/reset/sessiones/send", name="User_resetting_request_session_send")
     * @Method("POST")
     */
    public function resetSessionesSendEmailModulo() {
        $username = $this->container->get('request')->request->get('username');

        /** @var $user UserInterface */
        $user = $this->container->get('fos_user.user_manager')->findUserByUsernameOrEmail($username);

        if (null === $user) {
            return $this->container->get('templating')->renderResponse('UserBundle:Session:resetSessiones.html.twig', array('invalid_username' => $username));
        }

        if ($user->isSessionRequestNonExpired($this->container->getParameter('fos_user.resetting.token_ttl'))) {
            return $this->container->get('templating')->renderResponse('UserBundle:Session:sessionsAlreadyRequested.html.twig');
        }

        if (null === $user->getConfirmationTokenSession()) {
            /** @var $tokenGenerator \FOS\UserBundle\Util\TokenGeneratorInterface */
            $tokenGenerator = $this->container->get('fos_user.util.token_generator');
            $user->setConfirmationTokenSession($tokenGenerator->generateToken());
        }

        $this->container->get('session')->set(static::SESSION_EMAIL, $this->getObfuscatedEmail($user));
        $this->container->get('user.email')->sendResettingSessionMessage($user);
        $user->setPasswordRequestedAtSession(new \DateTime());
        $this->container->get('fos_user.user_manager')->updateUser($user);

        return new \Symfony\Component\HttpFoundation\RedirectResponse($this->container->get('router')->generate('fos_user_resetting_check_email'));
    }

    protected function getObfuscatedEmail(\FOS\UserBundle\Model\UserInterface $user) {

        $email = $user->getEmail();
        if (false !== $pos = strpos($email, '@')) {
            $email = '...' . substr($email, $pos);
        }

        return $email;
    }

    /**
     * Autocomplete a User entity.
     *
     * @Route("/reset/sessiones/send/{token}", name="User_resetting_request_session_send_token")
     */
    public function resetAction($token) {
        $em = $this->getDoctrine()->getManager();
        $sessionRequest = $this->getRequest()->getSession();

        $user = $em->getRepository('UserBundle:User')->UserByConfirmationTokenSession($token);

        if (null === $user) {
            throw new NotFoundHttpException(sprintf('The user with "confirmation token" does not exist for value "%s"', $token));
        }

        if (!$user->isSessionRequestNonExpired($this->container->getParameter('fos_user.resetting.token_ttl'))) {
            return new RedirectResponse($this->container->get('router')->generate('User_resetting_request_session_checkEmail'));
        }
        //elimino las sessiones

        if (count($user->getSessiones()) > 0) {
            $txtSessionesEntities = $em->getRepository('UserBundle:sessions')->findInArraySessionId(array_keys($user->getSessiones()));

            foreach ($txtSessionesEntities as $session) {
                if ($sessionRequest->getId() != $session->getSessId()) {

                    $em->remove($session);
                }
            }

            $em->flush();
        }


        $user->setConfirmationTokenSession(null);
        $user->setPasswordRequestedAtSession(null);
        $user->setSessiones(array());
        $this->container->get('fos_user.user_manager')->updateUser($user);
        $this->get('session')->getFlashBag()->add('success', 'Sessiones Restablecidas');

        $this->get("request")->getSession()->invalidate();
        $this->get("security.context")->setToken(null);
        //return new \Symfony\Component\HttpFoundation\RedirectResponse($this->generateUrl('inicio_plantilla'));

        return $this->redirect($this->generateUrl('inicio_plantilla'), 301);
    }

    /**
     * Autocomplete a User entity.
     *
     * @Route("/reset/sessiones/checkEmail", name="User_resetting_request_session_checkEmail")
     * @Method("POST")
     */
    public function checkEmailAction() {
        $session = $this->container->get('session');
        $email = $session->get(static::SESSION_EMAIL);
        $session->remove(static::SESSION_EMAIL);

        if (empty($email)) {
            // the user does not come from the sendEmail action
            return new RedirectResponse($this->container->get('router')->generate('fos_user_resetting_request'));
        }

        return $this->container->get('templating')->renderResponse('UserBundle:Sessions:checkEmail.html.' . $this->getEngine(), array(
                    'email' => $email,
        ));
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/edit-sessiones", name="user_edit_sessiones", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function editSessionesAction() {


        $user = $this->getUser();
        $entities = $user->getSessiones();



        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new User entity.
     *
     * @Route("/edit-sessiones", name="user_edit_sessiones_enviar")
     * @Method("POST")
     * @Template("UserBundle:Session:editSessiones.html.twig")
     */
    public function createAction() {

        $request = $this->getRequest();
        $sessionRequest = $request->getSession();
        $txtSessiones = $request->request->all()["txtSession"];
        $logout = false;
        $user = $this->getUser();
        $sessiones = $user->getSessiones();
        $em = $this->getDoctrine()->getManager();

        if (count($txtSessiones) > 0) {
            $txtSessionesEntities = $em->getRepository('UserBundle:sessions')->findInArraySessionId(array_keys($txtSessiones));
            $txtSessionesEntitiesArray = array();
            foreach ($txtSessionesEntities as $session) {
                $txtSessionesEntitiesArray[$session->getSessId()] = $session;
            }

            foreach ($txtSessiones as $key => $session) {
                if (isset($sessiones[$key]) == true && $session == "on") {
                    unset($sessiones[$key]);

                    if ($sessionRequest->getId() == $key) {
                        $logout = true;
                    } else {
                        $em->remove($txtSessionesEntitiesArray[$key]);
                    }
                }
            }

            $user->setSessiones($sessiones);
            $em->persist($user);
            $em->flush();
        }
        if ($logout) {
            $this->get("request")->getSession()->invalidate();
            $this->get("security.context")->setToken(null);
            return new \Symfony\Component\HttpFoundation\RedirectResponse($this->generateUrl('inicio_plantilla'));
        } else {
            return array(
                'entities' => $sessiones,
            );
        }
    }

}

<?php

namespace Tecspro\ARCBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ARCCampos
 *
 * @ORM\Table(name="arc_campos")
 * @ORM\Entity(repositoryClass="Tecspro\ARCBundle\Entity\ARCCamposRepository")
 */
class ARCCampos {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="cantidad_vehiculos", type="integer")
     */
    private $cantidadVehiculos;

    /**
     * @var string
     *
     * @ORM\Column(name="fraccion_de_tiempo_a_analizar", type="string", length=255)
     */
    private $fraccionDeTiempoAAnalizar;

    /**
     * @var boolean
     *
     * @ORM\Column(name="imprimir", type="boolean")
     */
    private $imprimir;

    /**
     * @ORM\OneToMany(targetEntity="Vehiculo", mappedBy="arcCampos", cascade={"persist", "remove"},orphanRemoval=true)
     */
    private $vehiculos;

    /**
     * @ORM\OneToOne(targetEntity="Tecspro\FrontBundle\Entity\Proyecto", inversedBy="arc_campos")
     */
    private $proyecto;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set cantidadVehiculos
     *
     * @param integer $cantidadVehiculos
     * @return ARCCampos
     */
    public function setCantidadVehiculos($cantidadVehiculos) {

        $this->cantidadVehiculos = $cantidadVehiculos;
        return $this;
    }

    /**
     * Get cantidadVehiculos
     *
     * @return integer 
     */
    public function getCantidadVehiculos() {

        return $this->cantidadVehiculos;
    }

    /**
     * Set imprimir
     *
     * @param boolean $imprimir
     * @return ARCCampos
     */
    public function setImprimir($imprimir) {

        $this->imprimir = $imprimir;
        return $this;
    }

    /**
     * Get imprimir
     *
     * @return boolean 
     */
    public function getImprimir() {

        return $this->imprimir;
    }

    /**
     * Constructor
     */
    public function __construct() {

        $this->resultadosARC = new \Doctrine\Common\Collections\ArrayCollection();
        $this->vehiculos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add vehiculos
     *
     * @param \Tecspro\ARCBundle\Entity\Vehiculo $vehiculos
     * @return ARCCampos
     */
    public function addVehiculo(\Tecspro\ARCBundle\Entity\Vehiculo $vehiculos) {

        $vehiculos->setArcCampos($this);
        $this->vehiculos[] = $vehiculos;

        return $this;
    }

    /**
     * Remove vehiculos
     *
     * @param \Tecspro\ARCBundle\Entity\Vehiculo $vehiculos
     */
    public function removeVehiculo(\Tecspro\ARCBundle\Entity\Vehiculo $vehiculos) {

        $vehiculos->setArcCampos(null);
       // $this->vehiculos->remove($vehiculos);
        $this->vehiculos->removeElement($vehiculos);
    }

    /**
     * Get vehiculos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVehiculos() {

        return $this->vehiculos;
    }

    /**
     * Set fraccionDeTiempoAAnalizar
     *
     * @param string $fraccionDeTiempoAAnalizar
     * @return ARCCampos
     */
    public function setFraccionDeTiempoAAnalizar($fraccionDeTiempoAAnalizar) {

        $this->fraccionDeTiempoAAnalizar = $fraccionDeTiempoAAnalizar;

        return $this;
    }

    /**
     * Get fraccionDeTiempoAAnalizar
     *
     * @return string 
     */
    public function getFraccionDeTiempoAAnalizar() {

        return $this->fraccionDeTiempoAAnalizar;
    }

    /**
     * Set proyecto
     *
     * @param \Tecspro\FrontBundle\Entity\Proyecto $proyecto
     * @return ARCCampos
     */
    public function setProyecto(\Tecspro\FrontBundle\Entity\Proyecto $proyecto = null) {

        $this->proyecto = $proyecto;

        return $this;
    }

    /**
     * Get proyecto
     *
     * @return \Tecspro\FrontBundle\Entity\Proyecto 
     */
    public function getProyecto() {

        return $this->proyecto;
    }

    public function __toString() {

        return $this->getId();
    }

}

<?php

namespace Tecspro\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        if (is_null($builder->getData()->getId())) {//New
            $required = true;
        } else {//Edit
            $required = false;
        }

        $builder
                ->add('username', null, array(
                    'label' => 'Nombre',
                    // 'label_attr' => sdrray(
                    //     'class' => 'form-label col-lg-3',
                    // ),
                    'attr' => array(
                        'class' => 'form-control',
                    )
                ))
                ->add('fecha_inicio', 'bootstrapdatetime', array(
                    "label" => 'Fecha de inicio',
                    "required" => true,
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                        )
                )
                ->add('plan_de_cuenta', null, array(
                    'attr' => array(
                        "required" => true,
                        'class' => 'form-control',
                    )
                ))
                ->add('email', null, array(
                    'attr' => array(
                        'class' => 'form-control',
                    )
                ))
                ->add('password', 'repeated', array(
                    'type' => 'password',
                    'required' => $required,
                    'invalid_message' => 'Las Contraseñas deben Coincidir.',
                    'options' => array(
                        'attr' => array(
                            'class' => 'password-field form-control'
                        )
                    ),
                    'first_options' => array(
                        'label' => 'Contraseña',
                        // 'label_attr' => array(
                        //     'class' => 'form-label col-lg-3',
                        // ),
                        'attr' => array(
                            'class' => 'form-control',
                        )
                    ),
                    'second_options' => array(
                        'label' => 'Repetir',
                        // 'label_attr' => array(
                        //     'class' => 'form-label col-lg-3',
                        // ),
                        'attr' => array(
                            'class' => 'form-control',
                        )
                    ),
        ));

        $builder
                ->add('user_roles', 'select2', array(
                    'label' => 'Roles',
                    'class' => 'UserBundle:Role',
                    'url' => 'User_autocomplete_user_roles',
                    'configs' => array(
                        'multiple' => true, //required true or false
                        'width' => 'off',
                    ),
                    'attr' => array(
                        'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                    ),
                        )
                )
        ;


        $builder
                ->add('modulos', 'select2', array(
                    'label' => 'Modulos',
                    'class' => 'Tecspro\ModuloBundle\Entity\Modulo',
                    'url' => 'User_autocomplete_modulos',
                    'configs' => array(
                        'multiple' => true, //required true or false
                        'width' => 'off',
                    ),
                    'attr' => array(
                        'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                    )
                        )
                )
                ->add('enabled', null, array(
                    'label' => 'Activo',
                        )
                )
                ->add('domicilios', 'collection', array(
                    'type' => new DomicilioType(),
                    'allow_add' => true,
                    'required' => true,
                    'by_reference' => false,
                    'label' => false,
                        )
                )
                ->add('telefonos', 'collection', array(
                    'type' => new TelefonoType(),
                    'allow_add' => true,
                    'required' => true,
                    'by_reference' => false,
                    'label' => false,
                        )
                )
                ->add('cantidadSessiones', null, array(
                    'label' => 'Cantidad Sessiones',
                    // 'label_attr' => sdrray(
                    //     'class' => 'form-label col-lg-3',
                    // ),
                    'attr' => array(
                        'class' => 'form-control',
                    )
                ))

        ;

        if ($required == false) {
            $builder
                    ->add('ResetearSessiones', 'checkbox', array(
                        'label' => 'Reset Sessiones',
                        'mapped' => false,
                    ))
            ;
        }
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Tecspro\UserBundle\Entity\User',
            'cascade_validation' => true
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'tecspro_userbundle_user';
    }

}

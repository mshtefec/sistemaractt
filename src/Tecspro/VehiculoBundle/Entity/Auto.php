<?php

namespace Tecspro\VehiculoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Auto
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Auto extends Vehiculo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="categorizacion", type="string", length=255)
     */
    protected $categorizacion;

    /**
     * @var string
     *
     * @ORM\Column(name="trocha", type="string", length=255)
     */
    protected $trocha;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set categorizacion
     *
     * @param string $categorizacion
     * @return Auto
     */
    public function setCategorizacion($categorizacion)
    {
        $this->categorizacion = $categorizacion;

        return $this;
    }

    /**
     * Get categorizacion
     *
     * @return string 
     */
    public function getCategorizacion()
    {
        return $this->categorizacion;
    }

    /**
     * Set trocha
     *
     * @param string $trocha
     * @return Auto
     */
    public function setTrocha($trocha)
    {
        $this->trocha = $trocha;

        return $this;
    }

    /**
     * Get trocha
     *
     * @return string 
     */
    public function getTrocha()
    {
        return $this->trocha;
    }
}

<?php

namespace Tecspro\ModuloBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Tecspro\ModuloBundle\Entity\Modulo;
use Tecspro\ModuloBundle\Form\ModuloType;
use Tecspro\ModuloBundle\Form\ModuloFilterType;
use Symfony\Component\HttpFoundation\JsonResponse;
use A2lix\I18nDoctrineBundle\Annotation\I18nDoctrine;

/**
 * Modulo controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/modulo")
 */
class ModuloController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Tecspro/ModuloBundle/Resources/config/Modulo.yml',
    );

    /**
     * Lists all Modulo entities.
     *
     * @Route("/", name="admin_modulo")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->config['filterType'] = new ModuloFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Modulo entity.
     *
     * @Route("/", name="admin_modulo_create")
     * @Method("POST")
     * @Template("ModuloBundle:Modulo:new.html.twig")
     */
    public function createAction() {
        $this->config['newType'] = new ModuloType();
        $response = parent::createAction();

        return $response;
    }

    /**
     * Displays a form to create a new Modulo entity.
     *
     * @Route("/new", name="admin_modulo_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->config['newType'] = new ModuloType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Modulo entity.
     *
     * @Route("/{id}", name="admin_modulo_show", options={"expose"=true})
     * @Method("GET")
     * @Template()
     * @I18nDoctrine
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Modulo entity.
     *
     * @Route("/{id}/edit", name="admin_modulo_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     * @I18nDoctrine
     */
    public function editAction($id) {
        $this->config['editType'] = new ModuloType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Modulo entity.
     *
     * @Route("/{id}", name="admin_modulo_update")
     * @Method("PUT")
     * @Template("ModuloBundle:Modulo:edit.html.twig")
     * @I18nDoctrine
     */
    public function updateAction($id) {
        $this->config['editType'] = new ModuloType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a Modulo entity.
     *
     * @Route("/{id}", name="admin_modulo_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter Modulo.
     *
     * @Route("/exporter/{format}", name="admin_modulo_export")
     */
    public function getExporter($format) {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Datatable Modulo.
     *
     * @Route("/get-table/", name="admin_modulo_table")
     */
    public function getDatatable() {
        $response = parent::getTable();

        return $response;
    }

    /**
     * Create query.
     * @return Doctrine\ORM\QueryBuilder $queryBuilder
     */
    protected function createQuery() {
        $em = $this->getDoctrine()->getManager();
        $config = $this->getConfig();
        $qb = $em->createQueryBuilder();
        $request = $this->getRequest();
        $em->getFilters()->enable('oneLocale')->setParameter('locale', "es");
        $qb
                ->select('a.id', 't.nombre')
                ->from($config['repository'], 'a')
                ->Join('a.translations', 't')
        ;

        $array = array(
            'query' => $qb,
            'tipoArray' => null
        );


        return $array;
    }

    /**
     *
     * @Route("/seleccionar-modulo/", name="admin_modulo_seleccionar_modulo")
     */
    public function seleccionarModulosAction() {
        $config = $this->getConfig();
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository($config['repository'])->findAll();
        return $this->render(
                        'ModuloBundle:Form:macro-modal-seleccionarmodulo.html.twig', array('entities' => $entities)
        );
    }

    /**
     *
     * @Route("/set-modulo/", name="admin_modulo_setsession_modulo")
     */
    public function setearSessionModuloAction() {
        $config = $this->getConfig();
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $modulo = $request->request->get('modulo', null);
        $array = array();
        $entity = $em->getRepository($config['repository'])->find($modulo);

        if (!$entity) {
            $resultado = false;
        } else {
            $session = $request->getSession();
            $session->set('moduloSeleccionado', $modulo);
            $session->set('moduloNombreSeleccionado', $entity->getNombre());
            $resultado = true;
            $user = $this->getUser();
            $user->setItemConfiguraciones("ulitmoModulo", $entity->getId());
            $em->persist($user);
            $em->flush();
        }


        $array[] = array(
            'resultado' => $resultado,
        );


        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

}

function mostrarNotifiacion($titulo, $texto, $tipo) {

    var opts = {
        title: $titulo,
        text: $texto,
        delay: 1000
    };
    switch ($tipo) {
        case 'error':
            opts.type = "error";
            break;
        case 'info':
            opts.type = "info";
            break;
        case 'success':
            opts.type = "success";
            break;
    }
    new PNotify(opts);
}

function confirmAlert($titulo, $texto) {
    return new PNotify({
        title: $titulo,
        text: $texto,
        icon: 'glyphicon glyphicon-question-sign',
        hide: false,
        confirm: {
            confirm: true
        },
        buttons: {
            closer: false,
            sticker: false
        },
        history: {
            history: false
        }
    });
}
function confirmAlertSiNoCancelar($titulo, $texto, $funcionSi, $funcionNo, $funcionCancelar) {
    return new PNotify({
        title: $titulo,
        text: $texto,
        icon: 'glyphicon glyphicon-question-sign',
        hide: false,
        confirm: {
            confirm: true,
            buttons: [{
                    text: 'Si',
                    click: function (notice) {
                        notice.remove();
                        if ($funcionSi !== "") {
                            eval($funcionSi + '()');
                        }
                    }
                }, {
                    text: 'No',
                    click: function (notice) {
                        notice.remove();
                        if ($funcionNo !== "") {
                            eval($funcionNo + '()');
                        }
                    }
                }, {
                    text: 'Cancelar',
                    click: function (notice) {
                        notice.remove();
                        if ($funcionCancelar !== "") {
                            eval($funcionCancelar + '()');
                        }
                    }
                }]
        },
        buttons: {
            closer: false,
            sticker: false
        },
        history: {
            history: false
        }
    });
}
function confirmAlertSiNo($titulo, $texto, $funcionSi, $funcionNo) {
    return new PNotify({
        title: $titulo,
        text: $texto,
        icon: 'glyphicon glyphicon-question-sign',
        hide: false,
        confirm: {
            confirm: true,
            buttons: [{
                    text: 'Si',
                    click: function (notice) {
                        notice.remove();
                        if ($funcionSi !== "") {
                            eval($funcionSi + '()');
                        }
                    }
                }, {
                    text: 'No',
                    click: function (notice) {
                        notice.remove();
                        if ($funcionNo !== "") {
                            eval($funcionNo + '()');
                        }
                    }
                }]
        },
        buttons: {
            closer: false,
            sticker: false
        },
        history: {
            history: false
        }
    });
}
function progreso($titulo, $texto, $funcionSi, $funcionNo) {
    return new PNotify({
        icon: 'fa fa-cog fa-spin',
        hide: false,
        buttons: {
            closer: false,
            sticker: false
        },
        history: {
            history: false
        },
        before_open: function (notice) {
            progress = notice.get().find("div.progress-bar");
            progress.width(cur_value + "%").attr("aria-valuenow", cur_value).find("span").html(cur_value + "%");
            // Pretend to do something.
            var timer = setInterval(function () {
                if (cur_value == 70) {
                    loader.update({
                        title: "Aligning discrete worms",
                        icon: "fa fa-circle-o-notch fa-spin"
                    });
                }
                if (cur_value == 80) {
                    loader.update({
                        title: "Connecting end points",
                        icon: "fa fa-refresh fa-spin"
                    });
                }
                if (cur_value == 90) {
                    loader.update({
                        title: "Dividing and conquering",
                        icon: "fa fa-spinner fa-spin"
                    });
                }
                if (cur_value >= 100) {
                    // Remove the interval.
                    window.clearInterval(timer);
                    loader.remove();
                    return;
                }
                cur_value += 1;
                progress.width(cur_value + "%").attr("aria-valuenow", cur_value).find("span").html(cur_value + "%");
            }, 65);
        }
    });
}

<?php

namespace Tecspro\ModuloBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Exclude;

/**
 * Modulo
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Tecspro\ModuloBundle\Entity\ModuloRepository")
 */
class Modulo {

    use \A2lix\I18nDoctrineBundle\Doctrine\ORM\Util\Translatable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Type("integer")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ArcIsActive", type="boolean")
     * @Type("boolean")
     */
    private $ArcIsActive;

    /**
     * @ORM\OneToMany(targetEntity="Categoria", mappedBy="modulo")
     * @Type("ArrayCollection<Tecspro\ModuloBundle\Entity\Categoria>")
     */
    private $categorias;

    /**
     * @ORM\OneToMany(targetEntity="Tecspro\FrontBundle\Entity\Proyecto", mappedBy="modulo")
     * @Exclude
     */
    private $proyectos;

    /**
     * @ORM\ManyToMany(targetEntity="Tecspro\UserBundle\Entity\User", inversedBy="modulos")
     * @ORM\JoinTable(name="users_modulo")
     * @Exclude
     */
    private $users;

    /**
     * @Type("ArrayCollection<Tecspro\ModuloBundle\Entity\ModuloTranslation>")
     */
    protected $translations;

    /**
     * @var string
     *
     * @ORM\Column(name="tablas", type="array")
     * @Exclude
     */
    private $tablas;

    /**
     * Constructor
     */
    public function __construct() {
        $this->categorias = new \Doctrine\Common\Collections\ArrayCollection();
        $this->proyectos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
        $this->translations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString() {
        return $this->getNombre();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Add categorias
     *
     * @param \Tecspro\ModuloBundle\Entity\Categoria $categorias
     * @return Modulo
     */
    public function addCategoria(\Tecspro\ModuloBundle\Entity\Categoria $categorias) {
        $categorias->setModulo($this);
        $this->categorias[] = $categorias;

        return $this;
    }

    /**
     * Remove categorias
     *
     * @param \Tecspro\ModuloBundle\Entity\Categoria $categorias
     */
    public function removeCategoria(\Tecspro\ModuloBundle\Entity\Categoria $categorias) {
        $this->categorias->removeElement($categorias);
    }

    /**
     * Get categorias
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategorias() {
        return $this->categorias;
    }

    /**
     * Add proyectos
     *
     * @param \Tecspro\FrontBundle\Entity\Proyecto $proyectos
     * @return Modulo
     */
    public function addProyecto(\Tecspro\FrontBundle\Entity\Proyecto $proyectos) {
        $proyectos->setModulo($this);
        $this->proyectos[] = $proyectos;

        return $this;
    }

    /**
     * Remove proyectos
     *
     * @param \Tecspro\FrontBundle\Entity\Proyecto $proyectos
     */
    public function removeProyecto(\Tecspro\FrontBundle\Entity\Proyecto $proyectos) {
        $this->proyectos->removeElement($proyectos);
    }

    /**
     * Get proyectos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProyectos() {
        return $this->proyectos;
    }

    /**
     * Add users
     *
     * @param \Tecspro\UserBundle\Entity\User $users
     * @return Modulo
     */
    public function addUser(\Tecspro\UserBundle\Entity\User $users) {
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \Tecspro\UserBundle\Entity\User $users
     */
    public function removeUser(\Tecspro\UserBundle\Entity\User $users) {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsers() {
        return $this->users;
    }

    /**
     * Set ArcIsActive
     *
     * @param boolean $arcIsActive
     * @return Modulo
     */
    public function setArcIsActive($arcIsActive) {
        $this->ArcIsActive = $arcIsActive;

        return $this;
    }

    /**
     * Get ArcIsActive
     *
     * @return boolean 
     */
    public function getArcIsActive() {
        return $this->ArcIsActive;
    }

    /**
     * Set tablas
     *
     * @param array $tablas
     * @return Modulo
     */
    public function setTablas($tablas) {
        $this->tablas = $tablas;

        return $this;
    }

    /**
     * Get tablas
     *
     * @return array 
     */
    public function getTablas() {
        return $this->tablas;
    }

}

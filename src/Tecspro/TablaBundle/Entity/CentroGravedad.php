<?php

namespace Tecspro\TablaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 * CentroGravedad
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Tecspro\TablaBundle\Entity\CentroGravedadRepository")
 */
class CentroGravedad {

    use \A2lix\I18nDoctrineBundle\Doctrine\ORM\Util\Translatable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="batallaCategoria1", type="string", length=255, nullable=true)
     */
    private $batallaCategoria1;

    /**
     * @var string
     *
     * @ORM\Column(name="batallaCategoria2", type="string", length=255, nullable=true)
     */
    private $batallaCategoria2;

    /**
     * @var string
     *
     * @ORM\Column(name="batallaCategoria3", type="string", length=255, nullable=true)
     */
    private $batallaCategoria3;

    /**
     * @var string
     *
     * @ORM\Column(name="batallaCategoria4", type="string", length=255, nullable=true)
     */
    private $batallaCategoria4;

    /**
     * @var string
     *
     * @ORM\Column(name="batallaCateogira5", type="string", length=255, nullable=true)
     */
    private $batallaCateogira5;

    /**
     * @var string
     *
     * @ORM\Column(name="altura", type="string", length=255, nullable=true)
     */
    private $altura;

    /**
     * @var string
     *
     * @ORM\Column(name="desviacionEstandar", type="string", length=255, nullable=true)
     */
    private $desviacionEstandar;
    protected $translations;

    /**
     * Constructor
     */
    public function __construct() {
        $this->translations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set batallaCategoria1
     *
     * @param string $batallaCategoria1
     * @return CentroGravedad
     */
    public function setBatallaCategoria1($batallaCategoria1) {
        $this->batallaCategoria1 = $batallaCategoria1;

        return $this;
    }

    /**
     * Get batallaCategoria1
     *
     * @return string 
     */
    public function getBatallaCategoria1() {
        return $this->batallaCategoria1;
    }

    /**
     * Set batallaCategoria2
     *
     * @param string $batallaCategoria2
     * @return CentroGravedad
     */
    public function setBatallaCategoria2($batallaCategoria2) {
        $this->batallaCategoria2 = $batallaCategoria2;

        return $this;
    }

    /**
     * Get batallaCategoria2
     *
     * @return string 
     */
    public function getBatallaCategoria2() {
        return $this->batallaCategoria2;
    }

    /**
     * Set batallaCategoria3
     *
     * @param string $batallaCategoria3
     * @return CentroGravedad
     */
    public function setBatallaCategoria3($batallaCategoria3) {
        $this->batallaCategoria3 = $batallaCategoria3;

        return $this;
    }

    /**
     * Get batallaCategoria3
     *
     * @return string 
     */
    public function getBatallaCategoria3() {
        return $this->batallaCategoria3;
    }

    /**
     * Set batallaCategoria4
     *
     * @param string $batallaCategoria4
     * @return CentroGravedad
     */
    public function setBatallaCategoria4($batallaCategoria4) {
        $this->batallaCategoria4 = $batallaCategoria4;

        return $this;
    }

    /**
     * Get batallaCategoria4
     *
     * @return string 
     */
    public function getBatallaCategoria4() {
        return $this->batallaCategoria4;
    }

    /**
     * Set batallaCateogira5
     *
     * @param string $batallaCateogira5
     * @return CentroGravedad
     */
    public function setBatallaCateogira5($batallaCateogira5) {
        $this->batallaCateogira5 = $batallaCateogira5;

        return $this;
    }

    /**
     * Get batallaCateogira5
     *
     * @return string 
     */
    public function getBatallaCateogira5() {
        return $this->batallaCateogira5;
    }

    /**
     * Set altura
     *
     * @param string $altura
     * @return CentroGravedad
     */
    public function setAltura($altura) {
        $this->altura = $altura;

        return $this;
    }

    /**
     * Get altura
     *
     * @return string 
     */
    public function getAltura() {
        return $this->altura;
    }

    /**
     * Set desviacionEstandar
     *
     * @param string $desviacionEstandar
     * @return CentroGravedad
     */
    public function setDesviacionEstandar($desviacionEstandar) {
        $this->desviacionEstandar = $desviacionEstandar;

        return $this;
    }

    /**
     * Get desviacionEstandar
     *
     * @return string 
     */
    public function getDesviacionEstandar() {
        return $this->desviacionEstandar;
    }

}

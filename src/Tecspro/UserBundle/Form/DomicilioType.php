<?php

namespace Tecspro\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Range;

class DomicilioType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('localidad', null, array(
                    'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                ))
                ->add('provincia', null, array(
                    'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                ))
                ->add('pais', null, array(
                    'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                ))                                
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Tecspro\UserBundle\Entity\Domicilio'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'Tecspro_Userbundle_domicilio';
    }

}

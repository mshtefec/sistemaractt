<?php

namespace Tecspro\ARCBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vehiculo
 *
 * @ORM\Table(name="arc_vehiculo")
 * @ORM\Entity(repositoryClass="Tecspro\ARCBundle\Entity\VehiculoRepository")
 */
class Vehiculo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, nullable=true)
     */
    private $nombre;

    /**
     * @var boolean
     *
     * @ORM\Column(name="peaton", type="boolean")
     */
    private $peaton;

    /**
     * @var string
     *
     * @ORM\Column(name="movimiento_previo_a_la_colision", type="string", length=255)
     */
    private $movimientoPrevioALaColision;

    /**
     * @var string
     *
     * @ORM\Column(name="velocidad_inicial", type="string", length=255, nullable=true)
     */
    private $velocidadInicial;

    /**
     * @var string
     *
     * @ORM\Column(name="unidad_velocidad_inicial", type="string", length=255)
     */
    private $unidadVelocidadInicial;

    /**
     * @var string
     *
     * @ORM\Column(name="velocidad_impacto", type="string", length=255, nullable=true)
     */
    private $velocidadImpacto;

    /**
     * @var string
     *
     * @ORM\Column(name="unidad_velocidad_impacto", type="string", length=255, nullable=true)
     */
    private $unidadVelocidadImpacto;

    /**
     * @var string
     *
     * @ORM\Column(name="tiempo_reaccion", type="string", length=255, nullable=true)
     */
    private $tiempoReaccion;

    /**
     * @var string
     *
     * @ORM\Column(name="desaceleracion", type="string", length=255, nullable=true)
     */
    private $desaceleracion;

    /**
     * @var string
     *
     * @ORM\Column(name="tiempo_preimpacto_a_considerar", type="string", length=255, nullable=true)
     */
    private $tiempoPreimpactoAConsiderar;

    /**
     * @ORM\ManyToOne(targetEntity="ARCCampos", inversedBy="vehiculos")
     * @ORM\JoinColumn(name="arc_campos_id", referencedColumnName="id")
     */
    private $arcCampos;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Vehiculo
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set peaton
     *
     * @param boolean $peaton
     * @return Vehiculo
     */
    public function setPeaton($peaton)
    {
        $this->peaton = $peaton;

        return $this;
    }

    /**
     * Get peaton
     *
     * @return boolean 
     */
    public function getPeaton()
    {
        return $this->peaton;
    }

    /**
     * Set movimientoPrevioALaColision
     *
     * @param string $movimientoPrevioALaColision
     * @return Vehiculo
     */
    public function setMovimientoPrevioALaColision($movimientoPrevioALaColision)
    {
        $this->movimientoPrevioALaColision = $movimientoPrevioALaColision;

        return $this;
    }

    /**
     * Get movimientoPrevioALaColision
     *
     * @return string 
     */
    public function getMovimientoPrevioALaColision()
    {
        return $this->movimientoPrevioALaColision;
    }

    /**
     * Set velocidadInicial
     *
     * @param string $velocidadInicial
     * @return Vehiculo
     */
    public function setVelocidadInicial($velocidadInicial)
    {
        $this->velocidadInicial = $velocidadInicial;

        return $this;
    }

    /**
     * Get velocidadInicial
     *
     * @return string 
     */
    public function getVelocidadInicial()
    {
        return $this->velocidadInicial;
    }

    /**
     * Set unidadVelocidadInicial
     *
     * @param string $unidadVelocidadInicial
     * @return Vehiculo
     */
    public function setUnidadVelocidadInicial($unidadVelocidadInicial)
    {
        $this->unidadVelocidadInicial = $unidadVelocidadInicial;

        return $this;
    }

    /**
     * Get unidadVelocidadInicial
     *
     * @return string 
     */
    public function getUnidadVelocidadInicial()
    {
        return $this->unidadVelocidadInicial;
    }

    /**
     * Set velocidadImpacto
     *
     * @param string $velocidadImpacto
     * @return Vehiculo
     */
    public function setVelocidadImpacto($velocidadImpacto)
    {
        $this->velocidadImpacto = $velocidadImpacto;

        return $this;
    }

    /**
     * Get velocidadImpacto
     *
     * @return string 
     */
    public function getVelocidadImpacto()
    {
        return $this->velocidadImpacto;
    }

    /**
     * Set unidadVelocidadImpacto
     *
     * @param string $unidadVelocidadImpacto
     * @return Vehiculo
     */
    public function setUnidadVelocidadImpacto($unidadVelocidadImpacto)
    {
        $this->unidadVelocidadImpacto = $unidadVelocidadImpacto;

        return $this;
    }

    /**
     * Get unidadVelocidadImpacto
     *
     * @return string 
     */
    public function getUnidadVelocidadImpacto()
    {
        return $this->unidadVelocidadImpacto;
    }

    /**
     * Set tiempoReaccion
     *
     * @param string $tiempoReaccion
     * @return Vehiculo
     */
    public function setTiempoReaccion($tiempoReaccion)
    {
        $this->tiempoReaccion = $tiempoReaccion;

        return $this;
    }

    /**
     * Get tiempoReaccion
     *
     * @return string 
     */
    public function getTiempoReaccion()
    {
        return $this->tiempoReaccion;
    }

    /**
     * Set desaceleracion
     *
     * @param string $desaceleracion
     * @return Vehiculo
     */
    public function setDesaceleracion($desaceleracion)
    {
        $this->desaceleracion = $desaceleracion;

        return $this;
    }

    /**
     * Get desaceleracion
     *
     * @return string 
     */
    public function getDesaceleracion()
    {
        return $this->desaceleracion;
    }

    /**
     * Set tiempoPreimpactoAConsiderar
     *
     * @param string $tiempoPreimpactoAConsiderar
     * @return Vehiculo
     */
    public function setTiempoPreimpactoAConsiderar($tiempoPreimpactoAConsiderar)
    {
        $this->tiempoPreimpactoAConsiderar = $tiempoPreimpactoAConsiderar;

        return $this;
    }

    /**
     * Get tiempoPreimpactoAConsiderar
     *
     * @return string 
     */
    public function getTiempoPreimpactoAConsiderar()
    {
        return $this->tiempoPreimpactoAConsiderar;
    }

    /**
     * Set arcCampos
     *
     * @param \Tecspro\ARCBundle\Entity\ARCCampos $arcCampos
     * @return Vehiculo
     */
    public function setArcCampos(\Tecspro\ARCBundle\Entity\ARCCampos $arcCampos = null)
    {
        $this->arcCampos = $arcCampos;

        return $this;
    }

    /**
     * Get arcCampos
     *
     * @return \Tecspro\ARCBundle\Entity\ARCCampos 
     */
    public function getArcCampos()
    {
        return $this->arcCampos;
    }
}

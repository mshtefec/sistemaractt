<?php

namespace Tecspro\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Translation\Loader\YamlFileLoader;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Translation\Loader\ArrayLoader;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller {

    /**
     * @Route("/", name="inicio_plantilla")
     * @Template()
     */
    public function indexAction() {//
        return array();
    }

    /**
     * @Route("/contacto", name="contactenos_plantilla")
     * @Method("get")
     * @Template()
     */
    public function contactoAction() {//
        return array();
    }

    /**
     * @Route("/contacto", name="contactenos_plantilla_send")
     * @Method("POST")
     * @Template("TecsproFrontBundle:Default:contacto.html.twig")
     */
    public function contactoSendAction() {//
        $request = $this->getRequest();

        $txtNombre = $request->request->all()['txtNombre'];
        $txtEmail = $request->request->all()['txtEmail'];
        $txtMensaje = $request->request->all()['txtMensaje'];
        $txtAsunto = $request->request->all()['txtAsunto'];

        if ($txtEmail != "") {
            $message = \Swift_Message::newInstance()
                    ->setSubject($txtAsunto)
                    ->setFrom($txtEmail)
                    ->setTo($this->container->getParameter('email_soporte'))
                    ->setBody("Nombre: $txtNombre \n\nEmail: \n\nMensaje: $txtEmail" . $txtMensaje)
            ;
            $this->get('mailer')->send($message);
            $this->get('session')->getFlashBag()->add('success', 'Email Enviado');

            return $this->redirect($this->generateUrl('contactenos_plantilla'), 301);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        return array();
    }

    /**
     * @Route("/languaje", name="traslation_plantilla")
     * @Template()
     */
    public function traslationAction() {//
        $request = $this->getRequest();
        $locale = $request->getSession()->get('_locale');

        return array('locale' => $locale);
    }

    /**
     * @Route("/cambiar-languaje/", name="load_traslation", options={"expose"=true})
     * @Template()
     */
    public function change_traslationAction() {
        $request = $this->getRequest();
        $languaje = $request->query->get('languaje', null);
        if (is_null($languaje))
            $languaje == "es";
        $em = $this->getDoctrine()->getManager();
        if ($languaje == "pt") {

            $request->setLocale('pt');
            $request->getSession()->set('_locale', 'pt');
        } elseif ($languaje == "es") {

            $request->setLocale('es');
            $request->getSession()->set('_locale', 'es');
        }
        $user = $this->getUser();
        $user->setItemConfiguraciones("lenguaje", $languaje);
        $this->get("formula")->cambiarIdioma();
        $em->persist($user);
        $em->flush();

        $response = new JsonResponse();
        $response->setData(true);

        return $response;
    }

}

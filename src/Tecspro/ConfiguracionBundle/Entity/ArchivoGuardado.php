<?php

namespace Tecspro\ConfiguracionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ArchivoGuardado
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class ArchivoGuardado
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
     * @var string
     *
     * @ORM\Column(name="nombreProyecto", type="string", length=255)
     */
    private $nombreProyecto;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaIncidente", type="datetime")
     */
    private $fechaIncidente;

    /**
     * @var string
     *
     * @ORM\Column(name="comentario", type="string", length=255)
     */
    private $comentario;

    /**
     * @ORM\OneToMany(targetEntity="Tecspro\ConfiguracionBundle\Entity\FormulaGuardada", mappedBy="archivoGuardado")
     * */
    private $formulaGuardada;

    /**
     * @ORM\OneToMany(targetEntity="Tecspro\ConfiguracionBundle\Entity\PartesInvolucradas", mappedBy="archivoGuardado")
     * */
    private $partesInvolucradas;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return ArchivoGuardado
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set nombreProyecto
     *
     * @param string $nombreProyecto
     * @return ArchivoGuardado
     */
    public function setNombreProyecto($nombreProyecto)
    {
        $this->nombreProyecto = $nombreProyecto;

        return $this;
    }

    /**
     * Get nombreProyecto
     *
     * @return string 
     */
    public function getNombreProyecto()
    {
        return $this->nombreProyecto;
    }

    /**
     * Set fechaIncidente
     *
     * @param \DateTime $fechaIncidente
     * @return ArchivoGuardado
     */
    public function setFechaIncidente($fechaIncidente)
    {
        $this->fechaIncidente = $fechaIncidente;

        return $this;
    }

    /**
     * Get fechaIncidente
     *
     * @return \DateTime 
     */
    public function getFechaIncidente()
    {
        return $this->fechaIncidente;
    }

    /**
     * Set comentario
     *
     * @param string $comentario
     * @return ArchivoGuardado
     */
    public function setComentario($comentario)
    {
        $this->comentario = $comentario;

        return $this;
    }

    /**
     * Get comentario
     *
     * @return string 
     */
    public function getComentario()
    {
        return $this->comentario;
    }
}

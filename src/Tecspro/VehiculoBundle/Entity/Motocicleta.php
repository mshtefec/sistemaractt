<?php

namespace Tecspro\VehiculoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Motocicleta
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Tecspro\VehiculoBundle\Entity\MotocicletaRepository")
 */
class Motocicleta extends Vehiculo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}

<?php

namespace Tecspro\TablaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * CoeficienteArrastrePeatonType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class CoeficienteArrastrePeatonType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('autor')
        ->add('translations', 'a2lix_translations', array(
                    'required' => true,
                    'fields' => array(
                       'trayectoria' => array(
                            'field_type' => 'text',
                            'locale_options' => array(
                                'es' => array(
                                    'label' => 'Trayectoria Español',
                                ),
                                'pt' => array(
                                    'label' => 'Trayectoria Portugues',
                                )
                            )
                        ),
                        'notas' => array(
                            'field_type' => 'text',
                            'locale_options' => array(
                                'es' => array(
                                    'label' => 'Notas Español',
                                ),
                                'pt' => array(
                                    'label' => 'Notas Portugues',
                                )
                            )
                        )
                    )
                ))
            ->add('factorArrastre')
            
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Tecspro\TablaBundle\Entity\CoeficienteArrastrePeaton'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'tecspro_tablabundle_coeficientearrastrepeaton';
    }
}

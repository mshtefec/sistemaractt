<?php

namespace Tecspro\ModuloBundle\Entity;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Exclude;

/**
 * Translation trait.
 *
 * Should be used inside translation entity
 */
trait Translation {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Type("integer")
     */
    protected $id;

    /**
     * @ORM\Column(length=10)
     * @Type("string")
     */
    protected $locale;

    /**
     * @Exclude
     */
    protected $translatable;

    public function getId() {
        return $this->id;
    }

    public function getTranslatable() {
        return $this->translatable;
    }

    public function setTranslatable($translatable) {
        $this->translatable = $translatable;
        return $this;
    }

    public function getLocale() {
        return $this->locale;
    }

    public function setLocale($locale) {
        $this->locale = $locale;
        return $this;
    }

}

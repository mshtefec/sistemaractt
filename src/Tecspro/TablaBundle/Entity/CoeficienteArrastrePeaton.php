<?php

namespace Tecspro\TablaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CoeficienteArrastrePeaton
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Tecspro\TablaBundle\Entity\CoeficienteArrastrePeatonRepository")
 */
class CoeficienteArrastrePeaton {

    use \A2lix\I18nDoctrineBundle\Doctrine\ORM\Util\Translatable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="autor", type="string", length=255, nullable=true)
     */
    private $autor;

    /**
     * @var string
     *
     * @ORM\Column(name="factorArrastre", type="string", length=255, nullable=true)
     */
    private $factorArrastre;
    protected $translations;

    /**
     * Constructor
     */
    public function __construct() {
        $this->translations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set autor
     *
     * @param string $autor
     * @return CoeficienteArrastrePeaton
     */
    public function setAutor($autor) {
        $this->autor = $autor;

        return $this;
    }

    /**
     * Get autor
     *
     * @return string 
     */
    public function getAutor() {
        return $this->autor;
    }

    /**
     * Set factorArrastre
     *
     * @param string $factorArrastre
     * @return CoeficienteArrastrePeaton
     */
    public function setFactorArrastre($factorArrastre) {
        $this->factorArrastre = $factorArrastre;

        return $this;
    }

    /**
     * Get factorArrastre
     *
     * @return string 
     */
    public function getFactorArrastre() {
        return $this->factorArrastre;
    }

}

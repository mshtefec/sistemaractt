<?php

namespace Tecspro\ModuloBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 * Unidad
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Tecspro\ModuloBundle\Entity\UnidadRepository")
 */
class Unidad {

    use \A2lix\I18nDoctrineBundle\Doctrine\ORM\Util\Translatable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Type("integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="simbolo", type="string", length=255)
     * @Type("string")
     */
    private $simbolo;

   

    /**
     * @var string
     *
     * @ORM\Column(name="operacion", type="string", length=255)
     * @Type("string")
     */
    private $operacion;
    /**
     * @Type("ArrayCollection<Tecspro\ModuloBundle\Entity\UnidadTranslation>")
     */
    protected $translations;

    public function __construct() {
        $this->translations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString() {
        return $this->simbolo;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set simbolo
     *
     * @param string $simbolo
     * @return Unidad
     */
    public function setSimbolo($simbolo) {
        $this->simbolo = $simbolo;

        return $this;
    }

    /**
     * Get simbolo
     *
     * @return string 
     */
    public function getSimbolo() {
        return $this->simbolo;
    }

    /**
     * Set operacion
     *
     * @param string $operacion
     * @return Unidad
     */
    public function setOperacion($operacion) {
        $this->operacion = $operacion;

        return $this;
    }

    /**
     * Get operacion
     *
     * @return string 
     */
    public function getOperacion() {
        return $this->operacion;
    }

}

<?php

namespace Tecspro\FrontBundle\Services;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use JMS\Serializer\SerializerBuilder;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\SecurityContext;

class Formula {

    private $em;
    private $session;

    /**
     * @var SecurityContext
     */
    protected $securityContext;

    public function __construct(SecurityContext $securityContext, Session $session, \Doctrine\ORM\EntityManager $em) {
        $this->em = $em;
        $this->session = $session;
        $this->securityContext = $securityContext;
        $this->session->start();
    }

    public function obtenerIdSession($arraySession) {
        $array = array();
        $ultimoValor = 1;
        foreach ($arraySession as $val) {
            $array[] = $val["key"];
        }
        if (count($array) > 0)
            $ultimoValor = max($array) + 1;

        return $ultimoValor;
    }

    public function findInArraySession($arraySession, $item) {

        if (is_null($arraySession) == false) {
            foreach ($arraySession as $key => $val) {
                if (isset($val["key"])) {
                    if ($val["key"] == $item)
                        return $key;
                }
            }
        }
        return false;
    }

    public function EliminarFormulasEnProyecto($_formula) {

//guardo la id del proyecto y los valores de la variables
//$formulas = $this->session->get('formulas', null);
        $proyectoSession = $this->session->get('proyecto', null);

        if (is_null($_formula) == false && is_null($proyectoSession) == false) {
            $proyectoEntity = $this->em->getRepository('TecsproFrontBundle:Proyecto')->find($proyectoSession);
//ladybug_dump_die($proyectoEntity->getFormulas()[$_formula["key"]]);

            if (isset($proyectoEntity->getFormulas()[$_formula["key"]])) {

                $array = $proyectoEntity->getFormulas();
                unset($array[$_formula["key"]]);
                $proyectoEntity->setFormulas($array);
            }

            $this->em->persist($proyectoEntity);
            $this->em->flush();
        }
    }

    public function EliminarTodasLasFormulasEnProyecto() {
//guardo la id del proyecto y los valores de la variables
//$formulas = $this->session->get('formulas', null);
        $proyectoSession = $this->session->get('proyecto', null);

        if (is_null($proyectoSession) == false) {
            $proyectoEntity = $this->em->getRepository('TecsproFrontBundle:Proyecto')->find($proyectoSession);
            $proyectoEntity->setFormulas(array());

            $this->em->persist($proyectoEntity);
            $this->em->flush();
        }
    }

    public function guardarFormulasDeSessionEnProyecto() {
        $proyectoSession = $this->session->get('proyecto', null);
        $proyectoEntity = $this->em->getRepository('TecsproFrontBundle:Proyecto')->find($proyectoSession);
        $formulaSession = $this->session->get('formulas', null);
        if (is_null($formulaSession) == false) {
            foreach ($formulaSession as $formula) {
                $array = array();
                $serializer = SerializerBuilder::create()->build();
// foreach ($formulas as $formula) {
//deserializo para setear los valores de las variables

                $entity = $serializer->deserialize($formula["formula"], 'Tecspro\ModuloBundle\Entity\Formula', 'json');

//seriealizo variables
                $variablesJson = $serializer->serialize($entity->getVariables(), 'json');

//serealizo ecuaciones
                $ecuacionesJson = $serializer->serialize($entity->getEcuacion(), 'json');

                $array[] = array(
                    'idFormula' => $entity->getId(),
                    'valores' => $variablesJson,
                    'ecuaciones' => $ecuacionesJson,
                    'imprimir' => $entity->getImprimir()
                );
//}
                $proyectoEntity->setItemFormulas($formula["key"], $array);
                $this->em->persist($proyectoEntity);
            }
            $this->em->flush();
        }
    }

    public function guardarFormulasArrayEnProyecto($formulaSessionArray) {
        $formulaSession = $this->session->get('formulas', null);
        $proyectoSession = $this->session->get('proyecto', null);
        $serializer = SerializerBuilder::create()->build();
        $itemNoEliminar = array();
        //guardo la id del proyecto y los valores de la variables

        $proyectoEntity = null;
        if (is_null($proyectoSession)) {
            $moduloSession = $this->session->get('moduloSeleccionado', null);
            $moduloEntity = $this->em->getRepository('ModuloBundle:Modulo')->find($moduloSession);
            $token = $this->securityContext->getToken();

            $proyectoEntity = new \Tecspro\FrontBundle\Entity\Proyecto();
            $proyectoEntity->setNombre((new \DateTime())->format('d/m/Y H:i'));
            $proyectoEntity->setUser($token->getUser());
            $proyectoEntity->setModulo($moduloEntity);
            $this->em->persist($proyectoEntity);
            $this->em->flush();
            $this->session->set('proyecto', $proyectoEntity->getId());
            $this->session->set('proyectoNombre', $proyectoEntity->getNombre());
        } else {
            $proyectoEntity = $this->em->getRepository('TecsproFrontBundle:Proyecto')->find($proyectoSession);
        }

        foreach ($formulaSessionArray as $key => $_formula) {

            if (is_array($_formula["formula"])) {
                $stringFormula = $serializer->serialize($_formula["formula"], 'json');
            } else {
                $stringFormula = $_formula["formula"];
            }

            $entityParamter = $serializer->deserialize($stringFormula, 'Tecspro\ModuloBundle\Entity\Formula', 'json');
            $resultado = $this->findInArraySession($formulaSession, (int) $_formula["key"]);

            $index = 0;
            //si no exite la crea 


            if ((is_bool($resultado) && $resultado == false) || (is_bool($resultado) == false &&
                    $formulaSession[$resultado]["idFormula"] != $entityParamter->getId())) {

                $array = array();
                $array['idFormula'] = $entityParamter->getId();
                $array['nombre'] = $serializer->serialize($entityParamter->getTranslations(), 'json');
                $array['formula'] = "";
                $array['imprimir'] = $entityParamter->getImprimir();
                $array['key'] = (int) $_formula["key"];
                $array['orden'] = $entityParamter->getGrupo()->getOrden();
                if (is_bool($resultado) && $resultado == false) {
                    $index = count($formulaSession) + 1;
                } else {
                    $index = $resultado;
                }
                $formulaSession[$index] = $array;
            } else {
                $index = $resultado;
                $array = $formulaSession[$index];
            }

            $itemNoEliminar[$index] = $index;
            if ($entityParamter->getSincronizado() == false) {

                $entityParamter->setSincronizado(true);
                $json = $serializer->serialize($entityParamter, 'json');

                $formulaSession[$index]['formula'] = $json;

//acutalizo proyecto
//seriealizo variables
                $variablesJson = $serializer->serialize($entityParamter->getVariables(), 'json');

//serealizo ecuaciones
                $ecuacionesJson = $serializer->serialize($entityParamter->getEcuacion(), 'json');

                $array[] = array(
                    'valores' => $variablesJson,
                    'nombrePersonalizado' => $entityParamter->getNombrePersonalizado(),
                    'ecuaciones' => $ecuacionesJson,
                );
//}
                $proyectoEntity->setItemFormulas($formulaSession[$index]['key'], $array);
            }
        }
        $arrayFormProyecto = $proyectoEntity->getFormulas();
        if (is_null($formulaSession) == false) {
            foreach ($formulaSession as $key => $form) {
                if (isset($itemNoEliminar[$key]) == false) {
                    unset($arrayFormProyecto[$form["key"]]);
                    unset($formulaSession[$key]);
                }
            }
        }
        $proyectoEntity->setFormulas($arrayFormProyecto);
        $this->em->persist($proyectoEntity);
        $this->session->set('formulas', $formulaSession);
        if (isset($index))
            $this->session->set('key', $formulaSession[$index]['key']);
        $this->em->flush();
    }

    public function guardarFormulasEnProyecto($formula) {
//guardo la id del proyecto y los valores de la variables
//$formulas = $this->session->get('formulas', null);
        $proyectoSession = $this->session->get('proyecto', null);
        $moduloSession = $this->session->get('moduloSeleccionado', null);
        $moduloEntity = $this->em->getRepository('ModuloBundle:Modulo')->find($moduloSession);

        $proyectoEntity = null;

        if (is_null($proyectoSession)) {
            $token = $this->securityContext->getToken();

            $proyectoEntity = new \Tecspro\FrontBundle\Entity\Proyecto();
            $proyectoEntity->setNombre("Nuevo Proyecto " . (new \DateTime())->format('d/m/Y h:i'));
            $proyectoEntity->setUser($token->getUser());
            $proyectoEntity->setModulo($moduloEntity);
            $this->em->persist($proyectoEntity);
            $this->em->flush();
            $this->session->set('proyecto', $proyectoEntity->getId());
            $this->session->set('proyectoNombre', $proyectoEntity->getNombre());
        }


        if (is_null($formula) == false) {
            if (is_null($proyectoEntity)) {
                $proyectoEntity = $this->em->getRepository('TecsproFrontBundle:Proyecto')->find($proyectoSession);
            }
            $array = array();
            $serializer = SerializerBuilder::create()->build();
// foreach ($formulas as $formula) {
//deserializo para setear los valores de las variables

            $entity = $serializer->deserialize($formula["formula"], 'Tecspro\ModuloBundle\Entity\Formula', 'json');

//seriealizo variables
            $variablesJson = $serializer->serialize($entity->getVariables(), 'json');

//serealizo ecuaciones
            $ecuacionesJson = $serializer->serialize($entity->getEcuacion(), 'json');

            $array[] = array(
                'idFormula' => $entity->getId(),
                'valores' => $variablesJson,
                'ecuaciones' => $ecuacionesJson,
                'imprimir' => $entity->getImprimir()
            );
//}
            $proyectoEntity->setItemFormulas($formula["key"], $array);
            $this->em->persist($proyectoEntity);
            $this->em->flush();
        }
    }

    public function getProyectoActual($nuevo = true) {

//guardo la id del proyecto y los valores de la variables
//$formulas = $this->session->get('formulas', null);
        $proyectoSession = $this->session->get('proyecto', null);
        $moduloSession = $this->session->get('moduloSeleccionado', null);

        if (!is_null($moduloSession)) {

            $moduloEntity = $this->em->getRepository('ModuloBundle:Modulo')->find($moduloSession);
        }

        $proyectoEntity = null;

        if (is_null($proyectoSession)) {

            if ($nuevo == false) {

                return $proyectoEntity;
            }

            $token = $this->securityContext->getToken();

            $proyectoEntity = new \Tecspro\FrontBundle\Entity\Proyecto();
            $proyectoEntity->setNombre((new \DateTime())->format('d/m/Y h:i'));
            $proyectoEntity->setUser($token->getUser());
            $proyectoEntity->setModulo($moduloEntity);

            $this->em->persist($proyectoEntity);
            $this->em->flush();

            $this->session->set('proyecto', $proyectoEntity->getId());
            $this->session->set('proyectoNombre', $proyectoEntity->getNombre());
        } else {

            $proyectoEntity = $this->em->getRepository('TecsproFrontBundle:Proyecto')->find($proyectoSession);
        }

        return $proyectoEntity;
    }

    public function cambiarIdioma() {
//guardo la id del proyecto y los valores de la variables

        $moduloSeleccionado = $this->session->get('moduloSeleccionado', null);
// $locale = $this->session->get('_locale', null);
//cambio de idioma al modulo seleccionado
        if (is_null($moduloSeleccionado) == false) {
            $moduloEntity = $this->em->getRepository('ModuloBundle:Modulo')->find($moduloSeleccionado);
//   ladybug_dump_die($moduloEntity->getNombre());
            $this->session->remove('moduloNombreSeleccionado');
            $this->session->set('moduloNombreSeleccionado', $moduloEntity->getNombre());
        }
    }

}

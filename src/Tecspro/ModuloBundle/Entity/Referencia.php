<?php

namespace Tecspro\ModuloBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Exclude;

/**
 * Referencia
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Referencia extends \MWSimple\Bundle\AdminCrudBundle\Entity\BaseFile {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Type("integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="idioma", type="string", length=255)
     *   @Type("string")
     */
    private $idioma;

    /**
     * @ORM\ManyToOne(targetEntity="Formula", inversedBy="referencias")
     * @ORM\JoinColumn(name="referencia_id", referencedColumnName="id")
     * @Exclude
     */
    private $formula;

    /**
     * @var string
     * @Type("string")
     */
    protected $uploadDir = "uploads/referencias";

    /**
     *
     * @ORM\Column(name="file_path", type="string", length=255, nullable=true)
     * @Type("string")
     */
    protected $filePath;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set idioma
     *
     * @param string $idioma
     * @return Referencia
     */
    public function setIdioma($idioma) {
        $this->idioma = $idioma;

        return $this;
    }

    /**
     * Get idioma
     *
     * @return string 
     */
    public function getIdioma() {
        return $this->idioma;
    }

    /**
     * Set pdf
     *
     * @param string $pdf
     * @return Referencia
     */
    public function setPdf($pdf) {
        $this->pdf = $pdf;

        return $this;
    }

    /**
     * Get pdf
     *
     * @return string 
     */
    public function getPdf() {
        return $this->pdf;
    }

    /**
     * Set formula
     *
     * @param \Tecspro\ModuloBundle\Entity\Formula $formula
     * @return Referencia
     */
    public function setFormula(\Tecspro\ModuloBundle\Entity\Formula $formula = null) {
        $this->formula = $formula;

        return $this;
    }

    /**
     * Get formula
     *
     * @return \Tecspro\ModuloBundle\Entity\Formula 
     */
    public function getFormula() {
        return $this->formula;
    }

}

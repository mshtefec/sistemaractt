<?php

namespace Tecspro\ModuloBundle\Entity;

use Doctrine\ORM\EntityRepository;

class FormulaRepository extends EntityRepository {

    public function existe($formulas) {

        return $this->createQueryBuilder('a')
                        ->select('a,v,u,ta,tv,tu')
                        ->leftJoin('a.translations', 'ta')
                        ->leftJoin('a.variables', 'v')
                        ->leftJoin('v.translations', 'tv')
                        ->leftJoin('v.unidad', 'u')
                        ->leftJoin('u.translations', 'tu')
                        ->where('a.id IN (:formulas)')
                        ->setParameter('formulas', $formulas)
                        ->orderBy('a.id', 'asc')
                        ->getQuery()
                        ->getResult()
        ;
    }

    public function finJoinEcuacionVariable($formulas) {

        return $this->createQueryBuilder('a')
                        ->select('a,v,u,ta,tv,tu,ec,ect')
                        ->leftJoin('a.translations', 'ta')
                        ->leftJoin('a.variables', 'v')
                        ->leftJoin('v.translations', 'tv')
                        ->leftJoin('v.unidad', 'u')
                        ->leftJoin('u.translations', 'tu')
                        ->leftJoin('a.ecuacion', 'ec')
                        ->leftJoin('ec.translations', 'ect')
                        ->where('a.id = :id')
                        ->setParameter('id', $formulas)
                        ->orderBy('a.id', 'asc')
                        ->getQuery()
                        ->getOneOrNullResult()
        ;
    }

    public function findFormulasByUnidad($unidad) {

        return $this->createQueryBuilder('a')
                        ->select('a,v,u,ta,tv,tu,ec,ect')
                        ->leftJoin('a.translations', 'ta')
                        ->leftJoin('a.variables', 'v')
                        ->leftJoin('v.translations', 'tv')
                        ->leftJoin('v.unidad', 'u')
                        ->leftJoin('u.translations', 'tu')
                        ->leftJoin('a.ecuacion', 'ec')
                        ->leftJoin('ec.unidadResultado', 'ur')
                        ->leftJoin('ur.unidad', 'uru')
                        ->leftJoin('ec.translations', 'ect')
                        ->where('u.id = :id')
                        ->orWhere('uru.id = :id')
                        ->setParameter('id', $unidad)
                        ->groupBy('a.id')
                        ->getQuery()
                        ->getResult()
        ;
    }

    public function findFormulasByConstante($constante) {

        return $this->createQueryBuilder('a')
                        ->select('a,v,u,ta,tv,tu,ec,ect')
                        ->leftJoin('a.translations', 'ta')
                        ->leftJoin('a.variables', 'v')
                        ->leftJoin('a.grupo', 'gr')
                        ->leftJoin('v.translations', 'tv')
                        ->leftJoin('v.unidad', 'u')
                        ->leftJoin('u.translations', 'tu')
                        ->leftJoin('a.ecuacion', 'ec')
                        ->leftJoin('gr.constantes', 'const')
                        ->leftJoin('ec.translations', 'ect')
                        ->where('const.id = :id')
                        ->setParameter('id', $constante)
                        ->groupBy('a.id')
                        ->getQuery()
                        ->getResult()
        ;
    }

    public function findFormulasByGrupos($grupos) {

        return $this->createQueryBuilder('a')
                        ->select('a,v,u,ta,tv,tu,ec,ect')
                        ->leftJoin('a.translations', 'ta')
                        ->leftJoin('a.variables', 'v')
                        ->leftJoin('v.translations', 'tv')
                        ->leftJoin('v.unidad', 'u')
                        ->leftJoin('a.grupo', 'g')
                        ->leftJoin('g.constantes', 'c')
                        ->leftJoin('u.translations', 'tu')
                        ->leftJoin('a.ecuacion', 'ec')
                        ->leftJoin('ec.translations', 'ect')
                        ->where('g.id IN (:grupos)')
                        ->setParameter('grupos', $grupos)
                        ->orderBy('a.id', 'asc')
                        ->getQuery()
                        ->getResult()
        ;
    }

}

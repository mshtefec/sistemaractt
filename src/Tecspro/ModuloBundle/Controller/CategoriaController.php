<?php

namespace Tecspro\ModuloBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Tecspro\ModuloBundle\Entity\Categoria;
use Tecspro\ModuloBundle\Form\CategoriaType;
use Tecspro\ModuloBundle\Form\CategoriaFilterType;
use A2lix\I18nDoctrineBundle\Annotation\I18nDoctrine;

/**
 * Categoria controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/categoria")
 */
class CategoriaController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Tecspro/ModuloBundle/Resources/config/Categoria.yml',
    );

    /**
     * Lists all Categoria entities.
     *
     * @Route("/", name="categoria")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->config['filterType'] = new CategoriaFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Categoria entity.
     *
     * @Route("/", name="categoria_create")
     * @Method("POST")
     * @Template("ModuloBundle:Categoria:new.html.twig")
     */
    public function createAction() {
        $this->config['newType'] = new CategoriaType();
        $config = $this->getConfig();
        $request = $this->getRequest();
        $entity = new $config['entity']();
        $form = $this->createCreateForm($config, $entity);
        $form->handleRequest($request);
        $moduloSeleccionado = $this->get('variableSession')->isModuloSeleccionado($request);

        if ($moduloSeleccionado['resultado'] == true) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $modulo = $em->getRepository('ModuloBundle:Modulo')->find($moduloSeleccionado['modulo']);
                $entity->setModulo($modulo);
                $em->persist($entity);
                $em->flush();
                $this->useACL($entity, 'create');

                $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

                $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $entity->getId()));

                return $this->redirect($nextAction);
            }
            $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');
        }
        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Categoria entity.
     *
     * @Route("/new", name="categoria_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->config['newType'] = new CategoriaType();
        $config = $this->getConfig();
        $em = $this->getDoctrine()->getManager();
        
        $request = $this->getRequest();
        $modulo = $request->getSession()->get('moduloSeleccionado');
        $ordenBd = $em->getRepository($config['repository'])->findUltimoOrden($modulo);
        if (is_null(current($ordenBd))) {
            $orden = 1;
        } else {
            $orden = current($ordenBd);
        }
        $entity = new $config['entity']();
        $entity->setOrden($orden);
        $form = $this->createCreateForm($config, $entity);

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a Categoria entity.
     *
     * @Route("/{id}", name="categoria_show", options={"expose"=true})
     * @Method("GET")
     * @Template()
     * @I18nDoctrine
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Categoria entity.
     *
     * @Route("/{id}/edit", name="categoria_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     * @I18nDoctrine
     */
    public function editAction($id) {
        $this->config['editType'] = new CategoriaType();
        $config = $this->getConfig();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
        }
        $this->useACL($entity, 'edit');
        $editForm = $this->createEditForm($config, $entity);
        $deleteForm = $this->createDeleteForm($config, $id);

        // remove the form to return to the view
        unset($config['editType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Categoria entity.
     *
     * @Route("/{id}", name="categoria_update")
     * @Method("PUT")
     * @Template("ModuloBundle:Categoria:edit.html.twig")
     * @I18nDoctrine
     */
    public function updateAction($id) {
        $this->config['editType'] = new CategoriaType();
        $config = $this->getConfig();
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
        }
        $this->useACL($entity, 'update');
        $deleteForm = $this->createDeleteForm($config, $id);
        $editForm = $this->createEditForm($config, $entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $array = array();

            foreach ($entity->getGrupos() as $grupo) {
                $array[] = $grupo->getId();
            }
            if (count($array)) {
                $formulas = $em->getRepository('ModuloBundle:Formula')->findFormulasByGrupos($array);
                $this->get('formulaBack')->actualizarFormulas($formulas);
            }
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $editForm->get('saveAndAdd')->isClicked() ?
                    $this->generateUrl($config['new']) :
                    $this->generateUrl($config['show'], array('id' => $id))
            ;

            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        // remove the form to return to the view
        unset($config['editType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Categoria entity.
     *
     * @Route("/{id}", name="categoria_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter Categoria.
     *
     * @Route("/exporter/{format}", name="categoria_export")
     */
    public function getExporter($format) {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Datatable Categoria.
     *
     * @Route("/get-table/", name="categoria_table")
     */
    public function getDatatable() {
        $response = parent::getTable();

        return $response;
    }

    /**
     * Create query.
     * @return Doctrine\ORM\QueryBuilder $queryBuilder
     */
    protected function createQuery() {
        $em = $this->getDoctrine()->getManager();
        $config = $this->getConfig();
        $qb = $em->createQueryBuilder();
        $request = $this->getRequest();
        $session = $request->getSession();
        $moduloSession = $session->get('moduloSeleccionado');
        $em->getFilters()->enable('oneLocale')->setParameter('locale', "es");
        $qb
                ->select('a.id', 't.nombre', 'a.orden')
                ->from($config['repository'], 'a')
                ->join('a.modulo', 'm')
                ->Join('a.translations', 't')
                ->where('m.id = ' . $moduloSession)
        ;

        $array = array(
            'query' => $qb,
            'tipoArray' => null
        );


        return $array;
    }

}

<?php

namespace Tecspro\ARCBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ResultadoARC
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Tecspro\ARCBundle\Entity\ResultadoARCRepository")
 */
class ResultadoARC
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tiempo", type="string", length=255)
     */
    private $tiempo;

    /**
     * @var string
     *
     * @ORM\Column(name="posicion", type="string", length=255)
     */
    private $posicion;

    /**
     * @var string
     *
     * @ORM\Column(name="velocidad", type="string", length=255)
     */
    private $velocidad;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=255)
     */
    private $estado;

  

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tiempo
     *
     * @param string $tiempo
     * @return ResultadoARC
     */
    public function setTiempo($tiempo)
    {
        $this->tiempo = $tiempo;

        return $this;
    }

    /**
     * Get tiempo
     *
     * @return string 
     */
    public function getTiempo()
    {
        return $this->tiempo;
    }

    /**
     * Set posicion
     *
     * @param string $posicion
     * @return ResultadoARC
     */
    public function setPosicion($posicion)
    {
        $this->posicion = $posicion;

        return $this;
    }

    /**
     * Get posicion
     *
     * @return string 
     */
    public function getPosicion()
    {
        return $this->posicion;
    }

    /**
     * Set velocidad
     *
     * @param string $velocidad
     * @return ResultadoARC
     */
    public function setVelocidad($velocidad)
    {
        $this->velocidad = $velocidad;

        return $this;
    }

    /**
     * Get velocidad
     *
     * @return string 
     */
    public function getVelocidad()
    {
        return $this->velocidad;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return ResultadoARC
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set arcCampos
     *
     * @param \Tecspro\ARCBundle\Entity\ARCCampos $arcCampos
     * @return ResultadoARC
     */
    public function setArcCampos(\Tecspro\ARCBundle\Entity\ARCCampos $arcCampos = null)
    {
        $this->arcCampos = $arcCampos;

        return $this;
    }

    /**
     * Get arcCampos
     *
     * @return \Tecspro\ARCBundle\Entity\ARCCampos 
     */
    public function getArcCampos()
    {
        return $this->arcCampos;
    }
}

<?php

namespace Tecspro\ModuloBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Tecspro\ModuloBundle\Entity\Grupo;
use Tecspro\ModuloBundle\Form\GrupoType;
use Tecspro\ModuloBundle\Form\GrupoFilterType;
use Symfony\Component\HttpFoundation\JsonResponse;
use A2lix\I18nDoctrineBundle\Annotation\I18nDoctrine;
use JMS\Serializer\SerializerBuilder;

/**
 * Grupo controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/grupo")
 */
class GrupoController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Tecspro/ModuloBundle/Resources/config/Grupo.yml',
    );

    /**
     * Lists all Grupo entities.
     *
     * @Route("/", name="admin_grupo")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->config['filterType'] = new GrupoFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Grupo entity.
     *
     * @Route("/", name="admin_grupo_create")
     * @Method("POST")
     * @Template("ModuloBundle:Grupo:new.html.twig")
     */
    public function createAction() {
        $request = $this->getRequest();
        $moduloSeleccionado = $this->get('variableSession')->isModuloSeleccionado($request);
        if ($moduloSeleccionado['resultado'] == true) {
            $this->config['newType'] = new GrupoType($moduloSeleccionado['modulo']);
            $config = $this->getConfig();

            $entity = new $config['entity']();
            $form = $this->createCreateForm($config, $entity);
            $form->handleRequest($request);

            if ($form->isValid()) {
                foreach ($entity->getFormulas() as $formula) {
                    foreach ($formula->getEcuacion() as $ecuacion) {
                        $continuar = true;
                        if (count($ecuacion->getUnidadresultado()) == 0) {
                            $this->get('session')->getFlashBag()->add('danger', 'La formula ' . $formula->getNombre() . ' no tiene unidades');

                            $continuar = false;
                        } else {
                            $tienePrincipal = false;
                            foreach ($ecuacion->getUnidadresultado() as $unidad) {

                                if ($unidad->getPrincipal() == true && !is_null($unidad->getUnidad())) {
                                    $tienePrincipal = true;
                                    $unidadSelecionada = $unidad->getUnidad()->getId();
                                    $ecuacion->setUnidadSeleccionada($unidadSelecionada);
                                    break;
                                }
                            }
                        }
                        if (!$tienePrincipal) {
                            $this->get('session')->getFlashBag()->add('danger', 'La formula ' . $formula->getNombre() . ' no tiene unidad principal');
                            $continuar = false;
                            break;
                        }
                    }
                }
                if ($continuar) {
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($entity);
                    $em->flush();

                    $serializer = SerializerBuilder::create()->build();


                    foreach ($entity->getFormulas() as $formula) {
                        $json = $serializer->serialize($formula, 'json');
                        $formula->setJson($json);
                        $em->persist($formula);
                    }

                    $em->flush();
                    $this->useACL($entity, 'create');

                    $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

                    $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $entity->getId()));

                    return $this->redirect($nextAction);
                }
            }
            $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

// remove the form to return to the view
            unset($config['newType']);

            return array(
                'config' => $config,
                'entity' => $entity,
                'form' => $form->createView(),
            );
        } else {
            return $this->redirect($this->generateUrl('admin_grupo'), 301);
        }
    }

    /**
     * Displays a form to create a new Grupo entity.
     *
     * @Route("/new", name="admin_grupo_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $request = $this->getRequest();
        $moduloSeleccionado = $this->get('variableSession')->isModuloSeleccionado($request);

        if ($moduloSeleccionado['resultado'] == true) {
            $this->config['newType'] = new GrupoType($moduloSeleccionado['modulo']);
            $response = parent::newAction();

            return $response;
        } else {
            return $this->redirect($this->generateUrl('admin_grupo'), 301);
        }
    }

    /**
     * Finds and displays a Grupo entity.
     *
     * @Route("/{id}", name="admin_grupo_show", options={"expose"=true})
     * @Method("GET")
     * @Template()
     * @I18nDoctrine
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Grupo entity.
     *
     * @Route("/{id}/edit", name="admin_grupo_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     * @I18nDoctrine
     */
    public function editAction($id) {
        $request = $this->getRequest();
        $moduloSeleccionado = $this->get('variableSession')->isModuloSeleccionado($request);

        if ($moduloSeleccionado['resultado'] == true) {
            $this->config['editType'] = new GrupoType($moduloSeleccionado['modulo']);
            $config = $this->getConfig();
            $em = $this->getDoctrine()->getManager();

            $entity = $em->getRepository($config['repository'])->findGrupoJoinFormula($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
            }

            $this->useACL($entity, 'edit');
            $editForm = $this->createEditForm($config, $entity);
            $deleteForm = $this->createDeleteForm($config, $id);

// remove the form to return to the view
            unset($config['editType']);

            return array(
                'config' => $config,
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            );
        } else {
            return $this->redirect($this->generateUrl('admin_grupo'), 301);
        }
    }

    /**
     * Edits an existing Grupo entity.
     *
     * @Route("/{id}", name="admin_grupo_update")
     * @Method("PUT")
     * @Template("ModuloBundle:Grupo:edit.html.twig")
     * @I18nDoctrine
     */
    public function updateAction($id) {
        $request = $this->getRequest();
        $moduloSeleccionado = $this->get('variableSession')->isModuloSeleccionado($request);

        if ($moduloSeleccionado['resultado'] == true) {

            $this->config['editType'] = new GrupoType($moduloSeleccionado['modulo']);
            $config = $this->getConfig();

            $em = $this->getDoctrine()->getManager();

            $entity = $em->getRepository($config['repository'])->findGrupoJoinFormula($id);
            $arrayEliminarUnidadEcuacion = array();
            foreach ($entity->getFormulas() as $formula) {
                foreach ($formula->getEcuacion() as $ecuacion) {
                    foreach ($ecuacion->getUnidadresultado() as $unidad) {
                        $arrayEliminarUnidadEcuacion[$unidad->getId()] = $unidad;
                    }
                }
            }

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
            }
            $this->useACL($entity, 'update');
            $deleteForm = $this->createDeleteForm($config, $id);
            $editForm = $this->createEditForm($config, $entity);
            $editForm->handleRequest($request);

            if ($editForm->isValid()) {

                $serializer = SerializerBuilder::create()->build();
                foreach ($entity->getFormulas() as $formula) {
                    foreach ($formula->getEcuacion() as $ecuacion) {
                        $continuar = true;
                        if (count($ecuacion->getUnidadresultado()) == 0) {
                            $this->get('session')->getFlashBag()->add('danger', 'La formula ' . $formula->getNombre() . ' no tiene unidades');

                            $continuar = false;
                            break;
                        } else {
                            $tienePrincipal = false;
                            foreach ($ecuacion->getUnidadresultado() as $unidad) {
                                if ($unidad->getPrincipal() == true && !is_null($unidad->getUnidad())) {
                                    $unidadSelecionada = $unidad->getUnidad()->getId();
                                    $ecuacion->setUnidadSeleccionada($unidadSelecionada);
                                    $tienePrincipal = true;
                                }

                                if (isset($arrayEliminarUnidadEcuacion[$unidad->getId()])) {
                                    unset($arrayEliminarUnidadEcuacion[$unidad->getId()]);
                                }
                            }
                            if (!$tienePrincipal) {
                                $this->get('session')->getFlashBag()->add('danger', 'La formula ' . $formula->getNombre() . ' no tiene unidad principal');
                                $continuar = false;
                                break;
                            }
                        }
                    }
                    $json = $serializer->serialize($formula, 'json');
                    $formula->setJson($json);
                }
                if ($continuar) {
                    foreach ($arrayEliminarUnidadEcuacion as $uni) {
                        $em->remove($uni);
                    }

                    $em->flush();
                    foreach ($entity->getFormulas() as $formula) {
                        $json = $serializer->serialize($formula, 'json');
                        $formula->setJson($json);
                        $em->persist($formula);
                    }

                    $em->flush();
                    $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

                    $nextAction = $editForm->get('saveAndAdd')->isClicked() ?
                            $this->generateUrl($config['new']) :
                            $this->generateUrl($config['show'], array('id' => $id))
                    ;

                    return $this->redirect($nextAction);
                }
            }

            $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

            // remove the form to return to the view
            unset($config['editType']);

            return array(
                'config' => $config,
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            );
        } else {
            
        }
    }

    /**
     * Create query.
     * @return Doctrine\ORM\QueryBuilder $queryBuilder
     * @I18nDoctrine
     */
    protected function createQuery() {
        $em = $this->getDoctrine()->getManager();
        $config = $this->getConfig();
        $qb = $em->createQueryBuilder();
        $request = $this->getRequest();
        $em->getFilters()->enable('oneLocale')->setParameter('locale', "es");
        $moduloSeleccionado = $this->get('variableSession')->isModuloSeleccionado($request);


        $qb
                ->select('a.id', 't.nombre', 'ct.nombre', 'a.orden')
                ->from($config['repository'], 'a')
                ->leftJoin('a.categoria', 'c')
                ->leftJoin('c.modulo', 'm')
                ->Join('a.translations', 't')
                ->Join('c.translations', 'ct')
                ->where('m.id = ' . $moduloSeleccionado['modulo'])
        ;


        $array = array(
            'query' => $qb,
            'tipoArray' => null
        );


        return $array;
    }

    /**
     * Deletes a Grupo entity.
     *
     * @Route("/{id}", name="admin_grupo_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter Grupo.
     *
     * @Route("/exporter/{format}", name="admin_grupo_export")
     */
    public function getExporter($format) {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Datatable Grupo.
     *
     * @Route("/get-table/", name="admin_grupo_table")
     */
    public function getDatatable() {
        $response = parent::getTable();

        return $response;
    }

    /**
     * Autocomplete a Noticia entity.
     *
     * @Route("/autocomplete-forms/get-constante", name="autocomplete_get_grupo_constante")
     */
    public function getAutocompleteConstante() {
        $request = $this->getRequest();
        $term = $request->query->get('q', null);

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ModuloBundle:Constante')->findByNombre($term);

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->__toString(),
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * Autocomplete a Noticia entity.
     *
     * @Route("/autocomplete-forms/get-unidad", name="autocomplete_get_grupo_unidad")
     */
    public function getAutocompleteUnidad() {
        $request = $this->getRequest();
        $term = $request->query->get('q', null);

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ModuloBundle:Unidad')->findByNombre($term);

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->__toString(),
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * Autocomplete a Noticia entity.
     *
     * @Route("/obtener-utimo-orden/by-categoria", name="Grupo_obtenerultimoorden_categoria")
     */
    public function getUltimoOrdenByCategoria() {
        $request = $this->getRequest();
        $term = $request->query->get('categoria', null);

        $em = $this->getDoctrine()->getManager();

        $ordenBd = $em->getRepository('ModuloBundle:Grupo')->findUltimoOrdenByCategoria($term);

        if (is_null(current($ordenBd))) {
            $orden = 1;
        } else {
            $orden = current($ordenBd);
        }
        $array = array();


        $array[] = $orden;
        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

}

<?php

namespace Tecspro\TablaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 * Categoria
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Tecspro\TablaBundle\Entity\CategoriaRepository")
 */
class TablaFriccionTranslation implements \A2lix\I18nDoctrineBundle\Doctrine\Interfaces\OneLocaleInterface {

      use \Tecspro\ModuloBundle\Entity\Translation;

    
    /**
     * @var string
     *
     * @ORM\Column(name="caracteristicasSuperficie", type="string", length=255)
     */
    private $caracteristicasSuperficie;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=255)
     */
    private $tipo;

   

    /**
     * Set caracteristicasSuperficie
     *
     * @param string $caracteristicasSuperficie
     * @return TablaFriccionTranslation
     */
    public function setCaracteristicasSuperficie($caracteristicasSuperficie)
    {
        $this->caracteristicasSuperficie = $caracteristicasSuperficie;

        return $this;
    }

    /**
     * Get caracteristicasSuperficie
     *
     * @return string 
     */
    public function getCaracteristicasSuperficie()
    {
        return $this->caracteristicasSuperficie;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return TablaFriccionTranslation
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }
}

<?php

namespace Tecspro\ModuloBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Exclude;

/**
 * Grupo
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Tecspro\ModuloBundle\Entity\GrupoRepository")
 */
class Grupo {

    use \A2lix\I18nDoctrineBundle\Doctrine\ORM\Util\Translatable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Type("integer")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="oculto", type="boolean")
     * @Type("boolean")
     */
    private $oculto;

    /**
     * @ORM\OneToMany(targetEntity="Formula", mappedBy="grupo", cascade={"all"})
     * @Exclude
     */
    private $formulas;

    /**
     * @ORM\ManyToOne(targetEntity="Categoria", inversedBy="grupos")
     * @ORM\JoinColumn(name="categoria_id", referencedColumnName="id")
     * @Type("Tecspro\ModuloBundle\Entity\Categoria")
     */
    private $categoria;

    /**
     * @ORM\ManyToMany(targetEntity="Constante")
     * @ORM\JoinTable(name="grupo_constante",
     *      joinColumns={@ORM\JoinColumn(name="constante_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="grupo_id", referencedColumnName="id")}
     *      )
     * @Type("ArrayCollection<Tecspro\ModuloBundle\Entity\Constante>")
     */
    private $constantes;

    /**
     * @var integer
     *
     * @ORM\Column(name="orden", type="integer")
     * @Type("integer")
     */
    private $orden;
    /**
     * @Type("ArrayCollection<Tecspro\ModuloBundle\Entity\GrupoTranslation>")
     */
    protected $translations;

    /**
     * Constructor
     */
    public function __construct() {
        $this->formulas = new \Doctrine\Common\Collections\ArrayCollection();
        $this->constantes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->oculto = false;
        $this->translations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set oculto
     *
     * @param boolean $oculto
     * @return Grupo
     */
    public function setOculto($oculto) {
        $this->oculto = $oculto;

        return $this;
    }

    /**
     * Get oculto
     *
     * @return boolean 
     */
    public function getOculto() {
        return $this->oculto;
    }

    /**
     * Add formulas
     *
     * @param \Tecspro\ModuloBundle\Entity\Formula $formulas
     * @return Grupo
     */
    public function addFormula(\Tecspro\ModuloBundle\Entity\Formula $formulas) {
        $formulas->setGrupo($this);
        $this->formulas[] = $formulas;

        return $this;
    }

    /**
     * Remove formulas
     *
     * @param \Tecspro\ModuloBundle\Entity\Formula $formulas
     */
    public function removeFormula(\Tecspro\ModuloBundle\Entity\Formula $formulas) {
        $this->formulas->removeElement($formulas);
        $formulas->setGrupo(null);
    }

    /**
     * Get formulas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFormulas() {
        return $this->formulas;
    }

    /**
     * Set categoria
     *
     * @param \Tecspro\ModuloBundle\Entity\Categoria $categoria
     * @return Grupo
     */
    public function setCategoria(\Tecspro\ModuloBundle\Entity\Categoria $categoria = null) {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return \Tecspro\ModuloBundle\Entity\Categoria 
     */
    public function getCategoria() {
        return $this->categoria;
    }

    /**
     * Add constantes
     *
     * @param \Tecspro\ModuloBundle\Entity\Constante $constantes
     * @return Grupo
     */
    public function addConstante(\Tecspro\ModuloBundle\Entity\Constante $constantes) {
        $this->constantes[] = $constantes;

        return $this;
    }

    /**
     * Remove constantes
     *
     * @param \Tecspro\ModuloBundle\Entity\Constante $constantes
     */
    public function removeConstante(\Tecspro\ModuloBundle\Entity\Constante $constantes) {
        $this->constantes->removeElement($constantes);
    }

    /**
     * Get constantes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getConstantes() {
        return $this->constantes;
    }

    /**
     * Set orden
     *
     * @param integer $orden
     * @return Grupo
     */
    public function setOrden($orden) {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return integer 
     */
    public function getOrden() {
        return $this->orden;
    }

}

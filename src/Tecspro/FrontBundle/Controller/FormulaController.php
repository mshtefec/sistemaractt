<?php

namespace Tecspro\FrontBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Tecspro\ModuloBundle\Entity\Modulo;
use Tecspro\ModuloBundle\Form\ModuloType;
use Tecspro\ModuloBundle\Form\ModuloFilterType;
use Symfony\Component\HttpFoundation\JsonResponse;
use JMS\Serializer\SerializerBuilder;
use Tecspro\ModuloBundle\Entity\Variable;
use A2lix\I18nDoctrineBundle\Annotation\I18nDoctrine;

/**
 * @Route("/")
 */
class FormulaController extends Controller {

    /**
     *
     * @Route("/seleccionar-modulo/", name="Formula_seleccionar", options={"expose"=true})
     * @I18nDoctrine
     */
    public function FormulaSeleccionarAction() {
//doble click en el arbol de formulas y guardo en session con una key d cantidad de formulas seleccionadas
        $idFormula = $this->getRequest()->get('id', null);
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        $session = $request->getSession();
        $lenguaje = $session->get('_locale', 'es');
        $entity = $em->getRepository('ModuloBundle:Formula')->finJoinEcuacionVariable($idFormula);

//$json = $this->container->get('serializer')->serialize($entity, 'json');
        //$serializer = SerializerBuilder::create()->build();
        // $json = $serializer->serialize($entity, 'json');
// string to put directly in the "src" of the tag <img>
        $cacheManager = $this->container->get('liip_imagine.cache.manager');
        $imagenFormula = null;
        $referenciaFormula = null;
        if (is_null($entity->getWebPath()) == false) {
            $imagenFormula = $cacheManager->getBrowserPath($entity->getWebPath(), 'upscale800x600');
        } else {
            $imagenDefault = $url = "bundles/tecsprofront/img/default.png";
            $imagenFormula = $cacheManager->getBrowserPath($imagenDefault, 'upscale800x600');
        }
        if (count($entity->getReferencias()) > 0) {
            foreach ($entity->getReferencias() as $referencia) {
                if ($referencia->getIdioma() == $lenguaje) {
                    $referenciaFormula = $referencia->getWebPath();
                    break;
                }
            }
        }

//session_destroy();
        $array = array();
        if (count($entity->getGrupo()->getFormulas()) > 1) {
            $nombre = $entity->getGrupo()->getOrden() . ". " . $entity->getOrden() . ". " . $entity->getNombre();
        } else {
            $nombre = $entity->getGrupo()->getOrden() . ". " . $entity->getNombre();
        }
        $array['idFormula'] = $entity->getId();
        $array['nombre'] = $nombre;
        $array['formula'] = $entity->getJson();
        $array['imagen'] = $imagenFormula;
        $array['referencia'] = $referenciaFormula;
        $array['imprimir'] = $entity->getImprimir();
        $formulas = $session->get('formulas', null);

        if (is_null($formulas)) {
            $formulas = array();
            $array['key'] = 1;
            // $formulas[] = $array;
            //$session->set('formulas', $formulas);
        } else {
//ladybug_dump_die(max(array(2, 3)));
            $array['key'] = $this->get('formula')->obtenerIdSession($formulas);
            //   $formulas[] = $array;
            //$session->set('formulas', $formulas);
        }
        $session->set('key', $array['key']);

//me fijo si ahy proyecto en session y guardo 
        //$this->get('formula')->guardarFormulasEnProyecto();

        $respuesta = array(
            'formulaSeleccionada' => $array,
                //  'formulasSession' => $formulas
        );
        $response = new JsonResponse();
        $response->setData($respuesta);

        return $response;
    }

    /**
     *
     * @Route("/guardar-formula/", name="Formula_session", options={"expose"=true})
     * @I18nDoctrine
     */
    public function FormulaGuardarSessionAction() {
        //actualizo los valores ingresados por formula seleccionada y las guardo en session

        $formula = $this->getRequest()->get('formula', null);
        $_formulaSession = $this->getRequest()->get('formSession', null);
        $formulaSessionArray = json_decode($_formulaSession, true);

        $request = $this->getRequest();
        $session = $request->getSession();
        $session->start();
        $proyectoSession = $session->get('proyecto', null);


        $nuevoProyecto = false;
        if (is_null($proyectoSession)) {
            $nuevoProyecto = true;
        }


        $this->get('formula')->guardarFormulasArrayEnProyecto($formulaSessionArray);

        $formulaSelect = null;
        $exito = true;

        $array = array(
            'formula' => $formulaSelect,
            'resultado' => $exito,
            'nuevoProyecto' => $nuevoProyecto,
        );
        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     *
     * @Route("/eliminar-formula/", name="Formula_session_eliminar")
     * @I18nDoctrine
     */
    public function FormulaEliminarSessionAction() {
//actualizo los valores ingresados por formula seleccionada y las guardo en session

        $key = $this->getRequest()->get('key', null);

//buscar en session la formula
        $request = $this->getRequest();
        $session = $request->getSession();
        $formulaSession = $session->get('formulas', null);
        $resultado = $this->get('formula')->findInArraySession($formulaSession, (int) $key);

        $exito = false;


        if (is_bool($resultado) == true && $resultado == false) {

            $exito = false;
        } else {

            $this->get('formula')->EliminarFormulasEnProyecto($formulaSession[$resultado]);
            $exito = true;
        }
        unset($formulaSession[$resultado]);
        $session->set('formulas', $formulaSession);
        $array = array(
            'resultado' => $exito,
                //'formulasSession' => $formulaSession
        );
        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     *
     * @Route("/seleccionar-formula/", name="Formula_session_seleccionar")
     * @I18nDoctrine
     */
    public function FormulaseleccionarSessionAction() {
//actualizo los valores ingresados por formula seleccionada y las guardo en session

        $key = $this->getRequest()->get('key', null);

//buscar en session la formula
        $request = $this->getRequest();
        $session = $request->getSession();
        $formulaSession = $session->get('formulas', null);
        $resultado = $this->get('formula')->findInArraySession($formulaSession, (int) $key);

        $exito = true;

        // $session->set('formulas', $formulaSession);
        //  $this->get('formula')->guardarFormulasEnProyecto();
        $array = array();
        $array['resultado'] = $exito;
        $array['formula'] = $formulaSession[$resultado];

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     *
     * @Route("/eliminar-formula-all/", name="Formula_session_eliminar_all")
     * @I18nDoctrine
     */
    public function FormulaEliminarSessionAllAction() {
        $request = $this->getRequest();
        $session = $request->getSession();
        $session->remove('formulas');
        $this->get('formula')->EliminarTodasLasFormulasEnProyecto();
        $response = new JsonResponse();
        $array = array(
            "resultado" => true
        );
        $response->setData($array);
        return $response;
    }

    /**
     *
     * @Route("/seleccionar-formula-by-grupo/", name="Formula_seleccionar_formulaByGrupo")
     */
    public function FormulaSeleccionarByGrupoAction() {
//doble click en el arbol de formulas y guardo en session con una key d cantidad de formulas seleccionadas
        $idGrupo = $this->getRequest()->get('id', null);
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ModuloBundle:Grupo')->find($idGrupo);

        $formulasArray = array();

        foreach ($entity->getFormulas() as $formula) {
            $text = $entity->getOrden() . "." . $formula->getOrden() . " " . $formula->getNombre();
            $formulasArray[] = array(
                'formula' => $text,
                'idFormula' => $formula->getId(),
                'json' => $formula->getJson(),
                'orden' => $entity->getOrden() . "." . $formula->getOrden()
            );
        }
        $response = new JsonResponse();
        $response->setData($formulasArray);

        return $response;
    }

    /**
     *
     * @Route("/actualiza-impresion/", name="Formula_boton_imprimir", options={"expose"=true})
     * @I18nDoctrine
     */
    public function ActualizarBotonImprimirAction() {
        $activo = $this->getRequest()->get('estado', null);
        $key = $this->getRequest()->get('key', null);
        $request = $this->getRequest();
        $session = $request->getSession();
        $formulaSession = $session->get('formulas', null);

        $resultado = $this->get('formula')->findInArraySession($formulaSession, (int) $key);
        if ($resultado != false) {
            $serializer = SerializerBuilder::create()->build();
            $entity = $serializer->deserialize($formulaSession[$resultado]['formula'], 'Tecspro\ModuloBundle\Entity\Formula', 'json');

            if ($activo == "true") {
                $entity->setImprimir(true);
            } else {
                $entity->setImprimir(false);
            }

            //serealizo
            $json = $serializer->serialize($entity, 'json');
            $formulaSession[$resultado]['formula'] = $json;
            $formulaSession[$resultado]['imprimir'] = $entity->getImprimir();
            $session->set('formulas', $formulaSession);
            $this->get('formula')->guardarFormulasEnProyecto($formulaSession[$resultado]);

            $respuesta = array(
                'resultado' => true,
                'valor' => $entity->getImprimir()
            );
        } else {
            $respuesta = array(
                'resultado' => true,
                'valor' => '0'
            );
        }
        $response = new JsonResponse();
        $response->setData($respuesta);

        return $response;
    }
}

<?php

namespace Tecspro\TablaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 * Categoria
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Tecspro\TablaBundle\Entity\InerciaRepository")
 */
class InerciaTranslation implements \A2lix\I18nDoctrineBundle\Doctrine\Interfaces\OneLocaleInterface {

      use \Tecspro\ModuloBundle\Entity\Translation;

    
     /**
     * @var string
     *
     * @ORM\Column(name="vehiculo", type="string", length=255)
     */
    private $vehiculo;


    /**
     * @var string
     *
     * @ORM\Column(name="categoria", type="string", length=255)
     */
    private $categoria;

    

    /**
     * @var string
     *
     * @ORM\Column(name="indicador", type="string", length=255)
     */
    private $indicador;

    /**
     * Set vehiculo
     *
     * @param string $vehiculo
     * @return InerciaTranslation
     */
    public function setVehiculo($vehiculo)
    {
        $this->vehiculo = $vehiculo;

        return $this;
    }

    /**
     * Get vehiculo
     *
     * @return string 
     */
    public function getVehiculo()
    {
        return $this->vehiculo;
    }

    /**
     * Set categoria
     *
     * @param string $categoria
     * @return InerciaTranslation
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return string 
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Set indicador
     *
     * @param string $indicador
     * @return InerciaTranslation
     */
    public function setIndicador($indicador)
    {
        $this->indicador = $indicador;

        return $this;
    }

    /**
     * Get indicador
     *
     * @return string 
     */
    public function getIndicador()
    {
        return $this->indicador;
    }
}

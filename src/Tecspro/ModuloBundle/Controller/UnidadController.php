<?php

namespace Tecspro\ModuloBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Tecspro\ModuloBundle\Entity\Unidad;
use Tecspro\ModuloBundle\Form\UnidadType;
use Tecspro\ModuloBundle\Form\UnidadFilterType;
use A2lix\I18nDoctrineBundle\Annotation\I18nDoctrine;

/**
 * Unidad controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/unidad")
 */
class UnidadController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Tecspro/ModuloBundle/Resources/config/Unidad.yml',
    );

    /**
     * Lists all Unidad entities.
     *
     * @Route("/", name="admin_unidad")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->config['filterType'] = new UnidadFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Unidad entity.
     *
     * @Route("/", name="admin_unidad_create")
     * @Method("POST")
     * @Template("ModuloBundle:Unidad:new.html.twig")
     */
    public function createAction() {
        $this->config['newType'] = new UnidadType();
        $response = parent::createAction();

        return $response;
    }

    /**
     * Displays a form to create a new Unidad entity.
     *
     * @Route("/new", name="admin_unidad_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->config['newType'] = new UnidadType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Unidad entity.
     *
     * @Route("/{id}", name="admin_unidad_show", options={"expose"=true})
     * @Method("GET")
     * @Template()
     * @I18nDoctrine
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Unidad entity.
     *
     * @Route("/{id}/edit", name="admin_unidad_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     * @I18nDoctrine
     */
    public function editAction($id) {
        $this->config['editType'] = new UnidadType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Unidad entity.
     *
     * @Route("/{id}", name="admin_unidad_update")
     * @Method("PUT")
     * @Template("ModuloBundle:Unidad:edit.html.twig")
     * @I18nDoctrine
     */
    public function updateAction($id) {
        $this->config['editType'] = new UnidadType();
        $config = $this->getConfig();
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
        }
        $this->useACL($entity, 'update');
        $deleteForm = $this->createDeleteForm($config, $id);
        $editForm = $this->createEditForm($config, $entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            $formulas = $em->getRepository('ModuloBundle:Formula')->findFormulasByUnidad($entity->getId());
            if (!is_null($formulas) && count($formulas) > 0)
                $this->get('formulaBack')->actualizarFormulas($formulas);


            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $editForm->get('saveAndAdd')->isClicked() ?
                    $this->generateUrl($config['new']) :
                    $this->generateUrl($config['show'], array('id' => $id))
            ;

            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        // remove the form to return to the view
        unset($config['editType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Unidad entity.
     *
     * @Route("/{id}", name="admin_unidad_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter Unidad.
     *
     * @Route("/exporter/{format}", name="admin_unidad_export")
     */
    public function getExporter($format) {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Datatable Unidad.
     *
     * @Route("/get-table/", name="admin_unidad_table")
     */
    public function getDatatable() {
        $response = parent::getTable();

        return $response;
    }

    /**
     * Create query.
     * @return Doctrine\ORM\QueryBuilder $queryBuilder
     */
    protected function createQuery() {
        $em = $this->getDoctrine()->getManager();
        $config = $this->getConfig();
        $qb = $em->createQueryBuilder();
        $request = $this->getRequest();
        $em->getFilters()->enable('oneLocale')->setParameter('locale', "es");
        $qb
                ->select('a.id', 'a.simbolo', 't.nombre', 't.magnitud')
                ->from($config['repository'], 'a')
                ->Join('a.translations', 't')
        ;

        $array = array(
            'query' => $qb,
            'tipoArray' => null
        );


        return $array;
    }

}

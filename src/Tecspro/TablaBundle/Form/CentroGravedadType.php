<?php

namespace Tecspro\TablaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * CentroGravedadType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class CentroGravedadType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('translations', 'a2lix_translations', array(
                    'required' => true,
                    'fields' => array(
                        'vehiculo' => array(
                            'field_type' => 'choice',
                            'locale_options' => array(
                                'es' => array(
                                    'label' => 'Vehículos Español',
                                ),
                                'pt' => array(
                                    'label' => 'Vehículos Portugues',
                                )
                            ),
                            'choices'  => array(
                                'es' => array(
                                    'VehiculosPasajeros' => 'Vehículos de pasajeros',
                                    'Pick Ups' => 'Pick Ups',
                                    'Multipropósito' => 'Multipropósito',
                                    'Vans- Mini Vans' => 'Vans- Mini Vans',
                                ),
                                 'pt' => array(
                                    'Veículos Passagerios' => 'Veículos Passagerios',
                                    'Pick Ups' => 'Pick Ups',
                                    'Multipropósito' => 'Multipropósito',
                                    'Vans- Mini Vans' => 'Vans- Mini Vans',
                                ),
                            ),
                        ),
                        'indice' => array(
                            'field_type' => 'choice',
                            'locale_options' => array(
                                'es' => array(
                                    'label' => 'Indice Español',
                                ),
                                'pt' => array(
                                    'label' => 'Indice Portugues',
                                )
                            ),
                            'choices'  => array(
                                'es' => array(
                                    'Altura al centro de gravedad' => 'Altura al centro de gravedad',
                                    'Desviación estándar' => 'Desviación estándar',
                                ),
                                 'pt' => array(
                                     'Altura do centro de gravedade' => 'Altura do centro de gravedade',
                                    'Desvio do padrão' => 'Desvio do padrão',
                                ),
                                 )
                        ),
                                                'categoria' => array(
                            'field_type' => 'text',
                            'locale_options' => array(
                                'es' => array(
                                    'label' => 'Categoria Español',
                                ),
                                'pt' => array(
                                    'label' => 'Categoria Portugues',
                                )
                            )
                        )
                    )
                ))
            ->add('batallaCategoria1')
            ->add('batallaCategoria2')
            ->add('batallaCategoria3')
            ->add('batallaCategoria4')
            ->add('batallaCateogira5')
            ->add('altura')
            ->add('desviacionEstandar')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Tecspro\TablaBundle\Entity\CentroGravedad'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'tecspro_tablabundle_centrogravedad';
    }
}

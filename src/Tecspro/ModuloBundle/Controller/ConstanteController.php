<?php

namespace Tecspro\ModuloBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Tecspro\ModuloBundle\Entity\Constante;
use Tecspro\ModuloBundle\Form\ConstanteType;
use Tecspro\ModuloBundle\Form\ConstanteFilterType;
use A2lix\I18nDoctrineBundle\Annotation\I18nDoctrine;

/**
 * Constante controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/constante")
 */
class ConstanteController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Tecspro/ModuloBundle/Resources/config/Constante.yml',
    );

    /**
     * Lists all Constante entities.
     *
     * @Route("/", name="admin_constante")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->config['filterType'] = new ConstanteFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Constante entity.
     *
     * @Route("/", name="admin_constante_create")
     * @Method("POST")
     * @Template("ModuloBundle:Constante:new.html.twig")
     */
    public function createAction() {
        $this->config['newType'] = new ConstanteType();
        $config = $this->getConfig();
        $request = $this->getRequest();
        $entity = new $config['entity']();
        $form = $this->createCreateForm($config, $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->useACL($entity, 'create');

            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $entity->getId()));

            return $this->redirect($nextAction);
        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Constante entity.
     *
     * @Route("/new", name="admin_constante_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->config['newType'] = new ConstanteType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Constante entity.
     *
     * @Route("/{id}", name="admin_constante_show", options={"expose"=true})
     * @Method("GET")
     * @Template()
     * @I18nDoctrine
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Constante entity.
     *
     * @Route("/{id}/edit", name="admin_constante_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     * @I18nDoctrine
     */
    public function editAction($id) {
        $this->config['editType'] = new ConstanteType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Constante entity.
     *
     * @Route("/{id}", name="admin_constante_update")
     * @Method("PUT")
     * @Template("ModuloBundle:Constante:edit.html.twig")
     * @I18nDoctrine
     */
    public function updateAction($id) {
        $this->config['editType'] = new ConstanteType();
        $config = $this->getConfig();
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
        }
        $this->useACL($entity, 'update');
        $deleteForm = $this->createDeleteForm($config, $id);
        $editForm = $this->createEditForm($config, $entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $formulas = $em->getRepository('ModuloBundle:Formula')->findFormulasByConstante($entity->getId());
          
            if (!is_null($formulas) && count($formulas) > 0)
                $this->get('formulaBack')->actualizarFormulas($formulas);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $editForm->get('saveAndAdd')->isClicked() ?
                    $this->generateUrl($config['new']) :
                    $this->generateUrl($config['show'], array('id' => $id))
            ;

            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        // remove the form to return to the view
        unset($config['editType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Constante entity.
     *
     * @Route("/{id}", name="admin_constante_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter Constante.
     *
     * @Route("/exporter/{format}", name="admin_constante_export")
     */
    public function getExporter($format) {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Datatable Constante.
     *
     * @Route("/get-table/", name="admin_constante_table")
     */
    public function getDatatable() {
        $response = parent::getTable();

        return $response;
    }

    /**
     * Create query.
     * @return Doctrine\ORM\QueryBuilder $queryBuilder
     */
    protected function createQuery() {
        $em = $this->getDoctrine()->getManager();
        $config = $this->getConfig();
        $qb = $em->createQueryBuilder();
        $request = $this->getRequest();
        $em->getFilters()->enable('oneLocale')->setParameter('locale', "es");
        $qb
                ->select('a.id', 't.nombre', 'a.valor', 'a.identificador', 'ut.nombre')
                ->from($config['repository'], 'a')
                ->Join('a.translations', 't')
                ->Join('a.unidad', 'u')
                ->Join('u.translations', 'ut')
        ;

        $array = array(
            'query' => $qb,
            'tipoArray' => null
        );


        return $array;
    }

}

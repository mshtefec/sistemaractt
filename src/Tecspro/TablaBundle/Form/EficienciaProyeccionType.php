<?php

namespace Tecspro\TablaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * EficienciaProyeccionType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class EficienciaProyeccionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('alturaCentroGravedadPeatonSobreCapoMetros')
            ->add('minima')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Tecspro\TablaBundle\Entity\EficienciaProyeccion'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'tecspro_tablabundle_eficienciaproyeccion';
    }
}

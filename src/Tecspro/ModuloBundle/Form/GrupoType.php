<?php

namespace Tecspro\ModuloBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Tecspro\ModuloBundle\Form\FormulaType;
use Doctrine\ORM\EntityRepository;

/**
 * GrupoType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class GrupoType extends AbstractType {

    private $modulo;

    public function __construct($modulo) {
        $this->modulo = $modulo;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('translations', 'a2lix_translations', array(
                    'required_locales' => array('es', 'pt'),
                    'fields' => array(
                        'nombre' => array(
                            'field_type' => 'text',
                            'locale_options' => array(
                                'es' => array(
                                    'label' => 'Nombre Español',
                                ),
                                'pt' => array(
                                    'label' => 'Nombre Portugues',
                                )
                            )
                        )
                    )
                ))
                ->add('categoria', 'entity', array(
                    'translation_domain' => 'TecsproFrontBundle',
                    'class' => 'ModuloBundle:Categoria',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                                ->join('u.modulo', 'm')
                                ->where('m.id= :mod')
                                ->setParameter('mod', $this->modulo)
                                ->orderBy('u.id', 'ASC');
                    },
                    'attr' => array(
                        'class' => 'txtCategoria'
                    )
                ))
                ->add('formulas', 'collection', array(
                    'type' => new FormulaType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'required' => true,
                    'by_reference' => false,
                    'prototype' => true,
                    'prototype_name' => '__name__',
                    'label' => false
                ))
                ->add('constantes', 'select2', array(
                    'label' => 'Constante',
                    'class' => 'ModuloBundle:Constante',
                    'url' => 'autocomplete_get_grupo_constante',
                    'required' => false,
                    'configs' => array(
                        'multiple' => true, //es requerido true o false
                        'width' => '100%'
                    ),
                ))
                ->add('orden', null, array(
                    'attr' => array(
                        'class' => 'txtOrden'
                    )
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Tecspro\ModuloBundle\Entity\Grupo',
            'cascade_validation' => true,
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'tecspro_modulobundle_grupo';
    }

}

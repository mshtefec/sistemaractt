<?php

namespace Tecspro\FrontBundle\Extension;

class frontExtension extends \Twig_Extension {

    public function getFilters() {
        return array(
            'decode' => new \Twig_Filter_Method($this, 'decode'),
        );
    }

    public function decode($json) {
        return json_decode($json,true);
    }

    public function getName() {
        return 'frontExtension';
    }

}
